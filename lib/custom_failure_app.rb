class CustomFailureApp < Devise::FailureApp
  def redirect
    store_location!
    message = warden.message || warden_options[:message]
    if message == :timeout     
      redirect_to new_user_session_path, notice: 'Your session has expired'
    else 
      super
    end
  end
end