Rails.application.routes.draw do    

  resources :wishlists, :only =>[:create]
  resources :subscribers
  devise_for :users, :controllers=> { :passwords => "passwords", :sessions => "sessions", :registrations => "registrations", :omniauth_callbacks => "omniauth_callbacks" }
  resources :users, except:[:index] do
    resources :wishlists , :except => [:create]
    resources :wishlist_items
    resources :vouchers
    resources :orders do
      get 'consignment/:id',to: "orders#consignment", :as => "consignment_pdf"
      resources :shipping_address
    end
    resources :shipping_addresses
    resources :profiles, :except => [:index, :destroy]
    get 'account',to: "users#account"
    get 'address',to: "users#address"
    get 'address_create',to: "users#address_create", :as => "address_create"
  end 
  get '/carts', to: redirect('/') 
  get 'find_variant_shipping_cost', to: 'sections#find_variant_shipping_cost', :as => "find_variant_shipping_cost"
  get 'find_cart_extra_cost', to: 'carts#find_cart_extra_cost', :as => "find_cart_extra_cost"
  get 'find_cart_availability', to: 'carts#find_cart_availability', :as => "find_cart_availability"
  resources :bank_accounts
  resources :cart_items
  resources :carts, :only =>[:show, :destroy, :edit, :update, :create, :index, :empty_cart] do 
    get 'pre_order', to: 'carts#pre_order', :as => "pre_order"
    get 'payment', to: 'carts#payment', :as => "payment"
    match 'payu_callback', to: 'carts#payu_return', via: :all
    get 'check_captcha', to: 'carts#check_captcha', :as => "check_captcha"
    get 'empty_cart', to: 'carts#empty_cart', :as => "empty_cart"
  end    

  resources :sections do 
    get 'sections/:id/:pid/add_to_cart', to: "sections#add_to_cart", :as => "add_to_cart"
    get 'searchs', to: 'sections#show'
    get ':productid', to: 'sections#showcase', as: :product_show
    get 'main_others', to: 'sections#main_others', :as => "main"
    get 'sub_others', to: 'sections#sub_others', :as => "sub" 
    collection { post :searchs, to: 'sections#show' }
    collection { get :searchs, to: 'sections#show' }
  end

  get 'check_user_login', to: 'sections#check_user_login', :as => "check_user_login"

  namespace :support do
    resources :ticket_tasks
    resources :ticket_replies
    resources :tickets
    get 'dashboard', to: 'supports#dashboard', :as => "dashboard"
    resources :orders do
      resources :order_revocation
      resources :shipments
    end
    resources :users do 
      resources :vouchers
    end
    resources :consignments
  end

  namespace :admin do
    resources :availability_plans do
      get 'find_country_pincode_with_id', to: 'availability_plans#find_country_pincode_with_id'
      resources :plan_countries do 
        resources :country_pincodes 
      end
    end
    resources :cod_policies do 
      get 'find_cod_country_pincode_with_id', to: 'cod_policies#find_cod_country_pincode_with_id'
      resources :cod_specific_countries do 
        resources :cod_specific_pincodes
      end
    end
    resources :price_ranges
    resources :weight_ranges
    resources :shipping_prices
    resources :shipping_weights
    resources :shipping_policies do
      get 'find_shipping_policy_shipping_weight', to: 'shipping_policies#find_shipping_policy_shipping_weight'
      resources :shipping_specific_countries do
        resources :shipping_specific_pincodes
      end
    end
    resources :failure_transactions
    resources :pages do
      post :delete_selected_page, :on => :collection
    end

    resources :sms_notifications
    resources :image_types do 
      resources :general_images do
        collection { post :sort }
      end
    end
    resources :wishlists
    resources :ticket_tasks
    resources :ticket_replies
    resources :refund_revocation_items
    resources :refund_revocations
    resources :replacement_revocations do 
      
    end
    resources :replacement_revocation_items
    resources :vouchers
    resources :pickup_tasks_pickup_consignments
    resources :pickup_consignments do
      
    end
    resources :pickup_consignment_items do 
      get 'pickup_consign_item_not_recevied', to: "pickup_consignment_items#pickup_consign_item_not_recevied"
      get 'pickup_consign_item_recevied', to: "pickup_consignment_items#pickup_consign_item_recevied"
    end
    resources :pickup_consignment_invoices
    resources :notification_customers
    resources :consignment_item_revocations
    resources :consignment_revocations
    resources :order_revocation_items
    resources :order_revocations
    resources :package_tasks_consignments, :only =>[:index, :destroy]
    resources :consignment_items
    resources :logistics
    get 'new_store', to: 'generals#new_store', :as => "new_store"    
    get 'check_image_type_exist' =>"image_types#check_image_type_exist"
    get 'check_image_name_exist' =>"image_types#check_image_name_exist"
    get 'check_plan_name_exist' =>"availability_plans#check_plan_name_exist"
    get 'check_brand_name_exist' =>"brands#check_brand_name_exist"
    get 'check_page_name_exist' =>"pages#check_page_name_exist"
    get 'check_weight_range_exist' =>"weight_ranges#check_weight_range_exist"    
    get 'check_price_range_exist' =>"price_ranges#check_price_range_exist"
    resources :generals do
      get 'edit_acc_spec', to: 'generals#edit_acc_spec', :as => "edit_acc_spec"
      get 'edit_seo_spec', to: 'generals#edit_seo_spec', :as => "edit_seo_spec"
      get 'edit_social_spec', to: 'generals#edit_social_spec', :as => "edit_social_spec"
    end
    resources :order_items do 
      get 'check_order_item_availability', to: 'order_items#check_order_item_availability'
    end 
    get 'dashboard',to: "admins#dashboard"
    resources :rejection_reasons
    resources :carts  
    resources :warehouse_dashboard do
      resources :warehouse_ticket_tasks
      resources :warehouse_ticket_replies
      resources :resend_tasks do
        get 'resend_consignments/:id', to: 'resend_consignments#show' ,:as => "consignment_show"
      end
      resources :resend_consignments 
      resources :resend_tasks_consignments
      resources :package_tasks do
        get 'consignments/', to: 'package_tasks#index' 
        get 'consignments/:id', to: 'consignments#show' ,:as => "task_consignment_show"
        get 'consignments/:id/consignment_logs', to: 'consignments#consignment_logs', :as => "consignment_logs"
      end
      resources :pickup_tasks do 
        get 'assigned_pickup_consignments/:id', to: 'assigned_pickup_consignments#show' ,:as => "pickup_consignment_show_path"
        resources :assigned_pickup_consignments do        
          
        end
      end
      resources :consignments, :except => [:show]
      resources :assigned_pickup_consignments
      get 'consignments/:id', to: 'consignments#index'
      resources :stocks
      get '/stocks', to: 'stocks#index'
      get '/stocks/:id', to: 'stocks#show'
      get 'orders/:id', to: 'warehouse_dashboard#orders', :as => "orders"
    end 
    resources :orders do 
      resources :shipments
      get 'shipments/:id', to: 'shipments#show', :as => "shipment_show"
      get 'order_logs', to: 'orders#order_logs', :as => "order_logs"
      get 'find_variant_with_sku', to: 'orders#find_variant_with_sku'
    end
    get 'brands/remove_product_from_brand', to: "brands#remove_product_from_brand"
    get 'categories/remove_product_from_category', to: "categories#remove_product_from_category"
    resources :generals 
    resources :warehouses_roles
    resources :product_properties
    resources :child_categories
    resources :categories do
      get ':product_id/to_display', to: "categories#to_display", :as => "to_display"
      get 'make_active', to: "categories#make_active"
      collection { post :sort }
      collection { post :promotion_sort }
      collection { post :remove_product_from_category }
    end
    resources :types
    resources :images do
      collection { post :sort }
      collection { post :banner_sort }
      collection { post :parent_image_sort }
      collection { post :child_image_sort }
    end
    resources :invoices
    resources :products_tax_plans
    resources :child_products
    resources :product_shipping_plans    
    resources :inventories
    resources :values
    resources :specifications    
    resources :brands do
      collection { post :remove_product_from_brand }
      post :delete_selected, :on => :collection
      post :delete_selected_brand, :on => :collection
    end
    resources :product_questions
    resources :permissions

    resources :shipping_plans do
      resources :shipping_countries do
        resources :shipping_states do
          resources :shipping_cities do
            resources :shipping_costs
          end
        end
      end
    end
    
    resources :warehouses do
      resources :addresses
    end

    resources :vendors do
      resources :addresses
    end

    resources :products do
      get 'cod_activate', to: "products#cod_activate", :as => "cod_activate" 
      resources :variants do 
        resources :inventories do 
          resources :regulations
        end
        collection { post :sort }
      end
      collection { post :sort }
      collection { post :promotion_sort }
      collection { post :promotion_update }
    end
   
    get 'find_state_city_pincode', to: "countries#find_state_city_pincode", :as => "find_state_city_pincode" 
    get 'countries/autocomplete/:search_param', to: "countries#autocomplete", :as => "countries_autocomplete" 
    resources :countries do 
      collection { post :import }
      resources :states do
        collection { post :import } 
        
      end
    end
    
    resources :district_cities do
      collection { post :import }
      resources :pincodes
    end
    
    resources :users do
      get 'orders', to: 'users#orders', :as => "orders"
      resources :addresses
      resources :profiles, :except => [:index, :destroy]
    end

    resources :roles do
      resources :block_lists
    end
    get 'for_blocklist', to: 'roles#for_blocklist', :as => "for_blocklist"

    resources :tax_plans do
      resources :tax_countries do
        resources :tax_states do
          resources :tax_cities
        end
      end
    end

  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  # You can have the root of your site routed with "root"
  root 'static_pages#index'
  get 'term-use', to: 'static_pages#term_use', as: :term_use
  get 'privacy', to: 'static_pages#privacy', as: :privacy
  get 'contact-us', to: 'static_pages#contact_us', as: :contact_us
  get 'help_desk', to: 'static_pages#help_desk', as: :help_desk

  get 'about-us', to: 'static_pages#about_us', as: :about_us
  get 'policy', to: 'static_pages#policy', as: :policy
  get '/:page_name', to: "static_pages#pageshow", :as => "pageshow"
  get "pages" => 'static_pages#pages', :as => 'pages'
  
 
  get 'ship-delivery', to: 'static_pages#ship_delivery', as: :ship_delivery


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  #get '*path' => 'static_pages#index', constraints: { id: /[A-Z]\d{5}/ }
end


