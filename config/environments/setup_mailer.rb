ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
  :address              => "smtp.zoho.com",
  :port                 => 465,
  :domain               => "ranees.in",
  :user_name            => "admin@ranees.in",
  :password             => "seep@ssw0rd",
  :authentication       => "plain",
  :ssl                  => true,
  :tls                  => true
}
ActionMailer::Base.default_url_options[:host] = "localhost:3000" 