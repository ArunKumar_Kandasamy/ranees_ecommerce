class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authorize
  before_action :securing_user
  before_action :sections

  def account
    if request.xhr?
      @profile=Profile.where(:user_id => current_user.id).first
      @vouchers = Admin::Voucher.all
      respond_to do |format|
        format.html{  
        }
        format.js {
          render :template => "#{$client_name}_standard_view/users/account_show.js.erb"          
        }
      end
    else
      @profile=Profile.where(:user_id => current_user.id).first
      @vouchers = Admin::Voucher.all
      render :template => "#{$client_name}_standard_view/users/account.html.erb"     
    end
  end

  def address
    if request.xhr?
       @new_shipping_address = ShippingAddress.new
      respond_to do |format|
        format.html{  
        }
        format.js {                  
          render :template => "#{$client_name}_standard_view/users/address_show.js.erb"          
        }
      end
    else
       @new_shipping_address = ShippingAddress.new
       render :template => "#{$client_name}_standard_view/users/address.html.erb"
    end
    
  end

  def index
    @users = User.all
  end

  def show
  end

  def address_create
    if request.xhr?
      @new_shipping_address = ShippingAddress.new
      respond_to do |format|
        format.html{  
        }
        format.js {                  
          render :template => "#{$client_name}_standard_view/users/shipp_address_form.js.erb"          
        }
      end
    else
       @new_shipping_address = ShippingAddress.new
       render :template => "#{$client_name}_standard_view/users/address_create.html.erb"
    end
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to @user, notice: 'User was successfully created.'
    else
      render 'new'
    end
  end

  def update
    if current_user.update(user_params)
      redirect_to cart_pre_order_path(current_cart), notice: 'User was successfully updated.'
    else
      render 'edit'
    end
  end

  def destroy
    if @user.destroy
      redirect_to users_url, notice: 'User was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :remember_me, addresses_attributes:[:id, :_destroy, :add_line1, :add_line2, :state, :country, :c_code, :phone, :connection])
    end
end