class WishlistsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :find_user
  before_action :set_wishlist, only: [:show, :edit, :update, :destroy]

  def index
    if request.xhr?
      @wishlist = @user.wishlists.first
      respond_to do |format|
        format.html{  
        }
        format.js {                  
          render :template => "#{$client_name}_standard_view/wishlists/wishlists_index_show.js.erb"          
        }
      end
    else
     @wishlist = @user.wishlists.first
     render :template => "#{$client_name}_standard_view/wishlists/index.html.erb" 
    end
  end

  def create
    if current_user.wishlists.first.present?
      WishlistItem.create(:variant_id => params[:wishlist][:variant_id],:wishlist_id => current_user.wishlists.first.id)
    else
      @wishlist = Wishlist.create(:user_id => current_user.id)
      WishlistItem.create(:variant_id => params[:wishlist][:variant_id],:wishlist_id => @wishlist.id)
    end
    @variant = Admin::Variant.find(params[:wishlist][:variant_id])
    @wishlist  = Wishlist.new
   
    if request.xhr?
      if params[:wishlist][:section].present?
        @product = @variant.product
        @section =  Admin::Category.find(params[:wishlist][:section])
        render :template => "remote_content/wishlist1.js.erb" 
      else
        render :template => "remote_content/wishlist.js.erb" 
      end
    else 
      if params[:wishlist][:section].present?
        @product = @variant.product
        @section =  Admin::Category.find(params[:wishlist][:section])
        redirect_to section_path(@section), notice: " Product has been Saved."
      else
        render :template => "remote_content/wishlist.js.erb" 
      end
    end
  end

  private
    def set_wishlist
      @wishlist = Wishlist.find(params[:id])
    end

    def wishlist_params
      params.require(:wishlist).permit(:user_id)
    end

    def find_user
      @user = current_user
    end

end
