class ShippingAddressesController < ApplicationController
  before_action :sections
  before_action :find_user
  before_action :set_shipping_address, only: [:show, :edit, :update, :destroy]

  def index
    @shipping_addresses = @user.shipping_address
  end

  def show
  end

  def new
    @shipping_address = ShippingAddress.new
  end

  def edit 
    if request.xhr?
      respond_to do |format|
        format.html{  
        }
        format.js {                  
          render :template => "shipping_addresses/address_show.js.erb"          
        }
      end
    else 

    end 
  end

  def create 
    if request.xhr?
      @user = User.find(params[:user_id])
      current_cart = @user.cart
      @voucher = @user.vouchers.last
      if params[:shipping_address][:user1_id].present?
        @shipping_address = @user.shipping_address.build(shipping_address_params)
        if @shipping_address.save
          render :template => "users/address_show.js.erb"
        else
          render :new
        end
      elsif params[:shipping_address_id].present? 
        if params[:shipping_address_id][:id].present?
            @cart = current_cart
            cart_shipping_address_exist = CartsShippingAddresses.find_by_cart_id(current_cart.id)
            if cart_shipping_address_exist.present?
              cart_shipping_address_exist.destroy
            end
            @shipping_address = ShippingAddress.find(params[:shipping_address_id][:id])
            CartsShippingAddresses.create(:cart_id => current_cart.id,:shipping_address_id => params[:shipping_address_id][:id])
            if current_cart.shipping_addresses.first.present?
              @main_param = current_cart.shipping_addresses.first.pincode
              @final_cost = current_cart.cart_availability_list( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
              if @final_cost.class == String 
                if @final_cost == "Available"
                  @total_shipping_cost = current_cart.find_cart_total_shipping_cost( Admin::Country.find_by_name(current_cart.shipping_addresses.first.country).id,current_cart.shipping_addresses.first.pincode)
                end
              end
            end
            @total_price= 0.0
            current_cart.cart_items.each do | cart_item|
              if cart_item.price.present?
                @total_price += cart_item.price
              end
            end 
            if @final_cost.class == String 
              if @final_cost == "Available"
                @tax_price = 0 
                @cart.cart_items.each do | cart_item| 
                  @variant = Admin::Variant.find(cart_item.variant_id)
                  if @variant.product.tax_violate
                     @tax_price = 0
                  else
                     @tax_price += @variant.variant_tax_cost(@cart.shipping_addresses.first.pincode,cart_item.qty)
                  end
                end
                @total_cross_price = @tax_price + @total_shipping_cost + @total_price
              end
            end
            render :template => "remote_content/update_payment_page.js.erb"
          else
            @new_shipping_address_form = 1
            current_user = User.find(params[:user_id])
            @voucher = @user.vouchers.last
            @shipping_address = @user.shipping_address.build(shipping_address_params)
            if @shipping_address.save
              cart_shipping_address_exist = CartsShippingAddresses.find_by_cart_id(current_cart.id)
              if cart_shipping_address_exist.present?
                cart_shipping_address_exist.destroy
              end
              @new_shipping_address = ShippingAddress.new
              CartsShippingAddresses.create(:cart_id => current_cart.id,:shipping_address_id => @shipping_address.id)
              current_cart.update(:pincode => @shipping_address.pincode)
              @total_price= 0.0
              current_cart.cart_items.each do | cart_item|
                if cart_item.price.present?
                  @total_price += cart_item.price
                end
              end 
              @cart = current_cart
               @a = (0..100000).to_a.shuffle
                if @cart.shipping_addresses.first.present?
                  @main_param = @cart.shipping_addresses.first.pincode
                  @final_cost = @cart.cart_availability_list( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
                  if @final_cost.class == String 
                    if @final_cost == "Available"
                      @total_shipping_cost = @cart.find_cart_total_shipping_cost( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
                    end
                  end
                end 
              if @final_cost.class == String 
                if @final_cost == "Available"
                  @tax_price = 0 
                  @cart.cart_items.each do | cart_item| 
                    @variant = Admin::Variant.find(cart_item.variant_id)
                    if @variant.product.tax_violate
                       @tax_price = 0
                    else
                       @tax_price += @variant.variant_tax_cost(@cart.shipping_addresses.first.pincode,cart_item.qty)
                    end
                  end
                  @total_cross_price = @tax_price + @total_shipping_cost + @total_price
                end
              end
                #render :template => "remote_content/preorder_refresh.js.erb"
                render :template => "remote_content/update_payment_page.js.erb"
              end
          end
      else
        @new_shipping_address_form = 1
        current_user = User.find(params[:user_id])
        @voucher = @user.vouchers.last
        @shipping_address = @user.shipping_address.build(shipping_address_params)
        if @shipping_address.save
          cart_shipping_address_exist = CartsShippingAddresses.find_by_cart_id(current_cart.id)
          if cart_shipping_address_exist.present?
            cart_shipping_address_exist.destroy
          end
          @new_shipping_address = ShippingAddress.new
          CartsShippingAddresses.create(:cart_id => current_cart.id,:shipping_address_id => @shipping_address.id)
          current_cart.update(:pincode => @shipping_address.pincode)
          @total_price= 0.0
          current_cart.cart_items.each do | cart_item|
            if cart_item.price.present?
              @total_price += cart_item.price
            end
          end 
          @cart = current_cart
          if params[:useraddress].present?
            render :template => "remote_content/user_address_ship.js.erb"
          elsif params[:shipping_address][:order_id].present?
            @order = Admin::Order.find(params[:shipping_address][:order_id])
            render :template => "remote_content/order_address_ship.js.erb"
          else
            @a = (0..100000).to_a.shuffle
            if @cart.shipping_addresses.first.present?
              @main_param = @cart.shipping_addresses.first.pincode
              @final_cost = @cart.cart_availability_list( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
              if @final_cost.class == String 
                if @final_cost == "Available"
                  @total_shipping_cost = @cart.find_cart_total_shipping_cost( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
                end
              end
            end 
          if @final_cost.class == String 
            if @final_cost == "Available"
              @tax_price = 0 
              @cart.cart_items.each do | cart_item| 
                @variant = Admin::Variant.find(cart_item.variant_id)  
                 if @variant.product.tax_violate
                   @tax_price = 0
                else
                   @tax_price += @variant.variant_tax_cost(@cart.shipping_addresses.first.pincode,cart_item.qty)
                end
              end
              @total_cross_price = @tax_price + @total_shipping_cost + @total_price
            end
          end
            #render :template => "remote_content/preorder_refresh.js.erb"
            render :template => "remote_content/update_payment_page.js.erb"
          end
        end
      end
    else
      @shipping_address = @user.shipping_address.build(shipping_address_params)
      if @shipping_address.save
        if params[:shipping_address][:user1_id].present?
          redirect_to user_address_path(@user), notice: 'Shipping address was successfully created.'
        elsif params[:shipping_address][:user2_id].present?
          redirect_to user_address_path(@user), notice: 'Shipping address was successfully created.'
        else  
          redirect_to user_shipping_address_path(@user, @shipping_address), notice: 'Shipping address was successfully created.'
        end
      else
        render :new
      end
    end
  end 

  def update
    if request.xhr?
      cart_shipping_address_exist = CartsShippingAddresses.find_by_cart_id(current_cart.id)
      if cart_shipping_address_exist.present?
        cart_shipping_address_exist.destroy
      end
      @new_shipping_address = ShippingAddress.new
      CartsShippingAddresses.create(:cart_id => current_cart.id,:shipping_address_id => @shipping_address.id)
      current_cart.update(:pincode => @shipping_address.pincode)
      current_cart.cart_items.each do |cart_item|
        @variant= Admin::Variant.where(:id => cart_item.variant_id).first
        @high_inventory_price= @variant.user_price(current_cart.pincode)
        cart_item.per_qty_price = @high_inventory_price
        cart_item.save
      end
      @total_price= 0.0
      current_cart.cart_items.each do | cart_item|
        if cart_item.price.present?
          @total_price += cart_item.price
        end
      end 
      @a = (0..100000).to_a.shuffle
          if current_cart.shipping_addresses.first.present?
            @main_param = current_cart.shipping_addresses.first.pincode
            @final_cost = current_cart.cart_availability_list( Admin::Country.find_by_name(current_cart.shipping_addresses.first.country).id,current_cart.shipping_addresses.first.pincode)
            if @final_cost.class == String 
              if @final_cost == "Available"
                @total_shipping_cost = current_cart.find_cart_total_shipping_cost( Admin::Country.find_by_name(current_cart.shipping_addresses.first.country).id,current_cart.shipping_addresses.first.pincode)
              end
            end
          end 
        if @final_cost.class == String 
          if @final_cost == "Available"
            @tax_price = 0 
            current_cart.cart_items.each do | cart_item| 
              @variant = Admin::Variant.find(cart_item.variant_id)
                 if @variant.product.tax_violate
                   @tax_price = 0
                else
                   @tax_price += @variant.variant_tax_cost(current_cart.shipping_addresses.first.pincode,cart_item.qty)
                end
            end
            @total_cross_price = @tax_price + @total_shipping_cost + @total_price
          end
        end
      render :template => "remote_content/preorder_refresh.js.erb"
    else
      if @shipping_address.update(shipping_address_params)
        redirect_to user_address_path(@user), notice: 'Shipping address was successfully updated.'
      else
        render :edit
      end
    end
  end

  def destroy
      if @shipping_address.destroy
        if params[:delete_id].present?
          redirect_to user_address_path(@user), notice: 'Shipping address was successfully destroyed.'
        else
          redirect_to cart_path(current_cart), notice: 'Shipping address was successfully destroyed.'
        end
      end
  end

  private
    def set_shipping_address
      @shipping_address = ShippingAddress.find(params[:id])
    end

    def shipping_address_params
      params.require(:shipping_address).permit(:user_id, :name, :add_line1, :add_line2,:pincode, :state, :country, :c_code, :phone)
    end

    def find_user
      @user = current_user
    end

end