class CartItemsController < ApplicationController
  before_action :find_actual_cost, only: [:update]
  before_action :sections
  respond_to :html, :json,:js

  def edit
  	@cart_item = CartItem.find(params[:id])
  end

  def update 
     if params[:view].present? 
       new_qty1 = params[:view]
    else
      new_qty = params[:cart_item][:qty]
    end
    @cart=Cart.find(@cart_item.cart_id) 
    respond_to do |format|
      if new_qty.present? 
        bal_qty=0
        @variant.inventories.each do |inventory|
          bal_qty+=inventory.balance_qty
        end
        if bal_qty.to_i >= new_qty.to_i
          @cart_item.update(cart_item_params)
          format.json  { render :json =>{ :message => "Updated Successfully"} }
        else
          @cart_item.update(cart_item_params)
          @cart_item.update(:qty => bal_qty)
          format.json  { render :json =>{ :message => "Quantity Unavailable"} }
        end
      elsif new_qty1.present?
        bal_qty=0
        @variant.inventories.each do |inventory|
          bal_qty+=inventory.balance_qty
        end
        if bal_qty.to_i >= new_qty1.to_i
          @cart_item.update(:qty => new_qty1)
          format.json  { render :json =>{ :message => "Updated Successfully"} }
        else
          @cart_item.update(:qty => new_qty1)
          @cart_item.update(:qty => bal_qty)
          format.json  { render :json =>{ :message => "Quantity Unavailable"} }
        end
      else
        @cart_item.update(:qty => new_qty1) 
        format.json  { render :json =>{ :message => "Updated Successfullys"} }
      end
      format.html { redirect_to admin_carts_show_path(@cart) }
    end
  end

  def destroy 
  	@cart_item=CartItem.find(params[:id])
    @cart=Cart.find(@cart_item.cart_id)
  	if @cart_item
  	  @cart_item.destroy
      if request.xhr?
        respond_to do |format|
          format.js {  
            if params[:payment].present?
              @total_price= 0.0
              @cod_unavail_item= Array.new 
              @cart.cart_items.each do | cart_item| 
                @total_price += cart_item.price
                @variant=Admin::Variant.where(:id => cart_item.variant_id).first
                @product= Admin::Product.where(:id => @variant.product_id).first
                if @cart.pincode.present?
                  if @product.check_cod(@cart.pincode) == false
                    @cod_unavail_item << cart_item
                  end
                end
              end
              render :template => "remote_content/refresh_payment.js.erb"
            elsif params[:show].present?
              @a = (0..100000).to_a.shuffle
              @total_price= 0.0
              @tax_price = 0 
              @cart.cart_items.each do | cart_item|
                @total_price += cart_item.price
                @variant = Admin::Variant.find(cart_item.variant_id)
                if @variant.product.tax_violate
                   @tax_price = 0
                else
                @tax_price += @variant.variant_tax_cost(@cart.shipping_addresses.first.pincode,cart_item.qty)
                end
              end
              if @cart.shipping_addresses.first.present?
                @main_param = @cart.shipping_addresses.first.pincode
                @final_cost = @cart.cart_availability_list( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
                if @final_cost.class == String 
                  if @final_cost == "Available"
                    @total_shipping_cost = @cart.find_cart_total_shipping_cost( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
                  end
                end
              end 
              if @final_cost.class == String 
                if @final_cost == "Available"
                  @total_cross_price = @tax_price + @total_shipping_cost + @total_price
                end
              end
              render :template => "remote_content/preorder_refresh1.js.erb"
            else
              @total_price= 0.0
              @cart.cart_items.each do | cart_item|
                @total_price += cart_item.price
              end
              render :template => "remote_content/refresh_cart.js.erb"
            end
          }
          format.html { }
        end
      else
        redirect_to cart_path(@cart)
      end
  	end
  end

  private
  def find_actual_cost
    @cart_item = CartItem.find(params[:id])
    @variant = Admin::Variant.find(@cart_item.variant_id)
    if params[:cart_item].present?
      if params[:cart_item][:pincode].present?
        @product = @variant.product
        @selling_price = @variant.inventories.first.selling_price 
        @selling_price = Admin::Product.user_price(params[:cart_item][:pincode], @product, @variant)
        @cart_item.update(:per_qty_price => @selling_price)
      end
    end
  end

  def cart_item_params
    params.require(:cart_item).permit(:variant_id, :cart_id, :qty, :price)
  end

end