class PasswordsController < Devise::PasswordsController
  before_action :sections

  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    respond_to do |format|
      format.js {
        if resource.errors.empty?  
          flash[:notice] = "A link has been send to ur email click that to change ur password"
          render :template => "remote_content/devise_password.js.erb"
          flash.discard
        else
          flash[:alert] = "Invalid email"
          render :template => "remote_content/devise_error_password.js.erb"
          flash.discard
        end
      }
      format.html{
        if resource.errors.empty?  
          flash[:notice] = "A link has been send to ur email click that to change ur password"
          redirect_to new_user_session_path
        else
          flash[:alert] = "Invalid email"
          redirect_to new_password_path(resource_name)
        end
      }
    end
  end
   
end 