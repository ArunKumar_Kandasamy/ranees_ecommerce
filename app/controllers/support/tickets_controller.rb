class Support::TicketsController < ApplicationController
  before_action :authenticate_user!, except: [:create]
  before_action :authorize , except: [:create] 
  before_action :authorize_csm, except: [:create]
  before_action :set_support_ticket, only: [:show, :edit, :update, :destroy]

  def index 
    @tickets = Support::Ticket.all
    @general_tickets = @tickets.where(:subject => "General")
    @order_tickets = @tickets.where(:subject => "Order")
    @voucher_tickets = @tickets.where(:subject => "Voucher")
    @task_assigned_tickets = @tickets.includes(:ticket_tasks)
    if request.xhr?
      if params[:general_page].present?
        render :template => "support/tickets/general_page.js.erb"
        @up_order = Support::Ticket.where(:subject => "General").all
      elsif params[:order_page].present?
        render :template => "support/tickets/order_page.js.erb"
        @wh_order = Support::Ticket.where(:subject => "Order").all
      elsif params[:voucher_page].present?
        render :template => "support/tickets/voucher_page.js.erb"
        @wh_order = Support::Ticket.where(:subject => "Voucher").all
      else
        @render_path = support_tickets_path
        @render_file = 'support/tickets/index'
        render "remote_content/common_render.js.erb"   
      end       
    else
      render :template => "support/tickets/index"
    end
  end

  def show
    @ticket_reply = @ticket.ticket_replies.first
    unless @ticket_reply.present?
      @new_ticket_reply = Support::TicketReply.new
      @assigned_user = Array.new
      User.joins(:role).each do |assign_user|
        if assign_user.role.name != 'u' && assign_user.email != current_user.email
          @assigned_user << assign_user
        end
      end
    end
    if request.xhr?
      @render_path = support_ticket_path(@ticket)
      @render_file = 'support/tickets/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "support/tickets/show"
    end
  end

  def new
    @ticket = Support::Ticket.new
  end

  def edit
  end

  def create
    @support_ticket = Support::Ticket.new(support_ticket_params)
    @support_ticket.save
    redirect_to help_desk_path, notice: 'Ticket was successfully created.'
  end

  def update
    if @ticket.update(support_ticket_params)
      if request.xhr?
        @ticket_reply = @ticket.ticket_replies.first
        unless @ticket_reply.present?
          @new_ticket_reply = Support::TicketReply.new
          @assigned_user = Array.new
          User.joins(:role).each do |assign_user|
            if assign_user.role.name != 'u' && assign_user.email != current_user.email
              @assigned_user << assign_user
            end
          end
        end
        flash['alert'] = 'Ticket was successfully updated.'
        @render_path = support_ticket_path(@ticket)
        @render_file = 'support/tickets/show'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "support/tickets/show"
      end
    end
  end

  def destroy
    @support_ticket.destroy
    respond_to do |format|
      format.html { redirect_to support_tickets_url, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_support_ticket
      @ticket = Support::Ticket.find(params[:id])
    end

    def support_ticket_params
      params.require(:support_ticket).permit(:name, :email, :user_id, :order_id, :consignment_id, :reason, :status, :subject, :body, :consignment_number, :order_number)
    end
end
