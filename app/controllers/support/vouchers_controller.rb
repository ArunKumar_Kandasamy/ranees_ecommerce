class Support::VouchersController < ApplicationController
  before_action :set_voucher, only: [:show, :edit, :update, :destroy]
  before_action :authorize
  def show
  
  end

  private
	# Use callbacks to share common setup or constraints between actions.
	def set_voucher
	  @vendor = Admin::Voucher.find(params[:id])
    end

	# Never trust parameters from the scary internet, only allow the white list through.
	def voucher_params
	  params.require(:admin_voucher).permit(:user_id, :order_id, :credit, :debit, :voucher_amount, :refund_revocation_id,:order_revocation_id)
	end
end