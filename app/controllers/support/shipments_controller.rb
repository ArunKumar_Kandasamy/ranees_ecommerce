class Support::ShipmentsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize

  before_action :set_order
  before_action :set_shipment, only: [:show, :edit,:update]

  def index
  end

  def show
  	@order = @shipment.order
    @consignment_items = @shipment.consignment_items
  end

  def update
    if params[:admin_consignment].present?
      if params[:admin_consignment][:status] == "Replacement"
        @shipment.create_replacement_for_lost
      elsif params[:admin_consignment][:status] == "Resend"

        @shipment.update(:status => "Resend") 
        @shipment.create_resend_consignment
      end
      redirect_to support_order_shipment_path(@shipment.order, @shipment), notice: "Consignment updated Successfully"  
    end
  end

  def destroy
  end

  private
    def set_shipment
      @shipment = Admin::Consignment.find(params[:id])
    end
  
    def set_order
      @order = Admin::Order.find(params[:order_id])
    end

    def consignment_params
      params.require(:admin_consignment).permit(:order_id,:warehouse_id, :conform, :cancel, :remarks, :delivery_status,:status)
    end
end
