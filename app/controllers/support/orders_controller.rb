class Support::OrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize  
  before_action :authorize_support
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  before_action :order_params, only: [:update]

  def index
    @param = params[:q] 
    @search = Admin::Order.all.search(params[:q])
    @orders = @search.result
    @wh_order = @search.result.where(:status => "wh").includes(:user) 
    @up_order = @search.result.where(:status => "up").includes(:user) 
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      if params[:up_page].present?
        render :template => "support/orders/up_order_page.js.erb"
        @up_order = Admin::Order.up_orders.all.page(params[:up_page]).per(5).search(params[:q])
      elsif params[:wh_page].present?
        render :template => "support/orders/wh_order_page.js.erb"
        @wh_order = Admin::Order.wh_orders.all.page(params[:wh_page]).per(5).search(params[:q])
      else
        @render_path = support_orders_path
        @render_file = 'support/orders/index'
        render "remote_content/common_render.js.erb" 
      end         
    else
      render :template => "support/orders/index"
    end

  end 

  def edit
    @new_shipping_address = ShippingAddress.new
    @items = @order.order_items
    @order_revocation = @order.order_revocations.build 
    if request.xhr?
      @render_path = edit_support_order_path(@order)
      @render_file = 'support/orders/edit'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "support/orders/edit"
    end
  end

  def show
    @all = @order.consignments
    @delivered = @order.consignments.where(:delivery_status => true)
    @notdelivered = @order.consignments.where(:delivery_status => false)
    @unprocess = @order.consignments.where(:delivery_status => nil,:conform => false)
    @processed = @order.consignments.where(:delivery_status => nil,:conform => true)
    @order_revocation = @order.order_revocations.build 
    if request.xhr?
      if params[:view].present?
        render :template => "admin/orders/admin_show.js.erb" 
      else
        @render_path = support_order_path(@order)
        @render_file = 'support/orders/show'
        render "remote_content/common_render.js.erb"
      end          
    else
      render :template => "support/orders/show"
    end 
  end
 
  def update
    if request.xhr? 
      if params[:admin_order][:for_check].present?
        @new_order_hash = params[:admin_order]
        render :template => "remote_content/order_edit_preview.js.erb"
      else
        if params[:admin_order][:shipping_address_id].present?
          if @order.status == "up" && @order.cancel.nil?
            @order.update(order_params)
            @order_log =  @order.create_order_log(current_user.id)
            @new_order_hash = params[:admin_order] 
            @new_order_hash[:order_items_attributes].each do |new_order_hash|
              @order_item_log = @order_log.order_item_logs.where(:variant_id => new_order_hash[1][:variant_id]).first
              @order_item_log.update(:reason => new_order_hash[1][:reason])
            end
            flash[:notice] = 'Order was successfully updated.'
            render :js => "window.location = '#{support_order_path(@order)}'"
          else
            flash[:notice] = 'Order cannot be updated.'
            render :js => "window.location = '#{support_order_path(@order)}'"
          end
        else
          if params[:admin_order][:warehouse_id].present?
            @order.update(:status => "wh")
            @order_log = @order.create_order_log(current_user.id)
            flash[:notice] = "Shipping Plan Added."
            render :template => "remote_content/order_warehouse.js.erb"
          else
            if params[:admin_order][:present_user].present?
              if @order.status == "up" && @order.check_consignment_status && @order.cancel.nil?
                @order.update(order_params)
                @order.create_order_log(current_user.id)
                render :template => "remote_content/order_warehouse.js.erb"
              else
                flash[:notice] = 'Sorry You Cannot Update This Order'
                render :template => "remote_content/order_warehouse.js.erb"
              end  
            end
          end
        end
      end 
    else 
      if @order.check_support_cancel_order_status
        if @order.payment_mode == 'COD'
          if params[:admin_order][:support_order_cancel].present?
            @order.update(order_params)
            @order.create_order_log(current_user.id)
            @notification= Admin::NotificationCustomer.first
            if @notification.present?
              if @notification.post_deli_cancel
                @order.order_cancel_sms 
                Admin::InvoiceMailer.email_order_cancelled(@order).deliver
              end
            end
            redirect_to support_order_path(@order), notice: 'Order was successfully Cancelled.'
          else
            redirect_to support_order_path(@order), notice: 'Order was successfully Cancelled.'
          end
        else
          if @order.update(:cancelled_by => current_user.id ,:cancel => params[:admin_order][:cancel], :remarks => params[:admin_order][:remarks])
            if params[:admin_order][:order_revocations_attributes].first[1][:revocation_mode] == "Cash Back"
              if params[:admin_order][:order_revocations_attributes].first[1][:bank_account_id].nil?
                @bankaccount = BankAccount.create(:name => params[:admin_order][:bank_accounts][:name], :num => params[:admin_order][:bank_accounts][:name], :bank_code => params[:admin_order][:bank_accounts][:ifsc_code], :user_id => @order.user_id)
                @bank_account_id = @bankaccount.id
              else
                @bank_account_id = params[:admin_order][:order_revocations_attributes].first[1][:bank_account_id] 
              end
              @revocation_mode = 'b'
            else
              @revocation_mode = 'v'
            end
            if @notification.present?
              if @notification.post_deli_cancel
                @order.order_cancel_sms 
                Admin::InvoiceMailer.email_order_cancelled(@order).deliver
              end
            end
            @order_revocation = @order.create_revocation_for_cancel
            @order_revocation.update(:bank_account_id =>  @bank_account_id,:revocation_mode => @revocation_mode)
            @order.create_order_log(current_user.id)
          else
            redirect_to support_order_path(@order), notice: 'Order Cannot Be Cancell Contact Our Support.'
          end
        end
      else
        redirect_to support_order_path(@order), notice: 'Order Cannot Be Cancell Contact Our Support.'
      end
    end
  end

  private 
    def set_order
      @order = Admin::Order.find(params[:id])
    end

    def order_params
      params.require(:admin_order).permit(:cancel, :remarks, :cancelled_by, :user_id, :pay_val, :cal_conf, :present_user, :prev_rec,  :shipping_address_id, :order_cost, :payment_mode, :payment_status, :order_number, :status,:warehouse_id, :prev_rec_reason, :pay_val_reason, :cal_conf_reason, order_items_attributes:[:id, :variant_id, :_destroy, :qty, :per_qty_price,:reason],consignments_attributes:[:id,:order_item_id,:warehouse_id,:consignment_cost],order_revocations_attributes:[:id, :revocation_mode ,:revocation_cost])
    end
end
