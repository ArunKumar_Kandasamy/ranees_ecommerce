class Support::OrderRevocationController < ApplicationController
	before_action :authenticate_user!
  before_action :authorize
  before_action :set_order

  def index
  	if request.xhr?
  		@render_path = support_order_order_revocation_index_path(@order)
      @render_file = 'support/order_revocation/index'
      render "remote_content/common_render.js.erb" 
    else
      render :template => "admin/order_revocation/index"
    end     
  end

  def show
  end

  def edit
  end

  def destroy
  end

  private 
    def set_order
      @order = Admin::Order.find(params[:order_id])
    end
end
