class Support::UsersController < ApplicationController
	before_filter :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update]
  before_action :authorize
  before_action :authorize_support

  def index
    @param = params[:q]  
    @search = User.all.search(params[:q])
    @users= @search.result.includes(:addresses, :profile)
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = support_users_path
      @render_file = 'support/users/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "support/users/index"
    end
  end

  def edit
    if request.xhr?
      @render_path = edit_support_user_path(@user)
      @render_file = 'support/users/edit'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "support/users/edit"
    end
  end

  def show
    @failure_transactions= @user.failure_transactions
    @vouchers= @user.vouchers
    if request.xhr?
      @render_path = support_user_path(@user)
      @render_file = 'support/users/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "support/users/show"
    end
  end

  def update
  end

  private
    # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

end
