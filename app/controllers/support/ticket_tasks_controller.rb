class Support::TicketTasksController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_support_ticket_task, only: [:show, :edit, :update, :destroy]
  before_action :restricting_user, only: [:show]
  
  def index
    @support_ticket_tasks = Support::TicketTask.where(:assigned_to => current_user.id)
    @general_tickets = @support_ticket_tasks.where("ticket_tasks.status != ?","redirected").joins(:ticket).where(:tickets => {:subject => 'General' })
    @order_tickets = @support_ticket_tasks.where("ticket_tasks.status != ?","redirected").joins(:ticket).where(:tickets => {:subject => 'Order' })
    @voucher_tickets = @support_ticket_tasks.where("ticket_tasks.status != ?","redirected").joins(:ticket).where(:tickets => {:subject => 'Voucher' })
    @task_assigned_tickets = @support_ticket_tasks.where(:status => "redirected").includes(:tasks)
    if request.xhr?
      @render_path = support_ticket_tasks_path
      @render_file = 'support/ticket_tasks/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "support/ticket_tasks/index"
    end
  end

  def show
    @ticket = @support_ticket_task.ticket
    @ticket_reply = @ticket.ticket_replies.first
    unless @ticket_reply.present?
      @new_ticket_reply = Support::TicketReply.new
      @assigned_user = Array.new
      User.joins(:role).each do |assign_user|
        if assign_user.role.name != 'u' && assign_user.email != current_user.email
          @assigned_user << assign_user
        end
      end
    end
    if request.xhr?
      @render_path = support_ticket_task_path(@ticket)
      @render_file = 'support/ticket_tasks/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "support/ticket_tasks/show"
    end
  end

  def new
    @support_ticket_task = Support::TicketTask.new
  end

  def edit
  end

  def create
    @support_ticket_task = Support::TicketTask.new(support_ticket_task_params)
    @support_ticket_task.save
    if params[:support_ticket_task][:parent_task_id].present?
      @parent_task = Support::TicketTask.find(params[:support_ticket_task][:parent_task_id])
      @parent_task.update(:status => "redirected")
      redirect_to support_ticket_task_path(params[:support_ticket_task][:parent_task_id]), notice: 'Ticket task was successfully created.'
    else
      redirect_to support_ticket_path(params[:support_ticket_task][:ticket_id]), notice: 'Ticket task was successfully created.'
    end
  end

  def update
    if @support_ticket_task.update(support_ticket_task_params)
      if request.xhr?
        flash['alert'] = 'Ticket task was successfully updated.'
        @render_path = support_ticket_task_path( @support_ticket_task)
        @render_file = 'support/ticket_tasks/show'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "support/ticket_tasks/show"
      end
    end
  end

  def destroy
    @support_ticket_task.destroy
    flash['alert'] = 'Ticket task was successfully destroyed.'
    @render_path = support_ticket_task_path
    @render_file = 'support/ticket_tasks/index'
    render "remote_content/common_render.js.erb"
  end

  private

    def restricting_user
      if current_user.id != @support_ticket_task.assigned_to
        redirect_to root_path
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_support_ticket_task
      @support_ticket_task = Support::TicketTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def support_ticket_task_params
      params.require(:support_ticket_task).permit(:assigned_by, :assigned_to, :priority, :status, :parent_task_id, :ticket_id)
    end
end
