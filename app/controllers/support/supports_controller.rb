class Support::SupportsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :authorize_support
  
  def dashboard
  	if request.xhr?
      @render_path = support_dashboard_path
      @render_file = 'support/supports/dashboard'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "support/supports/dashboard"
    end
  end
  
end
