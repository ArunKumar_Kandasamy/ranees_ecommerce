class ProfilesController < ApplicationController
  before_action :sections
  before_filter :authenticate_user!
  before_action :authorize
  before_action :securing_user
  before_action :set_profile, only: [:show, :edit, :update, :destroy]
  before_action :find_user


  def show
    if request.xhr?
      @profile=Profile.where(:user_id => current_user.id).first
      respond_to do |format|
        format.html{  
        }
        format.js {
          render :template => "#{$client_name}_standard_view/profiles/profile_show.js.erb"          
        }
      end
    else
      @profile=Profile.where(:user_id => current_user.id).first
      render :template => "#{$client_name}_standard_view/profiles/show.html.erb" 
    end
  end 

  def new 
    if request.xhr?
      @profile = Profile.new
      respond_to do |format|
        format.html{  
        }
        format.js {                  
          render :template => "#{$client_name}_standard_view/profiles/profile_new.js.erb"          
        }
      end
    else
      @profile = Profile.new
      render :template => "#{$client_name}_standard_view/profiles/new.html.erb" 
    end
   
  end

  def edit
    if request.xhr?
      respond_to do |format|
        format.html{  
           render :template => "#{$client_name}_standard_view/profiles/edit.html.erb"  
        }
        format.js {                  
          render :template => "#{$client_name}_standard_view/profiles/edit_show.js.erb"          
        }
      end
    else
      render :template => "#{$client_name}_standard_view/profiles/edit.html.erb"  
    end
  end

  def create
    @profile =@user.build_profile(profile_params)
    if @profile.save     
      redirect_to user_account_path(@user), notice: 'Profile was successfully created.'      
    else
      render 'new'
    end
  end

  def update
    if request.xhr?
      if @profile.update(profile_params)
        respond_to do |format|
          format.html{  
            redirect_to user_profile_path(@user, @profile), notice: 'Profile was successfully updated.'
          }
          format.js {
            @render_path = user_profile_path(@user, @profile)
            flash[:notice] = "Profile was successfully updated" 
            render :template => "#{$client_name}_standard_view/profiles/edit_show.js.erb"
                     
          }
        end
      else
        render 'edit'
      end
    else
      if @profile.update(profile_params)
        redirect_to user_profile_path(@user, @profile), notice: 'Profile was successfully updated.'
      else
        render 'edit'
      end
    end
    
  end

  private
    def set_profile
      @profile = Profile.find(params[:id])
    end

    def profile_params
      params.require(:profile).permit(:f_name, :l_name, :m_number, :gender)
    end

    def find_user
      @user = current_user
    end
end