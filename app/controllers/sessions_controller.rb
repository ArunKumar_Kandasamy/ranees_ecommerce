class SessionsController < Devise:: SessionsController
  before_action :sections
  before_action :check_bl_user, only: [:create]

  def create 
    self.resource = warden.authenticate(auth_options)
    if resource && resource.active_for_authentication?
      if resource.cart.present?
        @cart= resource.cart
        if session[:cart_id].present?
          @present_cart= Cart.where(:id => session[:cart_id]).first
          @present_cart.cart_items.each do | present_item|
            @cart_item=CartItem.where("variant_id = ? AND cart_id = ?", present_item.variant_id, @cart.id).first
            if @cart_item.present?
              @cart_item.qty += 1 
              @cart_item.save
              @cart_item.price = @cart_item.per_qty_price * @cart_item.qty
              @cart_item.save
            else
              @present_cart.cart_items.each do |cart_item|
                CartItem.create!(:variant_id => cart_item.variant_id, :cart_id => @cart.id, :price => cart_item.price, :per_qty_price =>cart_item.per_qty_price,  :qty => cart_item.qty)
              end
            end
          end
        end
      else
        current_cart.user_id= resource.id
        current_cart.save
      end
      
      respond_to do |format|
        format.html{
          sign_in(resource_name, resource) 
          session[:trigger_modal] = "1"
          if session[:previous_url]
            redirect_to session[:previous_url], notice: "Login Successful"
          else
            redirect_to root_path
          end
        }

        format.js {
          session[:trigger_modal] = "1"
          flash[:notice] = "Created account, signed in."
          render :template => "remote_content/devise_success_sign_up.js.erb"
          flash.discard
          sign_in(resource_name, resource)
        }
      end
    else
      respond_to do |format|
        format.html {
          flash[:alert] = "Invalid Username or Password"
          clean_up_passwords resource
          redirect_to new_user_session_path
        }
        format.js {
          flash[:alert] = "Invalid Username or Password"
          render :template => "remote_content/devise_errors_sign_in.js.erb"
          flash.discard
        }
      end
    end
  end

  def destroy
    session[:cart_id]= nil 
    super
  end

  def check_bl_user
    @user = User.find_by_email(params[:user][:email])
    if  @user.present?
      if  @user.role.name == "bl"
        flash[:alert] = "You are blocked as a spam user"
        redirect_to root_path
      end
    end
  end

end