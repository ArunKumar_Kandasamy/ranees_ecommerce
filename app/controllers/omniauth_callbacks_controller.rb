class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def all
    auth = request.env["omniauth.auth"]
    user = User.from_omniauth(auth)
    if user.persisted?
      if current_cart.present?
        if user.cart.nil?
          current_cart.user_id= user.id
          current_cart.save
        end
      end
    
      @permission= Admin::Permission.where(:email => auth.info.email).first
    
      if @permission.nil?
        @permission=Admin::Permission.create(:role_id => "1", :email => user.email, :user_id =>user.id, :active => "true")
      else
      	@permission=@permission.update_attributes(:user_id => user.id, :active => "true")
      end
      @identity= Identity.where(provider: auth.provider).first
      @identity= @identity.update_attributes(user_id: user.id)
      flash.notice = "Signed in!"
      sign_in_and_redirect user,event: :authentication
    else
      session["devise.user_attributes"] = user.attributes
      redirect_to new_user_registration_url
    end
  end
  alias_method :facebook, :all
  alias_method :linkedin, :all
  alias_method :google_oauth2, :all
end