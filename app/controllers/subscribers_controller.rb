class SubscribersController < ApplicationController
  before_action :set_subscriber, only: [:show, :edit, :update, :destroy]

  def index
    @subscribers = Subscriber.all
  end

  def show
  end

  def new
    @subscriber = Subscriber.new
  end

  def edit
  end

  def create
    @subscriber = Subscriber.new(subscriber_params)
    if @subscriber.save
      Admin::InvoiceMailer.email_subscription(@subscriber).deliver
      redirect_to session[:previous_url], notice: 'Subscriber was successfully created.'
    else
      redirect_to session[:previous_url], notice: 'You have already subscribed!!.'
    end
  end

  def update
    if @subscriber.update(subscriber_params)
      redirect_to @subscriber, notice: 'Subscriber was successfully updated.' 
    else
      render 'edit' 
    end
  end

  def destroy
    if @subscriber.destroy
      redirect_to subscribers_url, notice: 'Subscriber was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subscriber
      @subscriber = Subscriber.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subscriber_params
      params.require(:subscriber).permit(:email)
    end
end
