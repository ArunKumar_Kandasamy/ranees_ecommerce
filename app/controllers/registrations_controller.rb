class RegistrationsController < Devise::RegistrationsController
  before_action :sections
 
  def new
    super 
  end

  def edit
    if request.xhr?
      respond_to do |format|
        format.html{  
        }
        format.js {                  
          render :template => "devise/registrations/edit_show.js.erb"          
        }
      end
    else
    
    end
  end

  def create
    @user= User.new(user_params)
    if @user.save
      @permission = Admin::Permission.where(email: @user.email).first
      if @permission.nil?
        Admin::Permission.create(:role_id =>"1", :email => @user.email, :user_id => @user.id, :active => "true")
      else
        @permission.update_attributes(:user_id => @user.id, :active => "true")
      end
      respond_to do |format|
        format.html{
          sign_up(resource_name, resource)
          redirect_to root_path, notice: "Account Successfully Created"
        }
        format.js {
          flash[:notice] = "Created account, signed in."
          render :template => "remote_content/devise_success_sign_up.js.erb"
          flash.discard
          sign_up(resource_name, resource)
        }
      end
    else
      respond_to do |format|
        format.html {
          clean_up_passwords resource
          flash[:notice] = "There is already an account with this email address."
          redirect_to new_user_registration_path
          #respond_with resource
        }
        format.js {
          flash[:alert] = @user.errors.full_messages.to_sentence
          render :template => "remote_content/devise_errors.js.erb"
          flash.discard
        }
      end
    end
  end

  def update
    super
    @permission= @user.permission.update_attributes(:email => resource.email)
  end

  protected
  
  def after_update_path_for(resource)
    if current_user.profile.nil?
      new_user_profile_path(current_user)
    else
      user_profile_path(current_user, current_user.profile)
    end
  end

  private
  def set_user
    @user =User.find(params[:id])
  end

    # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :remember_me, :current_password)
  end
end