class SectionsController < ApplicationController
  before_action :set_section, only: [:showcase]
  before_action :sections
  before_action :check_status_visible, only: [:showcase]

  def index
    @categories = Admin::Category.all    
    @admin_parents = @categories.roots
    @products = Admin::Product.all 

    $subscriber = Subscriber.new
    @products = Admin::Product.where(:status => true,:visibility => true)
    @offer_products = @products.where(:promote => "o").sort_by(&:promotional_sort)
    @latest_products = @products.where(:promote => "l").sort_by(&:promotional_sort)
    @trending_products = @products.where(:promote => "t").sort_by(&:promotional_sort)
    @image_types = Admin::ImageType.all
    @section_products = @products.where('promote != ?','NULL')
    @image_types.where(:image_type => "section_slider").each do |image_type|
      @sections_slider_images = image_type.general_images
    end
    @image_types.where(:image_type => "index_box").each do |image_type|
      @index_box_images = image_type.general_images
    end
    @other_types = @image_types.where(:image_type => "slider")
    @pages = Admin::Page.all
    render :template => "#{$client_name}_custom_view/sections/index"
  end

  def main_others
    @categories = Admin::Category.all  
  end  

  def sub_others
    @categories = Admin::Category.all 
  end

  def check_user_login
    if current_user.present?
      render :json =>{ :exist => true}
    else
      render :json =>{ :exist => false}
    end
  end

  def show 
    @general_images = Admin::GeneralImage.all
    @pages = Admin::Page.all
    @admin_images = Admin::Image.all
    
    @categories = Admin::Category.all
    if params[:id].present?
      if params[:page].present?
        pages = params[:page]
        per_page = 16 * pages.to_i
        @section = Admin::Category.find(params[:q][:section_id])
      else
        per_page = 16
        @section = Admin::Category.find(params[:id]) 

      end
    else
      per_page = 16
      @section = Admin::Category.find(params[:q][:section_id])
    end
    if request.xhr?
      @brand = Admin::Brand.all
      @type = Admin::Type.all
      if @section.children.present?
        if params[:q][:s].present?
          @searchs = @section.products.where(:status => true,:visibility => true).joins(:inventories).search(params[:q])
        else 
          @searchs = @section.products.where(:status => true,:visibility => true).search(params[:q])
        end    
        @products = @searchs.result(distinct: true).where(:status => true,:visibility => true).includes(:types,:variants,:inventories)
        @products_pagination = @searchs.result(distinct: true).where(:status => true,:visibility => true).includes(:types,:variants,:inventories)
      else
        if params[:q][:s].present?
          @searchs = @section.products.where(:status => true,:visibility => true).joins(:inventories).search(params[:q])
        else 
          @searchs = @section.products.where(:status => true,:visibility => true).search(params[:q])
        end    
        @products = @searchs.result(distinct: true).where(:status => true,:visibility => true).includes(:types,:variants,:inventories)
        @products_pagination = @searchs.result(distinct: true).where(:status => true,:visibility => true).includes(:types,:variants,:inventories)
      end 
        render :template => "remote_content/search_result.js.erb"
    else
      @brand = Admin::Brand.all
      @type = Admin::Type.all
      if params[:q].present?
        if @section.children.present?
          if params[:q][:s].present?
            @searchs = @section.products.where(:status => true,:visibility => true).joins(:inventories).search(params[:q])
          else 
            @searchs = @section.products.where(:status => true,:visibility => true).search(params[:q])
          end    
          @products = @searchs.result(distinct: true).where(:status => true,:visibility => true).includes(:types,:variants,:inventories)
          @products_pagination = @searchs.result(distinct: true).where(:status => true,:visibility => true).includes(:types,:variants,:inventories)
        else
          if params[:q][:s].present?
            @searchs = @section.products.where(:status => true,:visibility => true).joins(:inventories).search(params[:q])
          else 
            @searchs = @section.products.where(:status => true,:visibility => true).search(params[:q])
          end    
          @products = @searchs.result(distinct: true).where(:status => true,:visibility => true).includes(:types,:variants,:inventories)
          @products_pagination = @searchs.result(distinct: true).where(:status => true,:visibility => true).includes(:types,:variants,:inventories)
        end 
      else
        unless @section.children.present? 
          @searchs = @section.products.where(:status => true,:visibility => true).search(params[:q])    
          @products = @searchs.result(distinct: true).includes(:types,:variants,:inventories)
        end 
      end
      render :template => "#{$client_name}_custom_view/sections/show"
    end 
    @searchs = @section.products.where(:status => true,:visibility => true).search(params[:q])    
    @products = @searchs.result(distinct: true).includes(:types,:variants,:inventories)
  end 

  
  # GET /sections/1/edit
  def showcase
    @image_types = Admin::ImageType.all
    @image_types.where(:image_type => "index_box").each do |image_type|
      @index_box_images = image_type.general_images
    end
    @wishlist  = Wishlist.new
    @categories = Admin::Category.all
    @product = Admin::Product.find(params[:productid])
    @products = Admin::Product.where(:status => true,:visibility => true)
    @related_products = @product.related_products(@section)
    @previous_product = @products.where('id < ?', params[:productid]).last
    @next_product = @products.where('id < ?', params[:productid]).first
    respond_to do |format|
      format.html { 
        @variant = @product.variants.where(:visible => true,:status => true).sort_by(&:sort_order).take(1).first
        @images =  @variant.images.sort_by(&:sort_order)
        @product_questions = @variant.product_questions.sort_by(&:question)
        @similar_products = @section.products.where(:status => true,:visibility => true)
        @other_variants = @product.variants.where('id != ?' , @variant.id).where(:visible => true,:status => true).sort_by(&:sort_order)
        if Admin::General.first.present?
          render :template => Admin::General.first.fixed_rate == true ? "#{$client_name}_custom_view/sections/showcase_with_fixed_rate" : "#{$client_name}_custom_view/sections/showcase"
        else
          render :template => "#{$client_name}_custom_view/sections/showcase"
        end
      }
      format.js {
        if params[:pin].present?
          session[:pincode] = params[:pin]
          @pincode = params[:pin] 
          @check_pincode= Admin::Pincode.where(:pincode => @pincode).first 
          @variant = Admin::Variant.find(params[:variant_id]).first
          @images =  @variant.images.sort_by(&:sort_order)
          @product_questions = @variant.product_questions.sort_by(&:question)
          @similar_products= @section.products.where(:status => true)
          if @check_pincode.present?
            @shipping= Admin::Product.shipping_cost(params[:pin], @product, @variant)
            if @shipping != ""
              @product_price= @variant.user_price(params[:pin])
              if @shipping.present?
                @shipping_status = @shipping.status
                @shipping_values = @shipping.cost
                if @shipping_status != false
                  @selling_price = Admin::Inventory.where(:variant_id => @variant.id).order('selling_price DESC').first.selling_price
                  @tax_cost = @product_price - (@shipping_values + @selling_price) 
                else
                end
              end
            end
          end
            render :template => "remote_content/get_rate.js.erb"
        else
          @variant = Admin::Variant.find(params[:variant_id][:id])
          @product = @variant.product
          @images =  @variant.images.sort_by(&:sort_order)
          @product_questions = @variant.product_questions.sort_by(&:question)
          @similar_products= @section.products.where(:status => true)
          @other_variants = @product.variants.where('id != ?' , @variant.id).where(:visible => true,:status => true).sort_by(&:sort_order)
          render :template => "remote_content/showcase.js.erb"
        end
      }
    end
  end

  def find_variant_shipping_cost
    @variant = Admin::Variant.find(params[:variant_id])
    if params[:pincode].present?
      @final_cost = @variant.find_variant_availability(params[:user][:id],params[:pincode][:pincode])
    else
      @final_cost = @variant.find_variant_availability(params[:user][:id])
    end
    render :template => "remote_content/variant_shipping_cost_show.js.erb"
  end

  
  def availability_shipping_cost_with_pincode(pincode)
    @pincode = Admin::ShippingSpecificPincode.where(:pincode => pincode).first
    if @pincode.present?
      return self.find_shipping_range(@shipping_country,true)
    else
      return "unavailable"
    end
  end

  private
    def set_section
      @section = Admin::Category.find(params[:section_id]) 
    end

    def section_params
      params[:section]
    end

    def check_status_visible
      @section = Admin::Category.find(params[:section_id]) or not_found
      @product= Admin::Product.find(params[:productid]) or not_found
      if (@product.visibility== false || @product.status== false)
        redirect_to section_path(@section), notice: "Product is not available for Purchase"
      end
    end

end