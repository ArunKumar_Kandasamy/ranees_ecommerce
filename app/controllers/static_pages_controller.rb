class StaticPagesController < ApplicationController

  before_action :sections
  before_action :set_section, only: [:index]
  
  def index
    $subscriber = Subscriber.new
    @products = Admin::Product.where(:status => true,:visibility => true)
    @offer_products = @products.where(:promote => "o").sort_by(&:promotional_sort)
    @latest_products = @products.where(:promote => "l").sort_by(&:promotional_sort)
    @trending_products = @products.where(:promote => "t").sort_by(&:promotional_sort)
    @image_types = Admin::ImageType.all
    @image_types.where(:image_type => "slider").each do |image_type|
       @slider_images = image_type.general_images
    end
    @image_types.where(:image_type => "index_box").each do |image_type|
       @index_box_images = image_type.general_images
    end
    @image_types.where(:image_type => "bxslider").each do |image_type|
       @bxsilder_images = image_type.general_images
    end
    @other_types = @image_types.where(:image_type => "slider")
    @pages = Admin::Page.all
     render :template => "static_pages/#{$client_name}_static_pages_view/index.html.erb"     
  end

  def session_clear
    session[:user_id]= nil
    session[:cart_id]= nil
    render :text => "session cleared"
  end

  def pages 
    @pages = Admin::Page.all
  end

  def pageshow
    @page = Admin::Page.find_by_page_name(params[:page_name])
    unless @page.present?
       redirect_to root_path
    end
  end

  def term_use
    render :template => "static_pages/#{$client_name}_static_pages_view/term_use.html.erb"    
  end

  def privacy
  end

  def products
    @products = Admin::Product.where(:status => true,:visibility => true)
    render :template => "static_pages/#{$client_name}_static_pages_view/products.html.erb"  
  end

  def benefit
    render :template => "static_pages/#{$client_name}_static_pages_view/benefit.html.erb"  
  end

  def recipe
    render :template => "static_pages/#{$client_name}_static_pages_view/recipe.html.erb"  
  end
 
  def about_us
    render :template => "static_pages/#{$client_name}_static_pages_view/about_us.html.erb"  
  end

  def policy
  end
  
  def help_desk
  end
  
  def contact_us
    render :template => "static_pages/#{$client_name}_static_pages_view/contact_us.html.erb"  
  end

  def ship_delivery
  end
  private
    # Use callbacks to share common setup or constraints between actions.
  def set_section
    
  end

end