class Admin::ConsignmentItemsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_consignment_item, only: [:destroy, :update,:edit]

  def create
    @warehouse_id = params[:admin_consignment_item][:warehouse_id]
    @order = Admin::Order.find(params[:admin_consignment_item][:order_id])
    if @order.check_order_status
      consignment = Admin::Consignment.where(:warehouse_id => params[:admin_consignment_item][:warehouse_id],:order_id => params[:admin_consignment_item][:order_id])
      if consignment.present?
        if consignment.first.conform == true
          new_consignment = Admin::Consignment.create(:warehouse_id => params[:admin_consignment_item][:warehouse_id],:order_id => params[:admin_consignment_item][:order_id])
        else
          new_consignment = consignment.first
        end
      else
        new_consignment = Admin::Consignment.create(:warehouse_id => params[:admin_consignment_item][:warehouse_id],:order_id => params[:admin_consignment_item][:order_id])
      end
      @order_item = Admin::OrderItem.find(params[:admin_consignment_item][:order_item_id])
      @consignment_item = Admin::ConsignmentItem.create(:qty => params[:admin_consignment_item][:qty], :variant_id => @order_item.variant_id, :per_qty_price => @order_item.per_qty_price, :consignment_id => new_consignment.id, :return_due => @order_item.return_due)
      Admin::OrderItemsConsignmentItems.create(:consignment_item_id => @consignment_item.id , :order_item_id => @order_item.id)
      @order.wh_status_update
      redirect_to admin_order_path(@order), notice: 'Consignment was successfully updated.'
    else
      redirect_to admin_order_path(@order), notice: 'Sorry consignment is not created.'
    end
  end 

  def edit
    if request.xhr?
      render :template => "remote_content/support_consignment_return_initiate.js.erb"
    end
  end

  def update
    @consignment_item.update(consignment_item_params)
    @order = Admin::Order.find(params[:admin_consignment_item][:order_id])
    @order.wh_status_update
    redirect_to admin_order_path(@consignment.order_id)
  end
  
  def destroy
    @warehouse = @consignment.warehouse
    @order = @consignment.order
    if @consignment_item.regulations.first.nil?
  	  @order_item = @consignment_item.order_items_consignment_items.first.order_item
      @order_item_consignment_item = Admin::OrderItemsConsignmentItems.where(:consignment_item_id => @consignment_item.id ,:order_item_id => @order_item.id).first
      @order_item_consignment_item.destroy
  	  if @consignment_item.destroy
        @order.wh_status_update
        if params[:assign_warehouse].present?
          redirect_to @order, notice: 'Consignment item was successfully destroyed.'
        else
          @package_task = @consignment.package_tasks.first 
    	  	if @consignment.present?
    	  	  redirect_to admin_warehouse_dashboard_package_task_task_consignment_show_path(@warehouse,@package_task,@consignment), notice: 'Consignment item was successfully destroyed.'
    	    else
    	      redirect_to admin_warehouse_dashboard_package_task_path(@warehouse,@package_task)
    	    end
        end
  	  end
  	else
      if params[:assign_warehouse].present?
        redirect_to @order, notice: 'Sorry you cannot delete this item.'
  	  else
        @package_task = @consignment.package_tasks.first 
        redirect_to admin_warehouse_dashboard_package_task_task_consignment_show_path(@warehouse,@package_task,@consignment), notice: 'Sorry you cannot delete this item.'
  	  end
    end
  end

  def consignment_item_params
    params.require(:admin_consignment_item).permit(:qty)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_consignment_item
      @consignment_item = Admin::ConsignmentItem.find(params[:id])
      @consignment = @consignment_item.consignment
    end
end
