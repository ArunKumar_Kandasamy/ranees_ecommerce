class Admin::WeightRangesController < ApplicationController
  before_action :set_admin_weight_range, only: [:show, :edit, :update, :destroy] 

  # GET /admin/weight_ranges
  # GET /admin/weight_ranges.json
  def index
    @admin_weight_ranges = Admin::WeightRange.all
    if request.xhr?
      @render_path = admin_weight_ranges_path
      @render_file = 'admin/weight_ranges/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/weight_ranges/index"
    end  
  end

  # GET /admin/weight_ranges/1
  # GET /admin/weight_ranges/1.json
  def show
  end

  # GET /admin/weight_ranges/new
  def new
    @admin_weight_range = Admin::WeightRange.new
    if request.xhr?
      @render_path = admin_weight_ranges_path
      @render_file = 'admin/weight_ranges/new'
      render "remote_content/common_modal_render.js.erb"          
    else
      render :template => "admin/weight_ranges/new"
    end 
  end

  # GET /admin/weight_ranges/1/edit
  def edit
    if request.xhr?
      @render_path = admin_weight_ranges_path
      @render_file = 'admin/weight_ranges/edit'
      render "remote_content/common_modal_render.js.erb"          
    else
      render :template => "admin/weight_ranges/edit"
    end  
  end

  # POST /admin/weight_ranges
  # POST /admin/weight_ranges.json
  def create
    @admin_weight_range = Admin::WeightRange.new(admin_weight_range_params)
    if @admin_weight_range.save
      @admin_weight_ranges = Admin::WeightRange.all
      flash[:notice] = 'WeightRange was successfully created.'
    else
      flash[:alert] = 'WeightRange cannot be created'
    end
    if request.xhr?
      @render_path = admin_weight_ranges_path
      @render_file = 'admin/weight_ranges/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/weight_ranges/index"
    end 
  end

  # PATCH/PUT /admin/weight_ranges/1
  # PATCH/PUT /admin/weight_ranges/1.json
  def update
    if @admin_weight_range.update(admin_weight_range_params)
      @admin_weight_ranges = Admin::WeightRange.all
      flash[:notice] = 'WeightRange was successfully updated.'
    else
      flash[:alert] = 'WeightRange cannot be updated'
    end
    if request.xhr?
      @render_path = admin_weight_ranges_path
      @render_file = 'admin/weight_ranges/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/weight_ranges/index"
    end 
  end

  # DELETE /admin/weight_ranges/1
  # DELETE /admin/weight_ranges/1.json
  def destroy
    @admin_weight_range.destroy
    @admin_weight_ranges = Admin::WeightRange.all
    flash[:alert] = 'WeightRange successfully deleted'
    @render_path = admin_weight_ranges_path
    @render_file = 'admin/weight_ranges/index'
    render "remote_content/common_render.js.erb" 
  end

  def check_weight_range_exist
    brand = params[:fieldValue]
    validate_ajax_except = params[:validate_ajax_except]
    if validate_ajax_except.present?
      @admin_weight_range = Admin::WeightRange.where("range = ?", brand).where("id != ?", validate_ajax_except).first
    else
      @admin_weight_range = Admin::WeightRange.where("range = ?", brand).first
    end
    if@admin_weight_range.present?
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => false } }
      end
    else
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => true } }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_weight_range
      @admin_weight_range = Admin::WeightRange.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_weight_range_params
      params.require(:admin_weight_range).permit(:range)
    end
end
