class Admin::CartsController < ApplicationController 
  before_filter :authenticate_user!
  before_action :authorize

  def index
    @param = params[:q]   
    @search = Cart.all.search(params[:q])
    @carts = @search.result
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_carts_path
      @render_file = 'admin/carts/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/carts/index"
    end
  end

  def show
  	@admin_cart = Cart.find(params[:id])
    @total_price= 0.0
    @admin_cart.cart_items.each do | cart_item|
      if cart_item.price.present?
        @total_price += cart_item.price
      end
    end
    if request.xhr?
      @render_path = admin_cart_path(@admin_cart)
      @render_file = 'admin/carts/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/carts/show"
    end
  end

  def edit
  end
end
