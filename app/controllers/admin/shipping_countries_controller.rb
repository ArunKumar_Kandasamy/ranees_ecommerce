class Admin::ShippingCountriesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_shipping_country, only: [:show, :edit, :update, :destroy]
  before_action :find_admin_shipping_plan

  def index
    @admin_shipping_countries = Admin::ShippingCountry.all
  end

  def show 
    respond_to do |format|
      format.html{
        @admin_shipping_countries = @admin_shipping_plan.countries
        @admin_shipping_country = Admin::ShippingCountry.new
        @admin_shipping_state = Admin::ShippingState.new
      }
      format.js {
        @filter_states = @admin_shipping_country.country.states.map{|x| x.id }
        @admin_shipping_states = @admin_shipping_country.shipping_states
        @filter_admin_shipping_states = @admin_shipping_country.shipping_states.map{|x| x.state_id }
        @filter_states = @filter_states - @filter_admin_shipping_states
        @filter_results = @admin_shipping_country.country.states.find(@filter_states)
        @admin_shipping_state = Admin::ShippingState.new
        render :template => "remote_content/shipping_plans.js.erb"
      }
    end
  end
 
  def new
    @admin_shipping_country = Admin::ShippingCountry.new
  end

  def edit
  end

  def create
    @admin_shipping_countries = @admin_shipping_plan.shipping_countries
    @admin_shipping_country = @admin_shipping_plan.shipping_countries.build(admin_shipping_country_params)
    if @admin_shipping_country.save
      respond_to do |format|
        format.html{
          redirect_to admin_shipping_plan_path(@admin_shipping_plan), notice: 'Shipping country was successfully created.'
        }
        format.js {
          flash[:notice] = "Shipping country was successfully created."
          render :template => "remote_content/shipping_countries_success.js.erb"
          flash.discard
        }
      end
    else
      render 'new'
    end
  end

  def update
    if @admin_shipping_country.update(admin_shipping_country_params)
      redirect_to @admin_shipping_country, notice: 'Shipping country was successfully updated.'
    else
      render 'edit'
    end
  end

  def destroy
    if @admin_shipping_country.destroy
      redirect_to admin_shipping_plan_path(@admin_shipping_plan), notice: 'Shipping country was successfully destroyed.' 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_shipping_country
      @admin_shipping_country = Admin::ShippingCountry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_shipping_country_params
      params.require(:admin_shipping_country).permit(:country_id)
    end

    def find_admin_shipping_plan
      @admin_shipping_plan = Admin::ShippingPlan.find(params[:shipping_plan_id])
    end
end
