class Admin::ImageTypesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_admin_image_type, only: [:show, :edit, :update, :destroy]

  # GET /admin/image_types
  # GET /admin/image_types.json
  def index
    @admin_image_types = Admin::ImageType.all
    if request.xhr?
      @render_path = admin_image_types_path
      @render_file = 'admin/image_types/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/image_types/index"
    end
  end
  

  def check_image_type_exist
    image_type = params[:fieldValue]
    validate_ajax_except = params[:validate_ajax_except]
    if validate_ajax_except.present?
      @admin_image_type = Admin::ImageType.where("lower(name) = ?", image_type.downcase).where("id != ?", validate_ajax_except).first
    else
      @admin_image_type = Admin::ImageType.where("lower(name) = ?", image_type.downcase).first
    end
    if @admin_image_type.present?
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => false } }
      end
    else
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => true } }
      end
    end
  end

  def check_image_name_exist
    image_type = params[:fieldValue]
    validate_ajax_except = params[:validate_ajax_except]
    if validate_ajax_except.present?  
      @admin_image_type = Admin::ImageType.where("lower(image_type) = ?", image_type.downcase).where("id != ?", validate_ajax_except).first
    else
      @admin_image_type = Admin::ImageType.where("lower(image_type) = ?", image_type.downcase).first
    end
    if @admin_image_type.present?
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => false } }
      end
    else
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => true } }
      end
    end
  end

  # GET /admin/image_types/1
  # GET /admin/image_types/1.json
  def show
    @admin_categories = Admin::Category.all
    @cat_id = Array.new
    @admin_type_ids= @admin_image_type.general_images.where(:image_type_id => @admin_image_type.id)
    @admin_cat_name=@admin_image_type.general_images.each do | image |
      if image.category.present? 
        @cat_id << image.category.name
      end 
    end
    @cat_id.uniq!
    if request.xhr?
      @render_path = admin_image_type_path(@admin_image_type)
      @render_file = 'admin/image_types/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/image_types/show"
    end
  end

  # GET /admin/image_types/new
  def new
    @admin_image_type = Admin::ImageType.new
    respond_to do |format|
      format.html{   
      }
      format.js{
        render :template => "remote_content/image_type_new.js.erb"  
      }
    end
  end

  # GET /admin/image_types/1/edit
  def edit
    respond_to do |format|
      format.html      
      format.js{
        render :template => "remote_content/image_type_edit.js.erb"
      }
    end  
  end

  # POST /admin/image_types
  # POST /admin/image_types.json
  def create
    @admin_image_type = Admin::ImageType.new(admin_image_type_params)
    if @admin_image_type.save
      @admin_image_types = Admin::ImageType.all
      flash[:notice] = 'Image type was successfully created.'
      @render_path = admin_image_types_path
      @render_file = 'admin/image_types/index'
      render "remote_content/common_render.js.erb"     
    else
      render :template => "remote_content/image_type_new.js.erb" 
    end
  end

  # PATCH/PUT /admin/image_types/1
  # PATCH/PUT /admin/image_types/1.json
  def update
    if @admin_image_type.update(admin_image_type_params)
      @admin_image_types = Admin::ImageType.all
      flash[:alert] = 'Image type was successfully updated.'
      @render_path = admin_image_type_path
      @render_file = 'admin/image_types/index'
      render "remote_content/common_render.js.erb"     
    else
      render :template => "remote_content/image_type_edit.js.erb" 
    end
  end

  # DELETE /admin/image_types/1
  # DELETE /admin/image_types/1.json
  def destroy
    @admin_image_type.destroy
    respond_to do |format|
      format.html { redirect_to admin_image_types_url, notice: 'Image type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_image_type
      @admin_image_type = Admin::ImageType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_image_type_params
      params.require(:admin_image_type).permit(:name, :image_type, :height, :width, :is_category, :is_video)
    end
end
