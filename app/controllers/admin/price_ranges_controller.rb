class Admin::PriceRangesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_price_range, only: [:show, :edit, :update, :destroy]

  # GET /admin/price_ranges
  # GET /admin/price_ranges.json 
  def index
    @admin_price_ranges = Admin::PriceRange.all
    if request.xhr?
      @render_path = admin_price_ranges_path
      @render_file = 'admin/price_ranges/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/price_ranges/index"
    end  
  end

  # GET /admin/price_ranges/1
  # GET /admin/price_ranges/1.json
  def show
  end

  # GET /admin/price_ranges/new
  def new
    @admin_price_range = Admin::PriceRange.new
    if request.xhr?
      @render_path = admin_price_ranges_path
      @render_file = 'admin/price_ranges/new'
      render "remote_content/common_modal_render.js.erb"          
    else
      render :template => "admin/price_ranges/new"
    end 
  end

  # GET /admin/price_ranges/1/edit
  def edit
    if request.xhr?
      @render_path = admin_price_ranges_path
      @render_file = 'admin/price_ranges/edit'
      render "remote_content/common_modal_render.js.erb"          
    else
      render :template => "admin/price_ranges/edit"
    end 
  end

  # POST /admin/price_ranges
  # POST /admin/price_ranges.json
  def create
    @admin_price_range = Admin::PriceRange.new(admin_price_range_params)

    if @admin_price_range.save
      @admin_price_ranges = Admin::PriceRange.all
      flash[:notice] = 'PriceRange was successfully created.'
    else
      flash[:alert] = 'PriceRange cannot be created'
    end
    if request.xhr?
      @render_path = admin_price_ranges_path
      @render_file = 'admin/price_ranges/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/price_ranges/index"
    end 
  end

  # PATCH/PUT /admin/price_ranges/1
  # PATCH/PUT /admin/price_ranges/1.json
  def update
    if @admin_price_range.update(admin_price_range_params)
      @admin_price_ranges = Admin::PriceRange.all
      flash[:notice] = 'PriceRange was successfully updated.'
    else
      flash[:alert] = 'PriceRange cannot be updated'
    end
    if request.xhr?
      @render_path = admin_price_ranges_path
      @render_file = 'admin/price_ranges/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/price_ranges/index"
    end 
  end

  # DELETE /admin/price_ranges/1
  # DELETE /admin/price_ranges/1.json
  def destroy
    @admin_price_range.destroy
    @admin_price_ranges = Admin::PriceRange.all
    flash[:alert] = 'PriceRange successfully deleted'
    @render_path = admin_price_ranges_path
    @render_file = 'admin/price_ranges/index'
    render "remote_content/common_render.js.erb" 
  end

  def check_price_range_exist
    brand = params[:fieldValue]
    validate_ajax_except = params[:validate_ajax_except]
    if validate_ajax_except.present?
      @admin_price_range = Admin::PriceRange.where("range = ?", brand).where("id != ?", validate_ajax_except).first
    else
      @admin_price_range = Admin::PriceRange.where("range = ?", brand).first
    end
    if@admin_price_range.present?
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => false } }
      end
    else
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => true } }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_price_range
      @admin_price_range = Admin::PriceRange.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_price_range_params
      params.require(:admin_price_range).permit(:range)
    end
end
