class Admin::ProductsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :authorize
  before_action :check_for_inventory, only: :destroy
  after_action :category_activation, only: [ :update]
  after_action :check_for_activation, only: [ :update]
 

  def index   
    @param = params[:q]   
    if request.xhr?  
      Admin::Product.joins(:categories)
      if params[:products_tab] == "invisible" || params[:invisible_page].present?
        @active_visible_false =  Admin::Product.where(:visibility => false).search(params[:q]).result.page(params[:invisible_page])
        render :template => "remote_content/invisible_products_search.js.erb"
      elsif params[:products_tab] == "visible" || params[:visible_page].present?
        @active_visible_true =  Admin::Product.where(:visibility => true).search(params[:q]).result.page(params[:visible_page])
        render :template => "remote_content/visible_products_search.js.erb"
      elsif params[:products_tab] == "active" || params[:active_page].present?
        @active_products =  Admin::Product.where(:status => true).search(params[:q]).result.page(params[:active_page])
        render :template => "remote_content/active_products_search.js.erb"
      elsif params[:products_tab] == "inactive" || params[:inactive_page].present?
        @active_products_false = Admin::Product.where(:status => false).search(params[:q]).result.page(params[:inactive_page])
        render :template => "remote_content/inactive_products_search.js.erb"
      elsif params[:products_tab] == "category" 
        @admin_categories= Admin::Category.all 
        @admin_parents = @admin_categories.roots
        render :template => "remote_content/product_category_search.js.erb"
      elsif params[:products_tab] == "promotional"
        @admin_categories= Admin::Category.all 
        @admin_parents = @admin_categories.roots
        render :template => "remote_content/promotional_product_search.js.erb"
      elsif params[:all_page].present?
        @search = Admin::Product.all.uniq.page(params[:all_page]).search(params[:q])
        @products = @search.result
        render :template => "remote_content/all_product_search.js.erb"
      elsif params[:q].present?  
        @search = Admin::Product.uniq.joins(:inventories).page(params[:all_page]).search(params[:q])
        @products = @search.result.includes(:types,:categories,:shipping_plans,:tax_plans,:warehouses,:vendors)
        @search.build_condition
        @search.build_sort if @search.sorts.empty?
        @param = params[:q]       
        respond_to do |format|          
          format.html{            
          } 
          format.js {            
            render :template => "remote_content/product_complete_search.js.erb"
          }          
        end
      else
        @search = Admin::Product.uniq.page(params[:all_page]).search(params[:q])
        @products = @search.result
        @search.build_condition if @search.conditions.empty?
        @search.build_sort if @search.sorts.empty?
        @render_path = admin_products_path
        @render_file = 'admin/products/index'
        render "remote_content/common_render.js.erb"
      end
    else  
      Admin::Product.joins(:categories) 
      @search = Admin::Product.uniq.page(params[:all_page]).search(params[:q])
      @products = @search.result
      @search.build_condition if @search.conditions.empty?
      @search.build_sort if @search.sorts.empty?
    end
  end 


  def cod_activate
    @product= Admin::Product.find(params[:product_id])
    if @product.cod == false 
      @product.cod= true
      @product.save
      redirect_to @product, notice: "Cod Activated"
    else
      @product.cod= false
      @product.save
      redirect_to @product, notice: "Cod De-activated"
    end
  end

  def show
    if params[:pin].present? 
      @product_price= Admin::Product.user_price(params[:pin], @product)
    end
    @price= @product_price
    @list_child_categories = Admin::Category.order(:name)
    @general = Admin::General.first
    if @general.present?
      @n = @general.category_level
    else
      @n = 3
    end
    @list_children_categories = @list_child_categories.reject { |h| h.ancestors.size != @n-1 }
    @product_shipping_plan= Admin::ProductShippingPlan.new 
    @products_tax_plans= Admin::ProductsTaxPlans.new
    @admin_inventory = Admin::Inventory.new
    @admin_category_product= Admin::CategoriesProducts.new
    @variants = @product.variants.sort_by(&:sort_order)
    @types = @product.types
   
    @availability_plans = Admin::AvailabilityPlan.all
    @trace_availability_plan = @availability_plans.reject { |h| h.id ==  @product.availability_plan_id }

    @availability = Admin::Category.all
    @product_category = @product.categories
    @trace_product_category = @list_children_categories - @product_category             
    @product_taxes = Admin::TaxPlan.all
    @product_tax = @product.tax_plans
    @trace_product_tax = @product_taxes - @product_tax
    if request.xhr?
      @render_path = admin_product_path(@product)
      @render_file = 'admin/products/show'
      render "remote_content/common_render.js.erb"
    else
      render :template => "admin/products/show"    
    end 
  end

  def new 
    @product = Admin::Product.new
    types = @product.product_types.build
    #variant = @product.variants.build
    #image = variant.images.build
    #product_properties= variant.product_properties.build
    if request.xhr?
      @render_path = new_admin_product_path
      @render_file = 'admin/products/new'
      render "remote_content/common_render.js.erb"        
    else
      render :template => "admin/products/new"
    end
  end

  
  def edit
    if request.xhr?
      if params[:for_configure].present?
        render "remote_content/product_for_configure.js.erb"  
      elsif params[:for_voilate].present?      
        render "remote_content/product_for_voilate.js.erb" 
      else
        @render_path = edit_admin_product_path(@product)
        @render_file = 'admin/products/edit'
        render "remote_content/common_render.js.erb"    
      end    
    else
      render :template => "admin/products/edit"
    end
  end

  def create
    @product = Admin::Product.new(product_params)
    if @product.save
       redirect_to @product, notice: 'Product was successfully created.' 
    else
      render :template => "admin/products/new"
    end
  end

  def update
    if @product.update(product_params)
      if request.xhr?
        if params[:admin_product][:visibility].present?
          render "remote_content/product_for_configure_show.js.erb"
        elsif params[:admin_product][:cod].present?
          render "remote_content/product_for_voilate_show.js.erb"
        elsif params[:admin_product][:availability_plan_id].present?
          @availability_plans = Admin::AvailabilityPlan.where(:status => true)
          @trace_availability_plan = @availability_plans.reject { |h| h.id ==  @product.availability_plan_id }
          render "remote_content/availability_plan_show.js.erb"
        else
          render :json => Admin::ProductQuestion.tokens(params[:q])
        end
      else
        respond_to do |format|
          format.html{
            redirect_to @product, alert: 'Product was successfully updated.'
          }
          format.json { 
            render :json => Admin::ProductQuestion.tokens(params[:q])
          }
        end
      end
    else
      render 'edit'
    end
  end

  def promotion_update
    if request.xhr?
      id = params[:product_id]
      respond_to do |format|
        format.js{
          @product=Admin::Product.where(:id => id).first
          @promotion= @product.promote
          @sort_maximum= Admin::Product.where(:promote => @promotion).maximum("promotional_sort")
          @sort_maximum+=1
          Admin::Product.where(id: id).update_all(:promotional_sort => @sort_maximum)
          @admin_categories= Admin::Category.all
          @admin_parents = @admin_categories.roots 
          render :template => "remote_content/update_promote.js.erb"
        }
      end
    else
    end
  end

  def sort
    params[:admin_product].each_with_index do |id, index|
      Admin::Product.where(id: id).update_all(sort_order: index+1)
    end
    render nothing: true
  end

  def promotion_sort
    params[:admin_product].each_with_index do |id, index|
      Admin::Product.where(id: id).update_all(promotional_sort: index+1)
    end
    render nothing: true
  end

  def destroy
    if @product.destroy
      redirect_to admin_products_path, error: 'Product was successfully destroyed.' 
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Admin::Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:admin_product).permit(:name, :description, :tax_violate, :status,:visibility, :promote, :ship_invent, :unavail_purchase, :brand_id, :availability_plan_id , :type_tokens, :return_due, :cod, :free_shipping, :based_on, variants_attributes:[:id, :product_id, :strike_rate, :_destroy, :status, :sku, :bar_code, :visible, :name, :ready_to_wear, images_attributes:[:id, :alt_text, :_destroy, :avatar, :variant_id], product_properties_attributes:[:id, :variant_id, :product_id, :_destroy, :product_question_tokens, :answer]])
    end

    def check_for_inventory
      if @product.inventories.present?
        if @product.bal_qty_check.to_i > 0
          redirect_to @product, notice: 'You are not allowed to perform this action'
        end
      end
    end

    def check_for_activation
      @product= Admin::Product.find(params[:id])
      if @product.activate_product
        @product.status= true
        @product.save
      else
        @product.status= false
        @product.save
      end
    end

    def category_activation
      @product= Admin::Product.find(params[:id])
      @admin_category= @product.categories.first
      if @admin_category.present?
        @admin_category.activate_related_category
      end
    end
end