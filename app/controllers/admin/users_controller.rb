class Admin::UsersController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authorize
  
  def account
    @profile = Profile.where(:user_id => current_user.id)
  end

  def orders
    @orders= Admin::Order.where(:user_id => params[:user_id])
  end

  def index 
    @param = params[:q]
    @search = User.all.search(params[:q])
    @permission = Admin::Permission.new
    @role= Admin::Role.order(:name).where("name != ?","pr")
    @roles= @role.each do |role|
      if role.name == "sa"
        role.name= t :sa
      elsif(role.name== "u")
        role.name= t :u
      else
        role.name= role.name
      end
    end
    @users = @search.result.includes(:addresses, :profile) 
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_users_path
      @render_file = 'admin/users/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/users/index"
    end
  end

  def show
    if request.xhr?
      @failure_transactions= @user.failure_transactions
      @render_path = admin_user_path(@user)
      @render_file = 'admin/users/show'
      render "remote_content/common_render.js.erb"          
    else
      @failure_transactions= @user.failure_transactions
      render :template => "admin/users/show"
    end
  end

  def new
    @user = User.new
  end

  def edit
    if request.xhr?
      @render_path = edit_admin_user_path(@user)
      @render_file = 'admin/users/edit'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/users/edit"
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to @user, notice: 'User was successfully created.'
    else
      render 'new'
    end
  end

  def update
    if @user.update(user_params)
      flash['alert'] = 'User was successfully updated.'
      @render_path = admin_user_path(@user)
      @render_file = 'admin/users/show'
      render "remote_content/common_render.js.erb"          
    else
      @render_path = edit_admin_user_path(@user)
      @render_file = 'admin/users/edit'
      render :template => "admin/users/edit"
    end
  end

  def destroy
    if @user.destroy
      redirect_to users_url, notice: 'User was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :remember_me)
    end
end