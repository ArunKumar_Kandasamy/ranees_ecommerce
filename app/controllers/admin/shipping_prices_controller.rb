class Admin::ShippingPricesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_shipping_price, only: [:show, :edit, :update, :destroy]

  # GET /admin/shipping_prices
  # GET /admin/shipping_prices.json
  def index
    @admin_shipping_prices = Admin::ShippingPrice.all
  end

  # GET /admin/shipping_prices/1
  # GET /admin/shipping_prices/1.json
  def show
  end

  # GET /admin/shipping_prices/new
  def new
    @admin_shipping_price = Admin::ShippingPrice.new
  end

  # GET /admin/shipping_prices/1/edit
  def edit
  end

  # POST /admin/shipping_prices
  # POST /admin/shipping_prices.json
  def create
    @admin_shipping_price = Admin::ShippingPrice.new(admin_shipping_price_params)

    respond_to do |format|
      if @admin_shipping_price.save
        format.html { redirect_to @admin_shipping_price, notice: 'Shipping price was successfully created.' }
        format.json { render :show, status: :created, location: @admin_shipping_price }
      else
        format.html { render :new }
        format.json { render json: @admin_shipping_price.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/shipping_prices/1
  # PATCH/PUT /admin/shipping_prices/1.json
  def update
    respond_to do |format|
      if @admin_shipping_price.update(admin_shipping_price_params)
        format.html { redirect_to @admin_shipping_price, notice: 'Shipping price was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_shipping_price }
      else
        format.html { render :edit }
        format.json { render json: @admin_shipping_price.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/shipping_prices/1
  # DELETE /admin/shipping_prices/1.json
  def destroy
    @admin_shipping_price.destroy
    respond_to do |format|
      format.html { redirect_to admin_shipping_prices_url, notice: 'Shipping price was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_shipping_price
      @admin_shipping_price = Admin::ShippingPrice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_shipping_price_params
      params.require(:admin_shipping_price).permit(:range, :cost, :shipping_policy_id, :shipping_specific_country_id, :shipping_specific_pincode_id)
    end
end
