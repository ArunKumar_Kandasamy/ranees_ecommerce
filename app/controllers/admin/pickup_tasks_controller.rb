class Admin::PickupTasksController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_pickup_task, only: [:show, :edit, :update, :destroy]
  before_action :set_warehouse

  def index
    @param = params[:q]
    @search = @warehouse.pickup_tasks.search(params[:q])
    @admin_pickup_tasks = @search.result.includes(:pickup_consignments)      
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr? 
      @render_path = admin_warehouse_dashboard_pickup_tasks_path(@warehouse)
      @render_file = 'admin/pickup_tasks/index'
      render "remote_content/common_render.js.erb"          
    else 
      render :template => "admin/pickup_tasks/index"
    end  
  end


  def show 
    if request.xhr?
      @render_path = admin_warehouse_dashboard_pickup_task_path(@warehouse, @admin_pickup_task)
      @render_file = 'admin/pickup_tasks/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/pickup_tasks/show"
    end  
  end

  def new
    @admin_pickup_task = Admin::PickupTask.new
  end

  def edit 
  end

  def create
    @status = false
    @new_task_hash = params[:admin_pickup_task]
    @new_task_hash[:pickup_tasks_pickup_consignments_attributes].each do |new_task_hash|
      @pickup_consignment = Admin::PickupConsignment.find(new_task_hash[1][:_pickup_consignment_id])
      if @pickup_consignment.pickup_tasks.first.nil?                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
         @status = true
      end
    end
    if @status == true
      @pickup_task = Admin::PickupTask.create(:warehouse_id => @warehouse.id) 
      @pickup_task.save
      @new_task_hash[:pickup_tasks_pickup_consignments_attributes].each do |new_task_hash|
        if @pickup_consignment.pickup_tasks.first.nil?                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
           Admin::PickupTasksPickupConsignments.create(:pickup_consignment_id => new_task_hash[1][:_pickup_consignment_id] , :pickup_task_id => @pickup_task.id)
        end
      end
      @admin_pickup_task = Admin::PickupTask.new(admin_pickup_task_params)
      redirect_to admin_warehouse_dashboard_pickup_task_path(@warehouse,@pickup_task), notice: 'Pickup task was successfully created.' 
    else
      redirect_to admin_warehouse_dashboard_consignments_path(@warehouse), notice: 'Sorry Pickup task failed.' 
    end
  end

  def update
    respond_to do |format|
      if @admin_pickup_task.update(admin_pickup_task_params)
        format.html { redirect_to @admin_pickup_task, notice: 'Pickup task was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_pickup_task }
      else
        format.html { render :edit }
        format.json { render json: @admin_pickup_task.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @admin_pickup_task.destroy
    respond_to do |format|
      format.html { redirect_to admin_pickup_tasks_url, notice: 'Pickup task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_warehouse
      @warehouse = Admin::Warehouse.find(params[:warehouse_dashboard_id])
    end

    def set_admin_pickup_task
      @admin_pickup_task = Admin::PickupTask.find(params[:id])
    end

    def admin_pickup_task_params
      params.require(:admin_pickup_task).permit(:task_number, :warehouse_id, pickup_tasks_pickup_consignments_attributes:[:pickup_consignment_id ,:_destroy],pickup_consignments_attributes:[:id ,:consignment_id])
    end
end
