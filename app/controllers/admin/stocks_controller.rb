class Admin::StocksController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  
  before_action :set_warehouse
  before_action :set_stock, only: [:show, :edit, :update]

  def index 
    @param = params[:q]
    if request.xhr?       
      @search = @warehouse.products.page(params[:page]).search(params[:q])
      @warehouse_products = @search.result.includes(:types,:categories,:tax_plans,:warehouses,:vendors)
      @active_visible_true = @warehouse_products.where(:visibility =>  true).page(params[:page])
      @active_visible_false = @warehouse_products.where(:visibility => false).page(params[:page])
      @active_products = @warehouse_products.where(:status => true).page(params[:page])
      @active_products_false = @warehouse_products.where(:status => false).page(params[:page])
      @search.build_condition
      @search.build_sort if @search.sorts.empty?        
      respond_to do |format|
        format.html{
        }
        format.js {
          render :template => "admin/stocks/wh_stock_search.js.erb"
        }
      end      
    else      
      @search = @warehouse.products.page(params[:page]).search(params[:q])
      @warehouse_products = @search.result.includes(:types,:categories,:tax_plans,:warehouses,:vendors)
      @active_visible_true = @warehouse_products.where(:visibility =>  true).page(params[:page])
      @active_visible_false = @warehouse_products.where(:visibility => false).page(params[:page])
      @active_products = @warehouse_products.where(:status => true).page(params[:page])
      @active_products_false = @warehouse_products.where(:status => false).page(params[:page])
      @search.build_condition
      @search.build_sort if @search.sorts.empty?
    end
  end

  def edit
  end

  def show
    @variant = @stock.variants    
  end

  def update
    if @stock.update(product_params)
      respond_to do |format|
        format.html{
          redirect_to admin_warehouse_dashboard_stocks_path(@warehouse.id), notice: 'Product was successfully updated.'
        }
        format.json { 
          render :json => Admin::ProductQuestion.tokens(params[:q])
        }
      end
    else
      render 'edit'
    end
  end

  def new
    @stock = Admin::Product.new
  end

  def delete
  end

  def create
  end

  private
    def set_stock
      @stock = Admin::Product.find(params[:id])
    end
  
    def set_warehouse
      @warehouse = Admin::Warehouse.find(params[:warehouse_dashboard_id])
    end

    def product_params
      params.require(:admin_product).permit(:name, :description, :status,:visibility, :promote, :ship_invent, :unavail_purchase, :brand_tokens , :type_tokens, variants_attributes:[:id, :product_id, :_destroy, :status, :sku, :bar_code, :visible, :name, images_attributes:[:id, :alt_text, :_destroy, :avatar, :variant_id], product_properties_attributes:[:id, :variant_id, :product_id, :_destroy, :product_question_tokens, :answer]])
    end

end
