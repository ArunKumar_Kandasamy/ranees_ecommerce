class Admin::StatesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_country
  before_action :set_state, only: [:show, :edit, :update, :destroy]
  before_action :authorize

  def index
    @search =  @country.states.search(params[:q])
    @states = @search.result
    if request.xhr?
      @render_path = admin_country_states_path(@country)
      @render_file = 'admin/states/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/states/index"
    end
  end

  def import
    Admin::State.import(params[:file])
    redirect_to admin_country_path(@country), notice: "States imported."
  end

  def show
    @district_cities = @state.district_cities
    if request.xhr?
      @render_path = admin_country_state_path(@country,@state)
      @render_file = 'admin/states/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/states/show"
    end
  end

  def new
    @state = Admin::State.new
    if request.xhr?
      @render_path = new_admin_country_state_path(@country)
      @render_file = 'admin/states/new'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/states/new"
    end
  end

  def edit
    if request.xhr?
      @render_path = edit_admin_country_state_path(@country,@state)
      @render_file = 'admin/states/edit'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/states/edit"
    end
  end 

  def create
    @state = @country.states.build(state_params)
    if @state.save
      flash[:alert] = 'State was successfully created.'
      @render_path = admin_country_state_path(@country,@state)
      @render_file = 'admin/states/show'
      render "remote_content/common_render.js.erb"
    else
      @state = Admin::State.new
      render :template => "admin/states/new"
    end
  end

  def update
    if @state.update(state_params)
      flash[:alert] = 'State was successfully updated.'
      @render_path = admin_country_states_path(@country)
      @render_file = 'admin/states/index'
      render "remote_content/common_render.js.erb"
    else
      render :template => "admin/states/edit"
    end
  end

  def destroy
    @state.destroy
    flash[:alert] = 'State was successfully destroyed.'
    @render_path = admin_country_states_path(@country)
    @render_file = 'admin/states/index'
    render "remote_content/common_render.js.erb"
  end

  private
    def set_state
      @state = Admin::State.find(params[:id])
    end

    def set_country
      @country = Admin::Country.find(params[:country_id])
    end

    def state_params
      params.require(:admin_state).permit(:country_id, :name , :state_code)
    end
end