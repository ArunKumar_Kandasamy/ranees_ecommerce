class Admin::PincodesController < ApplicationController 
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_country_state_district_city
  before_action :set_pincode, only: [:show, :edit, :update, :destroy]

  def index
    @pincodes = @district_city.pincodes 
    if request.xhr?
      @render_path = admin_country_state_district_city_pincodes_path(@country, @state,@district_city)
      @render_file = 'admin/pincodes/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/pincodes/index"
    end
  end

  def show
    if request.xhr?
      @render_path = admin_country_state_district_city_pincode_path(@country, @state,@district_city)
      @render_file = 'admin/pincodes/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/district_cities/show"
    end
  end

  def new
    @pincode = Admin::Pincode.new
    if request.xhr?
      @render_path = new_admin_country_state_district_city_pincode_path(@country, @state,@district_city)
      @render_file = 'admin/pincodes/new'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/district_cities/new"
    end
  end 

  def edit
    if request.xhr?
      @render_path = edit_admin_country_state_district_city_pincode_path(@country, @state, @district_city,@district_city,@pincode)
      @render_file = 'admin/pincodes/edit'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/pincodes/edit"
    end
  end

  def create
    @pincode = @district_city.pincodes.build(pincode_params)
    if @pincode.save
      @render_path = admin_country_state_district_city_pincode_path(@country, @state, @district_city, @pincode)
      @render_file = 'admin/pincodes/show'
      render "remote_content/common_render.js.erb"          
    else
      @pincode = Admin::Pincode.new
      render :template => "admin/pincodes/new"
    end
  end

  def update
    if @pincode.update(pincode_params)
      flash[:alert] = 'Pincode was successfully updated.'
      @render_path = admin_country_state_district_city_path(@country, @state, @district_city)
      @render_file = 'admin/district_cities/show'
      render "remote_content/common_render.js.erb"              
    else    
      @pincode = Admin::Pincode.new
      render :template => "admin/pincodes/edit"
    end
  end

  def destroy
    @pincode.destroy
    flash[:alert] = 'Pincode was successfully destroyed.'
    @render_path = admin_country_state_district_city_pincodes_path(@country, @state, @district_city)
    @render_file = 'admin/district_cities/show'
    render "remote_content/common_render.js.erb"          
  end

  private
    def set_pincode
      @pincode = Admin::Pincode.find(params[:id])
    end

    def pincode_params
      params.require(:admin_pincode).permit(:district_city_id, :pincode)
    end

    def set_country_state_district_city
      @country = Admin::Country.find(params[:country_id])
      @state = Admin::State.find(params[:state_id])
      @district_city = Admin::DistrictCity.find(params[:district_city_id])
    end
end