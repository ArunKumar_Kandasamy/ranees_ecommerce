class Admin::BrandsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_brand, only: [:show, :edit, :update, :destroy]
  before_action :authorize

  def index  
    @brand = Admin::Brand.new 
    if request.xhr?
      @brands =Admin::Brand.all
      @render_path = admin_brands_path
      @render_file = 'admin/brands/index'
      render "remote_content/common_render.js.erb"          
    else
      @brands =Admin::Brand.all
      render :template => "admin/brands/index"
    end
  end

  def check_brand_name_exist
    brand = params[:fieldValue]
    validate_ajax_except = params[:validate_ajax_except]
    if validate_ajax_except.present?
      @admin_brand = Admin::Brand.where("name = ?", brand).where("id != ?", validate_ajax_except).first
    else
      @admin_brand = Admin::Brand.where("name = ?", brand).first
    end
    if@admin_brand.present?
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => false } }
      end
    else
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => true } }
      end
    end
  end

  def show 
    @products = @brand.products.select(:name,:description,:status,:id)
    if request.xhr?
      @render_path = admin_brand_path(@brand)
      @render_file = 'admin/brands/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/brands/show"
    end
  end

  def new
    #@brand = Admin::Brand.new
    #if request.xhr?
      #@render_path = new_admin_brand_path
      #@render_file = 'admin/brands/new'
      #render "remote_content/common_render.js.erb"          
    #else
      #render :template => "admin/brands/new"
    #end
    @brand = Admin::Brand.new
    respond_to do |format|
      format.html{
        render :template => "remote_content/brand_new.js.erb"   
      }
      format.json { 
        render :json => Admin::Brand.tokens(params[:q])
      }
      format.js{
        render :template => "remote_content/brand_new.js.erb"  
      }
    end
  end

  def edit
    @brand = Admin::Brand.find(params[:id])
    respond_to do |format|
      format.html      
      format.js{
        render :template => "remote_content/brand_edit.js.erb"
      }
    end    
  end

  def create
    @brand = Admin::Brand.new(brand_params)
    if @brand.save
      @products = @brand.products.select(:name,:description,:status)
      flash[:notice] = 'Brand was successfully created.'
      @render_path = admin_brand_path(@brand)
      @render_file = 'admin/brands/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "remote_content/brand_new.js.erb"   
    end
  end

  def update
    if @brand.update(brand_params)
      @products = @brand.products.select(:name,:description,:status,:id)
      flash[:alert] = 'Brand was successfully updated.'
      @render_path = admin_brand_path(@brand)
      @render_file = 'admin/brands/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "remote_content/brand_edit.js.erb"  
    end
  end
  
  def remove_product_from_brand
    @brand = Admin::Brand.find(params[:id])
    @products = @brand.products.select(:name,:description,:status,:id)
    product = @brand.products.find(params[:product_id])
    if request.xhr?
      if product
        @brand.products.delete(product)
        @brands =Admin::Brand.all
        flash[:error] = 'Product was successfully Removed form this Brand.'
        @render_path = admin_brands_path(@brand)
        @render_file = 'admin/brands/show'
        render "remote_content/common_render.js.erb"
      end
    end
  end

  def delete_selected
    @brand = Admin::Brand.find(params[:Brand][:brand_id])
    @products = @brand.products.select(:name,:description,:status,:id)
    if request.xhr?
      if params[:ids].blank?
        flash[:error] = 'Please Select Any Product From THe Table!'
      else
        params[:ids].each do |id|
          product = @brand.products.find(id)
          @brand.products.delete(product)
        end
        flash[:error] = 'Selected products are deleted successfully!'
      end 
      @render_path = admin_brand_path(@brand)
      @render_file = 'admin/brands/show'
      render "remote_content/common_render.js.erb" 
    end
  end

  def delete_selected_brand
    @brands =Admin::Brand.all
    if request.xhr?
      if params[:ids].blank?
        flash[:error] = 'Please Select Any Brand From The Table!'
      else
        params[:ids].each do |id|
          @brand = @brands.find(id)
          @brand.delete
        end
        flash[:error] = 'Selected Brands are deleted successfully!'
      end 
      @render_path = admin_brands_path
      @render_file = 'admin/brands/index'
      render "remote_content/common_render.js.erb" 
    end
  end

  # DELETE /brands/1
  # DELETE /brands/1.json
  def destroy
    if request.xhr?
      if @brand.destroy
        flash[:error] = 'Brand was successfully destroyed.'
        @brands =Admin::Brand.all
        @render_path = admin_brands_path
        @render_file = 'admin/brands/index'
        render "remote_content/common_render.js.erb"
      end  
    else
      if @brand.destroy
        redirect_to admin_brands_path, notice: "Brand was successfully destroyed."
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_brand
      @brand = Admin::Brand.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def brand_params
      params.require(:admin_brand).permit(:name, :description)
    end
end
