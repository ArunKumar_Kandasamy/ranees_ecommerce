class Admin::CodPoliciesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_cod_policy, only: [:show, :edit, :update, :destroy]

  # GET /admin/cod_policies
  # GET /admin/cod_policies.json
  def index
    @admin_cod_policies = Admin::CodPolicy.all
    if request.xhr?
      @render_path = admin_cod_policies_path
      @render_file = 'admin/cod_policies/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/cod_policies/index"
    end
  end

  # GET /admin/cod_policies/1
  # GET /admin/cod_policies/1.json
  def show  
    @existing_countries= @admin_cod_policy.cod_specific_countries
    @filter_cod_countries = Admin::Country.all.map{|x| x.id}
    @existing_countries = @admin_cod_policy.cod_specific_countries.map{|x| x.country_id}
    @filter_countries = @filter_cod_countries - @existing_countries
    @country_list = Admin::Country.find(@filter_countries)

    cod_specific_country = @admin_cod_policy.cod_specific_countries.build
    cod_specific_pincode = cod_specific_country.cod_specific_pincodes.build
    if request.xhr?
      @render_path = admin_cod_policy_path(@admin_cod_policy)
      @render_file = 'admin/cod_policies/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/cod_policies/show"
    end
  end

  # GET /admin/cod_policies/new
  def new
    @admin_cod_policy = Admin::CodPolicy.new
  end

  # GET /admin/cod_policies/1/edit
  def edit 
     if request.xhr?
      @render_path = admin_cod_policy_path(@admin_cod_policy)
      @render_file = 'admin/cod_policies/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/cod_policies/show"
    end
  end

  # POST /admin/cod_policies
  # POST /admin/cod_policies.json
  def create
    @admin_cod_policy = Admin::CodPolicy.new(admin_cod_policy_params)

    respond_to do |format|
      if @admin_cod_policy.save
        format.html { redirect_to @admin_cod_policy, notice: 'Cod policy was successfully created.' }
        format.json { render :show, status: :created, location: @admin_cod_policy }
      else
        format.html { render :new }
        format.json { render json: @admin_cod_policy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/cod_policies/1
  # PATCH/PUT /admin/cod_policies/1.json
  def update
    respond_to do |format|
      if @admin_cod_policy.update(admin_cod_policy_params)
        format.html { redirect_to @admin_cod_policy, notice: 'Cod policy was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_cod_policy }
      else
        format.html { render :edit }
        format.json { render json: @admin_cod_policy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/cod_policies/1
  # DELETE /admin/cod_policies/1.json
  def destroy 
    @admin_cod_policies = Admin::CodPolicy.all
    @admin_cod_policy.destroy
    flash[:alert] = 'COD policy was successfully destroyed.'  
    @render_path = admin_cod_policies_path
    @render_file = 'admin/cod_policies/index'
    render "remote_content/common_render.js.erb"
  end


  def find_cod_country_pincode_with_id   
    @admin_cod_policy = Admin::CodPolicy.find(params[:id])
    plan_country = @admin_cod_policy.cod_specific_countries.build
    country_pincode = plan_country.cod_specific_pincodes.build
    @country = Admin::Country.find(params[:country_id])
    render :template => "remote_content/find_cod_country_pincode_with_id.js.erb"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_cod_policy
      @admin_cod_policy = Admin::CodPolicy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_cod_policy_params
      params.require(:admin_cod_policy).permit(:cod_availability, :all_country, cod_specific_countries_attributes:[:id, :cod_policy_id, :country_id, :all_place, :_destroy, cod_specific_pincodes_attributes:[:id, :cod_specific_country_id, :pincode, :cod_availability, :_destroy]])
    end
end
