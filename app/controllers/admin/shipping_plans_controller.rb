class Admin::ShippingPlansController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_shipping_plan, only: [:show, :edit, :update, :destroy]
  before_action :prevent_destroy, only: :destroy
  after_action :check_for_activation, only: [:update, :destroy]

  def index 
    @admin_shipping_plans =  Admin::ShippingPlan.all
    if request.xhr?
      @render_path = admin_shipping_plans_path
      @render_file = 'admin/shipping_plans/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/shipping_plans/index"
    end
  end 

  def show
    respond_to do |format|
      format.html{ 
        @admin_shipping_countries = @admin_shipping_plan.shipping_countries
        @filter_shipping_countries = Admin::Country.all.map{|x| x.id}
        @filter_admin_shipping_countries = @admin_shipping_plan.shipping_countries.map{|x| x.country_id}
        @filter_countries = @filter_shipping_countries - @filter_admin_shipping_countries
        @filter_countries_results = Admin::Country.find(@filter_countries)
        @admin_shipping_country = Admin::ShippingCountry.new
        @admin_shipping_state = Admin::ShippingState.new
        @admin_shipping_city= Admin::ShippingCity.new
        @admin_shipping_cost= Admin::ShippingCost.new
      }
      format.js {
        render :template => "remote_content/shipping_plans.js.erb"
      }
    end 
  end 
 
  def new
    @admin_general = Admin::General.all
    @admin_shipping_plan = Admin::ShippingPlan.new
    if request.xhr?
      @render_path = new_admin_shipping_plan_path
      @render_file = 'admin/shipping_plans/new'
      render "remote_content/common_modal_render.js.erb"          
    else
      render :template => "admin/shipping_plans/new"
    end
  end

  def edit
    if request.xhr?
      @render_path = new_admin_shipping_plan_path
      @render_file = 'admin/shipping_plans/edit'
      render "remote_content/common_modal_render.js.erb"          
    else
      render :template => "admin/shipping_plans/edit"
    end
  end

  def create
    @admin_shipping_plan = Admin::ShippingPlan.new(admin_shipping_plan_params)
    if @admin_shipping_plan.save
      @admin_shipping_plans =  Admin::ShippingPlan.all
      flash[:alert] = 'Shipping plan was successfully created.' 
      @render_path = admin_shipping_plans_path
      @render_file = 'admin/shipping_plans/index'
      render "remote_content/common_render.js.erb"
    else
      render 'new'
    end
  end

  def update
    if @admin_shipping_plan.update(admin_shipping_plan_params)
      @admin_shipping_plans =  Admin::ShippingPlan.all
      flash[:alert] = 'Shipping plan was successfully updated.' 
      @render_path = admin_shipping_plans_path
      @render_file = 'admin/shipping_plans/index'
      render "remote_content/common_render.js.erb" 
    else
      render 'edit'
    end
  end

  def destroy
    if @admin_shipping_plan.destroy
      @admin_shipping_plans =  Admin::ShippingPlan.all
      flash[:alert] = 'Shipping plan was successfully destroyed.'
      @render_path = admin_shipping_plans_path
      @render_file = 'admin/shipping_plans/index'
      render "remote_content/common_render.js.erb"          
    end
  end
 
  private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_admin_shipping_plan
    @admin_shipping_plan = Admin::ShippingPlan.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_shipping_plan_params
    params.require(:admin_shipping_plan).permit(:name, :range_from, :range_upto, :criteria, shipping_countries_attributes:[:id, :shipping_plan_id, :country_id, shipping_states_attributes:[:id, :shipping_country_id, :state_id, shipping_cities_attributes:[:id, :shipping_city_id, :city_id, shipping_costs_attributes:[:id, :shipping_city_id, :pincode, :cost]]]])
  end

  def prevent_destroy
    @products = @admin_shipping_plan.products
    if @products.present?
      redirect_to @admin_shipping_plan, notice: 'It is not possible to delete, because this plan is assigned to some products'
    end
  end

  def check_for_activation
    @products = @admin_shipping_plan.products
    if @products.present?
      @products.each do | product |
        if product.activate_product
          product.status= true
          product.save
        else
          product.status= false
          product.save
        end
      end
    end
  end
end
