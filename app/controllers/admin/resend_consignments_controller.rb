class Admin::ResendConsignmentsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_consignment, only: [:update, :show, :edit, :destroy]   
  before_action :set_warehouse 

  def index
    @param = params[:q]    
    @search = @warehouse.consignments.page(params[:page]).search(params[:q])
    @consignment = @search.result.where("parent_consignment_id IS NOT NULL")
    @admin_resend_task = Admin::ResendTask.new
    1.times { @admin_resend_task.resend_tasks_consignments.build }     
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_warehouse_dashboard_resend_consignments_path(@warehouse)
      @render_file = 'admin/resend_consignments/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/resend_consignments/index"
    end
  end 

  def show
    @order = @consignment.order
    @consignment_items = @consignment.consignment_items 
    @admin_resend_task = @consignment.resend_tasks.first
    if request.xhr?
      @render_path = admin_warehouse_dashboard_resend_task_consignment_show_path(@warehouse, @admin_resend_task, @consignment)
      @render_file = 'admin/resend_consignments/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/resend_consignments/show"
    end 
  end

  def update
  end

   private
    def set_consignment
      @consignment = Admin::Consignment.find(params[:id])
    end

    def set_warehouse
      @warehouse = Admin::Warehouse.find(params[:warehouse_dashboard_id])
    end
end
