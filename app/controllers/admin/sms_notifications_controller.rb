class Admin::SmsNotificationsController < ApplicationController
  before_action :set_admin_sms_notification, only: [:show, :edit, :update, :destroy]

  # GET /admin/sms_notifications
  # GET /admin/sms_notifications.json
  def index
    @admin_sms_notifications = Admin::SmsNotification.all
  end

  # GET /admin/sms_notifications/1
  # GET /admin/sms_notifications/1.json
  def show
    if request.xhr?
      @render_path = admin_sms_notification_path(@admin_sms_notification)
      @render_file = 'admin/sms_notifications/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/sms_notifications/show"
    end
  end

  # GET /admin/sms_notifications/new
  def new
    @admin_sms_notifications = Admin::SmsNotification.all
    if @admin_sms_notifications.first.present?
      @admin_sms_notification = @admin_sms_notifications.first
    else
      @admin_sms_notification = Admin::SmsNotification.new
    end
    if request.xhr?
      unless params[:content_for].present?
        @render_path = new_admin_sms_notification_path(@admin_sms_notification)
        @render_file = 'admin/sms_notifications/new'
        render "remote_content/common_render.js.erb"     
      else
        @content_for = params[:content_for]
        render "remote_content/sms_notification_content_form.js.erb"
      end     
    else
      render :template => "admin/sms_notifications/new"
    end
  end 

  # GET /admin/sms_notifications/1/edit 
  def edit
    if request.xhr?
       unless params[:content_for].present?
        @render_path = edit_admin_sms_notification_path(@admin_sms_notification)
        @render_file = 'admin/sms_notifications/edit'
        render "remote_content/common_render.js.erb"    
      else
        @content_for = params[:content_for]
        render "remote_content/sms_notification_content_form.js.erb"
      end            
    else
      render :template => "admin/sms_notifications/edit"
    end
  end

  # POST /admin/sms_notifications
  # POST /admin/sms_notifications.json
  def create
    @admin_sms_notification = Admin::SmsNotification.new(admin_sms_notification_params)

    respond_to do |format|
      if @admin_sms_notification.save
        format.html { redirect_to @admin_sms_notification, notice: 'Sms notification was successfully created.' }
        format.json { render :show, status: :created, location: @admin_sms_notification }
      else
        format.html { render :new }
        format.json { render json: @admin_sms_notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/sms_notifications/1
  # PATCH/PUT /admin/sms_notifications/1.json
  def update
    respond_to do |format|
      if @admin_sms_notification.update(admin_sms_notification_params)
        format.html { redirect_to @admin_sms_notification, notice: 'Sms notification was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_sms_notification }
      else
        format.html { render :edit }
        format.json { render json: @admin_sms_notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/sms_notifications/1
  # DELETE /admin/sms_notifications/1.json
  def destroy
    @admin_sms_notification.destroy
    respond_to do |format|
      format.html { redirect_to admin_sms_notifications_url, notice: 'Sms notification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_sms_notification
      @admin_sms_notification = Admin::SmsNotification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_sms_notification_params
      params.require(:admin_sms_notification).permit(:replacement_order, :order_create,  :partial_refund, :post_deli_cancel, :tracking_updated, :order_success, :order_failed, :pre_deli_cancel, :refund_complete, :user_id, :content_for_order_create, :content_for_order_placed, :content_for_tracking_updated, :content_for_order_success, :content_for_order_failed, :content_for_pre_deli_cancel, :content_for_post_deli_cancel, :content_for_refund_complete, :content_for_replacement_order, :content_for_partial_refund, :back_jobs, :content_for_back_jobs)
    end
end
