class Admin::RefundRevocationsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_refund_revocation, only: [:show, :edit, :update, :destroy]

  def index
    @param = params[:q]
    @search = Admin::RefundRevocation.all.page(params[:page]).search(params[:q])
    @admin_refund_revocations = @search.result
    @admin_bank_revocations= Admin::RefundRevocation.where(:revocation_mode => "b",:status => "pr")
    @admin_voucher_revocations= Admin::RefundRevocation.where(:revocation_mode => "v",:status => "pr")
    @voucher= Admin::Voucher.new
    @admin_refund_revocation= Admin::RefundRevocation.new
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_refund_revocations_path
      @render_file = 'admin/refund_revocations/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/refund_revocations/index"
    end    
  end

  def show
    @admin_order = Admin::Order.where(:id => @admin_refund_revocation.consignment.order_id).first
    @admin_order_logs= Admin::OrderLog.where(:order_id => @admin_order.id)
    if request.xhr?
      @render_path = admin_refund_revocation_path(@admin_refund_revocation)
      @render_file = 'admin/refund_revocations/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/refund_revocations/show"
    end 
  end

  def new
    @admin_refund_revocation = Admin::RefundRevocation.new
  end

  def edit
  end

  def create
    @admin_refund_revocation = Admin::RefundRevocation.new(admin_refund_revocation_params)
    if @admin_refund_revocation.save
      redirect_to @admin_refund_revocation, notice: 'Refund revocation was successfully created.' 
    else
     render 'new'
    end
  end

  def update
    if params[:admin_refund_revocation][:status] == "cancel"
      @admin_refund_revocation.refund_revocation_items.each do |refund_revocation_item|
        if refund_revocation_item.pickup_consignment_items.first.present?
          refund_revocation_item.pickup_consignment_items.first.destroy
        end
        @consignment_item = @consignment.consignment_items.find_by_variant_id(refund_revocation_item.variant_id)
        @consignment_item.qty += 1
        @consignment_item.save
        @admin_refund_revocation.update(:status => "cancel")
      end
      redirect_to support_order_shipment_path(@admin_refund_revocation.consignment.order,@admin_refund_revocation.consignment), notice: 'Refund revocation was successfully updated.' 
    else
      if @admin_refund_revocation.update(admin_refund_revocation_params)
        @admin_order = Admin::Order.where(:id => @admin_refund_revocation.consignment.order_id).first
        @admin_order_logs= Admin::OrderLog.where(:order_id => @admin_order.id)
        flash['alert'] = 'Refund revocation was successfully updated.' 
        @render_path = admin_refund_revocation_path(@admin_refund_revocation)
        @render_file = 'admin/refund_revocations/show'
        render "remote_content/common_render.js.erb"          
      else
        @render_path = admin_refund_revocation_path(@admin_refund_revocation)
        @render_file = 'admin/refund_revocations/show'
        render "remote_content/common_render.js.erb"      
      end 
    end
  end

  def destroy
    @admin_refund_revocation.destroy
    if params[:admin_refund_revocation][:support].present?
      redirect_to support_order_shipment_path(@admin_refund_revocation.consignment.order,@admin_refund_revocation.consignment), notice: 'Refund revocation was successfully destroyed.' 
    else
      flash['alert'] = 'Refund revocation was successfully destroyed.' 
      @render_path = admin_refund_revocations_path
      @render_file = 'admin/refund_revocations/index'
      render "remote_content/common_render.js.erb" 
    end
  end

  private
    def set_admin_refund_revocation
      @admin_refund_revocation = Admin::RefundRevocation.find(params[:id])
      @consignment = @admin_refund_revocation.consignment
    end

    def admin_refund_revocation_params
      params.require(:admin_refund_revocation).permit(:revocation_cost, :consignment_id, :created_by, :bank_account_id, :revocation_mode, :status, :trans_status, :transaction_id)
    end
end
