class Admin::ReplacementRevocationsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_replacement_revocation, only: [:show, :edit, :update, :destroy]

  def index
    @param = params[:q]
    if request.xhr?      
      @search = Admin::ReplacementRevocation.all.page(params[:page]).search(params[:q])
      @admin_replacement_revocations = @search.result
      @search.build_condition
      @search.build_sort if @search.sorts.empty?
      if request.xhr?
        @render_path = admin_replacement_revocations_path
        @render_file = 'admin/replacement_revocations/index'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "admin/replacement_revocations/index"
      end       
    end
  end

  def show
    @admin_consignment_item = Admin::ConsignmentItem.new
    if @admin_replacement_revocation.consignment.present?
      @admin_consignment= @admin_replacement_revocation.consignment
    end
    @items = @admin_replacement_revocation.replacement_revocation_items 
    @consignment= Admin::Consignment.new
    if request.xhr?
      @render_path = admin_replacement_revocation_path(@admin_replacement_revocation)
      @render_file = 'admin/replacement_revocations/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/replacement_revocations/show"
    end   
  end

  def newcreate_order_log
    @admin_replacement_revocation = Admin::ReplacementRevocation.new
  end

  def edit
  end

  def create
    @admin_replacement_revocation = Admin::ReplacementRevocation.new(admin_replacement_revocation_params)

    respond_to do |format|
      if @admin_replacement_revocation.save
        format.html { redirect_to @admin_replacement_revocation, notice: 'Replacement revocation was successfully created.' }
        format.json { render :show, status: :created, location: @admin_replacement_revocation }
      else
        format.html { render :new }
        format.json { render json: @admin_replacement_revocation.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    if params[:admin_consignment]
      new_consignment = Admin::Consignment.create(:warehouse_id => params[:admin_consignment][:consignments][:warehouse_id], :replacement_revocation_id => params[:admin_consignment][:replacement_revocation_id])
      @replacement_revocation_item = Admin::ReplacementRevocationItem.find(params[:admin_consignment][:consignments][:replacement_revocation_item_id])
      @consignment_item = Admin::ConsignmentItem.create(:qty => params[:admin_consignment][:consignments][:qty], :variant_id => @replacement_revocation_item.variant_id, :per_qty_price => @replacement_revocation_item.per_qty_price, :consignment_id => new_consignment.id, :shipping_cost => @replacement_revocation_item.shipping_cost, :tax => @replacement_revocation_item.tax , :actual_cost => @replacement_revocation_item.actual_cost, :replacement_revocation_item_id => @replacement_revocation_item.id)
      redirect_to admin_replacement_revocation_path(@admin_replacement_revocation), :notice => "Consignment Successfully Created"
    else
      if params[:admin_replacement_revocation][:status] == "cancel"
        @admin_replacement_revocation.replacement_revocation_items.each do |replacement_revocation_item|
          if replacement_revocation_item.pickup_consignment_items.first.present?
            replacement_revocation_item.pickup_consignment_items.first.destroy
          end
          @consignment_item = @consignment.consignment_items.find_by_variant_id(replacement_revocation_item.variant_id)
          @consignment_item.qty += 1
          @consignment_item.save
          @admin_replacement_revocation.update(:status => "cancel")
        end
        redirect_to support_order_shipment_path(@admin_replacement_revocation.consignment.order,@admin_replacement_revocation.consignment), notice: 'Replacement revocation was successfully updated.' 
      else
        if @admin_replacement_revocation.update(admin_replacement_revocation_params)
          redirect_to @admin_replacement_revocation, notice: 'Replacement revocation was successfully updated.' 
        else
          render :edit
        end
      end
    end
  end

  def destroy
    @admin_replacement_revocation.destroy
    if params[:admin_refund_revocation][:support].present?
      redirect_to support_order_shipment_path(@admin_replacement_revocation.consignment.order,@admin_replacement_revocation.consignment), notice: 'Replacement revocation was successfully destroyed.' 
    else
      redirect_to admin_replacement_revocations_url, notice: 'Refund revocation was successfully destroyed.' 
    end
  end

  private
    def set_admin_replacement_revocation
      @admin_replacement_revocation = Admin::ReplacementRevocation.find(params[:id])
      @consignment = @admin_replacement_revocation.consignment
    end

    def admin_replacement_revocation_params
      params.require(:admin_replacement_revocation).permit(:revocation_cost, :consignment_id, :created_by, :shipping_address_id, :replacement_revocation_number, :status)
    end
end
