class Admin::ConsignmentsController < ApplicationController
  before_action :set_consignment, only: [:update, :show, :edit, :destroy]   
  before_action :set_warehouse   

  def index
    @param = params[:q] 
    @search = @warehouse.consignments.search(params[:q])
    @search.build_condition
    @search.build_sort if @search.sorts.empty? 
    @search1 = @search.result
    @consignment =  @search1.where("order_id IS NOT NULL")
    @task_unassigned_consignment = @search1.where('id NOT IN (SELECT DISTINCT(consignment_id) FROM package_tasks_consignments)')
    @task_assigned_consignment = @search1.where('id IN (SELECT DISTINCT(consignment_id) FROM package_tasks_consignments)')
    @replacement_consignments = @search1.where("order_id IS NULL")
    @admin_package_task = Admin::PackageTask.new
    1.times { @admin_package_task.package_tasks_consignments.build }
    if request.xhr?  
      if params[:assigned_page].present?
        render :template => "remote_content/unassigned_page.js.erb"
      elsif params[:page].present?
        render :template => "remote_content/consignment_page.js.erb"
      else  
        @render_path = admin_warehouse_dashboard_consignments_path(@warehouse)
        @render_file = 'admin/consignments/index'
        render "remote_content/common_render.js.erb" 
      end         
    else
      render :template => "admin/consignments/index"
    end 
  end  

  def edit
  end 

  def create
    @consignment= Admin::Consignment.new(consignment_params)
    if @consignment.save
      if request.xhr?
      	@order = Admin::Order.find(@consignment.order_id)
      	render :template => "remote_content/consignment_refresh.js.erb"
      else
	    redirect_to admin_order_show_path(@consignment.order_id), notice: "Consignment Created Successfully"
      end
    end
  end

  def consignment_logs
    @consignment = Admin::Consignment.find(params[:id])
    @consignment_logs= @consignment.consignment_logs
    @admin_package_task = @consignment.package_tasks.first
  end

  def show
    @invoice = @consignment.invoices.first if @consignment.invoices.present?
    @order = @consignment.order
    @admin_regulation = Admin::Regulation.new
    @order = @consignment.order
    @replacement_revocation= @consignment.replacement_revocation
    @consignment_items = @consignment.consignment_items 
    @admin_package_task = @consignment.package_tasks.first
    respond_to do |format| 
      format.html do 
      end
      format.pdf do
        render pdf: "Invoice for #{@invoice.invoice_number}",                  # file name
               layout: 'layouts/application.pdf.erb',  # layout used
               show_as_html: params[:debug].present?    # allow debuging
      end
      format.js do 
        @render_path = admin_warehouse_dashboard_package_task_task_consignment_show_path(@warehouse,@admin_package_task,@consignment)
        @render_file = 'admin/consignments/show'
        render "remote_content/common_render.js.erb"   
      end
  end
    
  end

  def update 
    @notification= Admin::NotificationCustomer.first
    @sms_notification= Admin::SmsNotification.first 
    if request.xhr?
      @order= @consignment.order
        if @sms_notification.present? 
          if @sms_notification.order_success
            if @consignment.delivery_status
              @consignment.send_sms
            else
              if @sms_notification.pre_deli_cancel
                if @consignment.cancel.present?
                  @consignment.send_pre_deli_cancel_sms
                end
              end
            end
          end
        end      
        if @notification.present? 
          if @notification.order_success
            if @consignment.delivery_status
              Admin::InvoiceMailer.send_invoice(@consignment).deliver
            else
              if @notification.pre_deli_cancel
                if @consignment.cancel.present?
                  Admin::InvoiceMailer.email_pre_delivery_cancelled(@order,@consignment).deliver
                end
              end
            end
          end
        end
        @admin_package_task = @consignment.package_tasks.first
      	render :template => "remote_content/package_task_show_refresh.js.erb"
    else
      if params[:admin_consignment].present?
      if params[:admin_consignment][:product_code].present?
            @actual_weight = 0.0
            @consignment.consignment_items.each do |consignment_item|
               @actual_weight += Admin::Variant.find(consignment_item.variant_id).weight
           end
           @consignment.update(consignment_params)
           if @consignment.awb_no.present?
             @aws_no = @consignment.awb_no
           else
             @aws_no = @consignment.bluedart_generate_waybill(params[:admin_consignment][:product_code],@actual_weight)
           end
           if @aws_no.class == Array
               redirect_to admin_warehouse_dashboard_package_task_path(@warehouse ,@consignment.package_tasks.first), notice: "#{@aws_no[1]}"
           else
             @consignment.update(:awb_no =>  @aws_no,:tracking => @aws_no,:logistic_id => params[:admin_consignment][:logistic_id])
             @token_number = @consignment.bluedart_generate_pickupregistration(params[:admin_consignment][:product_code],@actual_weight,@aws_no)
             if @token_number.class == Array
                redirect_to admin_warehouse_dashboard_package_task_path(@warehouse ,@consignment.package_tasks.first), notice: "#{@token_number[1]}"
             else
                 @consignment.update(:token_no => @token_number)
               redirect_to admin_warehouse_dashboard_package_task_path(@warehouse ,@consignment.package_tasks.first), notice: "Pickup was successfully initiated"
             end
        end
      elsif params[:admin_consignment][:delivery_status].present?
          if params[:admin_consignment][:delivery_status] == true
            if @sms_notification.present? 
               if @sms_notification.order_success
                @consignment.send_tracking_update_sms
               end
            end
            if @notification.present? 
              if @notification.tracking_updated
                 Admin::InvoiceMailer.send_invoice(@consignment).deliver
              end
            end
          end
          @consignment.update(consignment_params)
          redirect_to admin_warehouse_dashboard_package_task_path(@warehouse ,@consignment.package_tasks.first), notice: "Consignment Updated Successfully"
      elsif params[:admin_consignment][:pickup_status].present?
         @status = params[:admin_consignment][:pickup_status] == "true" ? true : nil
          if @status == true
            if @sms_notification.present? 
               if @sms_notification.tracking_updated
                @consignment.send_tracking_update_sms
               end
            end
            if @notification.present? 
              if @notification.order_success
                  Admin::InvoiceMailer.email_order_tracking_update(@consignment.order,@consignment).deliver 
              end
            end
          end
         @consignment.update(:pickup_status => @status )
         redirect_to admin_warehouse_dashboard_package_task_path(@warehouse ,@consignment.package_tasks.first), notice: "Consignment Updated Successfully"
      elsif params[:admin_consignment][:cancel].present?
         @cancel_status = @consignment.bluedart_cancel_pickupregistration()
         if @cancel_status.class == Array
            redirect_to admin_warehouse_dashboard_package_task_path(@warehouse ,@consignment.package_tasks.first), notice: "#{@cancel_status[1]}"
         else
           @consignment.update(:tracking => "",:pickup_status => false,:token_no => "")
           redirect_to admin_warehouse_dashboard_package_task_path(@warehouse ,@consignment.package_tasks.first), notice: "Pickup was  Cancelled successfully"
         end
      else
         @consignment.update(consignment_params)
          if params[:admin_consignment][:cancel].present? 
            redirect_to support_orders_path, notice: "Consignment Updated Successfully"
            if @consignment.order != "COD"
              @order.create_revocation
            end
          else
            redirect_to admin_warehouse_dashboard_consignment_path(@warehouse , @consignment), notice: "Consignment Updated Successfully"
          end
       end
      else
        @new_consignment_hash = params[:admin_consignment]
      
          if @consignment.order.present?
            @order = @consignment.order
            if @order.cancel.nil? && @consignment.package_tasks.first.present?
              @consignment.update(:conform => true)
              if @order.order_revocations.first.nil?
                @order.create_revocation
              end
              @consignment.apply_voucher
              @invoice = Admin::Invoice.where(:consignment_id => @consignment.id).first_or_initialize
              @invoice.save
              redirect_to admin_warehouse_dashboard_package_task_task_consignment_show_path(@warehouse , @consignment.package_tasks.first,@consignment), notice: "Consignment confirmation was Successfully"
            else
              redirect_to admin_warehouse_dashboard_package_task_task_consignment_show_path(@warehouse , @consignment.package_tasks.first,@consignment), notice: "Consignment confirmation failed"
            end
          else
            @replacement_revocation = @consignment.replacement_revocation
            if @consignment.package_tasks.first.present?
              @consignment.update(:conform => true)
              @invoice = Admin::Invoice.where(:consignment_id => @consignment.id).first_or_initialize
              @invoice.save
              redirect_to admin_warehouse_dashboard_package_task_task_consignment_show_path(@warehouse , @consignment.package_tasks.first,@consignment), notice: "Consignment confirmation was Successfully"
            else
              redirect_to admin_warehouse_dashboard_package_task_task_consignment_show_path(@warehouse , @consignment.package_tasks.first,@consignment), notice: "Consignment confirmation failed"
            end
          end
    end
    end
  end

  def destroy
    if @consignment.destroy
      redirect_to admin_warehouse_dashboard_package_tasks_path(@warehouse ), notice: 'Consignment was successfully destroyed.'
    end
  end

  def consignment_params
  	params.require(:admin_consignment).permit(:pickup_status,:token_no,:aws_no,:pickup_date,:cancelled_by, :cancel, :remarks, :order_id, :replacement_revocation_id, :replacement_revocation_item_id, :warehouse_id, :conform, :logistic_id ,:tracking ,:delivery_status, :delivery_date, :reason ,consignment_items_attributes:[:id , :_destroy])
  end

  private
    def set_consignment
      @consignment = Admin::Consignment.find(params[:id])
    end

    def set_warehouse
      @warehouse = Admin::Warehouse.find(params[:warehouse_dashboard_id])
    end
end



