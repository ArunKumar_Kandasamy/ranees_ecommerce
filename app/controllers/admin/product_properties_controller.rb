class Admin::ProductPropertiesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_admin_product_property, only: [:show, :edit, :update, :destroy]
  before_action :authorize

  def index
    @admin_product_properties = Admin::ProductProperty.all
  end

  def show
  end
  
  def new
    @admin_product_property = Admin::ProductProperty.new
    respond_to do |format|
      format.html
      format.json { 
        render :json => Admin::ProductProperty.tokens(params[:q])
      }
    end
  end

  def edit
  end

  def create
    @admin_product_property = Admin::ProductProperty.new(admin_product_property_params)
    if @admin_product_property.save
      redirect_to @admin_product_property, notice: 'Product property was successfully created.' 
    else
      render 'new'
    end
  end

  def update
    if @admin_product_property.update(admin_product_property_params)
      redirect_to @admin_product_property, notice: 'Product property was successfully updated.' 
    else
      render 'edit'
    end
  end

  def destroy
    if @admin_product_property.destroy
      redirect_to admin_product_properties_url, notice: 'Product property was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_product_property
      @admin_product_property = Admin::ProductProperty.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_product_property_params
      params.require(:admin_product_property).permit(:product_id, :product_question_id, :answer)
    end
end
