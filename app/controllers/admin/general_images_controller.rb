class Admin::GeneralImagesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_admin_general_image, only: [:show, :edit, :update, :destroy]
  before_action :set_admin_image_type

  # GET /admin/general_images
  # GET /admin/general_images.json
  def index
    @admin_general_images = Admin::GeneralImage.all
    if request.xhr?
      @render_path = admin_general_images_path
      @render_file = 'admin/general_images/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/general_images/index" 
    end
  end

  # GET /admin/general_images/1
  # GET /admin/general_images/1.json
  def show
  end

  # GET /admin/general_images/new
  def new
    @admin_general_image = Admin::GeneralImage.new
    @list_child_categories = Admin::Category.order(:name)
    @general = Admin::General.first
    if @general.present?
      @n = @general.category_level
    else
      @n = 3
    end
    @list_children_categories = @list_child_categories.reject { |h| h.ancestors.size > @n-2 }
    if request.xhr?
      @render_path = new_admin_image_type_general_image_path
      @render_file = 'admin/general_images/new'
      render "remote_content/common_render.js.erb"          
    else
     render :template => "admin/general_images/new"
    end
  end

  # GET /admin/general_images/1/edit
  def edit
    @admin_general_image = Admin::GeneralImage.find(params[:id])
    @list_child_categories = Admin::Category.order(:name)
    @general = Admin::General.first
    if @general.present?
      @n = @general.category_level
    else
      @n = 3
    end
    @list_children_categories = @list_child_categories.reject { |h| h.ancestors.size > @n-2 }
    if request.xhr?
      @render_path = edit_admin_image_type_general_image_path
      @render_file = 'admin/general_images/edit'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/general_images/edit"
    end
  end

  # POST /admin/general_images
  # POST /admin/general_images.json
  def create
    @list_child_categories = Admin::Category.order(:name)
    @general = Admin::General.first
    if @general.present?
      @n = @general.category_level
    else
      @n = 3
    end
    @list_children_categories = @list_child_categories.reject { |h| h.ancestors.size > @n-2 }
    image_type = @admin_image_type
    @admin_general_image = @admin_image_type.general_images.build(admin_general_image_params)
    #@admin_general_image.update_attributes(admin_general_image_params)
    #@admin_general_image = Admin::GeneralImage.new(admin_general_image_params)
      if @admin_general_image.save
        if params[:admin_general_image][:avatar].blank?
          flash[:notice] = "General image was successfully created."
          redirect_to @admin_image_type
        else
          render :action => "crop"
        end
      else
        render :template => "admin/general_images/new"
      end
  end

  
  def sort
    params[:admin_general_image].each_with_index do |id, index|
      Admin::GeneralImage.where(id: id).update_all(sort_order: index+1)
    end
    render nothing: true
  end

  # PATCH/PUT /admin/general_images/1
  # PATCH/PUT /admin/general_images/1.json
  def update
    if @admin_general_image.update(admin_general_image_params)

      if params[:admin_general_image][:avatar].blank?
        if params[:admin_general_image][:crop_x].present?
          flash[:notice] = "General image was successfully updated."
          redirect_to @admin_image_type
        else
          render :action => "crop"
        end
      else
        render :action => "crop"
      end
    else
      format.html { render :edit }
      format.json { render json: @admin_general_image.errors, status: :unprocessable_entity }
    end
  end

  # DELETE /admin/general_images/1
  # DELETE /admin/general_images/1.json
  def destroy
    @admin_general_image.destroy
    @admin_general_images = Admin::GeneralImage.all    
    redirect_to @admin_image_type
  end

  private

    def set_admin_image_type
      @admin_image_type = Admin::ImageType.find(params[:image_type_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_admin_general_image
      @admin_general_image = Admin::GeneralImage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_general_image_params
      params.require(:admin_general_image).permit(:title, :link,:avatar,:description, :image_type_id, :category_id, :alt_text, :crop_x, :crop_y, :crop_w, :crop_h)
    end
end
