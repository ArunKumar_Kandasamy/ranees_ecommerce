class Admin::TicketTasksController < ApplicationController
  before_action :set_admin_ticket_task, only: [:show, :edit, :update, :destroy]

  def index
    @support_ticket_tasks = Support::TicketTask.where(:assigned_to => current_user.id)
    @general_tickets = @support_ticket_tasks.where("ticket_tasks.status != ?","redirected").joins(:ticket).where(:tickets => {:subject => 'General' })
    @order_tickets = @support_ticket_tasks.where("ticket_tasks.status != ?","redirected").joins(:ticket).where(:tickets => {:subject => 'Order' })
    @voucher_tickets = @support_ticket_tasks.where("ticket_tasks.status != ?","redirected").joins(:ticket).where(:tickets => {:subject => 'Voucher' })
    @task_assigned_tickets = @support_ticket_tasks.where(:status => "redirected").includes(:tasks)
    if request.xhr?
      @render_path = admin_ticket_tasks_path
      @render_file = 'admin/ticket_tasks/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/ticket_tasks/index"
    end  
  end

  def show
    @ticket = @support_ticket_task.ticket
    @ticket_reply = @ticket.ticket_replies.first
    unless @ticket_reply.present?
      @new_ticket_reply = Support::TicketReply.new
      @assigned_user = Array.new
      User.joins(:role).each do |assign_user|
        if assign_user.role.name != 'u' && assign_user.email != current_user.email
          @assigned_user << assign_user
        end
      end
    end
    if request.xhr?
      @render_path = admin_ticket_task_path(@support_ticket_task)
      @render_file = 'admin/ticket_tasks/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/ticket_tasks/show"
    end  
  end

  def new
    @admin_ticket_task = Support::TicketTask.new
  end

  def edit
  end

  def create
    @admin_ticket_task = Support::TicketTask.new(admin_ticket_task_params)
    @support_ticket_task = Support::TicketTask.new(admin_ticket_task_params)
    @support_ticket_task.save
    if params[:support_ticket_task][:parent_task_id].present?
      @parent_task = Support::TicketTask.find(params[:support_ticket_task][:parent_task_id])
      @parent_task.update(:status => "redirected")
      redirect_to admin_ticket_task_path(params[:support_ticket_task][:parent_task_id]), notice: 'Ticket task was successfully created.'
    else
      redirect_to admin_ticket_path(params[:support_ticket_task][:ticket_id]), notice: 'Ticket task was successfully created.'
    end
  end

  def update
    respond_to do |format|
      if @support_ticket_task.update(admin_ticket_task_params)
        format.html { redirect_to admin_ticket_task_path(@support_ticket_task), notice: 'Ticket task was successfully updated.' }
        format.json { render :show, status: :ok, location: @support_ticket_task }
      else
        format.html { render :edit }
        format.json { render json: @support_ticket_task.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @admin_ticket_task.destroy
    respond_to do |format|
      format.html { redirect_to admin_ticket_tasks_url, notice: 'Ticket task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_admin_ticket_task
      @support_ticket_task = Support::TicketTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_ticket_task_params
      params.require(:support_ticket_task).permit(:assigned_by, :assigned_to, :priority, :status, :parent_task_id, :ticket_id)
    end
end
