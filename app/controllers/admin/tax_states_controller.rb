class Admin::TaxStatesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_tax_state, only: [:show, :edit, :update, :destroy]
  before_action :find_admin_tax_plan
  before_action :find_admin_tax_country

  def index
    @admin_tax_states = Admin::TaxState.all
  end

  def show
  end

  def new
    @admin_tax_state = Admin::TaxState.new
  end

  def edit
  end

  def create
    @admin_tax_countries = @admin_tax_plan.tax_countries
    @admin_tax_state = @admin_tax_country.tax_states.build(admin_tax_state_params)
    if @admin_tax_state.save
      respond_to do |format|
        format.html{
          redirect_to admin_tax_plan_path(@admin_tax_plan), notice: 'Tax state was successfully created.'
        }
        format.js {
          flash[:notice] = "Tax state was successfully created."
          render :template => "remote_content/tax_countries_success.js.erb"
          flash.discard
        }
      end
    else
      render 'new'
    end    
  end

  def update
    if @admin_tax_state.update(admin_tax_state_params)
      redirect_to @admin_tax_state, notice: 'Tax state was successfully updated.'
    else
      render 'edit'
    end
  end

  def destroy
    if @admin_tax_state.destroy
      redirect_to admin_tax_states_url, notice: 'Tax state was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_tax_state
      @admin_tax_state = Admin::TaxState.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_tax_state_params
      params.require(:admin_tax_state).permit(:tax_country_id, :state_id)
    end
    def find_admin_tax_country
      @admin_tax_country = Admin::TaxCountry.find(params[:tax_country_id])
    end

    def find_admin_tax_plan
      @admin_tax_plan = Admin::TaxPlan.find(params[:tax_plan_id])
    end
end
