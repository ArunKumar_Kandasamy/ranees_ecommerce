class Admin::RejectionReasonsController < ApplicationController
  def create
    @rejection_reason= Admin::RejectionReason.new 
  	@admin_rejection = Admin::RejectionReason.new(admin_rejection_params)
    if @admin_rejection.save
      @order = Admin::Order.find(@admin_rejection.order_id)
      respond_to do |format|
        format.js { 
          render :template => "remote_content/rejection_refresh.js.erb"
        }
      end
    end
  end 
  
  def destroy
    @rejection_reason= Admin::RejectionReason.new 
    @admin_rejection_reason = Admin::RejectionReason.find(params[:id])
    @order = Admin::Order.find(@admin_rejection_reason.order_id)
    if @admin_rejection_reason.destroy
      respond_to do |format|
        format.js { 
          render :template => "remote_content/rejection_refresh.js.erb"
        }
      end
    end
  end

  private

  def admin_rejection_params
  	params.require(:admin_rejection_reason).permit(:order_id, :reason, :attr_value)
  end
end
