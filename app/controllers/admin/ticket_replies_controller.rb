class Admin::TicketRepliesController < ApplicationController
  before_action :set_support_ticket_reply, only: [:show, :edit, :update, :destroy]

  def index
    @support_ticket_replies = Support::TicketReply.all
  end

  def show
  end

  def new
    @support_ticket_reply = Support::TicketReply.new
  end

  def edit
  end

  def create
    @support_ticket_reply = Support::TicketReply.new(support_ticket_reply_params)
    @support_ticket_reply.save
    if params[:support_ticket_reply][:ticket_task_id].present?
      redirect_to admin_ticket_task_path(params[:support_ticket_reply][:ticket_task_id]), notice: 'Ticket reply was successfully created.'
    else
      redirect_to support_ticket_path(params[:support_ticket_reply][:ticket_id]),notice: 'Ticket reply was successfully created.'
    end
  end

  def update
    respond_to do |format|
      if @support_ticket_reply.update(support_ticket_reply_params)
        format.html { redirect_to @support_ticket_reply, notice: 'Ticket reply was successfully updated.' }
        format.json { render :show, status: :ok, location: @support_ticket_reply }
      else
        format.html { render :edit }
        format.json { render json: @support_ticket_reply.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @support_ticket_reply.destroy
    respond_to do |format|
      format.html { redirect_to support_ticket_replies_url, notice: 'Ticket reply was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_support_ticket_reply
      @support_ticket_reply = Support::TicketReply.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def support_ticket_reply_params
      params.require(:support_ticket_reply).permit(:ticket_id, :body, :from_email, :to_email, :ticket_task_id)
    end
end
