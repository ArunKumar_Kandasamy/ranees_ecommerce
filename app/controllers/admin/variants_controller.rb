class Admin::VariantsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_admin_variant, only: [:show, :edit, :update, :destroy]
  before_filter :authorize
  before_action :set_admin_product
  after_action :check_for_activation, only: [:create, :update]
  
  def index
    @admin_variants = Admin::Variant.all
  end

  def show
    if params[:view].present?
      @admin_inventory = Admin::Inventory.new
      @variants = @product.variants
      @render_path = admin_product_variant_path(@product,@admin_variant)
      @render_file = 'admin/variants/show'
      render "remote_content/common_render.js.erb"
    else
      if params[:pin].present? 
        @product_price= @admin_variant.user_price(params[:pin])
      end
      @admin_inventory = Admin::Inventory.new
      @variants = @product.variants
      respond_to do |format|
        format.html{
        }
        format.js {
          if params[:inventory].present?
            render "remote_content/add_inventory.js.erb"
          elsif params[:warehouse_id].present?
            @warehouse_id = params[:warehouse_id]
            render :template => "remote_content/add_inventory1.js.erb"
          else
            @render_path = admin_product_variant_path(@product,@admin_variant)
            @render_file = 'admin/variants/show'
            render "remote_content/common_render.js.erb"
            #render :template => "remote_content/add_inventory.js.erb"
          end  
        }
      end
    end
  end

  def new
    @admin_variant = Admin::Variant.new
    product_properties= @admin_variant.product_properties.build 
    image = @admin_variant.images.build
    if request.xhr?
      @render_path = new_admin_product_variant_path(@product)
      @render_file = 'admin/variants/new'
      render "remote_content/common_render.js.erb"
      #render "remote_content/common_modal_render.js.erb"         
    else
      render :template => "admin/variants/new"
    end
  end

  def edit
    @variant = @admin_variant
    if request.xhr?
      @render_path = edit_admin_product_variant_path(@product,@variant)
      @render_file = 'admin/variants/edit'
      render "remote_content/common_render.js.erb"
      #render "remote_content/common_modal_render.js.erb"         
    else
      render :template => "admin/variants/edit"
    end
  end

  def create
    @admin_variant = Admin::Variant.new(admin_variant_params)
    if @admin_variant.save
      @admin_variant.activate_variant
      #djfhjdfh
      redirect_to @product, notice: 'Variant was successfully created.'
    else
      @admin_variant = Admin::Variant.new
      product_properties= @admin_variant.product_properties.build 
      image = @admin_variant.images.build
      render 'new'
    end
  end

  def update
    if @admin_variant.update(admin_variant_params)
      @admin_variant.activate_variant
      if request.xhr?
        @admin_inventory = Admin::Inventory.new
        @variants = @product.variants
        @render_path = admin_product_variant_path(@product,@admin_variant)
        @render_file = 'admin/variants/show'
        render "remote_content/common_render.js.erb"
      else
        redirect_to admin_product_variant_path(@product,@admin_variant), notice: 'Variant was successfully updated.'
      end
    else
      flash[:notice] = "Properties cannot be nil"
      render 'edit'
    end
  end

  def sort
    params[:admin_variant].each_with_index do |id, index|
      Admin::Variant.where(id: id).update_all(sort_order: index+1)
    end
    render nothing: true
  end

  def destroy
    if @admin_variant.destroy
      redirect_to @product, notice: 'Variant was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_variant
      @admin_variant = Admin::Variant.find(params[:id])
    end

    def set_admin_product
      @product = Admin::Product.find(params[:product_id])
    end

    def check_for_activation
      if @product.activate_product
        @product.status= true
        @product.save
      else
        @product.status= false
        @product.save
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_variant_params
      params.require(:admin_variant).permit(:id, :title, :meta_desc, :product_id, :strike_rate, :_destroy, :status, :sku, :bar_code, :visible, :name, :ready_to_wear, :weight,:length,:breadth,:width, images_attributes:[:id, :alt_text, :_destroy, :avatar, :variant_id], product_properties_attributes:[:id, :variant_id, :product_id, :_destroy, :product_question_tokens, :answer])
    end

    
end
  