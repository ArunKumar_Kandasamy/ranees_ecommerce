class Admin::OrderItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :find_actual_cost, only: [:update,:check_order_item_availability]
  respond_to :html, :json,:js
  
  def create
  end

  def edit 
    
  end
  
  def check_order_item_availability    
    respond_to do |format| 
      @new_variant = Admin::Variant.find(params[:variant_id])
      @new_pincode =  ShippingAddress.find(params[:shipping_address_id]).pincode
      @new_country = ShippingAddress.find(params[:shipping_address_id]).country
      if params[:active] == "refresh_page"
        if @product.check_cod(@new_pincode)
          @per_qty_price = Admin::Inventory.where(:variant_id => @new_variant.id).order('selling_price DESC').first.selling_price
          @price = params[:qty].to_i * Admin::Inventory.where(:variant_id => @new_variant.id).order('selling_price DESC').first.selling_price
          format.js  { render :template => "remote_content/refresh_order_items_price.js.erb" }
        else
          format.json  { render :json =>{ :message => "Sorry COD Unavailable for this item",:error => 1} }
        end
      else
        if params[:active] == "variant"
          bal_qty = @order_item.bal_qty(@new_variant)
          if @product.check_cod(@new_pincode)
            price = Admin::Inventory.where(:variant_id => @new_variant.id).order('selling_price DESC').first.selling_price
            if bal_qty >= params[:qty].to_i
              if @order_item.per_qty_price < price 
                diff = price - @order_item.per_qty_price 
                format.json  { render :json =>{ :message => "This Product is #{diff} Rupees Higher than the Current Product",:error => 0} }
              elsif @order_item.per_qty_price > price 
                diff = @order_item.per_qty_price - price 
                format.json  { render :json =>{ :message => "This Product is #{diff} Rupees Lesser than the Current Product",:error => 0} }
              else
                format.json  { render :json =>{ :message => "success",:error => 0} }
              end
            else
              format.json  { render :json =>{ :message => "Sorry only #{bal_qty} available in this product",:error => 1} }
            end
          else
            format.json  { render :json =>{ :message => "Sorry COD Unavailable for this item",:error => 1} }
          end
        elsif params[:active] == "qty"
          if @order_item.qty < params[:qty].to_i
            @shipping =  @new_variant.find_variant_availability(@new_country,@new_pincode)
            if @shipping.status != "unavailable"
              if @order.payment_mode == "COD"
                if @product.check_cod(@new_pincode)
                  bal_qty = @order_item.bal_qty(@new_variant)
                  if bal_qty.to_i >= params[:qty].to_i
                    if @order_item.per_qty_price >= Admin::Inventory.where(:variant_id => @new_variant.id).order('selling_price DESC').first.selling_price
                      format.json  { render :json =>{ :message => "success",:error => 0} }
                    else
                      format.json  { render :json =>{ :message => "Sorry this Product price has been changed please place as new order",:error => 1} }
                    end
                  else
                    format.json  { render :json =>{ :message => "Quantity Unavailable,Only #{bal_qty} quantities are available",:error => 1} }
                  end
                else
                  format.json  { render :json =>{ :message => "Sorry COD Unavailable for this item",:error => 1} }
                end
              else
                format.json  { render :json =>{ :message => "Sorry you cannot increase the qty",:error => 1} }
              end
            else
              format.json  { render :json =>{ :message => "Sorry Shipping Unavailable for this item",:error => 1} }
            end
          elsif @order_item.qty == params[:qty].to_i 
            format.json  { render :json =>{ :message => "success",:error => 0} }
          else
            if @order.payment_mode == "COD"
              if @product.check_cod(@new_pincode)
                format.json  { render :json =>{ :message => "success",:error => 0} }
              else
                format.json  { render :json =>{ :message => "Sorry COD Unavailable for this item",:error => 1} }
              end
            else
              format.json  { render :json =>{ :message => "Your Balance Account Will Be Refund Soon",:error => 0} }
            end
          end
        else
          format.json  { render :json =>{ :message => "success",:error => 0} }
        end
      end
    end
  end

  def update 
    respond_to do |format|
      if params[:admin_order_item][:order_consignment].present?
      end
      if params[:admin_order_item][:variant_id].present?
        pincode = @order.shipping_address.pincode
        price = Admin::Inventory.where(:variant_id => params[:admin_order_item][:variant_id]).order('selling_price DESC').first.selling_price
        @order_item.update(:per_qty_price => price)
        @order_item.update(order_item_params) 
        format.json  { render :json =>{ :message => "Updated Successfullys"} }
      end
      if params[:admin_order_item][:qty].present?
      	if @order_item.qty < params[:admin_order_item][:qty].to_i
      	  pincode = @order.shipping_address.pincode
		      @shipping = Admin::Product.shipping_cost(pincode, @product, @variant)
      	  if @shipping.status != false
  	      	if @product.check_cod(pincode)
  		        bal_qty = @order_item.bal_qty(@variant)
  		        if bal_qty.to_i >= params[:admin_order_item][:qty].to_i
  		          if @order_item.per_qty_price >= @variant.user_price(pincode)
  		            @order_item.update(order_item_params)
  		            format.json  { render :json =>{ :message => "Updated Successfully"} }
  		          else
                  format.json  { render :json =>{ :message => "Sorry this Product price has been changed please place as new order"} }
  		          end
  		        else
  		          @order_item.update(order_item_params)
  		          @order_item.update(:qty => bal_qty)
  		          format.json  { render :json =>{ :message => "Quantity Unavailable"} }
  		        end
    		    else
    		      format.json  { render :json =>{ :message => "Sorry COD Unavailable for this item"} }
    		    end
    		  else
    		    format.json  { render :json =>{ :message => "Sorry Shipping Unavailable for this item"} }
    		  end
    		else
    		  @order_item.update(order_item_params)
    		  if @order.payment_mode == "COD"
      	    format.json  { render :json =>{ :message => "Updated Successfully"} }
          else
          	format.json  { render :json =>{ :message => "Your Balance Account Will Be Refund Soon"} }
          end
        end
      else
        @order_item.update(order_item_params) 
        format.json  { render :json =>{ :message => "Updated Successfullys"} }
      end
      format.html { redirect_to admin_order_path(@order) }
    end
  end

  def destroy 
  	@order_item=Admin::OrderItem.find(params[:id])
    @order=Admin::Order.find(@order_item.order_id)
    @items = @order.order_items
  	if @order_item && @order_item.consignment_items.first.nil?
  	  @order_item.destroy
  	  if request.xhr?
        respond_to do |format|
          format.js { 
          	render :template => "remote_content/refresh_order_items.js.erb"
          }
          format.html{
            redirect_to admin_order_path(@order)
          }
        end
      else
      	redirect_to admin_orders_path
      end
    else
      redirect_to admin_order_path(@order)
  	end
  end

  private
  def find_actual_cost
    @order_item = Admin::OrderItem.find(params[:id])
    @variant = Admin::Variant.find(@order_item.variant_id)
    @product = @variant.product
    @order=Admin::Order.find(@order_item.order_id)
    if params[:admin_order_item].present?
      if params[:admin_order_item][:pincode].present?
        @product = @variant.product
        @selling_price = @variant.inventories.first.selling_price 
        @selling_price = Admin::Product.user_price(params[:admin_order_item][:pincode], @product, @variant)
        @cart_item.update(:per_qty_price => @selling_price)
      end
    end
  end

  def order_item_params
    params.require(:admin_order_item).permit(:variant_id, :order_id, :qty, :price, :return_due)
  end

end
