class Admin::AddressesController < ApplicationController
  before_action :sections
  before_action :authenticate_user!

  def index
    @context = context
    @addresses = Admin::Address.all
  end

  def show
    @context = context
    @address = context.addresses.find(params[:id])
  end

  def new
    @context = context
    @address = @context.addresses.new
  end

  def edit
    @context = context
    @address = context.addresses.find(params[:id])
  end

  def create
    @context = context
    @address = @context.addresses.new(address_params)
    if @address.save
      redirect_to context_url(context), notice: "The address has been successfully created."
    end
  end

  def update
    @context= context
    @address= @context.addresses.find(params[:id])
    if @address.update(address_params)
      redirect_to context_url(context), notice: 'Address was successfully updated.'
    else
      render 'edit'
    end
  end

  def destroy
    if @address.destroy
      redirect_to addresses_url, notice: 'Address was successfully destroyed.' 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address
      @address = Admin::Address.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def address_params
      params.require(:address).permit(:add_line1, :add_line2, :state, :country, :c_code, :phone, :connection)
    end

    def context
      if params[:vendor_id]
        id = params[:vendor_id]
        Admin::Vendor.find(params[:vendor_id])
      elsif params[:user_id]
        id = params[:user_id]
        Admin::User.find(params[:user_id])
      else
        id= params[:warehouse_id]
        Admin::Warehouse.find(params[:warehouse_id])
      end
    end 

    def context_url(context)
      if Vendor === context
        admin_vendor_addresses_path(context)
      elsif User === context
        admin_user_addresses_path(context)
      else
        admin_warehouse_addresses_path(context)
      end
    end
end
