class Admin::FailureTransactionsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_failure_transaction, only: [:show, :edit, :update, :destroy]

  
  def index
    @admin_failure_transactions = Admin::FailureTransaction.all
  end

  def show
  end

  def new
    @admin_failure_transaction = Admin::FailureTransaction.new
  end

  def edit
  end

  def create
    @admin_failure_transaction = Admin::FailureTransaction.new(admin_failure_transaction_params)

    if @admin_failure_transaction.save
      redirect_to @admin_failure_transaction, notice: 'Failure transaction was successfully created.' 
    else
      render :new
    end
  end

  def update
      if @admin_failure_transaction.update(admin_failure_transaction_params)
        redirect_to @admin_failure_transaction, notice: 'Failure transaction was successfully updated.' 
      else
        render :edit
      end
  end

  def destroy
    if @admin_failure_transaction.destroy
      redirect_to admin_failure_transactions_path, notice: 'Failure transaction was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_failure_transaction
      @admin_failure_transaction = Admin::FailureTransaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_failure_transaction_params
      params.require(:admin_failure_transaction).permit(:txnid, :net_amount, :user_id, :mode, :status, :addedon, :productinfo, :name_on_card, :cardnum )
    end
end