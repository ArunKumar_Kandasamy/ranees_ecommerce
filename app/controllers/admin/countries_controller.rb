class Admin::CountriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_country, only: [:show, :edit, :update, :destroy]
  before_action :authorize

  def index
    @param = params[:q]
    @search =  Admin::Country.all.search(params[:q])
    @countries = @search.result
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_countries_path
      @render_file = 'admin/countries/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/countries/index"
    end
  end

  def import
    Admin::Country.import(params[:file])
    redirect_to admin_countries_path, notice: "Countries imported."
  end

  def show
    if params[:q].present?
       respond_to do |format|
        format.html
        format.json { 
          render :json => @country.pincodes.tokens(params[:q])
        }
      end
    else
      @states = @country.states
      if request.xhr?
        @render_path = admin_country_path(@country)
        @render_file = 'admin/countries/show'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "admin/countries/show"
      end  
    end
  end

  def new
    @country = Admin::Country.new
    if request.xhr?
      @render_path = new_admin_country_path
      @render_file = 'admin/countries/new'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/countries/new"
    end
  end

  def edit
    if request.xhr?
      @render_path = edit_admin_country_path(@country)
      @render_file = 'admin/countries/edit'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/countries/edit"
    end
  end

  def create
    @country = Admin::Country.new(country_params)
    if @country.save
      @states = @country.states
      flash[:alert] = 'Country was successfully created.'
      @render_path = admin_country_path(@country)
      @render_file = 'admin/countries/show'
      render "remote_content/common_render.js.erb"          
    else
      @country = Admin::Country.new
      render :template => "admin/countries/new"
    end
  end

  def update
    if @country.update(country_params)
      @states = @country.states
      flash[:alert] = 'Country was successfully updated.'
      @render_path = admin_country_path(@country)
      @render_file = 'admin/countries/show'
      render "remote_content/common_render.js.erb"  
    else
      render :template => "admin/countries/edit"
    end
  end

  def find_state_city_pincode
    if params[:country_id].present?
        @for_select = "country"
        @country = Admin::Country.find_by_name(params[:country_id])
        @states = @country.states
    elsif params[:state_id].present?
         @for_select = "state"
        @state = Admin::State.find_by_name(params[:state_id])
        @district_cities = @state.district_cities
    elsif params[:city_id].present?
         @for_select = "city"
        @city = Admin::DistrictCity.find_by_name(params[:city_id])
        @pincodes = @city.pincodes
    end
    render :template => "remote_content/country_select.js.erb"
  end

  def destroy
    @country.destroy
    flash[:alert] = 'Country was successfully destroyed.'
    @render_path = admin_countries_path
    @render_file = 'admin/countries/index'
    render "remote_content/common_render.js.erb"
  end

  private
    def set_country
      @country = Admin::Country.find(params[:id])
    end

    def country_params
      params.require(:admin_country).permit(:name,:country_code)
    end
end