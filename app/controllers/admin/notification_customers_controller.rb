class Admin::NotificationCustomersController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_notification_customer, only: [:show, :edit, :update, :destroy]

  def index
    @admin_notification_customers = Admin::NotificationCustomer.all
  end

  def show   
    if request.xhr?
      @render_path = admin_notification_customer_path(@admin_notification_customer)
      @render_file = 'admin/notification_customers/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/notification_customers/show"
    end
  end

  def new
    @admin_notification_customers = Admin::NotificationCustomer.all
    if @admin_notification_customers.first.present?
      @admin_notification_customer = @admin_notification_customers.first
    else
      @admin_notification_customer = Admin::NotificationCustomer.new
    end
    if request.xhr?
      unless params[:content_for].present?
        @render_path = new_admin_notification_customer_path(@admin_notification_customer)
        @render_file = 'admin/notification_customers/new'
        render "remote_content/common_render.js.erb"     
      else
        @content_for = params[:content_for]
        render "remote_content/notification_content_form.js.erb"
      end     
    else
      render :template => "admin/notification_customers/new"
    end
  end 

  def edit
    if request.xhr?
       unless params[:content_for].present?
        @render_path = edit_admin_notification_customer_path(@admin_notification_customer)
        @render_file = 'admin/notification_customers/edit'
        render "remote_content/common_render.js.erb"    
      else
        @content_for = params[:content_for]
        render "remote_content/notification_content_form.js.erb"
      end            
    else
      render :template => "admin/notification_customers/edit"
    end
  end

  def create
    @admin_notification_customer = Admin::NotificationCustomer.new(admin_notification_customer_params)
    if @admin_notification_customer.save
      flash[:alert] = 'Notification customer was successfully created.'
      @render_path = admin_notification_customer_path(@admin_notification_customer)
      @render_file = 'admin/notification_customers/show'
      render "remote_content/common_render.js.erb"          
    else
      @render_path = new_admin_notification_customer_path(@admin_notification_customer)
      @render_file = 'admin/notification_customers/new'
      render "remote_content/common_render.js.erb"  
    end
  end

  def update
    if @admin_notification_customer.update(admin_notification_customer_params)
      flash[:alert] = 'Notification customer was successfully updated.' 
      @render_path = admin_notification_customer_path(@admin_notification_customer)
      @render_file = 'admin/notification_customers/show'
      render "remote_content/common_render.js.erb"
    else
      render_path = edit_admin_notification_customer_path(@admin_notification_customer)
      @render_file = 'admin/notification_customers/edit'
      render "remote_content/common_render.js.erb"  
    end
  end

  def destroy
    if @admin_notification_customer.destroy
      redirect_to admin_notification_customers_url, notice: 'Notification customer was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_notification_customer
      @admin_notification_customer = Admin::NotificationCustomer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_notification_customer_params
      params.require(:admin_notification_customer).permit(:replacement_order, :order_create,  :partial_refund, :post_deli_cancel, :tracking_updated, :order_success, :order_failed, :pre_deli_cancel, :refund_complete, :user_id, :content_for_order_create, :content_for_order_placed, :content_for_tracking_updated, :content_for_order_success, :content_for_order_failed, :content_for_pre_deli_cancel, :content_for_post_deli_cancel, :content_for_refund_complete, :content_for_replacement_order, :content_for_partial_refund, :back_jobs, :content_for_back_jobs)
    end
end 