class Admin::WarehouseDashboardController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_warehouse, only: [:show]
  
  def index 
    if request.xhr?
      @render_path = admin_warehouse_dashboard_index_path
      @render_file = 'admin/warehouse_dashboard/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/warehouse_dashboard/index"
    end 
  end

  def product_index
  end

  def show
  	if current_user.role.name == 'pr' || current_user.role.name == 'sa'
  	elsif  @warehouse.warehouses_roles.first.present?
  		if @warehouse.warehouses_roles.first.role.name != current_user.role.name
  		  redirect_to root_path
  		end
  	else
  	  redirect_to root_path
  	end 
  end

  def orders       
    @cart_items= Array.new
    @order = Admin::Order.find(params[:id])
    @order.consignments.each do | consignment |
      @cart_items << consignment.cart_items
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_warehouse
      @warehouse = Admin::Warehouse.find(params[:id])
    end
end
