class Admin::WarehousesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_warehouse, only: [:show, :edit, :update, :destroy]
  before_action :authorize

  def index
    @warehouses = Admin::Warehouse.all.includes(:addresses)
    @warehouse = Admin::Warehouse.new
    1.times { @warehouse.addresses.build }
    if request.xhr?
      @render_path = admin_warehouses_path
      @render_file = 'admin/warehouses/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/warehouses/index"
    end
  end

  def show
    @addresses = @warehouse.addresses
    @warehouses_roles= Admin::WarehousesRoles.new
    @roles= Admin::Role.where("name != ? AND name != ? AND name != ?", "u", "pr", "sa")
    if request.xhr?
      @render_path = admin_warehouse_path(@warehouse)
      @render_file = 'admin/warehouses/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/warehouses/show"
    end
  end

  def new
    @warehouse = Admin::Warehouse.new
    1.times { @warehouse.addresses.build }
    if request.xhr?
      @render_path = new_admin_warehouse_path
      @render_file = 'admin/warehouses/new'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/warehouses/new"
    end
  end

  def edit 
    @method_filters= @warehouse.addresses
    if request.xhr?
      @render_path = edit_admin_warehouse_path(@warehouse)
      @render_file = 'admin/warehouses/edit'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/warehouses/edit"
    end
  end

  def create
    @warehouse = Admin::Warehouse.new(warehouse_params)
    if @warehouse.save
      @render_path = admin_warehouse_path(@warehouse)
      @addresses = @warehouse.addresses
      @warehouses_roles= Admin::WarehousesRoles.new
      @roles= Admin::Role.where("name != ? AND name != ? AND name != ?", "u", "pr", "sa")
      flash['alert'] = 'Warehouse was successfully created.' 
      @render_file = 'admin/warehouses/show'
      render "remote_content/common_render.js.erb"
    else
      @render_path = new_admin_warehouse_path
      @render_file = 'admin/warehouses/new'
      render "remote_content/common_render.js.erb"
    end
  end

  def update
    if @warehouse.update(warehouse_params)
      flash['alert'] = 'Warehouse was successfully updated.' 
      @addresses = @warehouse.addresses
      @warehouses_roles= Admin::WarehousesRoles.new
      @roles= Admin::Role.where("name != ? AND name != ? AND name != ?", "u", "pr", "sa")
      @render_path = admin_warehouse_path(@warehouse)
      @render_file = 'admin/warehouses/show'
      render "remote_content/common_render.js.erb"
    else
      @render_path = edit_admin_warehouse_path(@warehouse)
      @render_file = 'admin/warehouses/edit'
      render "remote_content/common_render.js.erb"
    end
  end

  def destroy
    if @warehouse.destroy
      @warehouses = Admin::Warehouse.all.includes(:addresses)
      @warehouse = Admin::Warehouse.new
      1.times { @warehouse.addresses.build }
      flash['alert'] = 'Warehouse was successfully destroyed.' 
      @render_path = admin_warehouses_path
      @render_file = 'admin/warehouses/index'
      render "remote_content/common_render.js.erb"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_warehouse
      @warehouse = Admin::Warehouse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def warehouse_params
      params.require(:admin_warehouse).permit(:name, addresses_attributes:[:id , :_destroy ,:add_line1, :add_line2, :state, :country, :c_code, :phone, :connection],consignments_attributes:[:conform ,:id ,:order_id])
    end
end