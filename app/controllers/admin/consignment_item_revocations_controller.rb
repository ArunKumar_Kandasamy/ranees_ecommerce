class Admin::ConsignmentItemRevocationsController < ApplicationController
  before_action :set_admin_consignment_item_revocation, only: [:show, :edit, :update, :destroy]

  def index
    @admin_consignment_item_revocations = Admin::ConsignmentItemRevocation.all
  end

  def show
  end

  def new
    @admin_consignment_item_revocation = Admin::ConsignmentItemRevocation.new
  end

  def edit
  end

  def create
    @admin_consignment_item_revocation = Admin::ConsignmentItemRevocation.new(admin_consignment_item_revocation_params)

    respond_to do |format|
      if @admin_consignment_item_revocation.save
        format.html { redirect_to @admin_consignment_item_revocation, notice: 'Consignment item revocation was successfully created.' }
        format.json { render :show, status: :created, location: @admin_consignment_item_revocation }
      else
        format.html { render :new }
        format.json { render json: @admin_consignment_item_revocation.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @admin_consignment_item_revocation.update(admin_consignment_item_revocation_params)
        format.html { redirect_to @admin_consignment_item_revocation, notice: 'Consignment item revocation was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_consignment_item_revocation }
      else
        format.html { render :edit }
        format.json { render json: @admin_consignment_item_revocation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @admin_consignment_item_revocation.destroy
    respond_to do |format|
      format.html { redirect_to admin_consignment_item_revocations_url, notice: 'Consignment item revocation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_admin_consignment_item_revocation
      @admin_consignment_item_revocation = Admin::ConsignmentItemRevocation.find(params[:id])
    end

    def admin_consignment_item_revocation_params
      params.require(:admin_consignment_item_revocation).permit(:consignment_revocation_id, :price, :qty, :variant_id, :per_qty_price, :compensate, :revocation_mode, :bank_account_id, :status)
    end
end
