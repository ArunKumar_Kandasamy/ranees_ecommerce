class Admin::ShippingStatesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_shipping_state, only: [:show, :edit, :update, :destroy]
  before_action :find_admin_shipping_plan
  before_action :find_admin_shipping_country

  def index
    @admin_shipping_states = Admin::ShippingState.all
  end

  def show
    respond_to do |format|
      format.html{
        @admin_shipping_countries = @admin_shipping_plan.countries
        @admin_shipping_country = Admin::ShippingCountry.new
        @admin_shipping_state = Admin::ShippingState.new        
      }
      format.js {
        @filter_states = @admin_shipping_country.country.states
        @filter_cities = @admin_shipping_state.state.district_cities.map{|x| x.id }
        @filter_admin_city = @admin_shipping_state.shipping_cities.map{|x| x.district_city_id}
        @filter_cities = @filter_cities - @filter_admin_city
        @filter_results = @admin_shipping_state.state.district_cities.find(@filter_cities)
        @admin_city = @admin_shipping_state.shipping_cities
        @admin_shipping_city = Admin::ShippingCity.new
        if @admin_city.present?
          @filter_cities = @filter_cities
        else
          @admin_shipping_state.shipping_cities.build
        end
        @admin_shipping_states = @admin_shipping_country.shipping_states
        render :template => "remote_content/shipping_cities.js.erb"
      }
    end
  end

  def new
    @admin_shipping_state = Admin::ShippingState.new
  end

  def edit
  end

  def create
    @admin_shipping_countries = @admin_shipping_plan.shipping_countries
    @admin_shipping_state = @admin_shipping_country.shipping_states.build(admin_shipping_state_params)
    if @admin_shipping_state.save
      respond_to do |format|
        format.html{
          redirect_to admin_shipping_plan_path(@admin_shipping_plan), notice: 'Shipping state was successfully created.'
        }
        format.js {
          flash[:notice] = "Shipping state was successfully created."
          render :template => "remote_content/shipping_countries_success.js.erb"
          flash.discard
        }
      end
    end
  end

  def update
    @admin_shipping_countries = @admin_shipping_plan.shipping_countries
    if @admin_shipping_state.update(admin_shipping_state_params)
      respond_to do |format|
        format.html{
          redirect_to admin_shipping_plan_path(@admin_shipping_plan), notice: 'Shipping cost was successfully created.'
        }
        format.js {
          flash[:notice] = "Shipping cost was successfully created."
          render :template => "remote_content/shipping_countries_success.js.erb"
          flash.discard
        }
      end
    end
  end

  def destroy
    if @admin_shipping_state.destroy
      redirect_to admin_shipping_plan_path(@admin_shipping_plan), notice: 'Shipping state was successfully destroyed.' 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_shipping_state
      @admin_shipping_state = Admin::ShippingState.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_shipping_state_params
      params.require(:admin_shipping_state).permit(:state_id, shipping_cities_attributes:[:id, :shipping_state_id, :cost, :district_city_id, :status])
    end

    def find_admin_shipping_country
      @admin_shipping_country = Admin::ShippingCountry.find(params[:shipping_country_id])
    end

    def find_admin_shipping_plan
      @admin_shipping_plan = Admin::ShippingPlan.find(params[:shipping_plan_id])
    end
end
