class Admin::PickupConsignmentInvoicesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_pickup_consignment_invoice, only: [:show, :edit, :update, :destroy]

  # GET /admin/pickup_consignment_invoices
  # GET /admin/pickup_consignment_invoices.json
  def index
    @admin_pickup_consignment_invoices = Admin::PickupConsignmentInvoice.all
  end

  # GET /admin/pickup_consignment_invoices/1
  # GET /admin/pickup_consignment_invoices/1.json
  def show
  end

  # GET /admin/pickup_consignment_invoices/new
  def new
    @admin_pickup_consignment_invoice = Admin::PickupConsignmentInvoice.new
  end

  # GET /admin/pickup_consignment_invoices/1/edit
  def edit
  end

  # POST /admin/pickup_consignment_invoices
  # POST /admin/pickup_consignment_invoices.json
  def create
    @admin_pickup_consignment_invoice = Admin::PickupConsignmentInvoice.new(admin_pickup_consignment_invoice_params)

    respond_to do |format|
      if @admin_pickup_consignment_invoice.save
        format.html { redirect_to @admin_pickup_consignment_invoice, notice: 'Pickup consignment invoice was successfully created.' }
        format.json { render :show, status: :created, location: @admin_pickup_consignment_invoice }
      else
        format.html { render :new }
        format.json { render json: @admin_pickup_consignment_invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/pickup_consignment_invoices/1
  # PATCH/PUT /admin/pickup_consignment_invoices/1.json
  def update
    respond_to do |format|
      if @admin_pickup_consignment_invoice.update(admin_pickup_consignment_invoice_params)
        format.html { redirect_to @admin_pickup_consignment_invoice, notice: 'Pickup consignment invoice was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_pickup_consignment_invoice }
      else
        format.html { render :edit }
        format.json { render json: @admin_pickup_consignment_invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/pickup_consignment_invoices/1
  # DELETE /admin/pickup_consignment_invoices/1.json
  def destroy
    @admin_pickup_consignment_invoice.destroy
    respond_to do |format|
      format.html { redirect_to admin_pickup_consignment_invoices_url, notice: 'Pickup consignment invoice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_pickup_consignment_invoice
      @admin_pickup_consignment_invoice = Admin::PickupConsignmentInvoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_pickup_consignment_invoice_params
      params.require(:admin_pickup_consignment_invoice).permit(:pickup_consignment_id, :barcode, :invoice_number)
    end
end
