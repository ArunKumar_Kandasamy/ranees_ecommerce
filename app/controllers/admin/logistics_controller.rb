class Admin::LogisticsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize 
  before_action :set_admin_logistic, only: [:show, :edit, :update, :destroy]

  def index 
    @admin_logistics = Admin::Logistic.all
    @admin_logistic = Admin::Logistic.new
    if request.xhr?
      @render_path = admin_logistics_path
      @render_file = 'admin/logistics/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/logistics/index"
    end
  end
  
  def show
    if request.xhr?
      @render_path = admin_logistic_path(@admin_logistic)
      @render_file = 'admin/logistics/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/logistics/show"
    end
  end

  def new
    @admin_logistic = Admin::Logistic.new
    if request.xhr?
      @render_path = new_admin_logistic_path
      @render_file = 'admin/logistics/new'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/logistics/new"
    end
  end

  def edit
    if request.xhr?
      @render_path = edit_admin_logistic_path(@admin_logistic)
      @render_file = 'admin/logistics/edit'
      render "remote_content/common_modal_render.js.erb"         
    else
      render :template => "admin/logistics/edit"
    end
  end

  def create
    @admin_logistic = Admin::Logistic.new(admin_logistic_params)
    if @admin_logistic.save
      if request.xhr?
        @admin_logistics = Admin::Logistic.all
        @admin_logistic = Admin::Logistic.new
        flash[:alert] = 'Logistic was successfully created.' 
        @render_path = admin_logistics_path
        @render_file = 'admin/logistics/index'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "admin/logistics/show"
      end
    else
      @admin_logistics = Admin::Logistic.all
      @admin_logistic = Admin::Logistic.new
      flash[:alert] = 'Logistic cannot be created.' 
      @render_path = admin_logistics_path
      @render_file = 'admin/logistics/index'
      render "remote_content/common_render.js.erb"
    end
  end

  def update
    if @admin_logistic.update(admin_logistic_params)
      @admin_logistics = Admin::Logistic.all
      @admin_logistic = Admin::Logistic.new
      flash[:alert] = 'Logistic was successfully updated.' 
      @render_path = admin_logistics_path
      @render_file = 'admin/logistics/index'
      render "remote_content/common_render.js.erb"          
    else
      @render_path = edit_admin_logistic_path(@admin_logistic)
      @render_file = 'admin/logistics/edit'
      render "remote_content/common_modal_render.js.erb" 
    end
  end

  def destroy
    @admin_logistic.destroy
    @admin_logistics = Admin::Logistic.all
    @admin_logistic = Admin::Logistic.new
    flash[:alert] = 'Logistic was successfully destroyed.' 
    @render_path = admin_logistics_path
    @render_file = 'admin/logistics/index'
    render "remote_content/common_render.js.erb"          
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_logistic
      @admin_logistic = Admin::Logistic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_logistic_params 
      params.require(:admin_logistic).permit(:name, :site, :status)
    end
end
