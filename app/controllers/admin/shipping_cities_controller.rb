class Admin::ShippingCitiesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_shipping_city, only: [:show, :edit, :update, :destroy]
  before_action :find_admin_shipping_plan
  before_action :find_admin_shipping_country
  before_action :find_admin_shipping_state

  def index
    @admin_shipping_cities = Admin::ShippingCity.all
  end

  def show
    respond_to do |format|
      format.html{
        @admin_shipping_countries = @admin_shipping_plan.countries
        @admin_shipping_country = Admin::ShippingCountry.new
        @admin_shipping_state = Admin::ShippingState.new
        @admin_shipping_city = Admin::Shipping.City.new        
      }
      format.js {        
        @filter_cities = @admin_shipping_state.state.district_cities
        @filter_costs = @admin_shipping_city.district_city.pincodes
        @filter_admin_cost = @admin_shipping_city.shipping_costs.map{|x| x.pincode_id}
        @admin_cost = @admin_shipping_city.shipping_costs
        @admin_shipping_cost = Admin::ShippingCost.new
        if @admin_cost.present?
          @filter_cities = @filter_cities
        else
          @admin_shipping_city.shipping_costs.build
        end
        @admin_shipping_states = @admin_shipping_country.shipping_states
        render :template => "remote_content/shipping_costs.js.erb"
      }
    end
  end

  def new
    @admin_shipping_city = Admin::ShippingCity.new
  end

  def edit
  end

  def create 
    @admin_shipping_countries = @admin_shipping_plan.shipping_countries     
    @admin_shipping_city = @admin_shipping_state.shipping_cities.build(admin_shipping_city_params)    
    if @admin_shipping_city.save      
      respond_to do |format|
        format.html{
          redirect_to admin_shipping_plan_path(@admin_shipping_plan), notice: 'Shipping city was successfully created.'
        }
        format.js {
          flash[:notice] = "Shipping city was successfully created."
          render :template => "remote_content/shipping_countries_success.js.erb"
          flash.discard
        }
      end    
    end
  end

  def update
    @admin_shipping_countries = @admin_shipping_plan.shipping_countries
    @filter_states = @admin_shipping_country.country.states
    @filter_cities = @admin_shipping_state.state.district_cities
    if @admin_shipping_city.update(admin_shipping_city_params)
      respond_to do |format|
        format.html{
          redirect_to admin_shipping_plan_path(@admin_shipping_plan), notice: 'Shipping cost was successfully created.'
        }
        format.js {
          flash[:notice] = "Shipping cost was successfully created."
          render :template => "remote_content/shipping_countries_success.js.erb"
          flash.discard
        }
      end
    else
      render 'edit'
    end
  end

  def destroy
    if @admin_shipping_city.destroy
      redirect_to admin_shipping_plan_path(@admin_shipping_plan), notice: 'Shipping city was successfully destroyed.'
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_shipping_city
      @admin_shipping_city = Admin::ShippingCity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_shipping_city_params
      params.require(:admin_shipping_city).permit(:district_city_id,shipping_costs_attributes:[:id, :shipping_district_city_id, :cost, :pincode_id, :cost, :status, :cod])
    end

    def find_admin_shipping_state
      @admin_shipping_state = Admin::ShippingState.find(params[:shipping_state_id])
    end

    def find_admin_shipping_country
      @admin_shipping_country = Admin::ShippingCountry.find(params[:shipping_country_id])
    end

    def find_admin_shipping_plan
      @admin_shipping_plan = Admin::ShippingPlan.find(params[:shipping_plan_id])
    end
end
