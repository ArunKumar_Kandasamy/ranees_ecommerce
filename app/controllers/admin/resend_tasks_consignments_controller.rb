class Admin::ResendTasksConsignmentsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_resend_tasks_consignment, only: [:show, :edit, :update, :destroy]

  # GET /admin/resend_tasks_consignments
  # GET /admin/resend_tasks_consignments.json
  def index
    @admin_resend_tasks_consignments = Admin::ResendTasksConsignment.all
  end

  # GET /admin/resend_tasks_consignments/1
  # GET /admin/resend_tasks_consignments/1.json
  def show
  end

  # GET /admin/resend_tasks_consignments/new
  def new
    @admin_resend_tasks_consignment = Admin::ResendTasksConsignment.new
  end

  # GET /admin/resend_tasks_consignments/1/edit
  def edit
  end

  # POST /admin/resend_tasks_consignments
  # POST /admin/resend_tasks_consignments.json
  def create
    @admin_resend_tasks_consignment = Admin::ResendTasksConsignment.new(admin_resend_tasks_consignment_params)

    respond_to do |format|
      if @admin_resend_tasks_consignment.save
        format.html { redirect_to @admin_resend_tasks_consignment, notice: 'Resend tasks consignment was successfully created.' }
        format.json { render :show, status: :created, location: @admin_resend_tasks_consignment }
      else
        format.html { render :new }
        format.json { render json: @admin_resend_tasks_consignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/resend_tasks_consignments/1
  # PATCH/PUT /admin/resend_tasks_consignments/1.json
  def update
    respond_to do |format|
      if @admin_resend_tasks_consignment.update(admin_resend_tasks_consignment_params)
        format.html { redirect_to @admin_resend_tasks_consignment, notice: 'Resend tasks consignment was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_resend_tasks_consignment }
      else
        format.html { render :edit }
        format.json { render json: @admin_resend_tasks_consignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/resend_tasks_consignments/1
  # DELETE /admin/resend_tasks_consignments/1.json
  def destroy
    @admin_resend_tasks_consignment.destroy
    respond_to do |format|
      format.html { redirect_to admin_resend_tasks_consignments_url, notice: 'Resend tasks consignment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_resend_tasks_consignment
      @admin_resend_tasks_consignment = Admin::ResendTasksConsignment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_resend_tasks_consignment_params
      params.require(:admin_resend_tasks_consignment).permit(:consignment_id, :resend_task_id)
    end
end
