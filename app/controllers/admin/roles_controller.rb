class Admin::RolesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_role, only: [:show, :edit, :update, :destroy]
  before_action :authorize  
  #before_action :role_check, only: [:show, :edit, :update, :destroy]

  def index   
    flash.discard
    @param = params[:q]   
    #@search = Admin::Role.where("name != ? AND name != ? AND name != ?", "sa", "u", "pr").search(params[:q])
    @search = Admin::Role.all.search(params[:q])
    @roles = @search.result.page(params[:page])
    @role= Admin::Role.order(:name)
    @roles.each do |role|
      if role.name == "sa"
        role.name= t :sa
      elsif(role.name== "u")
        role.name= t :u
      elsif(role.name == "pr") 
        role.name= t :pr
      else
        role.name= role.name
      end
    end
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_roles_path
      @render_file = 'admin/roles/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/roles/index"
    end
  end

  def show
    flash.discard
    Rails.application.eager_load!
    @block_lists = @role.block_lists
    @block_list = Admin::BlockList.new
    if request.xhr?
      #render :template => "remote_content/blocklist.js.erb"
      @render_path = admin_role_path(@role)
      @render_file = 'admin/roles/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/roles/show"
    end
  end

  def for_blocklist 
    flash.discard
    if request.xhr?
      if params[:url].present?
        @extracted_path =  Rails.application.routes.recognize_path(params[:url])
        @extracted_path[:controller] = @extracted_path[:controller].gsub('_',' ').titleize.gsub(' ','').split("/").join("::").to_s + "Controller"  
        @controller = @extracted_path[:controller]
        @extracted_path[:path] = params[:url]
        @action = @extracted_path[:action] 
        render :json => @extracted_path.to_json
      else
        @role=current_user.role
        @block_lists = @role.block_lists
        render "remote_content/blocklist.js.erb"
      end
    end
  end

  def new
    flash.discard
    @role = Admin::Role.new
    @check1 = params[:check] unless params[:check].nil?
    @met = params[:act].constantize unless params[:act].nil?
    if @met != ''
      @met1 = @met.action_methods.each {|m| puts m } unless params[:act].nil?
    end
    if @check1.present?
      @met2 = @met1.reject{|y| @check1.include? y}  unless @check1.nil?
      @met2 ||= @met1
    else
      @met2 ||= @met1
    end
    if @met1.present?
      render :json => @met2.to_json
    else
      if request.xhr?
        @render_path = new_admin_role_path
        @render_file = 'admin/roles/new'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "admin/roles/new"
      end
    end
  end

  def edit
    flash.discard
    @method_filters= @role.block_lists
    if @role.name == "sa"
      @role.name= t :sa
    elsif(@role.name== "u")
      @role.name= t :u
    elsif(@role.name == "pr")
      @role.name= t :pr
    else
      @role.name= @role.name
    end
    @check1 = params[:check] unless params[:check].nil?
    @met = params[:act].constantize unless params[:act].nil?
    if @met != ''
      @met1 = @met.action_methods.each {|m| puts m } unless params[:act].nil?
    end
    if @check1.present?
      @met2 = @met1.reject{|y| @check1.include? y}  unless @check1.nil?
      @met2 ||= @met1
    else
      @met2 ||= @met1
    end
    if @met1.present?
      render :json => @met2.to_json
    else
      if request.xhr?
        @render_path = edit_admin_role_path
        @render_file = 'admin/roles/edit'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "admin/roles/edit"
      end
    end
  end

  def create   
    flash.discard
    @role = Admin::Role.new(role_params)      
    if @role.save
      @block_lists = @role.block_lists
      @block_list = Admin::BlockList.new
      if request.xhr?
        flash[:alert] = 'Role was successfully created.'  
        @render_path = admin_role_path(@role)
        @render_file = 'admin/roles/show'
        render "remote_content/common_render.js.erb"  
      else
        render :template => 'admin/roles/new'
      end  
    else
      render :template => 'admin/roles/new'
    end
  end

  def update
    flash.discard
    @block_list = Admin::BlockList.new
    if @role.update(role_params)
      @block_lists = @role.block_lists
      flash[:alert] = 'Role was successfully updated.' 
      @render_path = admin_role_path(@role)
      @render_file = 'admin/roles/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template =>'edit'
    end
  end

  def destroy
    if @role.destroy
      redirect_to admin_roles_url, notice: 'Role was successfully destroyed.' 
    end
  end

  def append_feild
    @add = params[:add] unless params[:add].nil?
    respond_to do |format|
      format.json { 
        render :json => @add.to_json
      }
      format.html {}
    end
  end

  private
    def role_check
      @current_role=current_user.role
      @param_role= Admin::Role.find(params[:id])
      if (@current_role.name != "pr" || @current_role.name != "sa")
        if (@param_role.name== "pr" || @param_role.name == "sa" || @param_role.name == "u")
        redirect_to admin_roles_path, notice: 'Not allowed to Perform this action'
        end
      end
    end

    def set_role
      @role = Admin::Role.find(params[:id])
    end

    def role_params
      params.require(:admin_role).permit(:name, :description, block_lists_attributes:[:id, :con_name, :met_name, :role_id, :_destroy])
    end
end