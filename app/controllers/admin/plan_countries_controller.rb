class Admin::PlanCountriesController < ApplicationController 
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_availability_plan
  before_action :set_admin_plan_country, only: [:show, :edit, :update, :destroy]

  # GET /admin/plan_countries
  # GET /admin/plan_countries.json
  def index
    @admin_plan_countries = Admin::PlanCountry.all
  end

  # GET /admin/plan_countries/1
  # GET /admin/plan_countries/1.json
  def show
    if request.xhr?
      @render_path = admin_availability_plan_plan_country_path(@admin_availability_plan,@admin_plan_country)
      @render_file = 'admin/plan_countries/show'
      render "remote_content/common_render.js.erb"
    else
      render :template => "admin/plan_countries/show"
    end
  end

  # GET /admin/plan_countries/new
  def new
    @admin_plan_country = Admin::PlanCountry.new 
  end

  # GET /admin/plan_countries/1/edit
  def edit
  end

  # POST /admin/plan_countries
  # POST /admin/plan_countries.json
  def create
    @admin_plan_country = Admin::PlanCountry.new(admin_plan_country_params)

    respond_to do |format|
      if @admin_plan_country.save
        format.html { redirect_to @admin_plan_country, notice: 'Plan country was successfully created.' }
        format.json { render :show, status: :created, location: @admin_plan_country }
      else
        format.html { render :new }
        format.json { render json: @admin_plan_country.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/plan_countries/1
  # PATCH/PUT /admin/plan_countries/1.json
  def update
    if @admin_plan_country.update(admin_plan_country_params)
      flash[:alert] = 'Plan country successfully updated.'       
    else
      flash[:alert] = 'Plan country cannot be updated.' 
    end
    if request.xhr?
      @render_path = admin_availability_plan_plan_country_path(@admin_availability_plan,@admin_plan_country)
      @render_file = 'admin/plan_countries/show'
      render "remote_content/common_render.js.erb"
    else
      render :template => "admin/plan_countries/show"
    end
  end

  # DELETE /admin/plan_countries/1
  # DELETE /admin/plan_countries/1.json
  def destroy
    @admin_plan_country.destroy
    respond_to do |format|
      format.html { redirect_to admin_plan_countries_url, notice: 'Plan country was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_availability_plan
      @admin_availability_plan = Admin::AvailabilityPlan.find(params[:availability_plan_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_admin_plan_country
      @admin_plan_country = Admin::PlanCountry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_plan_country_params
      params.require(:admin_plan_country).permit(:availability_plan_id, :country_id, :all_place, country_pincodes_attributes:[:id, :plan_country_id, :pincode, :availability, :_destroy])
    end
end
