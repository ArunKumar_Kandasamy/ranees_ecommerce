class Admin::PackageTasksConsignmentsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_package_task, only: [:destroy]

  def index
  end

  def destroy
    @admin_package_task_consignment.destroy
    respond_to do |format|
      format.html { redirect_to admin_warehouse_dashboard_package_tasks_path(@warehouse), notice: 'Consignment Removed from Package task .' }
    end
  end

  private

	def set_admin_package_task
	  @admin_package_task_consignment = Admin::PackageTasksConsignments.find(params[:id])
	  @admin_package_task = @admin_package_task_consignment.package_task
	  @warehouse = @admin_package_task.warehouse
	end

end
