class Admin::CategoriesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_category, only: [:show, :edit, :update, :destroy]
  after_action :check_for_activation, only: [ :update]
  after_action :category_activation, only: [:update]

  def index  
    @admin_categories = Admin::Category.all
    @admin_parents = @admin_categories.roots
    if request.xhr?
      @render_path = admin_categories_path
      @render_file = 'admin/categories/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/categories/index" 
    end
  end  

  def make_active   
    @admin_category = Admin::Category.find(params[:category_id])
    if @admin_category.children.present?
      @children= @admin_category.children
    end 
    category= Admin::Category.where(:id => params[:category_id]).first
    if category.present?
      if category.active == false
        category.update_attributes(:active => true)
        @notice =  "Category was made visible"
      else
        category.update_attributes(:active => false)
        @notice =  "Category was made invisible"
      end
    end 
      flash['alert'] = @notice
      @admin_categories = Admin::Category.all
      @admin_parents = @admin_categories.roots
      @render_path = admin_categories_path
      @render_file = 'admin/categories/index'
      render "remote_content/common_render.js.erb"      
  end
  
  def remove_product_from_category
    @admin_category = Admin::Category.find(params[:id])
    @products = @admin_category.categories_products.select(:name,:description,:status,:id)
    product = @admin_category.products.find(params[:product_id]) 
    if request.xhr?
      if product
        @admin_category.products.delete(product)
        if @admin_category.children.present?
          @children= @admin_category.children
        end
        @admin_categories =Admin::Category.all
        flash['error'] = 'Product was successfully Removed form this Category.'
        @render_path = admin_categories_path(@admin_category)
        @render_file = 'admin/categories/show'
        render :template => "remote_content/admin_category_show.js.erb"
      end
    end
  end

  def to_display
    product= Admin::Product.where(:id => params[:product_id]).first
    if product.present?
      if product.visibility == false
        product.update_attributes(:visibility => true)
        redirect_to admin_categories_path, notice: "Product was made visible"
      else
        product.update_attributes(:visibility => false)
        redirect_to admin_categories_path, notice: "Product was made invisible"
      end
    end
  end

  def show
    @admin_category.activate_category
    if @admin_category.children.present?
      @children= @admin_category.children
    end 
    if request.xhr?
      @render_path = admin_category_path(@admin_category)
      @render_file = 'admin/categories/show'
      render :template => "remote_content/admin_category_show.js.erb"      
    else
      render :template => "admin/categories/show"
    end
  end

  def new
    @list_child_categories = Admin::Category.order(:name)
    @general = Admin::General.first
    if @general.present?
      @n = @general.category_level
    else
      @n = 3
    end
    @list_children_categories = @list_child_categories.reject { |h| h.ancestors.size > @n-2 }
    @admin_category = Admin::Category.new
    if request.xhr?
      @render_path = new_admin_category_path
      @render_file = 'admin/categories/new'
      render :template => "remote_content/category_new.js.erb"        
    else
      render :template => "admin/categories/new"
    end
  end

 
  def edit
    @list_child_categories = Admin::Category.where("id != ?", @admin_category.id)
    @general = Admin::General.first
    if @general.present?
      @n = @general.category_level
    else
      @n = 3
    end
    @list_children_categories = @list_child_categories.reject { |h| h.ancestors.size > @n-2 && h.id != @admin_category.parent_id }
    @list_children_categories = @list_children_categories.reject { |h| h.parent_id == @admin_category.id } 
    if @admin_category.parent_id.nil?
      @list_children_categories = @list_child_categories.reject { |h| h }
    end
    if request.xhr?
      @render_path = edit_admin_category_path(@admin_category)
      @render_file = 'admin/categories/edit'
      render :template => "remote_content/category_edit.js.erb"        
    else
      render :template => "admin/categories/edit"
    end
  end

  def create
    @admin_category = Admin::Category.new(admin_category_params)
    if @admin_category.save
      flash.discard
      if @admin_category.pub_time.present?
        @date_now= Time.now
        @admin_pub_date= @admin_category.pub_time
        @diff_time= TimeDifference.between(@admin_pub_date, @date_now).in_hours
        
        HardWorker.perform_in(@diff_time.hour, @admin_category.id)
      end
      @admin_categories = Admin::Category.all
      @admin_parents = @admin_categories.roots
      flash['notice'] = 'Category was successfully created.'
      @selected_tab = params[:admin_category][:selected_tab]
      @render_path = admin_categories_path
      @render_file = 'admin/categories/index'
      render "remote_content/common_render.js.erb"          
    else
      @render_path = new_admin_category_path
      @render_file = 'admin/categories/new'
      render "remote_content/common_render.js.erb"
    end
  end

  def update
    if @admin_category.update(admin_category_params)
      @admin_category.activate_related_category
      flash.discard
      flash['alert'] = 'Category was successfully updated.'
      @selected_tab = params[:admin_category][:selected_tab]
      @admin_categories = Admin::Category.all
      @admin_parents = @admin_categories.roots
      @render_path = admin_categories_path
      @render_file = 'admin/categories/index'
      render "remote_content/common_render.js.erb"           
    else
      @render_path = edit_admin_category_path
      @render_file = 'admin/categories/edit'
      render "remote_content/common_render.js.erb"
    end
  end

  def sort
    params[:admin_category].each_with_index do |id, index|
      Admin::Category.where(id: id).update_all(sort_order: index+1)
    end
    render nothing: true
  end

  def promotion_sort
    params[:admin_category].each_with_index do |id, index|
      Admin::Category.where(id: id).update_all(promotional_sort: index+1)
    end
    render nothing: true
  end

  def destroy
    if @admin_category.destroy
      @admin_categories = Admin::Category.all
      @admin_parents = @admin_categories.roots
      flash['error'] = 'Category was successfully destroyed.'
      @render_path = admin_categories_path
      @render_file = 'admin/categories/index'
      render "remote_content/common_render.js.erb"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_category
      @admin_category = Admin::Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_category_params
      params.require(:admin_category).permit(:status, :name, :description, :page_title, :meta_description, :active, :pub_time, :parent_id)
    end

    def check_for_activation
      @admin_category= Admin::Category.find(params[:id])
      @products = @admin_category.products
      if @products.present?
        @products.each do | product |
          if product.activate_product
            product.status= true
            product.save
          else
            product.status= false
            product.save
          end
        end
      end
    end

    def category_activation
      @admin_category.activate_related_category
    end
end
