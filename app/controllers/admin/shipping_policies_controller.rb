class Admin::ShippingPoliciesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_shipping_policy, only: [:show, :edit, :update, :destroy]

  # GET /admin/shipping_policies
  # GET /admin/shipping_policies.json
  def index
    @admin_shipping_policies = Admin::ShippingPolicy.all
    if request.xhr?
      @render_path = admin_shipping_policies_path
      @render_file = 'admin/shipping_policies/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/shipping_policies/index"
    end  
  end

  # GET /admin/shipping_policies/1
  # GET /admin/shipping_policies/1.json
  def show 
    @shipping_weights = Admin::WeightRange.all
    @shipping_prices = Admin::PriceRange.all 
    @weight_ranges = Admin::WeightRange.all.map{|x| x.range}
    @price_ranges = Admin::PriceRange.all.map{|x| x.range}
    if @admin_shipping_policy.shipping_weights.first.present?
      @existing_ranges = @admin_shipping_policy.shipping_weights.map{|x| x.range}
      @filter_ranges = @weight_ranges - @existing_ranges
      @filter_ranges.each do |filter_range|
        new_row = @admin_shipping_policy.shipping_weights.build
        new_row.range = filter_range
      end
    else
      @admin_shipping_policy.shipping_weights.build
    end
    if @admin_shipping_policy.shipping_prices.first.present?
      @existing_ranges = @admin_shipping_policy.shipping_prices.map{|x| x.range}
      @filter_ranges = @price_ranges - @existing_ranges
      @filter_ranges.each do |filter_range|
        new_row = @admin_shipping_policy.shipping_prices.build
        new_row.range = filter_range
      end
    else
      @admin_shipping_policy.shipping_prices.build
    end
    @existing_countries = @admin_shipping_policy.shipping_specific_countries
    @filter_shipping_countries = Admin::Country.all.map{|x| x.id}
    @existing_countries = @admin_shipping_policy.shipping_specific_countries.map{|x| x.country_id}
    @filter_countries = @filter_shipping_countries - @existing_countries
    @country_list = Admin::Country.find(@filter_countries)
    plan_country = @admin_shipping_policy.shipping_specific_countries.build
    country_pincode = plan_country.shipping_specific_pincodes.build
    if request.xhr?
      @render_path = admin_shipping_policy_path(@admin_shipping_policy)
      @render_file = 'admin/shipping_policies/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/shipping_policies/show"
    end 
  end

  # GET /admin/shipping_policies/new
  def new
    @admin_shipping_policy = Admin::ShippingPolicy.new
    if request.xhr?
      @render_path = new_admin_shipping_policy_path
      @render_file = 'admin/shipping_policies/new'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/shipping_policies/new"
    end 
  end

  # GET /admin/shipping_policies/1/edit
  def edit
  end

  # POST /admin/shipping_policies
  # POST /admin/shipping_policies.json
  def create
    @admin_shipping_policy = Admin::ShippingPolicy.new(admin_shipping_policy_params)

    respond_to do |format|
      if @admin_shipping_policy.save
        format.html { redirect_to @admin_shipping_policy, notice: 'Shipping policy was successfully created.' }
        format.json { render :show, status: :created, location: @admin_shipping_policy }
      else
        format.html { render :new }
        format.json { render json: @admin_shipping_policy.errors, status: :unprocessable_entity }
      end
    end
  end


  def find_shipping_policy_shipping_weight 
    @shipping_weights = Admin::WeightRange.all
    @shipping_prices = Admin::PriceRange.all
    @admin_shipping_policy = Admin::ShippingPolicy.find(params[:id])
    if params[:country_id].present?
      @country = Admin::Country.find(params[:country_id])
      shipping_specific_country = @admin_shipping_policy.shipping_specific_countries.build
      if params[:pincode].present?
        @pincodes = @country.pincodes
        shipping_specific_pincode = shipping_specific_country.shipping_specific_pincodes.build
        shipping_weight = shipping_specific_pincode.shipping_weights.build
        shipping_price = shipping_specific_pincode.shipping_prices.build
      else
        shipping_weight = shipping_specific_country.shipping_weights.build
        shipping_price = shipping_specific_country.shipping_prices.build
      end
    else
      shipping_weight = @admin_shipping_policy.shipping_weights.build
      shipping_price = @admin_shipping_policy.shipping_prices.build
    end
      render :template => "remote_content/find_shipping_policy_shipping_weight.js.erb"
  end

  # PATCH/PUT /admin/shipping_policies/1
  # PATCH/PUT /admin/shipping_policies/1.json
  def update
    respond_to do |format|
      if @admin_shipping_policy.update(admin_shipping_policy_params)
        format.html { redirect_to @admin_shipping_policy, notice: 'Shipping policy was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_shipping_policy }
      else
        format.html { render :edit }
        format.json { render json: @admin_shipping_policy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/shipping_policies/1
  # DELETE /admin/shipping_policies/1.json
  def destroy
    @admin_shipping_policy.destroy
    respond_to do |format|
      format.html { redirect_to admin_shipping_policies_url, notice: 'Shipping policy was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_shipping_policy
      @admin_shipping_policy = Admin::ShippingPolicy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_shipping_policy_params
      params.require(:admin_shipping_policy).permit(:all_country, :policy, :cost, shipping_weights_attributes:[:id,:range,:cost,:shipping_policy_id], shipping_prices_attributes:[:id,:range,:cost,:shipping_policy_id],shipping_specific_countries_attributes:[:id,:shipping_policy_id,:country_id,:policy,:cost,:all_place, shipping_weights_attributes:[:id,:range,:cost,:shipping_specific_country_id],shipping_prices_attributes:[:id,:range,:cost,:shipping_specific_country_id],shipping_specific_pincodes_attributes:[:id,:pincode,:shipping_specific_country_id, shipping_weights_attributes:[:id,:range,:cost,:shipping_specific_pincode_id], shipping_prices_attributes:[:id,:range,:cost,:shipping_specific_pincode_id]]])
    end
end
