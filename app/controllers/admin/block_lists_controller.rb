class Admin::BlockListsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_block_list, only: [:show, :edit, :update, :destroy]
  before_action :find_role
  before_action :authorize

  def index 
    @block_lists = @role.block_lists.page(params[:page]).per(9)
  end
  
  def show     
  end

  def new    
    @block_list = Admin::BlockList.new
    @check1 = params[:check] unless params[:check].nil?
    @met = params[:act].constantize unless params[:act].nil? 
    if @met != ''
      @metod = @met.action_methods.each {|m| puts m } unless params[:act].nil?
    end 
    @role.block_lists.each do |x| 
      if x.con_name == params[:act]
        @block_listing = @block_listing.to_a.push x.met_name 
      end
    end
    @block_listing ||= ''
    @methods = @metod.reject{|y| @block_listing.include? y}  unless @metod.nil?
    @methods ||= @metod
    respond_to do |format|
      format.json { 
        render :json => @methods.to_json
      }
      format.html {}
    end
  end

  def edit
    @met = params[:act].constantize unless params[:act].nil?
    if @met != ''
      @met1 = @met.action_methods.each {|m| puts m } unless params[:act].nil?
    end
    if @met1.present?
      render :json => @met1.to_json
    else
      if request.xhr?
        @render_path = edit_admin_role_block_list_path
        @render_file = 'admin/block_lists/edit'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "admin/block_lists/edit"
      end
    end
  end

  def create
    @block_list = @role.block_lists.build(block_list_params)
    if @block_list.save
      @block_list = Admin::BlockList.new
      @block_lists = @role.block_lists
      if request.xhr?
        flash[:alert] = 'Block list was successfully created.'
        @render_path = admin_role_path(@role)
        @render_file = 'admin/roles/show'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "admin/roles/show"
      end
    else 
      render :template => "admin/roles/show"
    end
  end

  def update
    if @block_list.update(block_list_params)
      redirect_to admin_role_path(@role), notice: 'Block list was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @block_list.destroy
    if request.xhr?
      flash[:alert] = 'Block list was successfully destroyed.'
      @render_path = admin_role_path(@role)
      @block_lists = @role.block_lists
      @block_list = Admin::BlockList.new
      @render_file = 'admin/roles/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/roles/show"
    end
  end

  private
    def set_block_list
      @block_list = Admin::BlockList.find(params[:id])
    end

    def block_list_params
      params.require(:admin_block_list).permit(:con_name, :met_name)
    end

    def find_role
      @role = Admin::Role.find(params[:role_id])
    end
end