class Admin::AdminsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  
  def dashboard
  	@admin_generals = Admin::General.first
  	if request.xhr?
      @render_path = admin_dashboard_path
      @render_file = 'admin/admins/dashboard'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/admins/dashboard"
    end
  end
  
end