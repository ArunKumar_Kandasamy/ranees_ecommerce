class Admin::AvailabilityPlansController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_availability_plan, only: [:show, :edit, :update, :destroy]

  # GET /admin/availability_plans
  # GET /admin/availability_plans.json
  def index  
    @admin_availability_plans = Admin::AvailabilityPlan.all
    if request.xhr?
      @render_path = admin_availability_plans_path
      @render_file = 'admin/availability_plans/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/availability_plans/index"
    end
  end 

  # GET /admin/availability_plans/1
  # GET /admin/availability_plans/1.json
  def show  
    @existing_countries = @admin_availability_plan.plan_countries
    @filter_shipping_countries = Admin::Country.all.map{|x| x.id}
    @existing_countries = @admin_availability_plan.plan_countries.map{|x| x.country_id}
    @filter_countries = @filter_shipping_countries - @existing_countries
    @country_list = Admin::Country.find(@filter_countries)

    plan_country = @admin_availability_plan.plan_countries.build
    country_pincode = plan_country.country_pincodes.build
    if request.xhr?
      @render_path = admin_availability_plan_path(@admin_availability_plan)
      @render_file = 'admin/availability_plans/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/availability_plans/show"
    end
  end

  def check_plan_name_exist
    plan = params[:fieldValue]
    validate_ajax_except = params[:validate_ajax_except]
    if validate_ajax_except.present?
      @admin_availability = Admin::AvailabilityPlan.where("name = ?", plan).where("id != ?", validate_ajax_except).first
    else
      @admin_availability = Admin::AvailabilityPlan.where("name = ?", plan).first
    end
    if@admin_availability.present?
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => false } }
      end
    else
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => true } }
      end
    end
  end

  # GET /admin/availability_plans/new
  def new
    @admin_availability_plan = Admin::AvailabilityPlan.new
    if request.xhr?
      @render_path = new_admin_availability_plan_path
      @render_file = 'admin/availability_plans/new'
      render "remote_content/common_modal_render.js.erb"          
    else
      render :template => "admin/availability_plans/new"
    end
  end

  # GET /admin/availability_plans/1/edit
  def edit
    if request.xhr?
      @render_path = edit_admin_availability_plan_path(@admin_availability_plan)
      @render_file = 'admin/availability_plans/edit'
      render "remote_content/common_modal_render.js.erb"          
    else
      render :template => "admin/availability_plans/edit"
    end
  end

  # POST /admin/availability_plans
  # POST /admin/availability_plans.json
  def create
    @admin_availability_plan = Admin::AvailabilityPlan.new(admin_availability_plan_params)
    if @admin_availability_plan.save
      if request.xhr?
        @admin_availability_plans = Admin::AvailabilityPlan.all
        @admin_availability_plan = Admin::AvailabilityPlan.new
        flash[:alert] = 'Plan was successfully created.' 
        @render_path = admin_availability_plans_path
        @render_file = 'admin/availability_plans/index'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "admin/availability_plans/show"
      end
    else
      @admin_availability_plans = Admin::AvailabilityPlan.all
      @admin_availability_plan = Admin::AvailabilityPlan.new
      flash[:alert] = 'Plan cannot be created.'  
      @render_path = admin_availability_plans_path
      @render_file = 'admin/availability_plans/index'
      render "remote_content/common_render.js.erb"
    end 
  end

  # PATCH/PUT /admin/availability_plans/1
  # PATCH/PUT /admin/availability_plans/1.json
  def update
    if @admin_availability_plan.update(admin_availability_plan_params)
       if request.xhr?
        @admin_availability_plans = Admin::AvailabilityPlan.all
        @admin_availability_plan = Admin::AvailabilityPlan.new
        flash[:alert] = 'Plan was successfully Updated.' 
        @render_path = admin_availability_plans_path
        @render_file = 'admin/availability_plans/index'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "admin/availability_plans/show"
      end
    else
      @admin_availability_plans = Admin::AvailabilityPlan.all
      @admin_availability_plan = Admin::AvailabilityPlan.new
      flash[:alert] = 'Plan cannot be updated.'  
      @render_path = admin_availability_plans_path
      @render_file = 'admin/availability_plans/index'
      render "remote_content/common_render.js.erb"
    end
  end

  def find_country_pincode_with_id   
    @admin_availability_plan = Admin::AvailabilityPlan.find(params[:id])
    plan_country = @admin_availability_plan.plan_countries.build

    country_pincode = plan_country.country_pincodes.build
    @country = Admin::Country.find(params[:country_id])
    render :template => "remote_content/find_country_pincode_with_id.js.erb"
  end

  # DELETE /admin/availability_plans/1
  # DELETE /admin/availability_plans/1.json
  def destroy
    @admin_availability_plan.destroy
    @admin_availability_plans = Admin::AvailabilityPlan.all
    @admin_availability_plan = Admin::AvailabilityPlan.new
    flash[:alert] = 'Plan was successfully destroyed.'  
    @render_path = admin_availability_plans_path
    @render_file = 'admin/availability_plans/index'
    render "remote_content/common_render.js.erb"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_availability_plan
      @admin_availability_plan = Admin::AvailabilityPlan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_availability_plan_params
      params.require(:admin_availability_plan).permit(:name, :all_country, plan_countries_attributes:[:id, :availability_plan_id, :country_id, :all_place, :_destroy, country_pincodes_attributes:[:id, :plan_country_id, :pincode, :availability, :_destroy]])
    end
end
