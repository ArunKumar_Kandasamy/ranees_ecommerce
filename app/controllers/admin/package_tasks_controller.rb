class Admin::PackageTasksController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_package_task, only: [:show, :edit, :update, :destroy]
  before_action :set_warehouse

  def index
    @param = params[:q]
    @search = @warehouse.package_tasks.search(params[:q])
    @admin_package_tasks = @search.result.includes(:consignments)      
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr? 
      @render_path = admin_warehouse_dashboard_package_tasks_path(@warehouse)
      @render_file = 'admin/package_tasks/index'
      render "remote_content/common_render.js.erb"          
    else 
      render :template => "admin/package_tasks/index"
    end  
  end

  def show
    if request.xhr? 
      @render_path = admin_warehouse_dashboard_package_task_path(@warehouse,@admin_package_task)
      @render_file = 'admin/package_tasks/show'
      render "remote_content/common_render.js.erb"          
    else 
      render :template => "admin/package_tasks/show"
    end 
  end 

  def new
    @admin_package_task = Admin::PackageTask.new
  end

  def edit
  end

  def create
    @status = false
    @new_task_hash = params[:admin_package_task]
    @new_task_hash[:package_tasks_consignments_attributes].each do |new_task_hash|
      @consignment = Admin::Consignment.find(new_task_hash[1][:_consignment_id])
      if new_task_hash[1][:conform] == "1"  &&  @consignment.package_tasks.first.nil?                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
         @status = true
      end
    end
    if @new_task_hash[:contact_person].present? && @new_task_hash[:closing_time].present?
      if @status == true
        @package_task = Admin::PackageTask.create(:warehouse_id => @warehouse.id,:contact_person => @new_task_hash[:contact_person],:closing_time => @new_task_hash[:closing_time]) 
        @package_task.save
        @new_task_hash[:package_tasks_consignments_attributes].each do |new_task_hash|
          if new_task_hash[1][:conform] == "1" && @consignment.package_tasks.first.nil?                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
             Admin::PackageTasksConsignments.create(:consignment_id => new_task_hash[1][:_consignment_id] , :package_task_id => @package_task.id)
          end
        end
        @admin_package_task = Admin::PackageTask.new(admin_package_task_params)
        redirect_to admin_warehouse_dashboard_package_task_path(@warehouse,@package_task), notice: 'Package task was successfully created.' 
      else
        redirect_to admin_warehouse_dashboard_consignments_path(@warehouse), notice: 'Sorry Package task failed.' 
      end
    else
      redirect_to admin_warehouse_dashboard_consignments_path(@warehouse), notice: 'Enter contact Details and Closing Time' 
    end
  end

  def update
    if params[:admin_package_task][:consignment_confirmation].present?
      @new_consignment_hash = params[:admin_package_task]
      @new_consignment_hash[:consignments_attributes].each do |new_consignment_hash|
        if new_consignment_hash[1][:conform] == "1"
          @consignment = Admin::Consignment.find(new_consignment_hash[1][:id])
          if @consignment.present?
            @order = @consignment.order
            if @order.check_order_status && @consignment.cancel.nil?
              @consignment.update(:conform => new_consignment_hash[1][:conform])
              @invoice = Admin::Invoice.where(:consignment_id => new_consignment_hash[1][:id]).first_or_initialize
              @invoice.save
            end
          end
        end
      end
      redirect_to admin_warehouse_dashboard_package_task_path(@warehouse,@admin_package_task), notice: 'Consignments confirmed successfully'
    else
      if params[:admin_package_task][:package_tasks_consignments_attributes].present?
        @new_consignment_hash = params[:admin_package_task]
        @new_consignment_hash[:package_tasks_consignments_attributes].each do |new_consignment_hash|
          @package_task_consignment = Admin::PackageTasksConsignments.find(new_consignment_hash[1][:id])
          if @package_task_consignment.consignment.conform == false && new_consignment_hash[1][:_destroy] == "1"
            @package_task_consignment.destroy
          end
        end
      end
      redirect_to admin_warehouse_dashboard_package_task_path(@warehouse,@admin_package_task), notice: 'Task was successfully updated'
    end 
  end

  def destroy
    @admin_package_task.destroy
    respond_to do |format|
      format.html { redirect_to admin_package_tasks_url, notice: 'Package task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_warehouse
      @warehouse = Admin::Warehouse.find(params[:warehouse_dashboard_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_admin_package_task
      @admin_package_task = Admin::PackageTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_package_task_params
      params.require(:admin_package_task).permit(:task_number ,:contact_person, :closing_time ,package_tasks_consignments_attributes:[:consignment_id ,:conform ,:_destroy],consignments_attributes:[:conform ,:id ,:order_id])
    end
end
