class Admin::InventoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_inventory, only: [:show, :edit, :update, :destroy]
  before_action :set_admin_variant, except: [:index]
  before_action :set_admin_product, except: [:index]
  after_action :check_for_activation, only: [:create, :update, :destroy]
  respond_to :html, :json
 
  def index
    @param = params[:q]    
    @search = Admin::Inventory.all.search(params[:q])
    @admin_inventories = @search.result.includes(:warehouse,:vendor,:user)
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_inventories_path
      @render_file = 'admin/inventories/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/inventories/index"
    end   
  end
  
  def show
    if params[:admin_variant].present?
        @variant = @admin_inventory.variant
        @admin_regulations = @admin_inventory.regulations
        @product = @variant.product
        render :template => "remote_content/admin_variant_inventory.js.erb"
    else
      @admin_regulations = @admin_inventory.regulations
      @admin_regulation = Admin::Regulation.new
      @regulation_totalqty=Admin::Regulation.sum(:qty) 
      respond_to do |format|
        format.html{
        }
        format.js {
          render :template => "remote_content/add_regulation.js.erb"
        }
      end
    end
  end

  def new
    @admin_inventory = Admin::Inventory.new
    @admin_regulations = Admin::Regulation.select('DISTINCT (reason)').where("reason like (?)","%#{params[:q]}%" )
    respond_to do |format|
      format.html
      format.json { 
          if @admin_regulations.empty?
            render :json => [{id: "<<<#{params[:q]}>>>",reason: "#{params[:q]}"}]
          else
             render :json => @admin_regulations
          end
          }
    end
  end

  def edit
  end

  def create
    @variants = @product.variants
    @admin_inventory = @variant.inventories.build(admin_inventory_params)
    @admin_inventory[:user_id]=current_user.id
    if @admin_inventory.save
      respond_to do |format|
        format.html{
        }
        format.js {
          if params[:admin_inventory][:se].present?
            @variant = @variants
            @stock = @product
            @warehouse = Admin::Warehouse.find(params[:admin_inventory][:warehouse_id])
            render :template => "remote_content/inventories1.js.erb"
          else
            @admin_inventory = Admin::Inventory.new
            #render :template => "remote_content/inventories.js.erb"
            @admin_variant = @variant
            @render_path = admin_product_variant_path(@product,@variant)
            @render_file = 'admin/variants/show'
            render "remote_content/common_render.js.erb"
          end
        }
      end   
    else
      render 'new'
    end
  end 

  def update
    if @admin_inventory.update(admin_inventory_params)  
      #@admin_inventory.balance_qty= @admin_inventory.qty
      @admin_inventory.save
      respond_with @admin_inventory
    end
  end

  def destroy
    if @admin_inventory.destroy
      redirect_to admin_inventories_url, notice: 'Inventory was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_inventory
      @admin_inventory = Admin::Inventory.find(params[:id])
    end

    def set_admin_variant
      @variant = Admin::Variant.find(params[:variant_id])
    end

    def set_admin_product
      @product = Admin::Product.find(params[:product_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_inventory_params
      params.require(:admin_inventory).permit(:warehouse_id, :vendor_id, :qty, :purchase_price, :user_id, :sold_qty, :balance_qty, :updated_through, :selling_price, :expiry_date, :regulation, :parent_id)
    end

    def check_for_activation
      @product=Admin::Product.find(params[:product_id])
      if @product.present?
        if @product.activate_product
          @product.status= true
          @product.save
        else
          @product.status= false
          @product.save
        end
      end
    end
end
