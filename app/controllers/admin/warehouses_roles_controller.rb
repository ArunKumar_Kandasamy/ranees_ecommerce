class Admin::WarehousesRolesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_admin_warehouses_roles, only: [:show, :edit, :update, :destroy]
  before_action :authorize
  
  def create
  	@warehouses_roles= Admin::WarehousesRoles.new(admin_warehouses_roles_params)
  	if @warehouses_roles.save
  	  redirect_to admin_warehouse_path(@warehouses_roles.warehouse_id), notice: "Warehouse Role is successfully created"
  	end
  end

  def edit
  end

  def update
  	if @warehouses_roles.update(set_admin_warehouses_role)
  	  redirect_to admin_warehouse_path(@warehouses_roles.warehouse_id), notice: "Warehouse Role is successfully created"
  	end
  end

  private
    def set_admin_warehouses_role
      @admin_products_tax_plans = Admin::WarehousesRoles.find(params[:id])
    end

	def admin_warehouses_roles_params
		params.require(:admin_warehouses_roles).permit(:warehouse_id, :role_id)
	end
end
