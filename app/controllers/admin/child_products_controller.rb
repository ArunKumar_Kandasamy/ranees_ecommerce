class Admin::ChildProductsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize

	def create 
	  @admin_category_product= Admin::CategoriesProducts.new(admin_child_product_params)
	  if @admin_category_product.save 
	  	respond_to do |format|
	      format.html{
	      	redirect_to admin_product_path(@admin_category_product.product_id), notice: "Product Category was successfully created."
	      }
	      format.js {
	      	@product = Admin::Product.find(@admin_category_product.product_id)
	      	@cat = Admin::Category.find(params[:admin_categories_products][:category_id])
	      	@cat.activate_related_category
	      	@admin_category_product= Admin::CategoriesProducts.new
	      	@product_categories = Admin::Category.all
	      	@general = Admin::General.first
		    @n = @general ? @general.category_level : 3
	      	@list_children_categories = @product_categories.reject { |h| h.ancestors.size != @n-1 } 
	        @product_category = @product.categories
	        @trace_product_category = @list_children_categories - @product_category    
	      	flash[:notice] = "Category Added."
	        render :template => "remote_content/add_cat.js.erb"
	      }
	  end
	  else
	  	render 'new'
	  end
	end

	def destroy
	  @category_product = Admin::CategoriesProducts.where(:category_id => params[:id] , :product_id =>  params[:product_id]).first
	  @product = Admin::Product.find(params[:product_id])
        if @category_product.destroy
      	     @cat = Admin::Category.find(params[:id])
      	     @cat.activate_related_category
	      respond_to do |format|
		      format.html{
		      	redirect_to admin_product_path(@category_product.product_id), notice: "Product Category was successfully created."
		      }
		      format.js {
		      	@admin_category_product= Admin::CategoriesProducts.new
		      	@product_categories = Admin::Category.all
		        @product_category = @product.categories
		        @trace_product_category = @product_categories - @product_category    
		      	flash[:notice] = "Category Added."
		        render :template => "remote_content/add_cat.js.erb"
		      }
		  end
      end
	end

	def update
	  if @admin_category_product.update(admin_child_product_params)
	    redirect_to admin_product_path(@admin_product_shipping_plan.product_id), notice: 'Product Shipping Plan was successfully updated.'
	  else
	  	render 'edit'
	  end
	end
	private

	def set_child_product
	  @admin_category_product_plan = Admin::ChildProduct.find(params[:id])
	end

	def admin_child_product_params
		params.require(:admin_categories_products).permit(:product_id, :category_id)
	end
end
