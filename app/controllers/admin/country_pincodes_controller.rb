class Admin::CountryPincodesController < ApplicationController
  before_action :set_admin_country_pincode, only: [:show, :edit, :update, :destroy]

  # GET /admin/country_pincodes
  # GET /admin/country_pincodes.json
  def index
    @admin_country_pincodes = Admin::CountryPincode.all
  end

  # GET /admin/country_pincodes/1
  # GET /admin/country_pincodes/1.json
  def show
  end

  # GET /admin/country_pincodes/new
  def new
    @admin_country_pincode = Admin::CountryPincode.new
  end

  # GET /admin/country_pincodes/1/edit
  def edit
  end

  # POST /admin/country_pincodes
  # POST /admin/country_pincodes.json
  def create
    @admin_country_pincode = Admin::CountryPincode.new(admin_country_pincode_params)

    respond_to do |format|
      if @admin_country_pincode.save
        format.html { redirect_to @admin_country_pincode, notice: 'Country pincode was successfully created.' }
        format.json { render :show, status: :created, location: @admin_country_pincode }
      else
        format.html { render :new }
        format.json { render json: @admin_country_pincode.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/country_pincodes/1
  # PATCH/PUT /admin/country_pincodes/1.json
  def update
    respond_to do |format|
      if @admin_country_pincode.update(admin_country_pincode_params)
        format.html { redirect_to @admin_country_pincode, notice: 'Country pincode was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_country_pincode }
      else
        format.html { render :edit }
        format.json { render json: @admin_country_pincode.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/country_pincodes/1
  # DELETE /admin/country_pincodes/1.json
  def destroy
    @admin_country_pincode.destroy
    respond_to do |format|
      format.html { redirect_to admin_country_pincodes_url, notice: 'Country pincode was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_country_pincode
      @admin_country_pincode = Admin::CountryPincode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_country_pincode_params
      params.require(:admin_country_pincode).permit(:plan_country_id, :pincode, :availability)
    end
end
