class Admin::ResendTasksController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_resend_task, only: [:show, :edit, :update, :destroy]
  before_action :set_warehouse 

  def index
    @param = params[:q]
    @search = Admin::ResendTask.all.page(params[:page]).search(params[:q])
    @admin_resend_tasks = @search.result
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_warehouse_dashboard_resend_tasks_path(@warehouse)
      @render_file = 'admin/resend_tasks/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/resend_tasks/index"
    end 
  end

  def show
    if request.xhr?
      @render_path = admin_warehouse_dashboard_resend_task_path(@warehouse ,@admin_resend_task)
      @render_file = 'admin/resend_tasks/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/resend_tasks/show"
    end 
  end

  def new
    @admin_resend_task = Admin::ResendTask.new
  end

  def edit
  end

  def create
    @status = false
    @new_task_hash = params[:admin_resend_task]
    @new_task_hash[:resend_tasks_consignments_attributes].each do |new_task_hash|
      @consignment = Admin::Consignment.find(new_task_hash[1][:_consignment_id])
      if new_task_hash[1][:conform] == "1"  &&  @consignment.resend_tasks.first.nil?                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
         @status = true
      end
    end
    if @status == true
      @resend_task = Admin::ResendTask.create(:warehouse_id => @warehouse.id) 
      @resend_task.save
      @new_task_hash[:resend_tasks_consignments_attributes].each do |new_task_hash|
        if new_task_hash[1][:conform] == "1" && @consignment.resend_tasks.first.nil?                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
           Admin::ResendTasksConsignments.create(:consignment_id => new_task_hash[1][:_consignment_id] , :resend_task_id => @resend_task.id)
        end
      end
      @admin_resend_task = Admin::ResendTask.new(admin_resend_task_params)
      redirect_to admin_warehouse_dashboard_resend_task_path(@warehouse,@resend_task), notice: 'Resend task was successfully created.' 
    else
      redirect_to admin_warehouse_dashboard_resend_consignments_path(@warehouse), notice: 'Sorry Resend task failed.' 
    end
  end

  def update
    respond_to do |format|
      if @admin_resend_task.update(admin_resend_task_params)
        format.html { redirect_to @admin_resend_task, notice: 'Resend task was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_resend_task }
      else
        format.html { render :edit }
        format.json { render json: @admin_resend_task.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @admin_resend_task.destroy
    respond_to do |format|
      format.html { redirect_to admin_resend_tasks_url, notice: 'Resend task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_resend_task
      @admin_resend_task = Admin::ResendTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_resend_task_params
      params.require(:admin_resend_task).permit(:task_number, :warehouse_id,resend_tasks_consignments_attributes:[:consignment_id ,:conform ,:_destroy],consignments_attributes:[:conform ,:id ,:order_id])
    end

    def set_warehouse
      @warehouse = Admin::Warehouse.find(params[:warehouse_dashboard_id])
    end
end
