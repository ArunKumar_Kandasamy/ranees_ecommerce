class Admin::OrderRevocationsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_order_revocation, only: [:show, :edit, :update, :destroy]

  def index
    @param = params[:q]
    @search = Admin::OrderRevocation.all.search(params[:q])
    @admin_order_revocations = @search.result
    @admin_bank_revocations= Admin::OrderRevocation.where(:revocation_mode => "b")
    @admin_voucher_revocations= Admin::OrderRevocation.where(:revocation_mode => "v")
    @voucher= Admin::Voucher.new
    @admin_order_revocation= Admin::OrderRevocation.new
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_order_revocations_path
      @render_file = 'admin/order_revocations/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/order_revocations/index"
    end    
  end

  def show
    @admin_order = Admin::Order.where(:id => @admin_order_revocation.order_id).first
    @admin_order_logs= Admin::OrderLog.where(:order_id => @admin_order.id)
    if request.xhr?
      @render_path = admin_order_revocation_path(@admin_order_revocation)
      @render_file = 'admin/order_revocations/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/order_revocations/show"
    end
  end

  def new
    @admin_order_revocation = Admin::OrderRevocation.new
  end

  def edit
  end

  def create
    @admin_order_revocation = Admin::OrderRevocation.new(admin_order_revocation_params)
    if @admin_order_revocation.save
      redirect_to @admin_order_revocation, notice: 'Order revocation was successfully created.' 
    else
     render 'new'
    end
  end

  def update
    if @admin_order_revocation.update(admin_order_revocation_params)
      @admin_order = Admin::Order.where(:id => @admin_order_revocation.order_id).first
      @admin_order_logs= Admin::OrderLog.where(:order_id => @admin_order.id)
      flash['alert'] = 'Order revocation was successfully updated.'
      @render_path = admin_order_revocation_path(@admin_order_revocation)
      @render_file = 'admin/order_revocations/show'
      render "remote_content/common_render.js.erb"          
    else
      @render_path = admin_order_revocation_path(@admin_order_revocation)
      @render_file = 'admin/order_revocations/show'
      render "remote_content/common_render.js.erb"      
    end 
  end

  def destroy
    if @admin_order_revocation.destroy
      redirect_to admin_order_revocations_url, notice: 'Order revocation was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_order_revocation
      @admin_order_revocation = Admin::OrderRevocation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_order_revocation_params
      params.require(:admin_order_revocation).permit(:revocation_cost, :order_id, :revocation_mode, :status, :compensate, :bank_account_id, :trans_status, :transaction_id)
    end
end
