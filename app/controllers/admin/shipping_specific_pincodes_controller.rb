class Admin::ShippingSpecificPincodesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_shipping_specific_pincode, only: [:show, :edit, :update, :destroy]

  # GET /admin/shipping_specific_pincodes
  # GET /admin/shipping_specific_pincodes.json
  def index
    @admin_shipping_specific_pincodes = Admin::ShippingSpecificPincode.all
  end

  # GET /admin/shipping_specific_pincodes/1
  # GET /admin/shipping_specific_pincodes/1.json
  def show
  end

  # GET /admin/shipping_specific_pincodes/new
  def new
    @admin_shipping_specific_pincode = Admin::ShippingSpecificPincode.new
  end

  # GET /admin/shipping_specific_pincodes/1/edit
  def edit
  end

  # POST /admin/shipping_specific_pincodes
  # POST /admin/shipping_specific_pincodes.json
  def create
    @admin_shipping_specific_pincode = Admin::ShippingSpecificPincode.new(admin_shipping_specific_pincode_params)

    respond_to do |format|
      if @admin_shipping_specific_pincode.save
        format.html { redirect_to @admin_shipping_specific_pincode, notice: 'Shipping specific pincode was successfully created.' }
        format.json { render :show, status: :created, location: @admin_shipping_specific_pincode }
      else
        format.html { render :new }
        format.json { render json: @admin_shipping_specific_pincode.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/shipping_specific_pincodes/1
  # PATCH/PUT /admin/shipping_specific_pincodes/1.json
  def update
    respond_to do |format|
      if @admin_shipping_specific_pincode.update(admin_shipping_specific_pincode_params)
        format.html { redirect_to @admin_shipping_specific_pincode, notice: 'Shipping specific pincode was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_shipping_specific_pincode }
      else
        format.html { render :edit }
        format.json { render json: @admin_shipping_specific_pincode.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/shipping_specific_pincodes/1
  # DELETE /admin/shipping_specific_pincodes/1.json
  def destroy
    @admin_shipping_specific_pincode.destroy
    respond_to do |format|
      format.html { redirect_to admin_shipping_specific_pincodes_url, notice: 'Shipping specific pincode was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_shipping_specific_pincode
      @admin_shipping_specific_pincode = Admin::ShippingSpecificPincode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_shipping_specific_pincode_params
      params.require(:admin_shipping_specific_pincode).permit(:shipping_specific_country_id, :pincode)
    end
end
