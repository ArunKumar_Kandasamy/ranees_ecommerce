class Admin::PickupConsignmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_pickup_consignment, only: [ :show, :edit, :update, :destroy]
  
  def index
    @param = params[:q]
    @search = Admin::PickupConsignment.all.search(params[:q])
    @admin_pickup_consignments = @search.result
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_pickup_consignments_path
      @render_file = 'admin/pickup_consignments/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/pickup_consignments/index"
    end
  end
  
  def show
    if request.xhr?
      @render_path = admin_pickup_consignment_path(@admin_pickup_consignment)
      @render_file = 'admin/pickup_consignments/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/pickup_consignments/show"
    end
  end

  def new
    if request.xhr?
      if params[:user_panel].present?
        @admin_pickup_consignment = Admin::PickupConsignment.new
        @consignment = Admin::Consignment.find(params[:consignment_id])
        @consignment_items = @consignment.consignment_items
        @order_revocation = @admin_pickup_consignment.pickup_consignment_items.build 
        render :template => "remote_content/user_consignment_return_initiate.js.erb"
      else
        @admin_pickup_consignment = Admin::PickupConsignment.new
        @consignment = Admin::Consignment.find(params[:consignment_id])
        @consignment_items = @consignment.consignment_items
        @order_revocation = @admin_pickup_consignment.pickup_consignment_items.build 
        render :template => "remote_content/support_consignment_return_initiate.js.erb"
      end
    end
  end

  def edit
  end

  def create
    @new_consignment_hash = params[:admin_pickup_consignment]
    @consignment = Admin::Consignment.find(params[:admin_pickup_consignment][:consignment_id])
    @consignment_log = @consignment.create_consignment_log(current_user.id) 
      @new_consignment_hash[:pickup_consignment_items_attributes].each do |new_consignment_hash| 
        if new_consignment_hash[1][:_destroy] != "1"
          if new_consignment_hash[1][:reason] != "Lost" && new_consignment_hash[1][:compensate] != "Partial Refund"
            @admin_pickup_consignment ||= Admin::PickupConsignment.create(:consignment_id => params[:admin_pickup_consignment][:consignment_id], :shipping_address_id => @consignment.order.shipping_address_id, :created_by => current_user.id,:status => "up")
          end
          @consignment_item = Admin::ConsignmentItem.find(new_consignment_hash[1][:consignment_item_id])
          if new_consignment_hash[1][:compensate] == "Refund" || new_consignment_hash[1][:compensate] == "Partial Refund"
            if new_consignment_hash[1][:compensate] == "Refund"
              if new_consignment_hash[1][:ship_dt] == "1" || new_consignment_hash[1][:tax_dt] == "1"
                if new_consignment_hash[1][:ship_dt] == "1"
                  @refund_amount = @consignment_item.per_qty_price - @consignment_item.shipping_cost
                end
                if new_consignment_hash[1][:tax_dt] == "1" 
                  @refund_amount = @consignment_item.per_qty_price - @consignment_item.tax
                end
              else
                @refund_amount = @consignment_item.per_qty_price
              end
            else
              @refund_amount = new_consignment_hash[1][:refund_amount]
            end
            @refund_revocation ||= Admin::RefundRevocation.create(:consignment_id => @consignment.id, :status => "up" ,:revocation_mode => new_consignment_hash[1][:revocation_mode], :bank_account_id => params[:admin_pickup_consignment][:bank_account_id])
            for i in 1..new_consignment_hash[1][:qty].to_i
              @refund_revocation_item = Admin::RefundRevocationItem.create(:refund_revocation_id => @refund_revocation.id,:reason =>new_consignment_hash[1][:reason] , :qty => 1 , :refund_amount => @refund_amount ,:variant_id => @consignment_item.variant_id ,:per_qty_price => @refund_amount, :ship_dt => new_consignment_hash[1][:ship_dt], :tax_dt => new_consignment_hash[1][:tax_dt], :TDR_dt => new_consignment_hash[1][:TDR_dt] ,  :actual_cost => @consignment_item.actual_cost)
              if new_consignment_hash[1][:reason] != "Lost" && new_consignment_hash[1][:compensate] != "Partial Refund"
                Admin::PickupConsignmentItem.create(:current_user => @current_user.id, :pickup_consignment_id => @admin_pickup_consignment.id, :refund_revocation_item_id => @refund_revocation_item.id , :qty => 1 ,:variant_id => @consignment_item.variant_id ,:per_qty_price => @consignment_item.per_qty_price)
              else
                @refund_revocation_item.update(:status => true)
              end
            end
          elsif new_consignment_hash[1][:compensate] == "Replacement"
            @replacement_revocation ||= Admin::ReplacementRevocation.create(:consignment_id => @consignment.id, :status => "up" , :shipping_address_id => @consignment.order.shipping_address_id, :created_by => current_user.id,)
            for i in 1..new_consignment_hash[1][:qty].to_i
              @replacement_revocation_item = Admin::ReplacementRevocationItem.create(:replacement_revocation_id => @replacement_revocation.id ,:reason =>new_consignment_hash[1][:reason], :qty => 1 ,:variant_id => @consignment_item.variant_id ,:per_qty_price => @consignment_item.per_qty_price, :shipping_cost =>  @consignment_item.shipping_cost, :tax =>  @consignment_item.tax, :actual_cost => @consignment_item.actual_cost)
              if new_consignment_hash[1][:reason] != "Lost"
                Admin::PickupConsignmentItem.create(:current_user => @current_user.id,:pickup_consignment_id => @admin_pickup_consignment.id, :replacement_revocation_item_id => @replacement_revocation_item.id , :qty => 1 ,:variant_id => @consignment_item.variant_id ,:per_qty_price => @consignment_item.per_qty_price)
              else
                @replacement_revocation_item.update(:status => true)
              end
            end
          end
          @remaining_qty = @consignment_item.qty - new_consignment_hash[1][:qty].to_i
          @consignment_item.update(:qty => @remaining_qty)
          @consignment_item_log = @consignment_log.consignment_item_logs.where(:variant_id => @consignment_item.variant_id).first
          @consignment_log.update(:reason => new_consignment_hash[1][:reason])
        end 
      end  
      if @admin_pickup_consignment.present?
        @invoice = Admin::PickupConsignmentInvoice.create(:pickup_consignment_id => @admin_pickup_consignment.id)
      end    
    if params[:admin_pickup_consignment][:user_panel].present?
      redirect_to user_order_path(current_user.id ,@consignment.order), notice: 'Return initiated successfully'
    else
      redirect_to support_order_path(@consignment.order), notice: 'Pickup consignment was successfully created.'
    end
  end

  def update
      if params[:admin_pickup_consignment][:support].present?
        if @admin_pickup_consignment.status == "up"
          @admin_pickup_consignment.update(admin_pickup_consignment_params)
          redirect_to support_order_path(@admin_pickup_consignment.consignment.order), notice: 'Pickup consignment was successfully cancelled.'
        else
          redirect_to support_order_path(@admin_pickup_consignment.consignment.order), notice: 'Cancellation failed.'
        end
      else
        if @admin_pickup_consignment.update(admin_pickup_consignment_params)
          flash['alert'] = 'Pickup consignment was successfully updated.' 
          @render_path = admin_pickup_consignments_path(@admin_pickup_consignment)
          @render_file = 'admin/pickup_consignments/show'
          render "remote_content/common_render.js.erb"          
        else
          @render_path = edit_admin_pickup_consignments_path(@admin_pickup_consignment)
          @render_file = 'admin/pickup_consignments/edit'
          render "remote_content/common_render.js.erb"   
          render :template => "admin/pickup_consignments/edit"
        end
      end
  end

  def destroy
    @admin_pickup_consignment.destroy
    respond_to do |format|
      format.html { redirect_to admin_pickup_consignments_url, notice: 'Pickup consignment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_admin_pickup_consignment
      @admin_pickup_consignment = Admin::PickupConsignment.find(params[:id])
    end

    def admin_pickup_consignment_params
      params.require(:admin_pickup_consignment).permit(:pickup_consignment_number, :consignment_cost, :consignment_id, :logistic_id, :warehouse_id, :status, :shipping_address_id, :created_by, :status, :received, pickup_consignment_items_attributes:[:id ,:price, :qty, :variant_id, :per_qty_price, :received, :_destroy])
    end
end
