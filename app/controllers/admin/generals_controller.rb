class Admin::GeneralsController < ApplicationController
  before_filter :authenticate_user!
  skip_before_filter :verify_authenticity_token, only: [:update, :create]
  before_action :authorize
  before_action :set_admin_general, only: [:show, :edit, :update, :destroy]
  before_action :unique_verify, only: [:new, :create, :new_store, :edit_acc_spec, :edit_seo_spec, :edit_social_spec]

  def index 
    @admin_generals = Admin::General.all
    if request.xhr?
      @render_path = admin_generals_path
      @render_file = 'admin/generals/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/generals/index"
    end
  end 

  def new_store
    @admin_general = Admin::General.new
    1.times { @admin_general.addresses.build }
    if request.xhr?
      @render_path = admin_new_store_path
      @render_file = 'admin/generals/new_store'
      render "remote_content/common_render.js.erb"          
    else
      render :template =>  "admin/generals/new_store"
    end
  end

  def edit_acc_spec
    @admin_general = Admin::General.first
    if request.xhr?
      @render_path = admin_general_edit_acc_spec_path(@admin_general)
      @render_file = 'admin/generals/edit_acc_spec'
      render "remote_content/common_render.js.erb"          
    else
      render :template => 'admin/generals/edit_acc_spec'
    end
  end

  def edit_seo_spec
    @admin_general = Admin::General.first
    if request.xhr?
      @render_path = admin_general_edit_seo_spec_path(@admin_general)
      @render_file = 'admin/generals/edit_seo_spec'
      render "remote_content/common_render.js.erb"          
    else
      render :template =>  'admin/generals/edit_seo_spec'
    end
  end

  def edit_social_spec
    @admin_general = Admin::General.first
    if request.xhr?
      @render_path = admin_general_edit_social_spec_path(@admin_general)
      @render_file = 'admin/generals/edit_social_spec'
      render "remote_content/common_render.js.erb"          
    else
      render :template => 'admin/generals/edit_social_spec'
    end
  end

  def new 
    @admin_generals = Admin::General.all
    unless @admin_generals.present? 
      @admin_general = Admin::General.new
      1.times { @admin_general.addresses.build }
      if request.xhr?
        @render_path = new_admin_general_path
        @render_file = 'admin/generals/new'
        render "remote_content/common_render.js.erb"          
      else
        render :template => 'admin/generals/new'
      end
    else
      render :template => 'admin/generals/index'
    end  
  end

  def edit 
    if @admin_general.addresses.nil? 
      1.times { @admin_general.addresses.build }
    end
    if request.xhr?
      @render_path = edit_admin_general_path(@admin_general)
      @render_file = 'admin/generals/edit'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/generals/edit"
    end
  end

  def create
    @admin_general = Admin::General.new(admin_general_params)
    if @admin_general.save
      if request.xhr?
        flash[:alert] = 'General was successfully created.' 
        @render_path = admin_generals_path
        @render_file = 'admin/generals/index'
        render "remote_content/common_render.js.erb"          
      else
          redirect_to admin_generals_path, notice: 'General was successfully created.' 
      end
    else
      render :template => 'admin/generals/new'
    end    
  end

  def update
    if @admin_general.update(admin_general_params)
      if @admin_general.save
        @admin_generals = Admin::General.all
        if params[:admin_general][:service2].present?
          @render_path = admin_generals_path
          @render_file = 'admin/generals/index'
          flash[:alert] = 'AccountDetails was successfully updated.'
          flash.discard
          render "remote_content/common_render.js.erb"  
        elsif params[:admin_general][:service1].present?
          @render_path = admin_generals_path
          @render_file = 'admin/generals/index'
          flash[:alert] = 'SEO Attributes was successfully updated.'
          flash.discard
          render "remote_content/common_render.js.erb"  
        elsif params[:admin_general][:service].present?
          @render_path = admin_generals_path
          @render_file = 'admin/generals/index'
          flash[:alert] = 'Social Media Link was successfully updated.'
          flash.discard
          render "remote_content/common_render.js.erb"  
        else 
          if request.xhr?
            @render_path = admin_generals_path
            @render_file = 'admin/generals/index'
            flash[:alert] = 'Store Details was successfully updated.' 
            flash.discard
            render "remote_content/common_render.js.erb"         
          else
            render :template => "admin/generals/index"
          end
        end
      else
        render_path = admin_generals_path
        @render_file = 'admin/generals/index'
        flash[:alert] = 'General was successfully Updated.'  
        flash.discard
      end 
    else
      if request.xhr?
        @render_path = edit_admin_general_path(@admin_general)
        @render_file = 'admin/generals/edit'
        render "remote_content/common_render.js.erb"          
      else
        render :template => "admin/generals/edit"
      end  
    end    
     
  end

  def destroy
    if @admin_general.destroy
      redirect_to admin_generals_url, notice: 'General was successfully destroyed.' 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_general
      @admin_general = Admin::General.find(params[:id])
    end

    def unique_verify
      if Admin::General.first.present?    
        #redirect_to edit_admin_general_path(Admin::General.first)
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_general_params
       params.require(:admin_general).permit(:about_us, :min_order, :min_cost, :fixed_rate, :cod_preference, :category_level, :contact_us ,:return, :refund, :cancellation, :book_flow, :price_desc, :ship_delivery, :fb_link, :tw_link, :gp_link, :pt_link, :term_use, :privacy, :security, :a_text, :store_name, :page_title, :meta_des, :account_email, :com_email, :legal_name, :acc_currency, :show_currency, :avatar, addresses_attributes:[:id , :_destroy ,:add_line1, :add_line2, :state, :country, :c_code, :phone, :connection])
    end
end
