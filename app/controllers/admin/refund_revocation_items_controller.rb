class Admin::RefundRevocationItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_refund_revocation_item, only: [:show, :edit, :update, :destroy]

  def index
    @admin_refund_revocation_items = Admin::RefundRevocationItem.all
  end

  def show
  end

  def new
    @admin_refund_revocation_item = Admin::RefundRevocationItem.new
  end

  def edit
  end

  def create
    @admin_refund_revocation_item = Admin::RefundRevocationItem.new(admin_refund_revocation_item_params)
    respond_to do |format|
      if @admin_refund_revocation_item.save
        format.html { redirect_to @admin_refund_revocation_item, notice: 'Refund revocation item was successfully created.' }
        format.json { render :show, status: :created, location: @admin_refund_revocation_item }
      else
        format.html { render :new }
        format.json { render json: @admin_refund_revocation_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @admin_refund_revocation_item.update(admin_refund_revocation_item_params)
        format.html { redirect_to @admin_refund_revocation_item, notice: 'Refund revocation item was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_refund_revocation_item }
      else
        format.html { render :edit }
        format.json { render json: @admin_refund_revocation_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @consignment_item = @consignment.consignment_items.find_by_variant_id(@admin_refund_revocation_item.variant_id)
    @consignment_item.qty += 1
    @consignment_item.save
    if @admin_refund_revocation_item.pickup_consignment_items.first.present?
      @admin_refund_revocation_item.pickup_consignment_items.first.destroy
    end
    @admin_refund_revocation_item.destroy
    redirect_to support_order_shipment_path(@admin_refund_revocation_item.refund_revocation.consignment.order,@admin_refund_revocation_item.refund_revocation.consignment), notice: 'Refund revocation item was successfully destroyed.' 
  end

  private
    def set_admin_refund_revocation_item
      @admin_refund_revocation_item = Admin::RefundRevocationItem.find(params[:id])
      @consignment = @admin_refund_revocation_item.refund_revocation.consignment
    end

    def admin_refund_revocation_item_params
      params.require(:admin_refund_revocation_item).permit(:refund_revocation_id, :price, :qty, :variant_id, :per_qty_price, :actual_price, :reason, :refund_amount, :ship_dt, :tax_dt, :TDR_dt, :status)
    end
end
