class Admin::ShippingSpecificCountriesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_shipping_policy
  before_action :set_admin_shipping_specific_country, only: [:show, :edit, :update, :destroy] 

  # GET /admin/shipping_specific_countries
  # GET /admin/shipping_specific_countries.json
  def index
    @admin_shipping_specific_countries = Admin::ShippingSpecificCountry.all
  end

  # GET /admin/shipping_specific_countries/1
  # GET /admin/shipping_specific_countries/1.json
  def show 
    @shipping_weights = Admin::WeightRange.all
    @shipping_prices = Admin::PriceRange.all
    @weight_ranges = Admin::WeightRange.all.map{|x| x.range}
    if @admin_shipping_specific_country.shipping_specific_pincodes.first.present?
      admin_shipping_specific_pincode = @admin_shipping_specific_country.shipping_specific_pincodes
      @admin_shipping_specific_country.shipping_specific_pincodes.each do |specific_pincode|
        @existing_ranges = specific_pincode.shipping_weights.map{|x| x.range}
        @filter_ranges = @weight_ranges - @existing_ranges
        @filter_ranges.each do |filter_range|
          new_row = specific_pincode.shipping_weights.build
          new_row.range = filter_range
        end
      end
    else
      admin_shipping_specific_pincode = @admin_shipping_specific_country.shipping_specific_pincodes.build
      admin_shipping_specific_pincode.shipping_weights.build
      admin_shipping_specific_pincode.shipping_prices.build
    end
    if @admin_shipping_specific_country.shipping_weights.first.present?
      @existing_ranges = @admin_shipping_specific_country.shipping_weights.map{|x| x.range}
      @filter_ranges = @weight_ranges - @existing_ranges
      @filter_ranges.each do |filter_range|
        new_row = @admin_shipping_specific_country.shipping_weights.build
        new_row.range = filter_range
      end
    else
      @admin_shipping_specific_country.shipping_weights.build
    end
    if @admin_shipping_specific_country.shipping_prices.first.present?

    else
      @admin_shipping_specific_country.shipping_prices.build 
    end 
    if request.xhr?
      @render_path = admin_shipping_policy_shipping_specific_country_path(@admin_shipping_policy,@admin_shipping_specific_country)
      @render_file = 'admin/shipping_specific_countries/show'
      render "remote_content/common_render.js.erb"
    else
      render :template => "admin/shipping_specific_countries/show"
    end
  end

  # GET /admin/shipping_specific_countries/new
  def new
    @admin_shipping_specific_country = Admin::ShippingSpecificCountry.new
  end

  # GET /admin/shipping_specific_countries/1/edit
  def edit
  end

  # POST /admin/shipping_specific_countries
  # POST /admin/shipping_specific_countries.json
  def create
    @admin_shipping_specific_country = Admin::ShippingSpecificCountry.new(admin_shipping_specific_country_params)

    respond_to do |format|
      if @admin_shipping_specific_country.save
        format.html { redirect_to @admin_shipping_specific_country, notice: 'Shipping specific country was successfully created.' }
        format.json { render :show, status: :created, location: @admin_shipping_specific_country }
      else
        format.html { render :new }
        format.json { render json: @admin_shipping_specific_country.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/shipping_specific_countries/1
  # PATCH/PUT /admin/shipping_specific_countries/1.json
  def update
    respond_to do |format|
      if @admin_shipping_specific_country.update(admin_shipping_specific_country_params)
        format.html { redirect_to admin_shipping_policy_shipping_specific_country_path(@admin_shipping_policy,@admin_shipping_specific_country), notice: 'Shipping specific country was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_shipping_specific_country }
      else
        format.html { render :edit }
        format.json { render json: @admin_shipping_specific_country.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/shipping_specific_countries/1
  # DELETE /admin/shipping_specific_countries/1.json
  def destroy
    @admin_shipping_specific_country.destroy
    respond_to do |format|
      format.html { redirect_to admin_shipping_specific_countries_url, notice: 'Shipping specific country was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_shipping_specific_country
      @admin_shipping_specific_country = Admin::ShippingSpecificCountry.find(params[:id])
    end

    def set_shipping_policy
      @admin_shipping_policy = Admin::ShippingPolicy.find(params[:shipping_policy_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_shipping_specific_country_params
      params.require(:admin_shipping_specific_country).permit(:all_place, :policy, :cost, :country_id, :shipping_policy_id , shipping_weights_attributes:[:id,:range,:cost,:shipping_specific_country_id],shipping_prices_attributes:[:id,:range,:cost,:shipping_specific_country_id],shipping_specific_pincodes_attributes:[:id,:pincode,:shipping_specific_country_id, shipping_weights_attributes:[:id,:range,:cost,:shipping_specific_pincode_id], shipping_prices_attributes:[:id,:range,:cost,:shipping_specific_pincode_id]])
    end
end
