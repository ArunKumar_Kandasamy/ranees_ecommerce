class Admin::ProductQuestionsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_admin_product_question, only: [:show, :edit, :update, :destroy]
  before_action :authorize

  def index
    @admin_product_questions = Admin::ProductQuestion.all
  end

  def show
  end

  def new
    @admin_product_question = Admin::ProductQuestion.new
    respond_to do |format|
      format.html
      format.json { 
        render :json => Admin::ProductQuestion.tokens(params[:q])
      }
    end
  end 

  def edit
  end

  def create
    @admin_product_question = Admin::ProductQuestion.new(admin_product_question_params)
    if @admin_product_question.save
      redirect_to @admin_product_question, notice: 'Product question was successfully created.'
    else
      render 'new'
    end
  end

  def update
    if @admin_product_question.update(admin_product_question_params)
      redirect_to @admin_product_question, notice: 'Product question was successfully updated.'
    else
      render 'edit'
    end
  end

  def destroy
    if @admin_product_question.destroy
      redirect_to admin_product_questions_url, notice: 'Product question was successfully destroyed.' 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_product_question
      @admin_product_question = Admin::ProductQuestion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_product_question_params
      params.require(:admin_product_question).permit(:question, product_answers_attributes:[:product_question_id, :answer])
    end
end