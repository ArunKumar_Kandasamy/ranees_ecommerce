class Admin::TaxCountriesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_tax_plan
  before_action :set_admin_tax_country, only: [:show, :edit, :update, :destroy]

  def index
    @admin_tax_countries = Admin::TaxCountry.all
  end

  # GET /admin/tax_countries/1
  # GET /admin/tax_countries/1.json
  def show
    respond_to do |format|
      format.html{
        @admin_tax_countries = @admin_tax_plan.countries
        @admin_tax_country = Admin::TaxCountry.new
        @admin_tax_state = Admin::TaxState.new
      }
      format.js {
        @filter_states = @admin_tax_country.country.states
        @admin_tax_states = @admin_tax_country.tax_states.map{|x| x.state_id}
        if @admin_tax_states.present?
          @filter_states = @filter_states
        else
          @admin_tax_country.tax_states.build
        end
        render :template => "remote_content/tax_plans.js.erb"
      }
    end        
  end

  # GET /admin/tax_countries/new
  def new
    @admin_tax_country = Admin::TaxCountry.new
  end

  # GET /admin/tax_countries/1/edit
  def edit
  end

  # POST /admin/tax_countries
  # POST /admin/tax_countries.json
  def create 
    @admin_tax_countries = @admin_tax_plan.countries
    @admin_tax_country = @admin_tax_plan.tax_countries.build(admin_tax_country_params)    
    if @admin_tax_country.save
      @admin_tax_countries = @admin_tax_plan.tax_countries
      respond_to do |format|
        format.html{
          redirect_to admin_tax_plan_tax_country_path(@admin_tax_plan,@admin_tax_country), notice: 'Tax country was successfully created.'         
        }
        format.js { 
          flash[:notice] = "Tax country was successfully created."
          @filter_tax_countries = Admin::Country.all.map{|x| x.id}
          @filter_admin_tax_countries = @admin_tax_plan.tax_countries.map{|x| x.country_id}
          @availability_plan = Admin::AvailabilityPlan.all
          @existing_countries_collection  = []
          @availability_plan.each do |availability_plan|
               @existing_countries_collection.push(*availability_plan.plan_countries.map{|x| x.country_id.nil? ? nil : x.country_id }).uniq
          end
          @current_available_countries = @existing_countries_collection.compact - @filter_admin_tax_countries
          @filter_countries_results = Admin::Country.find(@current_available_countries )
          @admin_tax_country = Admin::TaxCountry.new   
          @admin_tax_state = Admin::TaxState.new
          @render_path = admin_tax_plan_path(@admin_tax_plan)
          @render_file = 'admin/tax_plans/show'
          render "remote_content/common_render.js.erb" 
          flash.discard
        }
      end  
    else
      render 'new'
    end
  end
  
  def update
    @admin_tax_countries = @admin_tax_plan.tax_countries
    if @admin_tax_country.update(admin_tax_country_params)
      respond_to do |format|
        format.html{
          redirect_to admin_tax_plan_path(@admin_shipping_plan), notice: 'Shipping cost was successfully created.'
        }
        format.js {
          flash[:notice] = "Shipping cost was successfully created."
          render :template => "remote_content/tax_countries_success.js.erb"
          flash.discard
        }
      end
    end
  end

  def destroy
    if @admin_tax_country.destroy
      flash[:notice] = "Tax country was successfully destroyed."
    else
      flash[:notice] = "Tax country cannot be destroyed."
    end  
    @admin_tax_countries = @admin_tax_plan.tax_countries
    @filter_tax_countries = Admin::Country.all.map{|x| x.id}
    @filter_admin_tax_countries = @admin_tax_plan.tax_countries.map{|x| x.country_id}
    @filter_countries = @filter_tax_countries - @filter_admin_tax_countries
    @filter_countries_results = Admin::Country.find(@filter_countries)
    @admin_tax_country = Admin::TaxCountry.new   
    @admin_tax_state = Admin::TaxState.new
    flash[:notice] = "Tax country was successfully destroyed."
    @render_path = admin_tax_plan_path(@admin_tax_plan)
    @render_file = 'admin/tax_plans/show'
    render "remote_content/common_render.js.erb"          
    
  end

  private
    def set_admin_tax_plan
      @admin_tax_plan = Admin::TaxPlan.find(params[:tax_plan_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_admin_tax_country
      @admin_tax_country = Admin::TaxCountry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_tax_country_params
      params.require(:admin_tax_country).permit(:country_id, tax_states_attributes:[:id, :tax_country_id, :tax, :state_id, :status])
    end
end
