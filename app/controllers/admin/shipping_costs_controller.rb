class Admin::ShippingCostsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_shipping_cost, only: [:show, :edit, :update, :destroy]
  before_action :find_admin_shipping_plan
  before_action :find_admin_shipping_country
  before_action :find_admin_shipping_state
  before_action :find_admin_shipping_city

  def index
    @admin_shipping_costs = Admin::ShippingCost.all
  end

  def show
  end

  def new
    @admin_shipping_cost = Admin::ShippingCost.new
  end

  def edit
  end

  def create
    @admin_shipping_cost = @admin_shipping_city.shipping_costs.build(admin_shipping_cost_params)
    if @admin_shipping_cost.save
      respond_to do |format|
        format.html{
          redirect_to admin_tax_plan_path(@admin_tax_plan), notice: 'Shipping cost was successfully created.'
        }
        format.js {
          @admin_shipping_countries = @admin_shipping_plan.shipping_countries
          @admin_shipping_states = @admin_shipping_country.shipping_states
          @admin_shipping_cities = @admin_shipping_state.shipping_cities
          flash[:notice] = "Shipping cost was successfully created."
          render :template => "remote_content/shipping_cost_sucess.js.erb"
          flash.discard
        }
      end      
    else
      render 'new'
    end
  end

  def update
    
    if @admin_shipping_cost.update(admin_shipping_cost_params)
      redirect_to @admin_shipping_cost, notice: 'Shipping cost was successfully updated.' 
    else
      render 'edit'
    end
  end

  def destroy
    if @admin_shipping_cost.destroy
      redirect_to admin_shipping_costs_url, notice: 'Shipping cost was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_shipping_cost
      @admin_shipping_cost = Admin::ShippingCost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_shipping_cost_params
      params.require(:admin_shipping_cost).permit( :pincode_id, :cost)
    end

    def find_admin_shipping_city
      @admin_shipping_city = Admin::ShippingCity.find(params[:shipping_city_id])
    end

    def find_admin_shipping_state
      @admin_shipping_state = Admin::ShippingState.find(params[:shipping_state_id])
    end

    def find_admin_shipping_country
      @admin_shipping_country = Admin::ShippingCountry.find(params[:shipping_country_id])
    end

    def find_admin_shipping_plan
      @admin_shipping_plan = Admin::ShippingPlan.find(params[:shipping_plan_id])
    end
end
