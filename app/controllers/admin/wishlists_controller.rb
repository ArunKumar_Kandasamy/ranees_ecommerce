class Admin::WishlistsController < ApplicationController
	before_filter :authenticate_user!
  before_action :set_wishlist, only: [:show]
  before_action :authorize

	def index
		@param = params[:q] 
		@search = Wishlist.all.search(params[:q])
		@wishlists = @search.result
		@search.build_condition
        @search.build_sort if @search.sorts.empty?
	end

	def show
		@wishlist_items = @wishlist.wishlist_items
	end

private  
  def set_wishlist
    @wishlist = Wishlist.find(params[:id]) 
  end

end
