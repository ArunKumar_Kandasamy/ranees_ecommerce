class Admin::OrderRevocationItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_order_revocation_item, only: [:show, :edit, :update, :destroy]

  # GET /admin/order_revocation_items
  # GET /admin/order_revocation_items.json
  def index
    @admin_order_revocation_items = Admin::OrderRevocationItem.all
  end

  # GET /admin/order_revocation_items/1
  # GET /admin/order_revocation_items/1.json
  def show
  end

  # GET /admin/order_revocation_items/new
  def new
    @admin_order_revocation_item = Admin::OrderRevocationItem.new
  end

  # GET /admin/order_revocation_items/1/edit
  def edit
  end

  # POST /admin/order_revocation_items
  # POST /admin/order_revocation_items.json
  def create
    @admin_order_revocation_item = Admin::OrderRevocationItem.new(admin_order_revocation_item_params)

    respond_to do |format|
      if @admin_order_revocation_item.save
        format.html { redirect_to @admin_order_revocation_item, notice: 'Order revocation item was successfully created.' }
        format.json { render :show, status: :created, location: @admin_order_revocation_item }
      else
        format.html { render :new }
        format.json { render json: @admin_order_revocation_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/order_revocation_items/1
  # PATCH/PUT /admin/order_revocation_items/1.json
  def update
    respond_to do |format|
      if @admin_order_revocation_item.update(admin_order_revocation_item_params)
        format.html { redirect_to @admin_order_revocation_item, notice: 'Order revocation item was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_order_revocation_item }
      else
        format.html { render :edit }
        format.json { render json: @admin_order_revocation_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/order_revocation_items/1
  # DELETE /admin/order_revocation_items/1.json
  def destroy
    @admin_order_revocation_item.destroy
    respond_to do |format|
      format.html { redirect_to admin_order_revocation_items_url, notice: 'Order revocation item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_order_revocation_item
      @admin_order_revocation_item = Admin::OrderRevocationItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_order_revocation_item_params
      params.require(:admin_order_revocation_item).permit(:price, :order_revocation_id, :qty, :variant_id, :per_qty_price)
    end
end
