class Admin::VendorsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_vendor, only: [:show, :edit, :update, :destroy]
  before_action :authorize

  def index
    @param = params[:q]
    @search = Admin::Vendor.all.search(params[:q])
    @vendors = @search.result.includes(:addresses)
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_vendors_path
      @render_file = 'admin/vendors/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/vendors/index"
    end
  end

  def show
    @addresses = @vendor.addresses
    if request.xhr?
      @render_path = admin_vendor_path(@vendor)
      @render_file = 'admin/vendors/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/vendors/show"
    end
  end

  def new
    @vendor = Admin::Vendor.new
    1.times { @vendor.addresses.build }
    if request.xhr?
      @render_path = new_admin_vendor_path
      @render_file = 'admin/vendors/new'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/vendors/new"
    end
  end
                                                                                
  def edit
    @method_filters= @vendor.addresses
    if request.xhr?
      @render_path = edit_admin_vendor_path
      @render_file = 'admin/vendors/edit'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/vendors/edit"
    end
  end

  def create
    @vendor = Admin::Vendor.new(vendor_params)
    if @vendor.save
      @addresses = @vendor.addresses
      flash['alert'] = 'Vendor was successfully created.' 
      @render_path = admin_vendor_path(@vendor)
      @render_file = 'admin/vendors/show'
      render "remote_content/common_render.js.erb"          
    else
      @vendor = Admin::Vendor.new
      @render_path = new_admin_vendor_path
      @render_file = 'admin/vendors/new'
      render "remote_content/common_render.js.erb"
    end
  end

  def update
    if @vendor.update(vendor_params)
      @addresses = @vendor.addresses
      flash['alert'] = 'Vendor was successfully updated.' 
      @render_path = admin_vendor_path(@vendor)
      @render_file = 'admin/vendors/show'
      render "remote_content/common_render.js.erb"          
    else
      @render_path = edit_admin_vendor_path(@vendor)
      @render_file = 'admin/vendors/edit'
      render "remote_content/common_render.js.erb"
    end
  end

  def destroy
    if @vendor.destroy
      flash['alert'] = 'Vendor was successfully destroyed.' 
      @render_path = admin_vendors_path
      @render_file = 'admin/vendors/index'
      render "remote_content/common_render.js.erb"      
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vendor
      @vendor = Admin::Vendor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vendor_params
      params.require(:admin_vendor).permit(:name, addresses_attributes:[:id, :_destroy, :add_line1, :add_line2, :state, :country, :c_code, :phone, :connection])
    end
end