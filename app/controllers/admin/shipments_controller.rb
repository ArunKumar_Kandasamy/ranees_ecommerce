class Admin::ShipmentsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize

  before_action :set_order
  before_action :set_shipment, only: [:show, :edit,:update]
  
  def index
  end
  
  def edit 
    if request.xhr?
      @consignment = @shipment
      @consignment_revocation = @consignment.pickup_consignments.build 
      render :template => "remote_content/support_consignment_cancel.js.erb"
    end
  end
  
  def update 
    if params[:admin_consignment].present?
      if @shipment.delivery_status.nil?
        unless @shipment.check_regulation_present
          @shipment.update(consignment_params)
          @shipment.order.create_revocation
          redirect_to support_order_path(@shipment.order), notice: "Consignment Canceled Successfully"
        else
          redirect_to support_order_path(@shipment.order), notice: "Consignment Cancel failed"
        end
      elsif @shipment.delivery_status == false
        @shipment.update(consignment_params)
        if @shipment.order.payment_mode != "COD"
          if params[:admin_consignment][:refund_revocations][:revocation_mode] == "Cash Back"
              if params[:admin_consignment][:refund_revocations][:bank_account_id].nil?
                @bankaccount = BankAccount.create(:name => params[:admin_consignment][:bank_accounts][:name], :num => params[:admin_consignment][:bank_accounts][:name], :bank_code => params[:admin_consignment][:bank_accounts][:ifsc_code], :user_id => @order.user_id)
                @bank_account_id = @bankaccount.id
              else
                @bank_account_id = params[:admin_consignment][:order_revocations][:bank_account_id] 
              end
              @revocation_mode = 'b'
          else
            @revocation_mode = 'v'
          end
          @consignment_revocation = @shipment.create_revocation_for_cancel
          @consignment_revocation.update(:bank_account_id =>  @bank_account_id,:revocation_mode => @revocation_mode)
          redirect_to support_order_path(@shipment.order), notice: "Consignment Canceled Successfully"
        end
      else
        @shipment.update(consignment_params)
        redirect_to support_order_shipment_path(@shipment.order, @shipment), notice: "Consignment updated Successfully"  
      end
    end
  end 

  def show
  	@order = @shipment.order
    @consignment_items = @shipment.consignment_items
    if request.xhr?
      @render_path = admin_order_shipment_path(@order ,  @shipment)
      @render_file = 'admin/shipments/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/shipments/show"
    end
  end

  private
    def set_shipment
      @shipment = Admin::Consignment.find(params[:id])
    end
  
    def set_order
      @order = Admin::Order.find(params[:order_id])
    end

    def consignment_params
	  	params.require(:admin_consignment).permit(:order_id,:warehouse_id, :conform, :cancel, :remarks, :delivery_status )
	  end
end
