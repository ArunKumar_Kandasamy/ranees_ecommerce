class Admin::ReplacementRevocationItemsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_replacement_revocation_item, only: [:show, :edit, :update, :destroy]

  def index
    @admin_replacement_revocation_items = Admin::ReplacementRevocationItem.all
  end

  def show
  end

  def new
    @admin_replacement_revocation_item = Admin::ReplacementRevocationItem.new
  end

  def edit
  end

  def create
    @admin_replacement_revocation_item = Admin::ReplacementRevocationItem.new(admin_replacement_revocation_item_params)
    respond_to do |format|
      if @admin_replacement_revocation_item.save
        format.html { redirect_to @admin_replacement_revocation_item, notice: 'Replacement revocation item was successfully created.' }
        format.json { render :show, status: :created, location: @admin_replacement_revocation_item }
      else
        format.html { render :new }
        format.json { render json: @admin_replacement_revocation_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @admin_replacement_revocation_item.update(admin_replacement_revocation_item_params)
        format.html { redirect_to @admin_replacement_revocation_item, notice: 'Replacement revocation item was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_replacement_revocation_item }
      else
        format.html { render :edit }
        format.json { render json: @admin_replacement_revocation_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @consignment_item = @consignment.consignment_items.find_by_variant_id(@admin_replacement_revocation_item.variant_id)
    @consignment_item.qty += 1
    @consignment_item.save
    if @admin_replacement_revocation_item.pickup_consignment_items.first.present?
      @admin_replacement_revocation_item.pickup_consignment_items.first.destroy
    end
    @admin_replacement_revocation_item.destroy
    redirect_to support_order_shipment_path(@admin_replacement_revocation_item.replacement_revocation.consignment.order,@admin_replacement_revocation_item.replacement_revocation.consignment), notice: 'Replacement revocation item was successfully destroyed.' 
  end

  private
    def set_admin_replacement_revocation_item
      @admin_replacement_revocation_item = Admin::ReplacementRevocationItem.find(params[:id])
      @consignment = @admin_replacement_revocation_item.replacement_revocation.consignment
    end

    def admin_replacement_revocation_item_params
      params.require(:admin_replacement_revocation_item).permit(:replacement_revocation_id, :price, :qty, :variant_id, :per_qty_price, :actual_cost, :shipping_cost, :tax, :reason, :status)
    end
end
