class Admin::ImagesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_image, only: [:show, :edit, :update, :destroy]

  def index
    if request.xhr?
      @admin_images = Admin::Image.all
      @other_types = @admin_images.where(:type_of_image => "Banner")
      @admin_categories = Admin::Category.all
      @admin_parents = @admin_categories.roots
      @admin_parents.each do |admin_parent|
        @child_all = admin_parent.children
      end
      @product_images = @admin_images.where.not(:variant_id => nil)
      @render_path = admin_images_path
      @render_file = 'admin/images/index'
      render "remote_content/common_render.js.erb"          
    else
      @admin_images = Admin::Image.all
      @other_types = @admin_images.where(:type_of_image => "Banner")
      @admin_categories = Admin::Category.all
      @admin_parents = @admin_categories.roots
      @admin_parents.each do |admin_parent|
        @child_all = admin_parent.children
      end
      @product_images = @admin_images.where.not(:variant_id => nil)
      render :template => "admin/images/index"
    end
    
  end

  def show
  end

  def new
    if request.xhr?
      @admin_image = Admin::Image.new
      @cat_id = params[:category_id]
      @check_id = params[:check_id]
      respond_to do |format|
        format.html{
        }         
        format.js {
          render :template => "admin/images/image_create.js.erb"
        }
      end   
    else
      @admin_image = Admin::Image.new
    end
    
  end

  def banner_sort
    params[:admin_image].each_with_index do |id, index|
      Admin::Image.where(id: id).update_all(banner_sort: index+1)
    end
    render nothing: true
  end

  def child_image_sort
    params[:admin_image].each_with_index do |id, index|
      Admin::Image.where(id: id).update_all(child_image_sort: index+1)
    end
    render nothing: true
  end

  def edit
  end

  def create    
    @admin_image = Admin::Image.new(admin_image_params)
    if @admin_image.save
      redirect_to @admin_image, notice: 'Image was successfully created.'
    else
      render 'new'
    end
  end

  def update
    if @admin_image.update(admin_image_params)
      redirect_to @admin_image, notice: 'Image was successfully updated.'
    else
      render 'edit'
    end
  end

  def sort
    params[:admin_image].each_with_index do |id, index|
      Admin::Image.where(id: id).update_all(sort_order: index+1)
    end
    render nothing: true
  end

  def parent_image_sort
    params[:admin_image].each_with_index do |id, index|
      Admin::Image.where(id: id).update_all(parent_image_sort: index+1)
    end
    render nothing: true
  end

  def destroy
    if @admin_image.destroy
      redirect_to admin_images_url, notice: 'Image was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_image
      @admin_image = Admin::Image.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_image_params
      params.require(:admin_image).permit(:parent_image_sort,:child_image_sort,:category_id,:banner_sort, :avatar, :variant_id, :general_id, :alt_text, :type_of_image, :link_url)
    end
end
