class Admin::OrdersController < ApplicationController  
  before_action :authenticate_user!
  before_action :authorize  
  before_action :set_order, only: [:show, :edit, :update, :destroy]    
 
  def create     
    @new_consignment_hash = params[:admin_order]
    @order = Admin::Order.find(params[:admin_order][:order_id])
    if @order.check_order_status  
      @new_consignment_hash[:consignments_attributes].each do |new_consignment_hash|
        consignment = Admin::Consignment.where(:warehouse_id => new_consignment_hash[1][:warehouse_id],:order_id => params[:admin_order][:order_id])
        if consignment.present?
          new_consignment = consignment.first
        else 
          new_consignment = Admin::Consignment.create(:warehouse_id => new_consignment_hash[1][:warehouse_id],:order_id => params[:admin_order][:order_id])
        end
        @order_item = Admin::OrderItem.find(new_consignment_hash[1][:order_item_id])
        @consignment_item = Admin::ConsignmentItem.create(:qty => new_consignment_hash[1][:qty], :variant_id => @order_item.variant_id, :per_qty_price => @order_item.per_qty_price, :consignment_id => new_consignment.id, :return_due => @order_item.return_due, :shipping_cost => @order_item.shipping_cost, :tax => @order_item.tax , :actual_cost => @order_item.actual_cost)
        Admin::OrderItemsConsignmentItems.create(:consignment_item_id => @consignment_item.id , :order_item_id => @order_item.id)
        #@order.wh_status_update
        #@order.create_order_log(current_user.id)
      end
      @order.wh_status_update
      @order.create_order_log(current_user.id)
      redirect_to admin_order_path(@order), notice: 'Consignment was successfully created.'
    else
      redirect_to admin_order_path(@order), notice: 'Sorry consignment is not created.'
    end  
  end

  def index
    @param = params[:q]
    @search = Admin::Order.all.search(params[:q])
    @orders = @search.result.includes(:user)
    @search.build_condition   
    @wh_order = @search.result.wh_orders.page(params[:wh_page]).includes(:user)
    @up_order = @search.result.up_orders.page(params[:up_page]).includes(:user)
    @pa_order = @search.result.partial_assigned_orders.page(params[:pa_page]).includes(:user)
    if request.xhr?
      if params[:up_page].present?
        render :template => "admin/orders/up_order_page.js.erb"
        @up_order = Admin::Order.up_orders.all.page(params[:up_page]).search(params[:q])
      elsif params[:wh_page].present?
        render :template => "admin/orders/wh_order_page.js.erb"
        @wh_order = Admin::Order.wh_orders.all.page(params[:wh_page]).search(params[:q])
      elsif params[:pa_page].present?
        render :template => "admin/orders/pa_order_page.js.erb"
        @pa_order = Admin::Order.partial_assigned_orders.all.page(params[:pa_page]).search(params[:q])
      else
        @render_path = admin_orders_path
        @render_file = 'admin/orders/index'
        render "remote_content/common_render.js.erb"
      end          
    else
      render :template => "admin/orders/index"
    end

  end           
                              
  def show
    @items = @order.order_items 
  	@consignment= Admin::Order.new
    1.times { @consignment.consignments.build }
    @order = Admin::Order.find(params[:id])  
    @rejection_reason= Admin::RejectionReason.new 
    @admin_consignment_item = Admin::ConsignmentItem.new
    @order_warehouses = Admin::Warehouse.all
    @order_warehouse = @order.warehouse 
    if request.xhr?
      if params[:order_item].present?
        render :template => "remote_content/refresh_order_items.js.erb"
      elsif params[:view].present?
        render :template => "admin/orders/admin_show.js.erb"  
      else
        @render_path = admin_order_path(@order)
        @render_file = 'admin/orders/show'
        render "remote_content/common_render.js.erb"
      end                                 
    end                          
  end     

                         
  def find_variant_with_sku   
    if request.xhr?
      @order = Admin::Order.find(params[:order_id])          
      if @order.payment_mode == "COD"
        @variant = Admin::Variant.find_by_sku(params[:sku])  
        @new_pincode =  ShippingAddress.find(params[:shipping_address_id]).pincode
        if @variant.visible == true && @variant.status == true
          @product = @variant.product
          if @product.status == true && @product.visibility == true && @product.cod == true
            @shipping = Admin::Product.shipping_cost(@new_pincode, @product, @variant)
            if @shipping.status == true
              render :template => "remote_content/refresh_order_item_variants.js.erb"
            else
              @error = "Sorry Shipping Unavailable for this item" 
              render :template => "remote_content/refresh_order_item_variants_error.js.erb"
            end
          else
            @error = "Sorry Product Unavailable"
            render :template => "remote_content/refresh_order_item_variants_error.js.erb"
          end
        else
          @error = "Sorry Product Unavailable"
          render :template => "remote_content/refresh_order_item_variants_error.js.erb"
        end
      else
        @error = "Sorry this feature is not applicable"
        render :template => "remote_content/refresh_order_item_variants_error.js.erb"
      end
    end
  end
  
  def order_logs
    @order = Admin::Order.find(params[:order_id])
    @admin_order_logs= @order.order_logs
    if request.xhr?
      @render_path = admin_order_order_logs_path(@order)
      @render_file = 'admin/orders/order_logs'
      render "remote_content/common_render.js.erb"
    else
      render :template => "admin/orders/order_logs"    
    end  
  end

  def edit
    @new_shipping_address = ShippingAddress.new
    @items = @order.order_items
    @order_revocation = @order.order_revocations.build 
    if request.xhr?
      @render_path = edit_admin_order_path(@order)
      @render_file = 'admin/orders/edit'
      render "remote_content/common_render.js.erb"
    else
      render :template => "admin/orders/edit"    
    end  
  end
                
  def update  
    if request.xhr? 
      if params[:admin_order][:for_check].present?
        @new_order_hash = params[:admin_order]
        render :template => "remote_content/order_edit_preview.js.erb"
      else
        if params[:admin_order][:shipping_address_id].present?
          @order.update(order_params)
          @order.create_order_log(current_user.id)
          flash[:notice] = 'Order was successfully created.'
          render :js => "window.location = '#{admin_order_path(@order)}'"
        else
          if params[:admin_order][:warehouse_id].present?
            @order.wh_status_update
            @order.create_order_log(current_user.id)
            flash[:notice] = "Shipping Plan Added."
            render :template => "remote_content/order_warehouse.js.erb"
          else
            if params[:admin_order][:present_user].present?
              if @order.status == "up" && @order.check_consignment_status && @order.cancel.nil?
                @order.update(order_params) 
                @order.create_order_log(current_user.id)
                render :template => "remote_content/order_warehouse.js.erb"
              else
                flash[:notice] = 'Sorry You Cannot Update This Order'
                render :template => "remote_content/order_warehouse.js.erb"
              end  
            end
          end
        end
      end 
    else 
      if params[:admin_order][:update_consignment].present?
        @new_consignment_hash = params[:admin_order]
        @new_consignment_hash[:order_items_attributes].each do |new_consignment_hash|
          consignment = Admin::Consignment.where(:warehouse_id => new_consignment_hash[1][:warehouse_id],:order_id => params[:id])
          if consignment.present?
            new_consignment = consignment.first
          else
            new_consignment = Admin::Consignment.create(:warehouse_id => new_consignment_hash[1][:warehouse_id],:order_id => params[:id])
          end
          @order_item = Admin::OrderItem.find(new_consignment_hash[1][:id])
          if @order_item.consignment_item_id.present?
            @consignment_item = Admin::ConsignmentItem.find(@order_item.consignment_item_id)
            @consignment_item.consignment_id = new_consignment.id
            @consignment_item.save!
          else
            @consignment_item = Admin::ConsignmentItem.create(:qty => @order_item.qty, :variant_id => @order_item.variant_id, :price => @order_item.price, :per_qty_price => @order_item.per_qty_price, :consignment_id => new_consignment.id, :return_due => @order_item.return_due)
            @order_item.consignment_item_id = @consignment_item.id
            @order_item.save!
          end
          @order.consignments.each do |consignment|
            if consignment.consignment_items.first.nil?
              consignment.destroy
            end
          end
        end
        @order.create_order_log
        redirect_to admin_order_path(@order), notice: 'Consignment was successfully updated.'
      else
        @new_order_hash = params[:admin_order]
        @order.update(order_params)
        @order.create_order_log
        redirect_to admin_order_path(@order), notice: 'Order was successfully updated.'
      end
    end
  end 

  private 
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Admin::Order.find(params[:id])
    end
    def set_warehouse
      @warehouse = Admin::Warehouse.find(params[:warehouse_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:admin_order).permit(:user_id, :pay_val, :cal_conf, :present_user, :prev_rec,  :shipping_address_id, :order_cost, :payment_mode, :payment_status, :order_number, :status,:warehouse_id, :prev_rec_reason, :pay_val_reason, :cal_conf_reason, order_items_attributes:[:id, :variant_id, :_destroy, :qty, :per_qty_price],consignments_attributes:[:id,:order_item_id,:warehouse_id,:consignment_cost]) 
    end 
end