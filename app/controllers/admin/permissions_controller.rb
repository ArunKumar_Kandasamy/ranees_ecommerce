class Admin::PermissionsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_permission, only: [:edit, :update, :destroy]
  before_action :find_user
  before_action :authorize
   
  def index 
    flash.discard 
    @search = Admin::Permission.all.search(params[:q])
    @permissions = @search.result.includes(:role)
    @permission = Admin::Permission.new
    @role= Admin::Role.order(:name).where("name != ?","pr")
    @roles= @role.each do |role|
      if role.name == "sa"
        role.name= t :sa
      elsif(role.name== "u")
        role.name= t :u
      else
        role.name= role.name
      end
    end 
    @permissions.each do |permission|
      if permission.role.name == "sa"
        permission.role.name= t :sa
      elsif(permission.role.name == "u")
        permission.role.name= t :u
      elsif(permission.role.name == "pr")
        permission.role.name= t :pr
      else
        permission.role.name
      end
    end
    if request.xhr?
      @render_path = admin_permissions_path
      @render_file = 'admin/permissions/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/permissions/index"
    end
  end

  def edit
    #@role= Admin::Role.order(:name).where("name != ?","pr")
    @role= Admin::Role.order(:name)
    @roles= @role.each do |role|
      if role.name == "sa"
        role.name= t :sa
      elsif(role.name== "u")
        role.name= t :u
      else
        role.name= role.name
      end
    end
    if request.xhr?
      @render_path = edit_admin_permission_path(@permission)
      @render_file = 'admin/permissions/edit'
      render "remote_content/common_modal_render.js.erb"          
    else
      render :template => "admin/permissions/edit"
    end
  end

  def create
    @permission = Admin::Permission.new(permission_params)
    if @permission.save
      @search = Admin::Permission.all.search(params[:q])
      @permissions = @search.result.includes(:role)
      @permission = Admin::Permission.new
      @role= Admin::Role.order(:name).where("name != ?","pr")
      @roles= @role.each do |role|
        if role.name == "sa"
          role.name= t :sa
        elsif(role.name== "u")
          role.name= t :u
        else
          role.name= role.name
        end
      end
      @permissions.each do |permission|
        if permission.role.name == "sa"
          permission.role.name= t :sa
        elsif(permission.role.name == "u")
          permission.role.name= t :u
        elsif(permission.role.name == "pr")
          permission.role.name= t :pr
        else
          permission.role.name
        end
      end
      flash[:alert] = 'Invited successfully.'
      @render_path = admin_permissions_path
      @render_file = 'admin/permissions/index'
      render "remote_content/common_render.js.erb"
    end 
  end 

  def update
    if @permission.update(permission_params)
      @search = Admin::Permission.all.search(params[:q])
      @permissions = @search.result.includes(:role)
      @permission = Admin::Permission.new
      @role= Admin::Role.order(:name).where("name != ?","pr")
      @roles= @role.each do |role|
        if role.name == "sa"
          role.name= t :sa
        elsif(role.name== "u")
          role.name= t :u
        else
          role.name= role.name
        end
      end
      @permissions.each do |permission|
        if permission.role.name == "sa"
          permission.role.name= t :sa
        elsif(permission.role.name == "u")
          permission.role.name= t :u
        elsif(permission.role.name == "pr")
          permission.role.name= t :pr
        else
          permission.role.name
        end
      end
      flash[:alert] = 'Permission was successfully updated.'
      @render_path = admin_permissions_path
      @render_file = 'admin/permissions/index'
      render "remote_content/common_render.js.erb"          
    else
      @render_path = edit_admin_permission_path(@permission)
      @render_file = 'admin/permissions/edit'
      render "remote_content/common_modal_render.js.erb"
    end
  end

  def destroy
    if @permission.active == false 
      if @permission.destroy
        flash[:alert] = 'Permission was successfully destroyed.'
        @search = Admin::Permission.all.search(params[:q])
        @permissions = @search.result.includes(:role)
        @permission = Admin::Permission.new
        @role= Admin::Role.order(:name).where("name != ?","pr")
        @roles= @role.each do |role|
          if role.name == "sa"
            role.name= t :sa
          elsif(role.name== "u")
            role.name= t :u
          else
            role.name= role.name
          end
        end 
        @permissions.each do |permission|
          if permission.role.name == "sa"
            permission.role.name= t :sa
          elsif(permission.role.name == "u")
            permission.role.name= t :u
          elsif(permission.role.name == "pr")
            permission.role.name= t :pr
          else
            permission.role.name
          end
        end
        @render_path = admin_permissions_path
        @render_file = 'admin/permissions/index'
        render "remote_content/common_render.js.erb"     
      end
    else
      flash[:alert] = 'Not allowed to perform this action' 
      @render_path = admin_permission_path(@permission)
      @render_file = 'admin/permissions/show'
      render "remote_content/common_render.js.erb" 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_permission
      @permission = Admin::Permission.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def permission_params
      params.require(:admin_permission).permit(:user_id, :role_id, :active, :email, :block_lists_attributes)
    end

    def find_user
      @user=current_user
    end
end
