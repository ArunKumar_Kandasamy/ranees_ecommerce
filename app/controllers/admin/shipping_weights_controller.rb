class Admin::ShippingWeightsController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_shipping_weight, only: [:show, :edit, :update, :destroy]

  # GET /admin/shipping_weights
  # GET /admin/shipping_weights.json
  def index
    @admin_shipping_weights = Admin::ShippingWeight.all
  end

  # GET /admin/shipping_weights/1
  # GET /admin/shipping_weights/1.json
  def show
  end

  # GET /admin/shipping_weights/new
  def new
    @admin_shipping_weight = Admin::ShippingWeight.new
  end

  # GET /admin/shipping_weights/1/edit
  def edit
  end

  # POST /admin/shipping_weights
  # POST /admin/shipping_weights.json
  def create
    @admin_shipping_weight = Admin::ShippingWeight.new(admin_shipping_weight_params)

    respond_to do |format|
      if @admin_shipping_weight.save
        format.html { redirect_to @admin_shipping_weight, notice: 'Shipping weight was successfully created.' }
        format.json { render :show, status: :created, location: @admin_shipping_weight }
      else
        format.html { render :new }
        format.json { render json: @admin_shipping_weight.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/shipping_weights/1
  # PATCH/PUT /admin/shipping_weights/1.json
  def update
    respond_to do |format|
      if @admin_shipping_weight.update(admin_shipping_weight_params)
        format.html { redirect_to @admin_shipping_weight, notice: 'Shipping weight was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_shipping_weight }
      else
        format.html { render :edit }
        format.json { render json: @admin_shipping_weight.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/shipping_weights/1
  # DELETE /admin/shipping_weights/1.json
  def destroy
    @admin_shipping_weight.destroy
    respond_to do |format|
      format.html { redirect_to admin_shipping_weights_url, notice: 'Shipping weight was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_shipping_weight
      @admin_shipping_weight = Admin::ShippingWeight.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_shipping_weight_params
      params.require(:admin_shipping_weight).permit(:range, :cost, :shipping_policy_id, :shipping_specific_country_id, :shipping_specific_pincode_id)
    end
end
