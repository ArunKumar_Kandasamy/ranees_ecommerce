class Admin::AssignedPickupConsignmentsController < ApplicationController
	before_action :set_pickup_consignment, only: [:update, :show, :edit, :destroy]
  before_action :set_warehouse   

  def index
    @param = params[:q]     
    @search = Admin::PickupConsignment.all.page(params[:page]).search(params[:q])
    @admin_pickup_consignments = @search.result.where("parent_consignment_id IS NOT NULL")
    @pickup_consignment = @warehouse.pickup_consignments
    @admin_pickup_task = Admin::PickupTask.new
    1.times { @admin_pickup_task.pickup_tasks_pickup_consignments.build }
    @search.build_condition
    @search.build_sort if @search.sorts.empty?
    if request.xhr?
      @render_path = admin_warehouse_dashboard_assigned_pickup_consignments_path
      @render_file = 'admin/assigned_pickup_consignments/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/assigned_pickup_consignments/index"
    end
  end


  def show
    @admin_regulation = Admin::Regulation.new
    @order = @pickup_consignment.consignment.order
    @pickup_consignment_items = @pickup_consignment.pickup_consignment_items 
    @admin_pickup_task = @pickup_consignment.pickup_tasks.first
    if request.xhr?
      @render_path = admin_warehouse_dashboard_pickup_task_pickup_consignment_show_path_path(@warehouse,@admin_pickup_task,@pickup_consignment)
      @render_file = 'admin/assigned_pickup_consignments/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/assigned_pickup_consignments/show"
    end
  end

  def update
    @admin_regulation = Admin::Regulation.new
    @order = @pickup_consignment.consignment.order
    @pickup_consignment.update(admin_pickup_consignment_params)
    @pickup_consignment_items = @pickup_consignment.pickup_consignment_items 
    @admin_pickup_task = @pickup_consignment.pickup_tasks.first
    if request.xhr?
      @render_path = admin_warehouse_dashboard_pickup_task_pickup_consignment_show_path_path(@warehouse,@admin_pickup_task,@pickup_consignment)
      @render_file = 'admin/assigned_pickup_consignments/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/assigned_pickup_consignments/show"
    end
  end

  def destroy
  end

  private
    def set_pickup_consignment
      @pickup_consignment = Admin::PickupConsignment.find(params[:id])
    end

    def set_warehouse
      @warehouse = Admin::Warehouse.find(params[:warehouse_dashboard_id])
    end

    def admin_pickup_consignment_params
      params.require(:admin_pickup_consignment).permit(:pickup_consignment_number, :consignment_cost, :consignment_id, :logistic_id, :warehouse_id, :status, :shipping_address_id, :created_by, :status, :received, pickup_consignment_items_attributes:[:id ,:price, :qty, :variant_id, :per_qty_price, :received, :_destroy])
    end

end
