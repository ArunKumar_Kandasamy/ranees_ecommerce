class Admin::DistrictCitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_district_city, only: [:show, :edit, :update, :destroy]

  def index
    if request.xhr?
      @render_path = admin_country_state_district_cities_path(@country, @state)
      @render_file = 'admin/district_cities/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/district_cities/index"
    end
  end

  def import
    Admin::DistrictCity.import(params[:file])
    redirect_to admin_district_cities_path, notice: "Countries imported."
  end

  def show
    @pincodes = @district_city.pincodes
    if request.xhr?
      @render_path = admin_country_state_district_city_path(@country, @state,@district_city)
      @render_file = 'admin/district_cities/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/district_cities/show"
    end
  end

  def new
    @district_city = Admin::DistrictCity.new
    if request.xhr?
      @render_path = new_admin_country_state_district_city_path(@country, @state)
      @render_file = 'admin/district_cities/new'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/district_cities/new"
    end
  end

  def edit
    if request.xhr?
      @render_path = edit_admin_country_state_district_city_path(@country, @state, @district_city)
      @render_file = 'admin/district_cities/edit'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/district_cities/edit"
    end
  end

  def create 
    @district_city = @state.district_cities.build(district_city_params)
    if @district_city.save
      flash[:alert] = 'District city was successfully created.'
      @render_path = admin_country_state_district_city_path(@country, @state, @district_city)
      @render_file = 'admin/district_cities/show'
      render "remote_content/common_render.js.erb"          
    else
      @district_city = Admin::DistrictCity.new
      render :template => "admin/district_cities/show"
    end
  end

  def update
    if @district_city.update(district_city_params)
      flash[:alert] = 'District city was successfully updated.'
      @render_path = admin_country_state_district_city_path(@country, @state, @district_city)
      @render_file = 'admin/district_cities/show'
      render "remote_content/common_render.js.erb" 
    else
      render :template => "admin/district_cities/edit"
    end
  end

  def destroy
    @district_city.destroy
    flash[:alert] = 'District city was successfully destroyed.'
    @render_path = admin_country_state_district_cities_path(@country, @state)
    @render_file = 'admin/district_cities/index'
    render "remote_content/common_render.js.erb"
  end

  private
    def set_district_city
      @district_city = Admin::DistrictCity.find(params[:id])
    end

    def district_city_params
      params.require(:admin_district_city).permit(:state_id, :name, :district_code)
    end
end