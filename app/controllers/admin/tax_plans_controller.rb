class Admin::TaxPlansController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_tax_plan, only: [:show, :edit, :update, :destroy]
  after_action :check_for_activation, only: [ :update]

  def index  
    @param = params[:q]
    @search =  Admin::TaxPlan.all.search(params[:q])
    @admin_tax_plans = @search.result.page(params[:page])
    @search.build_condition
    @search.build_sort if @search.sorts.empty?  
    if request.xhr?
      @render_path = admin_tax_plans_path
      @render_file = 'admin/tax_plans/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/tax_plans/index"
    end
  end

  def show
    @admin_tax_countries = @admin_tax_plan.tax_countries
    @filter_tax_countries = Admin::Country.all.map{|x| x.id}
    @filter_admin_tax_countries = @admin_tax_plan.tax_countries.map{|x| x.country_id}
    
    @availability_plan = Admin::AvailabilityPlan.all
    @existing_countries_collection  = []
    @availability_plan.each do |availability_plan|
         @existing_countries_collection.push(*availability_plan.plan_countries.map{|x| x.country_id.nil? ? nil : x.country_id }).uniq
    end
    @current_available_countries = @existing_countries_collection.compact - @filter_admin_tax_countries
    @filter_countries_results = Admin::Country.find(@current_available_countries )
    @admin_tax_country = Admin::TaxCountry.new   
    @admin_tax_state = Admin::TaxState.new
    if request.xhr?
      @render_path = admin_tax_plan_path(@admin_tax_plan)
      @render_file = 'admin/tax_plans/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/tax_plans/show"
    end     
  end

  def new
    @admin_tax_plan = Admin::TaxPlan.new
    if request.xhr?
      @render_path = new_admin_tax_plan_path
      @render_file = 'admin/tax_plans/new'
      render "remote_content/common_modal_render.js.erb"          
    else
      render :template => "admin/tax_plans/new"
    end
  end

  def edit
    if request.xhr?
      @render_path = edit_admin_tax_plan_path(@admin_tax_plan)
      @render_file = 'admin/tax_plans/edit'
      render "remote_content/common_modal_render.js.erb"          
    else
      render :template => "admin/tax_plans/edit"
    end
  end

  def create
    @admin_tax_plan = Admin::TaxPlan.new(admin_tax_plan_params)
    if @admin_tax_plan.save
      @search =  Admin::TaxPlan.all.search(params[:q])
      @admin_tax_plans = @search.result.page(params[:page])
      @search.build_condition
      @search.build_sort if @search.sorts.empty?  
      flash[:alert] = 'Tax Plan was successfully created.'
      @render_path = admin_tax_plans_path
      @render_file = 'admin/tax_plans/index'
      render "remote_content/common_render.js.erb" 
    else
      @render_path = new_admin_tax_plan_path
      @render_file = 'admin/tax_plans/new'
      render "remote_content/common_modal_render.js.erb"
    end
  end

  def update
    if @admin_tax_plan.update(admin_tax_plan_params)
      @search =  Admin::TaxPlan.all.search(params[:q])
      @admin_tax_plans = @search.result.page(params[:page])
      @search.build_condition
      @search.build_sort if @search.sorts.empty?  
      flash[:alert] = 'Tax plan was successfully updated.'
      @render_path = admin_tax_plans_path
      @render_file = 'admin/tax_plans/index'
      render "remote_content/common_render.js.erb" 
    else
      @render_path = edit_admin_tax_plan_path(@admin_tax_plan)
      @render_file = 'admin/tax_plans/edit'
      render "remote_content/common_modal_render.js.erb"  
    end
  end

  def destroy
    if @admin_tax_plan.destroy
      @search =  Admin::TaxPlan.all.search(params[:q])
      @admin_tax_plans = @search.result.page(params[:page])
      @search.build_condition
      @search.build_sort if @search.sorts.empty?  
      flash[:alert] = 'Tax plan was successfully destroyed.'
      @render_path = admin_tax_plans_path
      @render_file = 'admin/tax_plans/index'
      render "remote_content/common_render.js.erb" 
    end
  end
 
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_tax_plan
      @admin_tax_plan = Admin::TaxPlan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_tax_plan_params
      params.require(:admin_tax_plan).permit(:name, :formula, :all_country, :all_country_tax, :rest_of_the_country_tax)
    end

    def check_for_activation
      @admin_tax_plan= Admin::TaxPlan.find(params[:id])
      @products = @admin_tax_plan.products
      if @products.present?
        @products.each do | product |
          if product.activate_product
            product.status= true
            product.save
          else
            product.status= false
            product.save
          end
        end
      end
    end
end
