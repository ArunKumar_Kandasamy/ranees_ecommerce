class Admin::RegulationsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_regulation, only: [:show, :edit, :update, :destroy]
  before_action :set_admin_inventory, only: [:index,:new, :create, :show, :edit, :update, :destroy]
  before_action :set_admin_variant
  before_action :set_admin_product
  respond_to :html, :json

  def index
    @admin_regulations = Admin::Regulation.all
  end

  def show
    @inventory= Admin::Inventory.new
    respond_to do |format|
      format.html{
      }
      format.js {
        render :template => "remote_content/add_inventory_regulation.js.erb"
      }
    end
  end

  def new
    @admin_regulation = Admin::Regulation.new
  end

  def edit
  end
 
  def create
    if params[:admin_regulation][:consignment_item_id].present?
      @variants = @product.variants
      consignment_item = Admin::ConsignmentItem.find(params[:admin_regulation][:consignment_item_id])
      task = Admin::PackageTask.find(params[:admin_regulation][:task_id])
      @bal = @admin_inventory.balance_qty - params[:admin_regulation][:qty].to_i
      if @bal >= 0 && consignment_item.check_availability_for_regulation_create(params[:admin_regulation][:qty]) && consignment_item.consignment.cancel.nil?
        @admin_regulation = @admin_inventory.regulations.build(admin_regulation_params)
        if @admin_regulation.save
          @balance=(@admin_inventory.balance_qty.to_i)-(@admin_regulation.qty.to_i)
          sold= (@admin_inventory.qty.to_i)- @balance
          @admin_inventory.balance_qty = @balance
          @admin_inventory.save
          @admin_inventory.update_attributes(sold_qty: sold)
          respond_to do |format| 
            format.html{
              redirect_to admin_warehouse_dashboard_package_task_task_consignment_show_path(@admin_inventory.warehouse,task,consignment_item.consignment)
            }
            format.js {
              @admin_inventory = Admin::Inventory.new
              render :template => "remote_content/inventories.js.erb"
            }
          end
        else
          render 'new'
        end
      else
        redirect_to admin_warehouse_dashboard_package_task_task_consignment_show_path(@admin_inventory.warehouse,task,consignment_item.consignment), notice: 'Regulation cannot be created.'
      end
    else
      @variants = @product.variants
      @admin_regulation = @admin_inventory.regulations.build(admin_regulation_params)
      if @admin_regulation.save
        @admin_inventory= Admin::Inventory.find_by_id(@admin_regulation.inventory_id)
        @balance=(@admin_inventory.balance_qty.to_i)-(@admin_regulation.qty.to_i)
        sold= (@admin_inventory.qty.to_i)- @balance
        @admin_inventory.balance_qty = @balance
        @admin_inventory.save
        @admin_inventory.update_attributes(sold_qty: sold)
        respond_to do |format|
          format.html{
          }
          format.js {
            @admin_variant = @variant
            #render :template => "remote_content/inventories.js.erb"
            @render_path = admin_product_variant_path(@product,@variant)
            @render_file = 'admin/variants/show'
            render "remote_content/common_render.js.erb"
          }
        end
      else
        render 'new'
      end
    end

  end 

  def update    
    prev_qty = @admin_regulation.qty 
    if @admin_regulation.update(admin_regulation_params)
      @admin_inventory= Admin::Inventory.find_by_id(@admin_regulation.inventory_id)
      if prev_qty < @admin_regulation.qty
        qty = (@admin_regulation.qty.to_i) - (prev_qty.to_i)
        @balance=(@admin_inventory.balance_qty.to_i)-(qty.to_i)
        sold= (@admin_inventory.qty.to_i)- @balance
      elsif prev_qty > @admin_regulation.qty
        qty = (prev_qty.to_i) - (@admin_regulation.qty.to_i)
        @balance = (@admin_inventory.balance_qty.to_i) + (qty.to_i)
        sold = (@admin_inventory.sold_qty.to_i) - qty
      end
      @admin_inventory.balance_qty = @balance
      @admin_inventory.save
      @admin_inventory.update_attributes(sold_qty: sold)
    end
    if params[:admin_regulation][:consignment_item_id].present?
      consignment = Admin::ConsignmentItem.find(params[:admin_regulation][:consignment_item_id]).consignment
      task = Admin::PackageTask.find(params[:admin_regulation][:task_id])
      redirect_to admin_warehouse_dashboard_package_task_task_consignment_show_path(@admin_inventory.warehouse,task,consignment)
    else
      respond_with @admin_regulation
    end
  end

  def destroy
    if @admin_regulation.destroy
      redirect_to admin_inventory_regulations_path(@admin_inventory), notice: 'Regulation was successfully destroyed.' 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_regulation
      @admin_regulation = Admin::Regulation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_regulation_params
      params.require(:admin_regulation).permit(:inventory_id, :qty, :reason, :note, :consignment_item_id)
    end

    def set_admin_inventory
      @admin_inventory = Admin::Inventory.find(params[:inventory_id])
    end

    def set_admin_variant
      @variant = Admin::Variant.find(params[:variant_id])
    end

    def set_admin_product
      @product = Admin::Product.find(params[:product_id])
    end
end
