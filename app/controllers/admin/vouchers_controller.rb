class Admin::VouchersController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_voucher, only: [:show, :edit, :update, :destroy]
  before_action :authorize

	def create
	@voucher = Admin::Voucher.new(voucher_params)
    if @voucher.save
    	if @voucher.refund_revocation_id.present?
    		redirect_to admin_refund_revocations_path, notice: 'Voucher was successfully created.' 
    	else
        redirect_to admin_order_revocations_path, notice: 'Voucher was successfully created.' 
      end
    else
      render 'new'
    end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_voucher
		  @vendor = Admin::Vendor.find(params[:id])
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def voucher_params
		  params.require(:admin_voucher).permit(:user_id, :order_id, :credit, :debit, :voucher_amount, :refund_revocation_id,:order_revocation_id)
		end
end
