class Admin::TypesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_admin_type, only: [:show, :edit, :update, :destroy]
  before_filter :authorize
  def index
    @admin_types = Admin::Type.all
  end

  def show
  end

  def new
    @admin_type = Admin::Type.new
    respond_to do |format|
      format.html
      format.json { 
        render :json => Admin::Type.tokens(params[:q])
      }
    end
  end

  def edit
  end

  def create
    @admin_type = Admin::Type.new(admin_type_params)
    if @admin_type.save
      redirect_to @admin_type, notice: 'Type was successfully created.'
    else
      render 'new'
    end
  end

  def update
    if @admin_type.update(admin_type_params)
      redirect_to @admin_type, notice: 'Type was successfully updated.'
    else
      render 'edit'
    end
  end

  def destroy
    if @admin_type.destroy
      redirect_to admin_types_url, notice: 'Type was successfully destroyed.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_type
      @admin_type = Admin::Type.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_type_params
      params[:admin_type]
    end
end
