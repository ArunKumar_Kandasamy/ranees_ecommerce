class Admin::PickupConsignmentItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_pickup_consignment_item, only: [:show, :edit, :update, :destroy]
  # GET /admin/pickup_consignment_items
  # GET /admin/pickup_consignment_items.json
  def index
    @admin_pickup_consignment_items = Admin::PickupConsignmentItem.all
  end

  # GET /admin/pickup_consignment_items/1
  # GET /admin/pickup_consignment_items/1.json
  def show
  end

  

  # GET /admin/pickup_consignment_items/new
  def new
    @admin_pickup_consignment_item = Admin::PickupConsignmentItem.new
  end

  # GET /admin/pickup_consignment_items/1/edit
  def edit   
  end

  # POST /admin/pickup_consignment_items
  # POST /admin/pickup_consignment_items.json
  def create
    @admin_pickup_consignment_item = Admin::PickupConsignmentItem.new(admin_pickup_consignment_item_params)
    respond_to do |format|
      if @admin_pickup_consignment_item.save
        format.html { redirect_to @admin_pickup_consignment_item, notice: 'Pickup consignment item was successfully created.' }
        format.json { render :show, status: :created, location: @admin_pickup_consignment_item }
      else
        format.html { render :new }
        format.json { render json: @admin_pickup_consignment_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/pickup_consignment_items/1
  # PATCH/PUT /admin/pickup_consignment_items/1.json
  def update
    respond_to do |format|
      if @admin_pickup_consignment_item.update(admin_pickup_consignment_item_params)        
        format.html { redirect_to admin_warehouse_dashboard_pickup_task_pickup_consignment_show_path_path(@admin_pickup_consignment_item.pickup_consignment.warehouse_id , @admin_pickup_consignment_item.pickup_consignment.pickup_tasks.first.id, @admin_pickup_consignment_item.pickup_consignment.id), notice: 'Pickup consignment item was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_pickup_consignment_item }
      else
        format.html { render :edit }
        format.json { render json: @admin_pickup_consignment_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/pickup_consignment_items/1
  # DELETE /admin/pickup_consignment_items/1.json
  def destroy
    @admin_pickup_consignment_item.destroy
    respond_to do |format|
      format.html { redirect_to admin_pickup_consignment_items_url, notice: 'Pickup consignment item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_pickup_consignment_item
      @admin_pickup_consignment_item = Admin::PickupConsignmentItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_pickup_consignment_item_params
      params.require(:admin_pickup_consignment_item).permit(:pickup_consignment_id, :price, :qty, :variant_id, :per_qty_price, :received)
    end
end
