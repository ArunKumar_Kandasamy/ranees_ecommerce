class Admin::ProductsTaxPlansController < ApplicationController
	before_filter :authenticate_user!
    before_action :authorize

	def create
	  @admin_products_tax_plans= Admin::ProductsTaxPlans.new(admin_tax_plans_params)
	  if @admin_products_tax_plans.save
	    respond_to do |format|
	      format.html{
	      	redirect_to admin_product_path(@admin_products_tax_plans.product_id), notice: "Product Tax Plan was successfully created."
	      }
	      format.js {
	      	@product = Admin::Product.find(@admin_products_tax_plans.product_id)
	      	@products_tax_plans= Admin::ProductsTaxPlans.new
	      	@product_taxes = Admin::TaxPlan.all
	      	@product_categories = Admin::Category.all
	      	@admin_category_product= Admin::CategoriesProducts.new
	        @product_category = @product.categories
	        @trace_product_category = @product_categories - @product_category 
	        @product_tax = @product.tax_plans
	        @trace_product_tax = @product_taxes - @product_tax
	      	flash[:notice] = "Tax Added."
	        render :template => "remote_content/add_tax_plan.js.erb"
	      } 
      end 
	  else 
	  end
	end 

	def update
	  if @admin_products_tax_plans.update(admin_products_tax_plans_params)
	    redirect_to admin_product_path(@admin_products_tax_plans.product_id), notice: 'Product Tax Plan was successfully updated.'
	  else
	  end
    end
	private

	def set_admin_products_tax_plans
      @admin_products_tax_plans = Admin::ProductsTaxPlans.find(params[:id])
    end

	def admin_tax_plans_params
		params.require(:admin_products_tax_plans).permit(:product_id, :tax_plan_id)
	end

end
