class Admin::CodSpecificCountriesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize
  before_action :set_admin_cod_specific_country, only: [:show, :edit, :update, :destroy]
  before_action :set_admin_cod_policy
  # GET /admin/cod_specific_countries
  # GET /admin/cod_specific_countries.json
  def index
    @admin_cod_specific_countries = Admin::CodSpecificCountry.all
  end

  # GET /admin/cod_specific_countries/1
  # GET /admin/cod_specific_countries/1.json
  def show
  end

  # GET /admin/cod_specific_countries/new
  def new
    @admin_cod_specific_country = Admin::CodSpecificCountry.new
  end

  # GET /admin/cod_specific_countries/1/edit
  def edit
  end

  # POST /admin/cod_specific_countries
  # POST /admin/cod_specific_countries.json
  def create
    @admin_cod_specific_country = Admin::CodSpecificCountry.new(admin_cod_specific_country_params)

    respond_to do |format|
      if @admin_cod_specific_country.save
        format.html { redirect_to @admin_cod_specific_country, notice: 'Cod specific country was successfully created.' }
        format.json { render :show, status: :created, location: @admin_cod_specific_country }
      else
        format.html { render :new }
        format.json { render json: @admin_cod_specific_country.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/cod_specific_countries/1
  # PATCH/PUT /admin/cod_specific_countries/1.json
  def update
    respond_to do |format|
      if @admin_cod_specific_country.update(admin_cod_specific_country_params)
        format.html { redirect_to @admin_cod_specific_country, notice: 'Cod specific country was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_cod_specific_country }
      else
        format.html { render :edit }
        format.json { render json: @admin_cod_specific_country.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/cod_specific_countries/1
  # DELETE /admin/cod_specific_countries/1.json
  def destroy
    @admin_cod_specific_country.destroy
    respond_to do |format|
      format.html { redirect_to admin_cod_specific_countries_url, notice: 'Cod specific country was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_admin_cod_policy
      @admin_cod_policy = Admin::CodPolicy.find(params[:cod_policy_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_admin_cod_specific_country
      @admin_cod_specific_country = Admin::CodSpecificCountry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_cod_specific_country_params
      params.require(:admin_cod_specific_country).permit(:cod_policy_id, :country_id, :all_place, cod_specific_pincodes_attributes:[:id, :cod_specific_country_id, :pincode, :cod_availability, :_destroy])
    end
end
