class Admin::PickupTasksPickupConsignmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize
  before_action :set_admin_pickup_tasks_pickup_consignment, only: [:show, :edit, :update, :destroy]

  def index
    @admin_pickup_tasks_pickup_consignments = Admin::PickupTasksPickupConsignment.all
  end

  def show
  end

  def new
    @admin_pickup_tasks_pickup_consignment = Admin::PickupTasksPickupConsignment.new
  end

  def edit
  end

  def create
    @admin_pickup_tasks_pickup_consignment = Admin::PickupTasksPickupConsignment.new(admin_pickup_tasks_pickup_consignment_params)

    respond_to do |format|
      if @admin_pickup_tasks_pickup_consignment.save
        format.html { redirect_to @admin_pickup_tasks_pickup_consignment, notice: 'Pickup tasks pickup consignment was successfully created.' }
        format.json { render :show, status: :created, location: @admin_pickup_tasks_pickup_consignment }
      else
        format.html { render :new }
        format.json { render json: @admin_pickup_tasks_pickup_consignment.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @admin_pickup_tasks_pickup_consignment.update(admin_pickup_tasks_pickup_consignment_params)
        format.html { redirect_to @admin_pickup_tasks_pickup_consignment, notice: 'Pickup tasks pickup consignment was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_pickup_tasks_pickup_consignment }
      else
        format.html { render :edit }
        format.json { render json: @admin_pickup_tasks_pickup_consignment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @admin_pickup_tasks_pickup_consignment.destroy
    respond_to do |format|
      format.html { redirect_to admin_pickup_tasks_pickup_consignments_url, notice: 'Pickup tasks pickup consignment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_admin_pickup_tasks_pickup_consignment
      @admin_pickup_tasks_pickup_consignment = Admin::PickupTasksPickupConsignment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_pickup_tasks_pickup_consignment_params
      params.require(:admin_pickup_tasks_pickup_consignment).permit(:pickup_task_id, :pickup_consignment_id)
    end
end
