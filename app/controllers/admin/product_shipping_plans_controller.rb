class Admin::ProductShippingPlansController < ApplicationController
	before_filter :authenticate_user!
    before_action :authorize

	def create
	  @admin_product_shipping_plan= Admin::ProductShippingPlan.new(admin_shipping_plan_params)
	  if @admin_product_shipping_plan.save
	    respond_to do |format|
	      format.html{
	      	redirect_to admin_product_path(@admin_product_shipping_plan.product_id), notice: "Product Shipping Plan was successfully created."
	      }
	      format.js {
	      	@product = Admin::Product.find(@admin_product_shipping_plan.product_id)
	      	@product_shipping_plan= Admin::ProductShippingPlan.new
	      	@product_shipping_plans = Admin::ShippingPlan.all
		    @shipping_plan = @product.shipping_plans
		    @trace_shipping_plan = @product_shipping_plans - @shipping_plan
	      	flash[:notice] = "Shipping Plan Added."
	        render :template => "remote_content/add_ship_plan.js.erb"
	      }
      end
	  else
	  end
	end

	def update
	  if @admin_product_shipping_plan.update(admin_product_shipping_plan_params)
	    redirect_to admin_product_path(@admin_product_shipping_plan.product_id), notice: 'Product Shipping Plan was successfully updated.'
	  else
	  end
    end
	private

	def set_admin_product_shipping_plan
      @admin_product_shipping_plan = Admin::ProductShippingPlan.find(params[:id])
    end

	def admin_shipping_plan_params
		params.require(:admin_product_shipping_plan).permit(:product_id, :shipping_plan_id)
	end

end
