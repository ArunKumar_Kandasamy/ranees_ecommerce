class Admin::PagesController < ApplicationController
  before_filter :authenticate_user!
  before_action :authorize 
  before_action :set_admin_page, only: [:show, :edit, :update, :destroy]

  def index
    @admin_pages = Admin::Page.all
    if request.xhr?
      @render_path = admin_pages_path
      @render_file = 'admin/pages/index'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/pages/index"
    end
  end

  def show
    if request.xhr?
      @render_path = admin_page_path(@admin_page)
      @render_file = 'admin/pages/show'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/pages/show"
    end
  end

  def delete_selected_page
    @admin_pages =Admin::Page.all
    if request.xhr?
      if params[:ids].blank?
        flash[:error] = 'Please Select Any Page From The Table!'
      else
        params[:ids].each do |id|
          @page = @admin_pages.find(id)
          @page.delete
        end
        flash[:error] = 'Selected Pages are deleted successfully!'
      end 
      @render_path = admin_pages_path
      @render_file = 'admin/pages/index'
      render "remote_content/common_render.js.erb" 
    end
  end

  def check_page_name_exist
    page = params[:fieldValue]
    validate_ajax_except = params[:validate_ajax_except]
    if validate_ajax_except.present?
      @admin_page = Admin::Page.where("lower(page_name) = ?", page.downcase).where("id != ?", validate_ajax_except).first
    else
      @admin_page = Admin::Page.where("lower(page_name) = ?", page.downcase).first
    end
    if @admin_page.present?
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => false } }
      end
    else
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => true } }
      end
    end
  end

  def new
    @admin_page = Admin::Page.new
    if request.xhr?
      @render_path = new_admin_page_path
      @render_file = 'admin/pages/new'
      render "remote_content/common_render.js.erb"          
    else
      render :template => "admin/pages/new"
    end
  end

  def edit
    if request.xhr?
      @render_path = edit_admin_page_path(@admin_page)
      @render_file = 'admin/pages/edit'
      render "remote_content/common_render.js.erb"         
    else
      render :template => "admin/pages/edit"
    end
  end

  def create
    @admin_page = Admin::Page.new(admin_page_params)
    if @admin_page.save
      @admin_pages = Admin::Page.all
      flash[:noticelower] = 'Page was successfully created.' 
      @render_path = admin_pages_path
      @render_file = 'admin/pages/index'
      render "remote_content/common_render.js.erb"          
    else
      @render_path = new_admin_page_path
      @render_file = 'admin/pages/new'
      render "remote_content/common_render.js.erb" 
    end  
  end

  def update
    if @admin_page.update(admin_page_params)
      flash[:alert] = 'Page was successfully updated.'
      @render_path = admin_page_path(@admin_page)
      @render_file = 'admin/pages/show'
      render "remote_content/common_render.js.erb"          
    else
      render action: "edit"
    end
  end

  def destroy
    if request.xhr?
      if @admin_page.destroy
        flash[:error] = 'Page was successfully destroyed.'
        @admin_pages =Admin::Page.all
        @render_path = admin_pages_path
        @render_file = 'admin/pages/index'
        render "remote_content/common_render.js.erb"
      end  
    else
      if @admin_page.destroy
        redirect_to admin_pages_path, notice: "Page was successfully destroyed."
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_page
      @admin_page = Admin::Page.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_page_params
      params.require(:admin_page).permit(:active,:page_name, :page_type, :page_link, :page_content)
    end
end
