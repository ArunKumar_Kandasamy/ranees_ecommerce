class VouchersController < ApplicationController
  before_action :set_voucher, only: [:show, :edit, :update, :destroy]
  before_action :authorize

  def index
    if request.xhr?
      @vouchers = Admin::Voucher.all
      respond_to do |format|
        format.html{  
        }
        format.js {                  
          render :template => "#{$client_name}_standard_view/vouchers/vouchers_index_show.js.erb"          
        }
      end
    else
     @vouchers = Admin::Voucher.all 
     render :template => "#{$client_name}_standard_view/vouchers/index.html.erb"          
    end
  end


  def show  
  end

  private
	# Use callbacks to share common setup or constraints between actions.
	def set_voucher
	  @voucher = Admin::Voucher.find(params[:id])
    end

	# Never trust parameters from the scary internet, only allow the white list through.
	def voucher_params
	  params.require(:admin_voucher).permit(:user_id, :order_id, :credit, :debit, :voucher_amount, :refund_revocation_id,:order_revocation_id)
	end
end