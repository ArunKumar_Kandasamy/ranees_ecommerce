class ApplicationController < ActionController::Base
  include SimpleCaptcha::ControllerHelpers
  protect_from_forgery with: :null_session
  before_action :sections
  before_filter :store_location
  before_filter :configure_permitted_parameters, if: :devise_controller?
  after_filter :for_cache
  before_filter :set_subscriber


  def for_cache
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  def set_subscriber
    $subscriber= Subscriber.new
  end

  def general_fixed_rate
    if Admin::General.first.present?
      if Admin::General.first.fixed_rate == true
        return true
      else
        return false
      end
    else
      return false
    end
  end

  def set_current_user
    User.current = current_user
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password) }
  end

  def store_location
    return unless request.get? 
    if (request.path != "/users/sign_in" &&
        request.path != "/users/sign_up" &&
        request.path != "/users/password/new" &&
        request.path != "/users/password/edit" &&
        request.path != "/users/confirmation" &&
        request.path != "/users/sign_out" &&
        !request.xhr?) # don't store ajax calls
      session[:previous_url] = request.fullpath
    end
  end

  def after_sign_in_path_for(resource)
    if request.env['omniauth.origin']
      root_path
    else
      session[:previous_url] || root_path
    end
  end

  def after_sign_up_path_for(resource)
    after_sign_in_path_for(resource)
  end
  def after_update_path_for(resource)
    session[:previous_url] || root_path
  end
  
  def authorize
  	@controller= params[:controller].titleize.split("/").join("::").to_s + "Controller"
  	@method= params[:action]
    current_user.block_lists.each do |block_list|
      @con_name=block_list.con_name
      @method_name=block_list.met_name
      if(current_user.nil?)
      	redirect_to root_path
      elsif(@con_name== @controller)
        if @method_name.nil?
          redirect_to root_path
        elsif(@method_name==@method)
      	  redirect_to root_path
        end
      end
    end
  end

  def authorize_support
    unless current_user.role.name == 'sa' || current_user.role.name == 'pr' || current_user.role.name == 'support'
      redirect_to root_path
    end
  end

  def authorize_csm
    unless current_user.role.name == 'sa' || current_user.role.name == 'pr' || current_user.role.name == 'csm'
      redirect_to root_path
    end
  end

  def sections
    parent_count_limit = 5
    @sections_status = Admin::Category.where(:status => true,:active => true)
    @sections_parent_count=@sections_status.roots.count
    @sections= @sections_status.roots.limit(parent_count_limit)
    if @sections_parent_count > parent_count_limit
      @parent_others_status= true
    else
      @parent_others_status= false
    end
  end 

  def find_user
    @user = current_user
  end

  def securing_user
    @user= Profile.where(:user_id => params[:user_id]).first
    if @user.present?
      if(@user.user_id!=current_user.id)
        redirect_to root_path
      end
    end
  end




  def current_cart
    if current_user.present?
      if current_user.cart.present?
        @current_cart= current_user.cart
      elsif session[:cart_id]
        @current_cart ||= Cart.find(session[:cart_id])
      end
    elsif session[:cart_id]
      @current_cart ||= Cart.find(session[:cart_id])
    end
    if session[:cart_id].nil?
      if current_user.present?
        if current_user.cart.present? 
          @current_cart= current_user.cart
        else
          @current_cart = Cart.create(:user_id => current_user.id, :ip => "122.164.202.75")
          @current_cart.save
          session[:cart_id] = @current_cart.id
        end
      else
        @current_cart = Cart.create(:ip => "122.164.202.75")
        session[:cart_id] = @current_cart.id
      end
    end
    @current_cart
  end
end
