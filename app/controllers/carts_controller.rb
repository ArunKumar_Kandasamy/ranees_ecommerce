class CartsController < ApplicationController 
  before_action :sections
  before_action :check_cart , only: [:showcase,:show,:destroy,:update]
  before_action :variant_validate_update , only: [:showcase,:show,:destroy]

  def index
    @search = Cart.search(params[:q])
    @carts= @search.result.includes(:user)
  end

  def payu_return 
    notification = PayuIndia::Notification.new(request.query_string, options = {:key => 'nzP2KA', :salt => 'E1Ts871Z', :params => params})
    @cart = Cart.find(params[:cart_id])
    user= @cart.user_id
    current_user= User.find(user)
    @total=0
    @cart.cart_items.each do |cart_item|
      @total+=cart_item.price
    end 
    
    # notification.invoice is order id/cart id which you have submited from payment direction page.
    if notification.acknowledge   
      if notification.complete? 
        if @cart.cart_items.present?
          @availability = 0
          @tax_price = 0
          @cart.cart_items.each do |cart_item|
            @variant = Admin::Variant.find(cart_item.variant_id)
            @avail_qty = @variant.find_balance_qty
            if @variant.product.tax_violate
                @tax_price = 0
            else
             @tax_price += @variant.variant_tax_cost(@cart.shipping_addresses.first.pincode,cart_item.qty)
            end
            if @avail_qty.to_i < cart_item.qty
              @availability = 1
            end
          end
          @final_cost = @cart.cart_availability_list( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
          @total_shipping_cost = @cart.find_cart_total_shipping_cost( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
          @total_cross_price = @tax_price + @total_shipping_cost + @total
            
          if @availability == 0
            if params[:net_amount_debit].to_f < @total_cross_price 
              @voucher= User.find(current_user.id).vouchers.last.voucher_amount
              @order = Admin::Order.create(:user_id => current_user.id, :shipping_address_id => @cart.shipping_addresses.first.id,  :order_cost => params[:net_amount_debit], :payment_mode => params[:mode], :payment_status => params[:status], :order_number => "OD1", :status => "up", :ip => "122.164.202.75", :voucher_amount => @voucher, :balance_amount => 0,:shipping_cost => @total_shipping_cost,:tax =>  @tax_price)
              Admin::Voucher.create(:debit => 0,:user_id => current_user.id, :order_id => @order.id)
            else
              @order = Admin::Order.create(:user_id => current_user.id, :shipping_address_id => @cart.shipping_addresses.first.id,  :order_cost => params[:net_amount_debit], :payment_mode => params[:mode], :payment_status => params[:status], :order_number => "OD1", :status => "up", :ip => "122.164.202.75",:voucher_amount => 0,:balance_amount => 0,:shipping_cost => @total_shipping_cost,:tax =>  @tax_price)
            end
            #@location=Geocoder.search("122.164.202.75")
            #@order.location= @location.first.city
            #@order.save
            @cart.cart_items.each do | cart_item |
              variant = Admin::Variant.find(cart_item.variant_id)
              return_due = variant.product.return_due
              Admin::OrderItem.create(:name => variant.name ,:qty => cart_item.qty, :variant_id => cart_item.variant_id, :price => cart_item.price, :per_qty_price => cart_item.per_qty_price, :order_id => @order.id , :return_due => return_due, :actual_cost => cart_item.actual_cost)
              cart_item.destroy
            end
            @notification= Admin::NotificationCustomer.first
            @sms_notification= Admin::SmsNotification.first
            if @sms_notification.present?
              if @sms_notification.pre_deli_cancel
                @order.order_cancel_sms 
              end
            end
            if @notification.present?
              if @notification.order_create 
                Admin::InvoiceMailer.email_order_placed(@order).deliver 
              end
            end
            @order.create_order_log(current_user)
            session[:cart_id]= nil
            redirect_to user_order_path(current_user, @order), notice: " Your Payment was successfully processed. Order was successfully created."
            

          else
            redirect_to cart_path(current_cart)
            flash[:notice] = "Quantity Unavailable Only #{@avail_qty} Quantity is available in #{@variant.name}"
            
          end
        else
          redirect_to root_path
        end
      else          
        @failure_transaction= Admin::FailureTransaction.create(:txnid => params['txnid'], :net_amount => params['net_amount'], :user_id => current_user, :mode => params['mode'], :status => params['status'], :addedon => params['addedon'], :productinfo => params['productinfo'], :name_on_card => params['name_on_card'], :cardnum => params['card_num'] )
        redirect_to cart_payment_path(@cart.id), notice: "We are unable to processs your transaction. Please try again."
      end
    else
      redirect_to cart_payment_path(@cart.id), notice: "We are unable to processs your transaction. Please try again."
    end  
  end

  def show
    @new_shipping_address = ShippingAddress.new
    @cart= Cart.find(current_cart.id)
    @total_price= 0.0
    user= @cart.user_id
    @a = (0..100000).to_a.shuffle
    @total_price= 0.0
    @cod_unavail_item= Array.new 
    @cart.cart_items.each do |cart_item| 
      @total_price += cart_item.price
      @variant=Admin::Variant.where(:id => cart_item.variant_id).first
      @product= Admin::Product.where(:id => @variant.product_id).first
      if general_fixed_rate
        if @product.cod == false
          @cod_unavail_item << cart_item
        end
      else
        if @product.check_cod(@cart.pincode) == false
          @cod_unavail_item << cart_item
        end
      end
    end
    if @cart.shipping_addresses.first.present?
      @main_param = @cart.shipping_addresses.first.pincode
      @final_cost = @cart.cart_availability_list( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
      if @final_cost.class == String 
        if @final_cost == "Available"
          @total_shipping_cost = @cart.find_cart_total_shipping_cost( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
          @tax_price = 0 
          @cart.cart_items.each do | cart_item| 
            @variant = Admin::Variant.find(cart_item.variant_id)
             if @variant.product.tax_violate
                 @tax_price = 0
              else
              @tax_price += @variant.variant_tax_cost(@cart.shipping_addresses.first.pincode,cart_item.qty)
              end
          end
          @total_cross_price = @tax_price + @total_shipping_cost + @total_price
        end
      end
    end 
    if request.xhr?
      if params[:view].present?
        render :template => "remote_content/refresh_cart_show.js.erb"
      elsif params[:payment_mode] == "COD" || params[:payment_mode] == "CC"
        current_user= User.find(@cart.user_id)
        @voucher = current_user.vouchers.last
        @payment_mode = params[:payment_mode]
        render :template => "remote_content/order_payment.js.erb"
      elsif params[:voucher] == "1"
        current_user= User.find(@cart.user_id)
        @voucher = current_user.vouchers.last
        render :template => "remote_content/payment_apply_voucher.js.erb"
      elsif params[:payment_method] == "1"
        current_user= User.find(@cart.user_id)
        if params[:voucher] == "2"
          @voucher = current_user.vouchers.last
        end
        render :template => "remote_content/proceed_to_payment_method.js.erb"
      else
        render :template => "remote_content/cart.js.erb"
      end 
    else
      respond_to do |format|
        format.js {
          render :template => "remote_content/cart.js.erb" 
        }
        format.html {
           render :template => "#{$client_name}_standard_view/carts/show.html.erb"
        }
      end
    end
  end

  def payment
    @general = Admin::General.first
    @new_shipping_address = ShippingAddress.new
    @cart= Cart.find(current_cart.id)
    @total_price= 0.0
    @cart.cart_items.each do | cart_item|
      if cart_item.price.present?
        @total_price += cart_item.price
      end
    end

    user= @cart.user_id
    current_user= User.find(@cart.user_id)
    @a = (0..100000).to_a.shuffle
    @total_price= 0.0
    @cod_unavail_item= Array.new 
    @cart.cart_items.each do | cart_item| 
      @total_price += cart_item.price
      @variant=Admin::Variant.where(:id => cart_item.variant_id).first
      @product= Admin::Product.where(:id => @variant.product_id).first
      if general_fixed_rate
        if @product.cod == false
          @cod_unavail_item << cart_item
        end
      else
        if @product.check_cod(@cart.pincode) == false
          @cod_unavail_item << cart_item
        end
      end
    end
    if request.xhr?
      if params[:view].present?
        if @cart.shipping_addresses.first.present?
          @main_param = @cart.shipping_addresses.first.pincode
          @final_cost = @cart.cart_availability_list( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
          if @final_cost.class == String 
            if @final_cost == "Available"
              @total_shipping_cost = @cart.find_cart_total_shipping_cost( Admin::Country.find_by_name(@cart.shipping_addresses.first.country).id,@cart.shipping_addresses.first.pincode)
            end
          end
        end 
        if @final_cost.class == String 
          if @final_cost == "Available"
            @tax_price = 0 
            @cart.cart_items.each do | cart_item| 
              @variant = Admin::Variant.find(cart_item.variant_id)
               if @variant.product.tax_violate
                 @tax_price = 0
              else
              @tax_price += @variant.variant_tax_cost(@cart.shipping_addresses.first.pincode,cart_item.qty)
              end
            end
            @total_cross_price = @tax_price + @total_shipping_cost + @total_price
          end
        end
        render :template => "remote_content/refresh_cart_show.js.erb"
      elsif params[:voucher] == "1"
        @voucher = current_user.vouchers.last
        render :template => "remote_content/payment_apply_voucher.js.erb"
      else
        render :template => "remote_content/cart.js.erb"
      end 
    else
      respond_to do |format|
        format.js {
          render :template => "remote_content/cart.js.erb" 
        }
        format.html {
          render :template => "#{$client_name}_standard_view/carts/payment"
        }
      end
    end

    @general= Admin::General.first
    @cart= Cart.find(params[:cart_id])
    user= @cart.user_id
    current_user= User.find(user)
    @a = (0..100000).to_a.shuffle
    @total_price= 0.0
    @cod_unavail_item= Array.new 
    @cart.cart_items.each do | cart_item| 
      @total_price += cart_item.price
      @variant=Admin::Variant.where(:id => cart_item.variant_id).first
      @product= Admin::Product.where(:id => @variant.product_id).first
      if general_fixed_rate
        if @product.cod == false
          @cod_unavail_item << cart_item
        end
      else
        if @product.check_cod(@cart.pincode) == false
          @cod_unavail_item << cart_item
        end
      end
    end
    if request.xhr?
      if params[:voucher] == "1"
        @voucher = current_user.vouchers.last
      end
      render :template => "remote_content/payment_apply_voucher.js.erb" 
    end
  end


  def find_cart_extra_cost 
    @cart = Cart.find(params[:cart_id])
    if params[:pincode].present?
      if params[:pincode][:pincode] != ""
        @final_cost = @cart.cart_availability_list(params[:user][:id],params[:pincode][:pincode])
        @main_param = params[:pincode][:pincode]
        if @final_cost.class == String 
          if @final_cost == "Available"
            @total_price = 0 
            @tax_price = 0 
            @total_shipping_cost = 0
            if @final_cost.class == String 
              if @final_cost == "Available"
                @total_shipping_cost = @cart.find_cart_total_shipping_cost(params[:user][:id],params[:pincode][:pincode])
              end
            end
            @cart.cart_items.each do | cart_item| 
              @variant = Admin::Variant.find(cart_item.variant_id)
              @total_price += cart_item.price
               if @variant.product.tax_violate
                @tax_price = 0
              else
                @tax_price += @variant.variant_tax_cost(params[:pincode][:pincode],cart_item.qty)
              end
            end
            @total_extra_cost = @tax_price + @total_shipping_cost
            @total_cross_price = @tax_price + @total_shipping_cost + @total_price
          end
        end
      else
         @final_cost = @cart.cart_availability_list(params[:user][:id])
          if @final_cost.class == String
            @final_cost = Admin::Country.find(params[:user][:id]).pincodes
          end
      end
    else
      @final_cost = @cart.cart_availability_list(params[:user][:id])
      if @final_cost.class == String
        @final_cost = Admin::Country.find(params[:user][:id]).pincodes
      end
    end
    render :template => "remote_content/variant1_shipping_cost_show.js.erb"
  end

  def find_cart_availability
    @cart = Cart.find(params[:cart_id])
    if params[:pincode].present?
      @main_param = params[:pincode][:pincode]
      @final_cost = @cart.cart_availability_list(params[:user][:id],params[:pincode][:pincode])
    else
      @main_param = params[:user][:id]
      @final_cost = @cart.cart_availability_list(params[:user][:id])
    end
    if @final_cost.first.class.name == "CartItem"
      @total_price= 0.0
      @cart.cart_items.each do | cart_item| 
        @total_price += cart_item.price
      end
    end
    render :template => "remote_content/variant_shipping_cost_show.js.erb"
  end
  
  def destroy
  	@cart= Cart.find(params[:id])
  	if @cart
  	  session[:cart_id]=nil
  	  @cart.destroy
  	  redirect_to admin_carts_path, notice: "Cart Dumped successfully"
  	end
  end

  def create
    @product = Admin::Product.find(params[:pid])
    @variant_id = params[:variant_id]
    @variant = Admin::Variant.find(params[:variant_id])
    if @variant.find_balance_qty.to_i > 0
      @selling_price = Admin::Inventory.where(:variant_id => @variant.id).order('selling_price DESC').first.selling_price
      @actual_cost = Admin::Inventory.where(:variant_id => @variant.id).order('selling_price DESC').first.selling_price
      @current_cart = current_cart
      @cart_find= @current_cart.cart_items.where(:variant_id => @variant_id).first
      if @current_cart.cart_items.present?
        if @cart_find
          @cart_find.qty +=1 
          @cart_find.save
          if @cart_find.price.present?
            @cart_find.price= @cart_find.per_qty_price * @cart_find.qty
          end
          @cart_find.save
        else
          @cart_item = @current_cart.cart_items.build(:variant_id => @variant_id, :qty => 1, :price =>@selling_price, :per_qty_price => @selling_price, :actual_cost => @actual_cost)
          @cart_item.save
        end
      else
        @cart_item = @current_cart.cart_items.build(:variant_id => @variant_id, :qty => 1, :price =>@selling_price, :per_qty_price => @selling_price, :actual_cost => @actual_cost)
        @cart_item.save
      end
      @cart= Cart.find(current_cart.id)
        @total_price= 0.0
        @cart.cart_items.each do | cart_item|
          if cart_item.price.present?
            @total_price += cart_item.price
          end
        end
      if request.xhr?
        #flash[:notice] = "Added #{@product.name} to cart."
        respond_to do |format|
          format.js {
            render :template => "remote_content/cart.js.erb" 
          }
        end
      else
        redirect_to cart_path(@cart)
      end
    else
      flash[:notice] = "Quantity Unavailable for #{@variant.name}"
      redirect_to cart_path(@cart)
    end
  end

  def empty_cart
    current_cart.cart_items.each do |cart_item|
      cart_item.destroy
    end
    redirect_to cart_path(current_cart)
  end

  def update
    @cart=Cart.find(params[:id])
    if params[:cart][:pincode].present?
      if Admin::Pincode.where(:pincode => params[:cart][:pincode]).first.present?
        @cart.update(cart_params)
        @cart.cart_items.each do |cart_item|
          @variant= Admin::Variant.where(:id => cart_item.variant_id).first
          @selling_price = Admin::Inventory.where(:variant_id => @variant.id).order('selling_price DESC').first.selling_price
          @shipping_values = @shipping.cost
          @high_inventory_price= @selling_price
          cart_item.per_qty_price = @selling_price
          cart_item.save
        end
        respond_to do |format|
          format.json  { render :json =>{ :message => "Updated Successfully"} }
        end
      else
        respond_to do |format|
          format.json  { render :json =>{ :message => "Sorry pincode unavailable"} }
        end
      end
    else
      @cart.update(cart_params)
      @cart.cart_items.each do |cart_item|
        @variant= Admin::Variant.where(:id => cart_item.variant_id).first
        @shipping = Admin::Product.shipping_cost(@cart.pincode, @variant.product, @variant)
        @selling_price = Admin::Inventory.where(:variant_id => @variant.id).order('selling_price DESC').first.selling_price
        cart_item.per_qty_price = @selling_price
        cart_item.save
      end
      respond_to do |format|
        format.json  { render :json =>{ :message => "Updated Successfully"} }
      end
    end
    
  end

  private
    def check_cart
      if current_user.present?
        if current_user.permission == 'u'
          if current_user.cart.present?
            if (params[:id] != current_user.cart.id.to_s)
              redirect_to root_path
            end
          end
        end
      elsif(params[:id]!= session[:cart_id].to_s)
        redirect_to root_path
      end
    end
      
    def cart_params
      params.require(:cart).permit(:user_id, :pincode)
    end


    def variant_validate_update
      @cart= Cart.find(params[:id])
      if @cart.cart_items.present?
        @cart.cart_items.each do |item|
          @variant = Admin::Variant.where(:id => item.variant_id).first
          if @variant.visible && @variant.status == true
            if @cart.pincode.present?
              @high_inventory_price= Admin::Inventory.where(:variant_id => @variant.id).order('selling_price DESC').first.selling_price
              if item.per_qty_price > @high_inventory_price || item.per_qty_price < @high_inventory_price
                item.per_qty_price= @high_inventory_price
                item.save
              end
            end
          end
        end
      end
    end

end
