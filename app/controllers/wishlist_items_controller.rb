class WishlistItemsController < ApplicationController
  before_action :authorize
  before_action :set_wishlist_item, only: [:destroy]

  def destroy
    @wishlist_item.destroy
    @user = current_user
    @wishlist = @user.wishlists.first
    respond_to do |format|
      format.html{  
      }
      format.js {                  
        render :template => "#{$client_name}_standard_view/wishlists/wishlists_index_show.js.erb"          
      }
    end
  end

  private
    def set_wishlist_item
      @wishlist_item = WishlistItem.find(params[:id])
    end

    def wishlist_item_params
      params.require(:wishlist_item).permit(:wishlist_id, :variant_id)
    end
end
