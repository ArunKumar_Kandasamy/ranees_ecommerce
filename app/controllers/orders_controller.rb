class OrdersController < ApplicationController  
  before_action :authenticate_user!
  before_action :authorize
  before_action :restricting_order, except: [:update, :admin_show, :admin_index, :order_logs]
  before_action :set_order, only: [:show, :edit, :update, :destroy]

  def index
    if request.xhr?
      @orders = current_user.orders      
      respond_to do |format|
        format.html{  
        }
        format.js {                  
          render :template => "#{$client_name}_standard_view/orders/order_index_show.js.erb"          
        }
      end
    else
      @orders = current_user.orders
      render :template => "#{$client_name}_standard_view/orders/index.html.erb"     
    end
  end  

  def show 
    if request.xhr?
      @order_revocation = @order.order_revocations.build 
      @order_log = @order.order_logs.first
      @order_log_order_items = @order_log.order_item_logs.map{|x| x.variant_id}
      @current_order_items = @order.order_items.map{|x| x.variant_id}
      @deleted_items_id = @order_log_order_items - @current_order_items     
      respond_to do |format|
        format.html{  
        }
        format.js {                  
          render :template => "#{$client_name}_standard_view/orders/order_show_show.js.erb"          
        }
      end
    else
      @order_revocation = @order.order_revocations.build 
      @order_log = @order.order_logs.first
      @order_log_order_items = @order_log.order_item_logs.map{|x| x.variant_id}
      @current_order_items = @order.order_items.map{|x| x.variant_id}
      @deleted_items_id = @order_log_order_items - @current_order_items
       render :template => "#{$client_name}_standard_view/orders/show.html.erb"        
    end
            
  end

  def consignment
    @order = Admin::Order.find(params[:order_id])
    @consignment = Admin::Consignment.find(params[:id])
    @warehouse = @consignment.warehouse 
    @invoice = @consignment.invoices.first if @consignment.invoices.present?
    respond_to do |format|
      format.pdf do
        render pdf: "Invoice for #{@invoice.invoice_number}",                  # file name
               layout: 'layouts/application.pdf.erb',  # layout used
               show_as_html: params[:debug].present?    # allow debuging
      end
    end
  end                                                         
        

  def new
    @order = Admin::Order.new
  end

  def edit
    @new_shipping_address = ShippingAddress.new
  end
 
  def create  
    if current_cart.cart_items.present? 
      if simple_captcha_valid?
        @availability = 0
        @tax_price = 0
        @total_price = 0 
        current_cart.cart_items.each do |cart_item|
          @variant = Admin::Variant.find(cart_item.variant_id)
          @avail_qty = @variant.find_balance_qty
          @total_price += cart_item.price
          if @variant.product.tax_violate
             @tax_price = 0
          else
             @tax_price += @variant.variant_tax_cost(current_cart.shipping_addresses.first.pincode,cart_item.qty)
          end
          if @avail_qty.to_i < cart_item.qty
            @availability = 1
          end
        end
        @final_cost = current_cart.cart_availability_list( Admin::Country.find_by_name(current_cart.shipping_addresses.first.country).id,current_cart.shipping_addresses.first.pincode)
        @total_shipping_cost = current_cart.find_cart_total_shipping_cost( Admin::Country.find_by_name(current_cart.shipping_addresses.first.country).id,current_cart.shipping_addresses.first.pincode)
        @total_cross_price = @tax_price + @total_shipping_cost + @total_price
        if @availability == 0 && @final_cost.class == String 
          @order = Admin::Order.create(:user_id => current_user.id, :shipping_address_id => current_cart.shipping_addresses.first.id,  :order_cost => @total_cross_price, :payment_mode => params[:order][:payment_mode], :payment_status => true, :order_number => "OD1", :status => "up", :ip => "122.164.202.75",:voucher_amount => params[:order][:voucher_amount],:balance_amount => params[:order][:voucher_amount],:shipping_cost => @total_shipping_cost,:tax =>  @tax_price)
          if params[:order][:voucher_amount].present? 
            if  params[:order][:voucher_amount].to_i > 0.0
            Admin::Voucher.create(:debit => params[:order][:voucher_amount],:user_id => current_user.id, :order_id => @order.id)
            end
          end
          #@location=Geocoder.search("122.164.202.75")
          #@order.location= @location.first.city
          #@order.save
          current_cart.cart_items.each do | cart_item |
            variant = Admin::Variant.find(cart_item.variant_id)
            return_due = variant.product.return_due
            Admin::OrderItem.create(:name => variant.name,:qty => cart_item.qty, :variant_id => cart_item.variant_id, :price => cart_item.price, :per_qty_price => cart_item.per_qty_price, :order_id => @order.id , :return_due => return_due, :actual_cost => cart_item.actual_cost)
            cart_item.destroy
          end
          @order.create_order_log(current_user.id)
          session[:cart_id]= nil
          @notification= Admin::NotificationCustomer.first
          @sms_notification= Admin::SmsNotification.first
          if @sms_notification.present?
            if @sms_notification.order_create
              @order.send_sms 
            end
          end
          if @notification.present?
            if @notification.order_create
              Admin::InvoiceMailer.email_order_placed(@order,@order.order_items).deliver 
            end
          end
          flash[:notice] = 'Order was successfully created.'
          render :js => "window.location = '#{user_order_path(current_user, @order)}'"
        else
          flash[:notice] = "Quantity Unavailable Only #{@avail_qty} Quantity is available in #{@variant.name}"
          render :js => "window.location = '#{cart_path(current_cart)}'"
        end
      else
        flash[:notice] = 'Captcha Not Same'
        render :template => "remote_content/captcha_failure.js.erb"
      end
    else
      flash[:notice] = 'Cart item is nil.Please select the items and place the order.'
      render :js => "window.location = '#{root_path}'"
    end
  end

  def update
    if @order.check_user_cancel_order_status
      if @order.update(:cancelled_by => current_user.id ,:cancel => params[:admin_order][:cancel], :remarks => params[:admin_order][:remarks])
        @notification= Admin::NotificationCustomer.first
        @sms_notification= Admin::SmsNotification.first
        if @sms_notification.present?
          if @sms_notification.pre_deli_cancel
            @order.order_cancel_sms 
          end
        end
        if @notification.present?
          if @notification.pre_deli_cancel
            @order.order_cancel_sms 
            Admin::InvoiceMailer.email_order_cancelled(@order).deliver
          end
        end
        if @order.payment_mode != "COD"
          if params[:admin_order][:order_revocations_attributes].first[1][:revocation_mode] == "Cash Back"
            if params[:admin_order][:order_revocations_attributes].first[1][:bank_account_id].nil?
              @bankaccount = BankAccount.create(:name => params[:admin_order][:bank_accounts][:name], :num => params[:admin_order][:bank_accounts][:name], :bank_code => params[:admin_order][:bank_accounts][:ifsc_code], :user_id => @order.user_id)
              @bank_account_id = @bankaccount.id
            else
              @bank_account_id = params[:admin_order][:order_revocations_attributes].first[1][:bank_account_id] 
            end
            @revocation_mode = 'b'
          else
            @revocation_mode = 'v'
          end  
          @order_revocation = @order.create_revocation_for_cancel
          @order_revocation.update(:bank_account_id =>  @bank_account_id,:revocation_mode => @revocation_mode)
        end
        if params[:admin_order][:user_order_cancel].present? 
          if @sms_notification.present?
            if @sms_notification.post_deli_cancel
              @order.order_cancel_sms 
            end
          end
          if @notification.present?
            if @notification.post_deli_cancel
              Admin::InvoiceMailer.email_order_cancelled(@order).deliver
            end
          end
          redirect_to user_orders_path(current_user.id), notice: 'Order was successfully Cancelled.'
        else
          redirect_to admin_order_path(@order), notice: 'Order was successfully updated.'
        end
        @order.create_order_log(current_user.id)
      end
    else
      redirect_to user_orders_path(current_user.id), notice: 'Order Cannot Be Cancelled Contact Our Support.'
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def to_params
    end

    def set_order
      @order = Admin::Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params 
      params.require(:order).permit(:cancel, :remarks, :user_id, :pay_val, :cal_conf, :present_user, :prev_rec,  :shipping_address_id, :order_cost, :payment_mode, :payment_status, :order_number, :status,:warehouse_id, :prev_rec_reason, :pay_val_reason, :cal_conf_reason, order_items_attributes:[:id, :variant_id, :_destroy, :qty, :return_due ],order_revocations_attributes:[:id, :revocation_mode ,:revocation_cost])
    end

    def restricting_order
      if current_user.id.to_s!= params[:user_id]
        redirect_to root_path
      end
    end
end
