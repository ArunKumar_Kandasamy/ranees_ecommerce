module ApplicationHelper

  def general_fixed_rate 
    if Admin::General.first.present?
      if Admin::General.first.fixed_rate == true
        return true
      else
        return false
      end
    else
      return false
    end
  end

  

  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end

  def link_to_order_add_fields(name, f, g, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder,variant_id: g)
    end
    
    link_to(name, '#', class: "add_fields", data: {id: id,variant_id: g ,fields: fields.gsub("\n", "")})
  end


  def link_to_plan_add_fields(name, f, g, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder,country_id: g)
    end
    
    link_to(name, '#', class: "add_fields", data: {id: id,country_id: g ,fields: fields.gsub("\n", "")})
  end

  def links_to_add_fields(name, f, type)
    new_object = f.object.send "build_#{type}"
    id = "new_#{type}"
    fields = f.send("#{type}_fields", new_object, child_index: id) do |builder|
      render(type.to_s + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end

  def for_parent_limit
    @admin_categories = Admin::Category.all
    @admin_limit = @admin_categories.roots.limit(2)
    @admin_par= @admin_categories.roots.offset(2)    
    return @admin_par
  end

  def for_child_limit
    @admin_categories = Admin::Category.all
    @admin_categories.roots.each do|parent|
      @admin_limit =  parent.children.limit(2)
      @admin_par= parent.children.offset(2) 
    end       
    return @admin_par
  end

  def general
    Admin::General.first
  end

  def notification_customer
    Admin::NotificationCustomer.first
  end 

  def sms_notification
    Admin::SmsNotification.first
  end 

  def find_user
    @user = current_user
  end

  def resource_name
    :user
  end
 
  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def flash_normal
    render "flashes"
  end

  def co_name
    @all_controllers = ApplicationController.descendants.each do |app| 
      app
    end
  end

  def latest_products
    Admin::Product.where(:status => true,:visibility => true,:promote => 'l').sort_by(&:promotional_sort)
  end

  def offer_products
    Admin::Product.where(:status => true,:visibility => true,:promote => 'o').sort_by(&:promotional_sort)
  end

  def check_link(path)
    @route = Rails.application.routes.recognize_path(path)    
    @controller=@route[:controller].titleize.split("/").join("::").to_s + "Controller"    
    if current_user.block_lists.first.present?      
      current_user.role.block_lists.each do |block_list|        
        if (block_list.con_name == @controller && block_list.met_name == @route[:action])
          return false
        end
      end
    else
      return true
    end
  end

  def search_users(role_name)
    @role = Admin::Role.where(:name => role_name).first
    @permissions= Admin::Permission.where(:role_id => @role.id)
    @permission_filtered = @permissions.each do |permission|
      @users= User.where(:id => permission.user_id)
    end
    return @users
  end

  def search_permissions(role_name)
    @role = Admin::Role.where(:name => role_name).first
    @permissions= Admin::Permission.where(:role_id => @role.id)
    return @permissions
  end

  def selling_prices_check(product)  
    product.variants.each do|variant|
      variant.inventories.each do|inventory|
        if inventory.selling_price.present? || inventory.balance_qty != 0
          return true
        else
          return false
        end
      end
    end
  end    

  def balance_qty(variant)  
    bal_qty=0
    order_qty = 0
    variant.inventories.each do |inventory|
      bal_qty+=inventory.balance_qty
    end 
    orders = Admin::Order.where(:status => "up")
    orders.each do |order|
      order_items = order.order_items.where(:variant_id => variant.id)
      order_items.each do |order_item|
        order_qty+=order_item.qty
      end
    end 
    bala_qty= bal_qty - order_qty
    return bala_qty.to_s
  end

  def warehouse_bal_qty(variant)     
    @warehouses = Admin::Warehouse.all
    @warehouse_bal_qtys = {}
    @warehouses.each do |warehouse|      
      wh_balance_qty=0
      @warehouse_balance_qty = Admin::Inventory.where(:variant_id => variant.id, :warehouse_id => warehouse.id)
      @warehouse_balance_qty.each do |wh_bal_qty|
        wh_balance_qty += wh_bal_qty.balance_qty      
      end
      @warehouse_bal_qtys.merge!(warehouse.name => wh_balance_qty)
    end   
    return @warehouse_bal_qtys
  end

  def current_cart_total
    @total = 0 
    current_cart.cart_items.each do |cart_item|
      @total += cart_item.price
    end
    return @total
  end

  def current_cart
    if current_user.present?
      if current_user.cart.present?
        @current_cart= current_user.cart
      elsif session[:cart_id]
        @current_cart ||= Cart.find(session[:cart_id])
      end
    elsif session[:cart_id]
      @current_cart ||= Cart.find(session[:cart_id])
    end
    if session[:cart_id].nil?
      if current_user.present?
        if current_user.cart.present? 
          @current_cart= current_user.cart
        else
          @current_cart = Cart.create(:user_id => current_user.id, :ip => "122.164.202.75")
          @location = Geocoder.search("122.164.202.75")
          @current_cart.location= @location.first.city
          @current_cart.save
          session[:cart_id] = @current_cart.id
        end
      else
        @current_cart = Cart.create(:ip => "122.164.202.75")
        #@location=Geocoder.search("122.164.202.75")
        #@current_cart.location= @location.first.city
        @current_cart.save
        session[:cart_id] = @current_cart.id
      end
    end
    @current_cart
  end

  def confirm_cart(cart) 
    @cart= Cart.find(cart)
    if @cart.cart_items.present?
      @cart.cart_items.each do |item|
        @variant= Admin::Variant.where(:id => item.variant_id).first
        product = Admin::Product.find(@variant.product_id)
        if ((@variant.visible == true) && (@variant.status == true))
          if general_fixed_rate
            if item.per_qty_price.present?  && item.price.present?
              if product.unavail_purchase == false
                if  balance_qty(@variant).to_i == 0 
                  return false
                elsif balance_qty(@variant).to_i < item.qty 
                  return false
                end
              end
            else
              return false
            end 
          else
            if item.per_qty_price.present?  && item.price.present?
              if product.unavail_purchase == false
                if  balance_qty(@variant).to_i == 0 
                  return false
                elsif balance_qty(@variant).to_i < item.qty 
                  return false
                end
              end
            else
              return false
            end
          end
        else
          return false
        end
      end
    else
      return false
    end
    return true
  end

  def min_order_check(cart)
    general= Admin::General.first
    @total_price= 0.0
    if @cart.pincode.nil?
      return true
    elsif general.present?
      cart.cart_items.each do | cart_item| 
        @total_price += cart_item.price
      end
      if general.min_order
        if general.min_cost.present?
          if general.min_cost < @total_price
            return true
          else
            return false
          end
        end
      else
        return true
      end
    end
  end

  def check_warehouse_permission
    if current_user.present?
      if current_user.role.name == 'sa' || current_user.role.name == 'pr' 
        @warehouses = Admin::Warehouse.all
      elsif current_user.role.warehouses.first.present?
        @warehouses = current_user.role.warehouses
      end
    return @warehouses
    end
  end

  def warehouse_lookup(order_item)
    @inventory = Admin::Inventory.select("warehouse_id, sum(balance_qty) as balance_qty").where(:variant_id => order_item.variant_id).where('balance_qty != ?','0').group('warehouse_id')
    @inventory1 = @inventory
    @inventory.each do |inventory|
      @consignment_items = Admin::ConsignmentItem.joins(:consignment).select("consignment_id ,variant_id,sum(qty) as qtys").where(:variant_id => order_item.variant_id).where('consignments.warehouse_id' => inventory.warehouse_id).group('variant_id','consignment_items.id')
      if @consignment_items.present?
        qty = 0 
        @consignment_items.each do |consignment_item|
          qty += consignment_item.qtys
        end
        inventory.balance_qty = inventory.balance_qty - qty
        @inventory1 = @inventory.select { |h| h.balance_qty > 0 }
      end
    end
    return @inventory1
  end

  def filter_warehouse_lookup(order_item)
    @inventory = Admin::Inventory.select("warehouse_id, sum(balance_qty) as balance_qty").where(:variant_id => order_item.variant_id).where('balance_qty != ?','0').group('warehouse_id')
    @inventory.each do |inventory|
      @consignments = Admin::ConsignmentItem.joins(:consignment).select("variant_id,sum(qty) as qtys").where(:variant_id => order_item.variant_id).where('consignments.warehouse_id' => inventory.warehouse_id).group('variant_id','consignment_items.id')
      if @consignments.present?
        qty = 0 
        @consignments.each do |consignment|
          qty += consignment.qtys
        end
        inventory.balance_qty = inventory.balance_qty - qty
        order_item.consignment_items.each do |consignment_item|
          @inventory1 = @inventory.select { |h| h.warehouse_id!= consignment_item.consignment.warehouse_id }
          @inventory1 = @inventory1.select { |h| h.balance_qty > 0 }
        end
      end
    end
    return @inventory1
  end

  def filter_general_image_category(image_type)
    @list_category = Admin::Category.select(:name,:id).joins(:general_images).select("general_images.image_type_id").where('general_images.image_type_id' => image_type.id).group('name','general_images.category_id')
  end
 
  def inventory_lookup(variant,warehouse)
    @inventory= Admin::Inventory.where(:variant_id => variant.id,:warehouse_id => warehouse.id).where('balance_qty != ?','0')
    return @inventory
  end
 
  def inventory_regulation_lookup(variant,warehouse,consignment_item)
    @inventory = Admin::Inventory.where(:variant_id => variant.id,:warehouse_id => warehouse.id).where('balance_qty != ?','0').map{|x| x.id}
    @filter_inventory = consignment_item.regulations.map{|x| x.inventory_id}
    @filter_regulation_inventory = @inventory - @filter_inventory
    @filter_regulation_inventory_results = Admin::Inventory.find(@filter_regulation_inventory)
    return @filter_regulation_inventory_results
  end

  def order_regulation_max_qty(regulation,consignment_item)
    bal_qty = regulation.inventory.balance_qty
    consignment_item_qty = consignment_item.qty
    consignment_item_qty_sum = 0 
    consignment_item.regulations.each do |regulation|
      consignment_item_qty_sum += regulation.qty 
    end
    if consignment_item_qty >  consignment_item_qty_sum
      remaining_qty = consignment_item_qty -  consignment_item_qty_sum
      if bal_qty > remaining_qty
        qty = regulation.qty + remaining_qty
      else
        qty = regulation.qty 
      end
    else
       qty = regulation.qty 
    end
    return qty
  end

  def assign_warehouse_max_qty(order_item,consignment_item,warehouse)
    order_item_qty = order_item.qty
    consignment_item_qty = consignment_item.qty
    warehouse_qty = check_order_assigned_warehouse_bal_qty(consignment_item)
    consignment_item_qty_sum = consignment_assigned_order_item_qty(order_item)
    if order_item_qty > consignment_item_qty_sum
      remaining_qty = order_item_qty - consignment_item_qty_sum
      if warehouse_qty > remaining_qty
        qty = consignment_item_qty + remaining_qty
      else
        qty = consignment_item_qty
      end
    else
      qty = consignment_item_qty
    end
    return qty
  end

  def consignment_assigned_order_item_qty(order_item)
    consignment_item_qty_sum = 0
    order_item.consignment_items.each do |consignment_item|
      consignment_item_qty_sum += consignment_item.qty
    end
    return consignment_item_qty_sum
  end
  def consignment_assigned_replacement_revocation_item_qty(replacement_revocation_item)
    consignment_item_qty_sum = 0
    replacement_revocation_item.consignment_items.each do |consignment_item|
      consignment_item_qty_sum += consignment_item.qty
    end
    return consignment_item_qty_sum
  end

  def check_order_assigned_warehouse_bal_qty(consignment_item)
    @inventory = Admin::Inventory.select("warehouse_id, sum(balance_qty) as balance_qtys").where(:variant_id => consignment_item.variant_id,:warehouse_id => consignment_item.consignment.warehouse.id).where('balance_qty != ?','0').group('warehouse_id')
    @consignments = Admin::ConsignmentItem.joins(:consignment).select("variant_id,sum(qty) as qtys").where(:variant_id => consignment_item.variant_id).where('consignments.warehouse_id' => consignment_item.consignment.warehouse.id).group('variant_id')
    qty = 0 
    inventory_qty = 0 
    @inventory.each do |inventory|
      inventory_qty += inventory.balance_qtys
    end 
    @consignments.each do |consignment|
      qty += consignment.qtys
    end
    return inventory_qty - qty
  end

  def plans_check(product)
    Admin::Product.plans_check(product)
  end

  def selling_price_check(product)
    Admin::Product.selling_price_check(product)
  end

  def reason_helper(reason)
    case reason
    when "d"
      return "Damage"
    when "r"
      return "Return to vendor"
    when "e"
      return "Expired"
    when "s"
      return "Stolen"
    when "c"
      return "Credit Note"
    end
  end

  def similar_image(product)
    variant= product.variants.order(sort_order: :asc).first
    image=variant.images.order(sort_order: :asc).first
    return image
  end

  def check_store_details(general)
    if general.present?
      if general.store_name.present? && general.account_email.present? &&  general.com_email.present? &&  general.legal_name.present?
        return true
      else
        return false
      end
    else
      return false
    end 
  end

  def check_currency(general)
    if general.present?
      if general.acc_currency.present? && general.show_currency.present?
        return true
      else
        return false
      end
    else
      return false
    end 
  end

  def check_up_order
    return Admin::Order.all.where(:status => "up").count 
  end

  def check_seo(general)
    if general.present?
      if general.page_title.present? && general.meta_des.present? 
        return true
      else
        return false
      end
    else
      return false
    end 
  end

  def check_social(general)
    if general.present?
      if general.fb_link.present? && general.tw_link.present? && general.gp_link.present? && general.pt_link.present?
        return true
      else
        return false
      end
    else
      return false
    end 
  end

  def check_user_deligate_status(consignment)
    if @order.consignments.present?
      @order.consignments.each do |consignment|
        if consignment.package_tasks.first.present? && consignment.conform == true
          return true
        else
          return false
        end
      end
    else
    end
  end

  def check_admin_deligate_status(consignment)
    if @order.consignments.present?
      @order.consignments.each do |consignment|
        if consignment.package_tasks.first.present?
          return true
        else
          return false
        end
      end
    else
    end
  end

  def check_user_consignment_success(consignment)    
    if @order.consignments.present?
      @order.consignments.each do |consignment|
        consignment.consignment_items.each do |consignment_item|
          if consignment_item.policy == true
            return true
          else
            return false
          end
        end
      end
    end
  end

  def check_consignment_status_in_order(order)
    if order.consignments.present?
      order.consignments.each do |consignment|
        if consignment.conform == true
          return true
        end
      end
    else
      return true
    end
  end

  def check_all_consignment_order(order)
    if order.consignments.present?
      order.consignments.each do |consignment|
        if consignment.conform != true
          return false
        end
      end
      return true
    else
      return false
    end
  end
  

  def cp(path)
    "open1" if current_page?(path)
  end
end 