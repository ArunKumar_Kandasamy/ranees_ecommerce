module Admin::OrderItemsHelper
	def limit_bal_qty(bal_qty)
	  @collect = "[" 
	  if bal_qty.to_i >= 10 
         @q = (1..10) 
      else 
        @q = (1..bal_qty.to_i) 
      end 
      @q.each do |i| 
        @collect += "[#{i},#{i}]" 
        if i.equal? @q.last  
          @collect += "]"
        else 
          @collect += "," 
        end  
      end 
      return @collect
	end
end
