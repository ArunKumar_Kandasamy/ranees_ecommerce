module Admin::OrderHelper
  def up_order_qty(variant)
  	up_qty = 0
    Admin::Order.where(:status => "up").each do |order|
	  up_item = order.order_items.where(:variant_id => variant.id).first
	  if up_item.present?
	    up_qty += up_item.qty
	  end
	end 
	return up_qty
  end
end
