class Admin::InvoiceMailer < ActionMailer::Base
  default from: "pk@proproots.com"
  

  def email_subscription(subscriber)
    @email= subscriber.email
    mail(
      :subject => "You have been subscribed to Ranees",
      :to      => @email
    )
  end

  def send_invoice(consignment)
	@consignment=consignment
	@invoice = consignment.invoices.first if consignment.invoices.present?
    @consignment_items = @consignment.consignment_items
    @order = consignment.order
    @user_id= consignment.order.user_id
    @user_email= User.find(@user_id).email
    mail(
      :subject => "Invoice for #{@consignment.consignment_number}",
      :to      => @user_email
    )
  end

  def send_back_job_email(category)
    @category=category
    general = Admin::General.first
    if general.present?
      @to_email = general.account_email
      if @to_email.present?
        mail( 
          :subject => "category #{@category.name} is successfully activated",
          :to      => @to_email
        )
      end
    end
  end

  def email_order_placed(order,order_items)
    @order=order
    @order_items = order_items
    @order_user_email= User.find(@order.user_id).email
    mail(
      :subject => "Your order with ranees.in #{order.order_number} has been placed successfully",
      :to      => @order_user_email
    )
  end

  def email_order_confirmed(order)
    @order=order
    @order_user_email= User.find(@order.user_id).email
    mail(
      :subject => " Order Confirmed for #{order.order_number}",
      :to      => @order_user_email
    )
  end 

  def email_order_tracking_update(order,consignment)
    @order=order
    @consignment = consignment
    @consignment_items = @consignment.consignment_items
    @order_user_email= User.find(order.user_id).email
    mail(
      :subject => "Shipment of items in order #{order.order_number} by glendalegreentea.com",
      :to      => @order_user_email
    )
  end 

  def email_replacement_order_create(order,replacement_revocation)
    @order=order
    @replacement_revocation = replacement_revocation
    @replacement_revocation_items = @replacement_revocation.replacement_revocation_items
    @order_user_email= User.find(@order.user_id).email
    mail(
      :subject => " Your replacement order has been created successfully for #{order.order_number}",
      :to      => @order_user_email
    )
  end 

  def email_partial_refund_create(order,refund_revocation)
    @order=order
    @refund_revocation = refund_revocation
    @order_user_email= User.find(@order.user_id).email
    mail(
      :subject => " Your partial refund has been created successfully for #{order.order_number}",
      :to      => @order_user_email
    )
  end 

  def email_order_cancelled(order)
    @order=order
    @order_items = @order.order_items
    @order_user_email= User.find(@order.user_id).email
    mail(
      :subject => "Your Order with ranees.in #{order.order_number} has been successfully Cancelled!",
      :to      => @order_user_email
    )
  end

  def email_pre_delivery_cancelled(order,consignment)
    @order=order
    @consignment = consignment
    @consignment_items = @consignment.consignment_items
    @order_user_email= User.find(@order.user_id).email
    mail(
      :subject => "Your Order with ranees.in #{order.order_number} has been successfully Cancelled!",
      :to      => @order_user_email
    )
  end 

  def email_post_delivery_cancelled(order,consignment)
    @order=order
    @order_user_email= User.find(@order.user_id).email
    @consignment = consignment
    @consignment_items = @consignment.consignment_items
    mail(
      :subject => "Your Order with ranees.in #{order.order_number} has been successfully Cancelled!",
      :to      => @order_user_email
    )
  end 

end
