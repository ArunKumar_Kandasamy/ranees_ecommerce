class HardWorker
  include Sidekiq::Worker
  def perform(category_id)
  	@admin_category= Admin::Category.find(category_id)
  	@admin_category.status= true
  	@admin_category.pub_time= nil
  	@admin_category.save    
  	Admin::InvoiceMailer.send_back_job_email(@admin_category).deliver 
  end 
end