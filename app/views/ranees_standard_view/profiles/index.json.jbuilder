json.array!(@profiles) do |profile|
  json.extract! profile, :id, :f_name, :l_name, :m_number, :gender
  json.url profile_url(profile, format: :json)
end
