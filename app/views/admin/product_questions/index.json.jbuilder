json.array!(@admin_product_questions) do |admin_product_question|
  json.extract! admin_product_question, :id
  json.url admin_product_question_url(admin_product_question, format: :json)
end
