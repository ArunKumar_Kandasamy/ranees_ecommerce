json.array!(@admin_shipping_policies) do |admin_shipping_policy|
  json.extract! admin_shipping_policy, :id, :all_country, :free, :flat, :cost, :variable_based
  json.url admin_shipping_policy_url(admin_shipping_policy, format: :json)
end
