json.array!(@admin_sms_notifications) do |admin_sms_notification|
  json.extract! admin_sms_notification, :id
  json.url admin_sms_notification_url(admin_sms_notification, format: :json)
end
