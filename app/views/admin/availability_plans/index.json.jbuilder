json.array!(@admin_availability_plans) do |admin_availability_plan|
  json.extract! admin_availability_plan, :id, :name, :all_country
  json.url admin_availability_plan_url(admin_availability_plan, format: :json)
end
