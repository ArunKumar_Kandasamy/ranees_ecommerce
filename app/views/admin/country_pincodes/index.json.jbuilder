json.array!(@admin_country_pincodes) do |admin_country_pincode|
  json.extract! admin_country_pincode, :id, :plan_country_id, :pincode, :availability
  json.url admin_country_pincode_url(admin_country_pincode, format: :json)
end
