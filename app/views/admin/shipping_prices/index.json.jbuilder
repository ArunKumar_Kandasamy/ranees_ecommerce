json.array!(@admin_shipping_prices) do |admin_shipping_price|
  json.extract! admin_shipping_price, :id, :range, :cost, :shipping_policy_id, :shipping_specific_country_id, :shipping_specific_pincode_id
  json.url admin_shipping_price_url(admin_shipping_price, format: :json)
end
