json.array!(@admin_cod_specific_pincodes) do |admin_cod_specific_pincode|
  json.extract! admin_cod_specific_pincode, :id
  json.url admin_cod_specific_pincode_url(admin_cod_specific_pincode, format: :json)
end
