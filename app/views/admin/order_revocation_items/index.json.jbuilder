json.array!(@admin_order_revocation_items) do |admin_order_revocation_item|
  json.extract! admin_order_revocation_item, :id, :price, :order_revocation_id, :qty, :variant_id, :per_qty_price
  json.url admin_order_revocation_item_url(admin_order_revocation_item, format: :json)
end
