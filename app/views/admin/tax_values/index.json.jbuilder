json.array!(@admin_tax_values) do |admin_tax_value|
  json.extract! admin_tax_value, :id
  json.url admin_tax_value_url(admin_tax_value, format: :json)
end
