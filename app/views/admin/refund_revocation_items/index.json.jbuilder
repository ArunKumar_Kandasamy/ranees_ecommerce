json.array!(@admin_refund_revocation_items) do |admin_refund_revocation_item|
  json.extract! admin_refund_revocation_item, :id, :refund_revocation_id, :price, :qty, :variant_id, :per_qty_price, :actual_price, :reason, :refund_amount, :ship_dt, :tax_dt, :TDR_td, :status
  json.url admin_refund_revocation_item_url(admin_refund_revocation_item, format: :json)
end
