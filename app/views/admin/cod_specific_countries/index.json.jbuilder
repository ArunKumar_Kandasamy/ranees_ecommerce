json.array!(@admin_cod_specific_countries) do |admin_cod_specific_country|
  json.extract! admin_cod_specific_country, :id
  json.url admin_cod_specific_country_url(admin_cod_specific_country, format: :json)
end
