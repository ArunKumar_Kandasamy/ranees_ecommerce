json.array!(@admin_plan_countries) do |admin_plan_country|
  json.extract! admin_plan_country, :id, :availability_plan_id, :country_id, :all_place
  json.url admin_plan_country_url(admin_plan_country, format: :json)
end
