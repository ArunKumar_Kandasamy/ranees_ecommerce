json.array!(@admin_product_properties) do |admin_product_property|
  json.extract! admin_product_property, :id, :product_id, :product_question_id, :answer
  json.url admin_product_property_url(admin_product_property, format: :json)
end
