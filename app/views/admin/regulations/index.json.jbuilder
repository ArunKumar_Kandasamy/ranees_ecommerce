json.array!(@admin_regulations) do |admin_regulation|
  json.extract! admin_regulation, :id
  json.url admin_regulation_url(admin_regulation, format: :json)
end
