json.array!(@admin_failure_transactions) do |admin_failure_transaction|
  json.extract! admin_failure_transaction, :id
  json.url admin_failure_transaction_url(admin_failure_transaction, format: :json)
end
