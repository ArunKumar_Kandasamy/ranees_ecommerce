json.array!(@admin_tax_countries) do |admin_tax_country|
  json.extract! admin_tax_country, :id
  json.url admin_tax_country_url(admin_tax_country, format: :json)
end
