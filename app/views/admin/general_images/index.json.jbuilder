json.array!(@admin_general_images) do |admin_general_image|
  json.extract! admin_general_image, :id, :avatar, :image_type_id, :category_id, :alt_text
  json.url admin_general_image_url(admin_general_image, format: :json)
end
