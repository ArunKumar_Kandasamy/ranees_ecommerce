json.array!(@admin_shipping_specific_pincodes) do |admin_shipping_specific_pincode|
  json.extract! admin_shipping_specific_pincode, :id, :shipping_specific_country_id, :pincode
  json.url admin_shipping_specific_pincode_url(admin_shipping_specific_pincode, format: :json)
end
