json.array!(@admin_types) do |admin_type|
  json.extract! admin_type, :id
  json.url admin_type_url(admin_type, format: :json)
end
