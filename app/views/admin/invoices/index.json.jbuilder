json.array!(@admin_invoices) do |admin_invoice|
  json.extract! admin_invoice, :id, :consignment_id, :barcode, :invoice_number
  json.url admin_invoice_url(admin_invoice, format: :json)
end
