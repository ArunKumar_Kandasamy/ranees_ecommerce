json.array!(@admin_image_types) do |admin_image_type|
  json.extract! admin_image_type, :id, :type, :height, :width
  json.url admin_image_type_url(admin_image_type, format: :json)
end
