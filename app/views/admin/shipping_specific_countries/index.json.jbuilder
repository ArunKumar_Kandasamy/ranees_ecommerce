json.array!(@admin_shipping_specific_countries) do |admin_shipping_specific_country|
  json.extract! admin_shipping_specific_country, :id, :all_place, :free, :flat, :cost, :variable_based, :country_id, :shipping_policy_id
  json.url admin_shipping_specific_country_url(admin_shipping_specific_country, format: :json)
end
