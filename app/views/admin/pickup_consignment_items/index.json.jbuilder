json.array!(@admin_pickup_consignment_items) do |admin_pickup_consignment_item|
  json.extract! admin_pickup_consignment_item, :id, :pickup_consignment_id, :price, :qty, :variant_id, :per_qty_price
  json.url admin_pickup_consignment_item_url(admin_pickup_consignment_item, format: :json)
end
