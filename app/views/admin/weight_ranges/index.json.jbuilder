json.array!(@admin_weight_ranges) do |admin_weight_range|
  json.extract! admin_weight_range, :id, :range
  json.url admin_weight_range_url(admin_weight_range, format: :json)
end
