json.array!(@admin_tax_states) do |admin_tax_state|
  json.extract! admin_tax_state, :id
  json.url admin_tax_state_url(admin_tax_state, format: :json)
end
