json.array!(@admin_cod_policies) do |admin_cod_policy|
  json.extract! admin_cod_policy, :id
  json.url admin_cod_policy_url(admin_cod_policy, format: :json)
end
