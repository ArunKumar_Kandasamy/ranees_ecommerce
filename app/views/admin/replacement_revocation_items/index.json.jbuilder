json.array!(@admin_replacement_revocation_items) do |admin_replacement_revocation_item|
  json.extract! admin_replacement_revocation_item, :id, :replacement_revocation_id, :price, :qty, :variant_id, :per_qty_price, :actual_price, :shipping_cost, :tax, :reason, :status
  json.url admin_replacement_revocation_item_url(admin_replacement_revocation_item, format: :json)
end
