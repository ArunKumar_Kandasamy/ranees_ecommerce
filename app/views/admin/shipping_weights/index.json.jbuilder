json.array!(@admin_shipping_weights) do |admin_shipping_weight|
  json.extract! admin_shipping_weight, :id, :range, :cost, :shipping_policy_id, :shipping_specific_country_id, :shipping_specific_pincode_id
  json.url admin_shipping_weight_url(admin_shipping_weight, format: :json)
end
