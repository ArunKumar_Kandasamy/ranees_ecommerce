json.array!(@admin_price_ranges) do |admin_price_range|
  json.extract! admin_price_range, :id, :range
  json.url admin_price_range_url(admin_price_range, format: :json)
end
