json.array!(@bank_accounts) do |bank_account|
  json.extract! bank_account, :id, :name, :user_id, :num, :bank_code, :ifsc_code
  json.url bank_account_url(bank_account, format: :json)
end
