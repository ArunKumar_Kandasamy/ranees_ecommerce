# == Schema Information
#
# Table name: shipping_addresses
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  name       :string(255)
#  add_line1  :string(255)
#  add_line2  :string(255)
#  state      :string(255)
#  country    :string(255)
#  c_code     :string(255)
#  phone      :string(255)
#  created_at :datetime
#  updated_at :datetime
#  pincode    :string(255)
#

class ShippingAddress < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :user
  has_many :carts_shipping_addresses, class_name: "CartsShippingAddresses"
  has_many :carts, through: :carts_shipping_addresses
  has_many :orders
  has_many :order_logs, :dependent => :destroy, class_name: "Admin::OrderLog"
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 


  def full_address
    "#{self.name},#{add_line1},#{add_line2},#{state},#{country} #{pincode}"
  end
end
