# == Schema Information
#
# Table name: wishlist_items
#
#  id          :integer          not null, primary key
#  wishlist_id :integer
#  variant_id  :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class WishlistItem < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :wishlist,class_name: "Wishlist"
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
