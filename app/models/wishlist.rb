# == Schema Information
#
# Table name: wishlists
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Wishlist < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :user,class_name: "User"
  has_many :wishlist_items,class_name: "WishlistItem"
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
