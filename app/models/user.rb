# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  provider               :string(255)
#  uid                    :string(255)
#

class User < ActiveRecord::Base 
  liquid_methods :email
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :timeoutable, :timeout_in => 60.minutes
  #GEMS USED
  #ACCESSORS
  attr_accessor :current_password
  #ASSOCIATIONS
  has_many :failure_transactions, class_name: "Admin::FailureTransaction", dependent: :delete_all
  has_many :addresses, as: :connection, class_name: "Admin::Address"
  has_many :bank_accounts, class_name: "BankAccount"
  has_many :shipping_address, class_name: "Users::ShippingAddress"
  accepts_nested_attributes_for :addresses, allow_destroy: true,:reject_if => lambda { |a| a[:add_line1].blank? }
  has_many :identities, :dependent => :destroy
  has_many :wishlists, class_name: "Wishlist"
  has_one :permission, class_name: "Admin::Permission", dependent: :delete
  has_one :profile, :dependent => :destroy
  has_one :role, class_name: "Admin::Role", through: :permission
  has_one :cart, :dependent => :destroy
  has_many :cart_items, through: :cart
  has_many :shipping_address, dependent: :destroy
  has_many :orders, class_name: "Admin::Order"
  has_many :vouchers, class_name: "Admin::Voucher"
  accepts_nested_attributes_for :bank_accounts, allow_destroy: true
  #VALIDATIONS 
  #CALLBACKS
  #SCOPES

  def block_lists
    self.permission.role.block_lists
  end

  #CUSTOM SCOPES
  #OTHER METHODS
  def self.from_omniauth(auth)
    @email= User.where(:email=> auth.info.email).first
    if @email.nil?
      where(auth.slice(:provider, :uid)).first_or_create do |user|
       user.provider = auth.provider
       user.uid = auth.uid
       user.email = auth.info.email
       @identity = Identity.create(:uid => auth.uid, :provider => auth.provider, :user_id => user.id)
      end
    else
      @identity = Identity.where(:uid => auth.uid, :provider => auth.provider).first
       if @identity.nil?
         @identity = Identity.create(:uid => auth.uid, :provider => auth.provider, :user_id => @email.id)
       else
        @identity= Identity.where(provider: auth.provider).first
        @identity= @identity.update_attributes(user_id: @email.id)
       end
        return @email
    end
  end 
   
  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
    new(session["devise.user_attributes"], without_protection: true) do |user|
      user.attributes = params
      user.valid?
      end
    else
      super
    end    
  end

  def update_with_password(params={})  
    if params[:password].blank? 
      params.delete(:password) 
      params.delete(:password_confirmation) if params[:password_confirmation].blank? 
    end 
    update_attributes(params) 
  end

  def password_required?
    super && provider.blank?
  end 

  def combined_value
    "#{self.email}(#{self.role.name})"
  end

  def check_for_wishlist_item_presence(variant)
    @status = true
    if self.wishlists.first.present?
      self.wishlists.first.wishlist_items.each do |wishlist_item|
        if wishlist_item.variant_id == variant
          @status = false
        end
      end
    else
      @status = true
    end
    return @status
  end

  #JOBS
  #PRIVATE 
end
