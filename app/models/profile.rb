# == Schema Information
#
# Table name: profiles
#
#  id         :integer          not null, primary key
#  f_name     :string(255)
#  l_name     :string(255)
#  m_number   :string(255)
#  gender     :string(255)
#  created_at :datetime
#  updated_at :datetime
#  user_id    :integer
#

class Profile < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :user
  #VALIDATIONS
  validates :f_name, :presence => true
  validates :m_number, :presence => true
  validates :gender, :presence => true
  validates :user_id, :presence => true
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
 