# == Schema Information
#
# Table name: carts_shipping_addresses
#
#  id                  :integer          not null, primary key
#  cart_id             :integer
#  created_at          :datetime
#  updated_at          :datetime
#  shipping_address_id :integer
#

class CartsShippingAddresses < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :cart
  belongs_to :shipping_address
  #VALIDATIONS
  validates_uniqueness_of :cart_id, :scope => [:shipping_address_id]
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
