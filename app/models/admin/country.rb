# == Schema Information
#
# Table name: countries
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  country_code :string(255)
#

class Admin::Country < ActiveRecord::Base
  #GEMS USED
  #ASSOCIATIONS
  has_many :states, class_name: "Admin::State", dependent: :destroy
  has_many :district_cities, through: :states, class_name: "Admin::DistrictCity"
  has_many :pincodes, through: :district_cities, class_name: "Admin::Pincode"
  has_many :plan_countries, class_name: "Admin::PlanCountry"
  has_many :cod_specific_countries, class_name: "Admin::CodSpecificCountry"
  has_many :shipping_specific_countries, class_name: "Admin::ShippingSpecificCountry" 
  #VALIDATIONS
  validates :name, :presence => true, :uniqueness => true
  #validates :country_code, :presence => true, :uniqueness => true, :length => { :maximum => 10 }
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      Admin::Country.create! row.to_hash
    end
  end 
  #JOBS
  #PRIVATE
end
