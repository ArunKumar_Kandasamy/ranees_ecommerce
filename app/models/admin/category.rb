# == Schema Information
#
# Table name: categories
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  description      :text
#  page_title       :string(255)
#  meta_description :text
#  active           :boolean
#  pub_time         :datetime
#  parent_id        :integer
#  created_at       :datetime
#  updated_at       :datetime
#  status           :boolean
#  promotional_sort :integer
#  sort_order       :integer          default(0)
#

class Admin::Category < ActiveRecord::Base
  #GEMS USED
  acts_as_tree :order => "name"  
  #ACCESSORS
  attr_accessor :selected_tab , :parent_category
  #ASSOCIATIONS
  has_many :categories_products, :dependent => :destroy, class_name: "Admin::CategoriesProducts"
  has_many :products, through: :categories_products
  has_many :categories, through: :categories_products
  has_many :images, :dependent => :destroy
  has_many :general_images, :dependent => :destroy
  #VALIDATIONS
  validates :name, :presence => true
  validates :description, :presence => true
  validates_length_of  :page_title, :maximum => 60, :allow_blank => true
  validates_length_of  :meta_description, :maximum => 160, :allow_blank => true
  validates :active, :default => false
  #CALLBACKS
  before_create :sort_order_update
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def sort_order_update
    if self.parent_id.present?
      @sort_maximum= Admin::Category.where(:parent_id => nil).maximum("sort_order")
    else
      @sort_maximum= Admin::Category.where(:parent_id => self.parent_id).maximum("sort_order")
    end
      if @sort_maximum.nil?
        @sort_maximum = 1
      else
        @sort_maximum+=1
      end
      self.sort_order=@sort_maximum
  end 

  def for_delete
    if self.products.present?
      unless self.products.first.present? || self.descendants.first.present? 
        return true
      else
        return false
      end
    elsif self.descendants.present? 
      unless self.descendants.first.present?
        return true
      else
        return false
      end
    else
      return true
    end
  end

  def image_type_select
    if general_images.first.present?
      "#{name}(exist)"
    else
      "#{name}"
    end
  end 

  def combined_value
    parent = ""
    self.ancestors.reverse.each do |a|
      if parent != ''  
        parent << "#{a.name}/"
      else
        parent = "#{a.name}/"
      end
    end 
    "#{parent}#{self.name}"
  end

  def check_header_others_status
    child_count_limit = 3
    g_child_count_limit = 3
    if self.ancestors.present?
      count_limit = g_child_count_limit
    else
      count_limit = child_count_limit
    end
    child_count = self.children.count
    if child_count > count_limit
      return true
    else
      return false
    end
  end

  def check_header_limit    
    child_count_limit = 3
    g_child_count_limit = 3
    if self.ancestors.present?
      self.children.where(:status => true,:active => true).limit(g_child_count_limit)
    else
      self.children.where(:status => true,:active => true).limit(child_count_limit)
    end
  end

  def activate_category  
    @general = Admin::General.first
    if @general.present?
      @level= @general.category_level
      if self.status== true
        return self.products.where(:status => true).count > 0 ? true : false
      else
        return false
      end
    end
  end

  def activate_related_category
    if self.activate_category
      if self.ancestors.present?
        self.ancestors.each do | admin_category|
          if admin_category.status
            admin_category.active= true
            admin_category.save
          else
            @category_status = false
          end
        end
        unless @category_status.present?
          self.active= true
          self.save
        end
      else
        self.status= true
        self.save
      end
    else 
      if self.ancestors.present?
        self.ancestors.each do | admin_category|
          admin_category.active= false
          admin_category.save
        end
      else
        self.active= false
        self.save
      end
    end
  end


  #JOBS
  #PRIVATE
  private

  def self.ransackable_attributes(auth_object = nil)
    ['name']
  end
end
