# == Schema Information
#
# Table name: warehouses
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Admin::Warehouse < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :cart_items, class_name: "CartItem"
  has_many :vendors, through: :products
  has_many :addresses, as: :connection, class_name: "Admin::Address"
  has_many :inventories
  has_many :warehouses_roles, class_name: "Admin::WarehousesRoles"
  has_many :consignments, class_name: "Admin::Consignment"
  has_many :pickup_consignments, class_name: "Admin::PickupConsignment"
  has_many :consignment_items, through: :consignments, class_name: "Admin::ConsignmentItem"
  has_many :invoices, through: :consignments, class_name: "Admin::Invoice"
  has_many :orders, through: :consignments, class_name: "Admin::Order"
  has_many :package_tasks, class_name: "Admin::PackageTask"
  has_many :pickup_tasks, class_name: "Admin::PickupTask"
  has_many :variants, -> { uniq }, through: :inventories
  has_many :products, -> { uniq }, through: :variants
  accepts_nested_attributes_for :addresses, allow_destroy: true
  accepts_nested_attributes_for :consignments, allow_destroy: true
  #VALIDATIONS
  validates :name, :presence => true, :uniqueness => true
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE

  def full_name
    "#{name} #{created_at}"
  end 
  
  def with_country
    "#{name} (#{addresses.first.country})"
  end

end
