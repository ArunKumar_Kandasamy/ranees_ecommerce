# == Schema Information
#
# Table name: district_cities
#
#  id            :integer          not null, primary key
#  state_id      :integer
#  name          :string(255)
#  district_code :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class Admin::DistrictCity < ActiveRecord::Base
  #GEMS USED
  #ASSOCIATIONS
  belongs_to :state
  has_many :pincodes, class_name: "Admin::Pincode", dependent: :destroy
  #VALIDATIONS
  #validates :name, :presence => true, :uniqueness => true, :length => { :maximum => 30 }
  #validates :district_code, :presence => true, :uniqueness => true, :length => { :maximum => 10 }
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      Admin::DistrictCity.create! row.to_hash
    end
  end 
  #JOBS
  #PRIVATE
end
