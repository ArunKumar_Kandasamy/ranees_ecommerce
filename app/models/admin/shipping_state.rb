# == Schema Information
#
# Table name: shipping_states
#
#  id                  :integer          not null, primary key
#  shipping_country_id :integer
#  state_id            :integer
#  created_at          :datetime
#  updated_at          :datetime
#

class Admin::ShippingState < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :shipping_cities, :dependent => :destroy
  has_many :district_cities, through: :shipping_cities
  belongs_to :shipping_country
  belongs_to :state
  #VALIDATIONS
  validates :state_id, :presence => true
  validates_uniqueness_of :shipping_country_id, :scope => [:state_id]
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  #JOBS
  #PRIVATE 
end
 
