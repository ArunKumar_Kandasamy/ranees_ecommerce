# == Schema Information
#
# Table name: regulations
#
#  id                  :integer          not null, primary key
#  inventory_id        :integer
#  qty                 :integer
#  reason              :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  note                :string(255)
#  consignment_item_id :integer
#  request_id          :integer
#

class Admin::Regulation < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :inventory, class_name:"Admin::Inventory"
  belongs_to :consignment_item, class_name:"Admin::ConsignmentItem"
  #VALIDATIONS
  validates :reason, :presence => true
  validates :qty, :presence => true
  validates :note, :presence => true
  #CALLBACKS
  before_save :save_reason
  #before_save :manage_qty_inventory
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
  def save_reason
    case self.reason
    when "Damage"
      self.reason= "d"
    when "Return to vendor"
      self.reason= "r"
    when "Expired"
      self.reason= "e"
    when "Stolen"
      self.reason= "s"
    when "Credit Note"
      self.reason= "c"
    end
  end

  def manage_qty_inventory
    @admin_inventory= Admin::Inventory.find_by_id(self.inventory_id)
    balance=(@admin_inventory.balance_qty.to_i)-(self.qty.to_i)
    sold= (@admin_inventory.qty.to_i)- balance
    @admin_inventory.balance_qty = balance
    @admin_inventory.save
    @admin_inventory.update_attributes(sold_qty: sold)
  end

end
