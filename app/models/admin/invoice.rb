# == Schema Information
#
# Table name: invoices
#
#  id             :integer          not null, primary key
#  consignment_id :integer
#  barcode        :string(255)
#  invoice_number :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#

class Admin::Invoice < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  
  #ASSOCIATIONS
  belongs_to :consignment, class_name:"Admin::Consignment"
  belongs_to :order, class_name:"Admin::Order"
  belongs_to :warehouse, class_name:"Admin::Warehouse"
  belongs_to :package_task, class_name:"Admin::PackageTask"
  #VALIDATIONS
  #CALLBACKS

  before_save :invoice_number_auto_increment
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE


  def invoice_number_auto_increment
    datee = Date.today.to_datetime
    day = datee.mday
    month = datee.mon  
    invoices = Admin::Invoice.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if invoices.present?
      invoice_id = invoices.last.invoice_number[-3..-1].to_i
      invoice_num = invoice_id+=1      
      invoice_length = Math.log10(invoice_num).to_i + 1
      if invoice_length == 1
        self.invoice_number = "INVNO#{day}#{month}000#{invoice_num}"
      elsif invoice_length == 2
        self.invoice_number = "INVNO#{day}#{month}00#{invoice_num}"
      elsif invoice_length == 3
        self.invoice_number = "INVNO#{day}#{month}0#{invoice_num}"
      else
        self.invoice_number = "INVNO#{day}#{month}#{invoice_num}"    
      end
    else
      self.invoice_number = "INVNO#{day}#{month}0001"
    end
  end 
end
