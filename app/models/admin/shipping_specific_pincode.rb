# == Schema Information
#
# Table name: shipping_specific_pincodes
#
#  id                           :integer          not null, primary key
#  shipping_specific_country_id :integer
#  pincode                      :string(255)
#  created_at                   :datetime
#  updated_at                   :datetime
#

class Admin::ShippingSpecificPincode < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  
  #ASSOCIATIONS
  has_many :shipping_weights, class_name: "Admin::ShippingWeight"
  has_many :shipping_prices, class_name: "Admin::ShippingPrice"
  belongs_to :shipping_specific_country
  accepts_nested_attributes_for :shipping_weights, allow_destroy: true
  accepts_nested_attributes_for :shipping_prices, allow_destroy: true
  #VALIDATIONS 
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
end
