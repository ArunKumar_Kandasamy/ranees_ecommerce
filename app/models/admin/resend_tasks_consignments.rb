# == Schema Information
#
# Table name: resend_tasks_consignments
#
#  id             :integer          not null, primary key
#  consignment_id :integer
#  resend_task_id :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class Admin::ResendTasksConsignments < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  attr_accessor :conform
  #ASSOCIATIONS
  belongs_to :consignment, class_name: "Admin::Consignment"
  belongs_to :resend_task, class_name: "Admin::ResendTask"
  #VALIDATIONS
  validates_uniqueness_of :consignment_id, :scope => [:resend_task_id]
  #CALLBACKS
  after_destroy :update_resendtask
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  
  def update_resendtask
    if self.resend_task.consignments.first.nil?
      self.resend_task.destroy
    end
  end
  #JOBS
  #PRIVATE
end
