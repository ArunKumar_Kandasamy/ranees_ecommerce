# == Schema Information
#
# Table name: orders
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  order_cost          :float
#  payment_mode        :string(255)
#  payment_status      :boolean
#  order_number        :string(255)
#  status              :string(255)
#  shipping_address_id :integer
#  ip                  :string(255)
#  location            :string(255)
#  pay_val             :boolean
#  cal_conf            :boolean
#  prev_rec            :boolean
#  created_at          :datetime
#  updated_at          :datetime
#  cancel              :string(255)
#  remarks             :text
#  cancelled_by        :integer
#  voucher_amount      :float
#  balance_amount      :float
#  tax                 :float
#  shipping_cost       :float
#

 
class Admin::Order < ActiveRecord::Base 
  liquid_methods :order_number, :user, :order_cost
  apply_simple_captcha
  #GEMS USED
  #ACCESSORS
  attr_accessor :present_user,:prev_rec_reason,:pay_val_reason,:cal_conf_reason
  #ASSOCIATIONS
  has_many :order_items, class_name: "Admin::OrderItem"
  has_many :consignments, class_name: "Admin::Consignment"
  has_many :order_revocations, class_name: "Admin::OrderRevocation"
  has_many :consignment_items, through: :consignments, class_name: "Admin::ConsignmentItem"
  has_many :invoices, through: :consignments, class_name: "Admin::Invoice"
  has_many :cart_items, :dependent => :destroy, class_name: "CartItem"
  has_many :rejection_reasons, :dependent => :destroy, class_name: "Admin::RejectionReason"
  has_many :order_logs, :dependent => :destroy, class_name: "Admin::OrderLog"
  belongs_to :user
  belongs_to :shipping_address
  belongs_to :warehouse, class_name: "Admin::Warehouse"
  accepts_nested_attributes_for :order_items, allow_destroy: true
  accepts_nested_attributes_for :consignments, allow_destroy: true
  accepts_nested_attributes_for :order_revocations, allow_destroy: true
  has_many :vouchers, class_name: "Admin::Voucher", through: :order_revocations
  #VALIDATIONS
  #CALLBACKS
  before_create :order_number_auto_increment
  #before_update :update_order_price
  after_update :for_order_logs

  #after_create :order_item_create
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def order_shipping_cost
    @country = Array.new 
    self.order_items.each do |order_item|
      @variant = Admin::Variant.find(order_item.variant_id)
      @product = @variant.product
      unless @product.availability_plan.all_country 
        @country << @product.availability_plan.plan_countries.map{|x| x.country_id}
      end
    end
  end 
  


  def send_sms  
    if self.present?
      @shipping_id= self.shipping_address_id
      @country= ShippingAddress.find(@shipping_id).country
      c= Country.find_country_by_name(@country)
      @country_code= c.country_code.to_s
      @shipping_phone= "+" + @country_code + ShippingAddress.find(@shipping_id).phone
      account_sid = "ACa7b7742ad9c96cc541db77b78ee7067c"
      auth_token = "50743bd26cf4d7faca5df98b729a1e67"
      client = Twilio::REST::Client.new account_sid, auth_token
      @body = Liquid::Template.parse(Admin::SmsNotification.first.content_for_order_create).render 'order' => @order
       
      from = "+18057255347" # Your Twilio number
      client.account.messages.create(
        :from => from,
        :to => @shipping_phone,
        :body => @body.html_safe 
        )
        puts "Sent message to #{@shipping_phone}"
    end 
  end

  def order_cancel_sms
    if self.present?
      @shipping_id= self.shipping_address_id
      @country= ShippingAddress.find(@shipping_id).country
      c= Country.find_country_by_name(@country)
      @country_code= c.country_code.to_s
      @shipping_phone= "+" + @country_code + ShippingAddress.find(@shipping_id).phone
      account_sid = "ACa7b7742ad9c96cc541db77b78ee7067c"
      auth_token = "50743bd26cf4d7faca5df98b729a1e67"
      client = Twilio::REST::Client.new account_sid, auth_token
      @body = Liquid::Template.parse(Admin::SmsNotification.first.content_for_order_failed).render 'order' => @order
       
      from = "+18057255347" # Your Twilio number
      client.account.messages.create(
        :from => from,
        :to => @shipping_phone,
        :body => @body.html_safe 
        )
        puts "Sent message to #{@shipping_phone}"
    end 
  end
  
  def order_number_auto_increment
    datee = Date.today.to_datetime 
    day = datee.mday
    daily = day.to_s.size
    if daily == 1
      day = "0#{day}"
    else
      day = day
    end
    month = datee.mon  
    orders = Admin::Order.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if orders.present?
      order_id = orders.last.order_number[-3..-1]
      order_increment = order_id.to_i
      order_num = order_increment + 1     
      order_length = order_num.to_s.size
      if order_length == 1
        self.order_number = "OD#{day}#{month}000#{order_num}"
      elsif order_length == 2
        self.order_number = "OD#{day}#{month}00#{order_num}"
      elsif order_length == 3
        self.order_number = "OD#{day}#{month}0#{order_num}"
      else
        self.order_number = "OD#{day}#{month}#{order_num}"    
      end
    else
      self.order_number = "OD#{day}#{month}0001"
    end
  end
  
  def check_user_deligate_status
    @status = true
    if self.consignments.present?
      self.consignments.each do |consignment|
        if consignment.package_tasks.first.nil?
          @status = false
        end
      end
    else
      @status = false
    end
    return @status
  end

  def check_user_order_status 
    if self.cancel.nil? && self.cal_conf == true && self.prev_rec == true && self.pay_val == true
      return true
    else
      return false
    end
  end

  def check_user_cancel_order_status
    if self.status == "up" && self.cancel.nil?
      return true
    else
      return false
    end
  end

  def check_consignment_confirmation
    if self.status == "wh" 
      self.consignments.where(:cancel => nil).each do |consignment|
        if consignment.conform == false
          return false
        end
      end
      return true
    else
      return false
    end
  end

  def self.up_orders 
    self.where(:status => "up")
  end

  def self.wh_orders
    self.where(:status => "wh")
  end

  def qty
    quantity = 0
    self.order_items.each do |order_item|
      quantity = quantity + order_item.qty
    end
    return quantity
  end

  def consignment_qty
    quantity = 0
    self.consignment_items.each do |consignment_item|
      quantity = quantity + consignment_item.qty
    end
    return quantity
  end

  # def self.partially_assigned_orders
  #   partially_assign_orders = Array.new
  #   self.where(status:"WH").each do |order|
  #     if order.qty != order.consignment_qty
  #       partially_assign_orders << order
  #     end
  #     return partially_assign_orders
  #   end
  # end


  def self.partial_assigned_orders
    self.where(:status => "pwh")
  end

  #JOBS
  #PRIVATE 

  def check_order_verification
    if self.pay_val == true
      return true
    else
      return false        
    end
  end
  
  def check_order_consignment
    @status = true
    if self.consignments.present?
      self.order_items.each do |order_item|
        if order_item.consignment_items.first.nil?
          return false
        end
      end
    else
      return false        
    end
    return @status
  end

  def update_order_price 
    @order = Admin::Order.find(self.id)
    total=0
    @order.order_items.each do |order_item|
      total += order_item.price 
    end
    @order.order_cost = total
    @order.save
  end


  def create_order_log(user_id)
    self.update_order_price
    @order_log = Admin::OrderLog.create(:updated_by => user_id,:status => self.status, :order_id => self.id, :shipping_address_id => self.shipping_address_id, :pay_val => self.pay_val, :cal_conf => self.cal_conf, :prev_rec => self.prev_rec, :cancel => self.cancel, :remarks => self.remarks)
    self.order_items.each do |order_item|
      @order_item_log =Admin::OrderItemLog.create(:order_log_id => @order_log.id, :qty => order_item.qty, :price => order_item.price, :variant_id => order_item.variant_id, :per_qty_price => order_item.per_qty_price)
    end
    @order_log.update_order_log_price
    return @order_log
  end

  def check_previous_attr_change
    @order = Admin::Order.find(self.id)
    @order.order_logs.each do |order_log|
      order_log
    end
  end


  def create_revocation
    if check_consignment_confirmation
      if self.payment_mode != "COD"
        @revocation_cost = self.order_logs.first.order_cost-self.order_cost
        if @revocation_cost > 0 
          @revocation = Admin::OrderRevocation.create(:revocation_cost => @revocation_cost, :order_id => self.id, :revocation_mode => 'v',:status => "up")
          self.order_logs.first.order_item_logs.each do |order_log_item|
            @order_item = self.order_items.where(:variant_id => order_log_item.variant_id,:per_qty_price => order_log_item.per_qty_price).first
            if @order_item .present?
              @revocation_item_cost = order_log_item.price - @order_item.price
              @revocation_item_qty = order_log_item.qty - @order_item.qty
            else
              @revocation_item_cost = order_log_item.price
              @revocation_item_qty = order_log_item.qty
            end
            if @revocation_item_cost > 0
              Admin::OrderRevocationItem.create(:order_revocation_id => @revocation.id , :price => @revocation_item_cost , :qty => @revocation_item_qty ,:variant_id => order_log_item.variant_id ,:per_qty_price => order_log_item.per_qty_price)
            end
          end
        end
      else
        if self.balance_amount.present?
          if self.balance_amount > 0
            @revocation = Admin::OrderRevocation.create(:revocation_cost => self.balance_amount, :order_id => self.id, :revocation_mode => 'v')
          end
        end
      end
    end
  end

  def check_consignment_status
    @status = true 
    if self.consignments.present?
      self.consignments.each do |consignment|
        if consignment.conform == true
          return false
        end
      end
    end
    return @status
  end

  def check_consignment_conform_status
    @status = true
    if self.consignments.present?
      self.consignments.each do |consignment|
        if consignment.conform == false
          return false
        end
      end
    else
      return false
    end
    return @status
  end
 
  def check_consignment_task_status
    @status = true
    if self.consignments.present?
      self.consignments.each do |consignment|
        if consignment.conform == false || consignment.package_tasks.first.nil?
          return false
        end
      end
    else
      return false
    end
    return @status
  end

  def check_order_status 
    if self.cancel.nil? && self.cal_conf == true && self.prev_rec == true && self.pay_val == true
      return true
    else
      return false
    end
  end

  def check_deligate_status
    @status = true
    if self.consignments.present?
      self.consignments.each do |consignment|
        if consignment.package_tasks.first.nil?
          return false
        end
      end
    else
      return false
    end
    return @status
  end

  def check_assign_warehouse
    if self.status == "wh"
      return true
    else
      return false
    end
  end

  def wh_status_update
    @status = 0
    @partial = 0
    if self.consignments.present? 
      self.order_items.each do |order_item|
        sum = 0
        order_item.consignment_items.each do |consignment_item|
          if consignment_item.consignment.cancel.nil?
            sum += consignment_item.qty
          end
        end
        if order_item.qty ==  sum 
          @status = 1
        elsif order_item.qty > sum 
          @partial = 1
        end
      end
      if @status == 1 && @partial == 1
        self.update(:status => "pwh")
      elsif @status == 1 && @partial == 0
        self.update(:status => "wh")
      end
    else
      self.update(:status => "up")
    end
  end
  

  def for_order_logs
    if (self.pay_val_changed? && self.cal_conf_changed? && self.prev_rec_changed?)
      if self.pay_val_reason.present?
       @reason = Admin::RejectionReason.create(:reason => self.pay_val_reason, :attr_value => "pay_val",:order_id => self.id)
      end
      if self.cal_conf_reason.present?
        @reason = Admin::RejectionReason.create(:reason => self.cal_conf_reason, :attr_value => "cal_conf",:order_id => self.id)
      end
      if self.prev_rec_reason.present?
        @reason = Admin::RejectionReason.create(:reason => self.prev_rec_reason, :attr_value => "prev_rec",:order_id => self.id)
      end

    elsif (self.pay_val_changed? && self.cal_conf_changed?)
      if self.pay_val_reason.present?
        @reason = Admin::RejectionReason.create(:reason => self.pay_val_reason, :attr_value => "pay_val",:order_id => self.id)
      end
      if self.cal_conf_reason.present?
        @reason = Admin::RejectionReason.create(:reason => self.cal_conf_reason, :attr_value => "cal_conf",:order_id => self.id)
      end

    elsif (self.cal_conf_changed? && self.prev_rec_changed?)
      if self.cal_conf_reason.present?
        @reason = Admin::RejectionReason.create(:reason => self.cal_conf_reason, :attr_value => "cal_conf",:order_id => self.id)
      end
      if self.prev_rec_reason.present?
        @reason = Admin::RejectionReason.create(:reason => self.prev_rec_reason, :attr_value => "prev_rec",:order_id => self.id)
      end
    
    elsif (self.pay_val_changed? && self.prev_rec_changed?)
      if self.cal_conf_reason.present?
        @reason = Admin::RejectionReason.create(:reason => self.cal_conf_reason, :attr_value => "cal_conf",:order_id =>  self.id)
      end
      if self.prev_rec_reason.present?
        @reason = Admin::RejectionReason.create(:reason => self.prev_rec_reason, :attr_value => "prev_rec",:order_id => self.id)
      end

    elsif (self.pay_val_changed?)
      if self.pay_val_reason.present?
        @reason = Admin::RejectionReason.create(:reason => self.pay_val_reason, :attr_value => "pay_val",:order_id => self.id)
      end

    elsif (self.cal_conf_changed?)
      if self.cal_conf_reason.present?
        @reason = Admin::RejectionReason.create(:reason => self.cal_conf_reason, :attr_value => "cal_conf",:order_id => self.id)
      end

    elsif (self.prev_rec_changed?)
      if self.prev_rec_reason.present?
        @reason = Admin::RejectionReason.create(:reason => self.prev_rec_reason, :attr_value => "prev_rec",:order_id => self.id)
      end
    end
  end

  def order_item_create
    self.cart_items.each do | cart_item |
      Admin::OrderItem.create(:qty => cart_item.qty, :variant_id => cart_item.variant_id, :price => cart_item.price, :per_qty_price => cart_item.per_qty_price, :order_id => self.id)
    end
    self.cart_items.destroy
  end

  def check_order_shipping_unavailable_item(pincode)
    @shipping_unavailable_item = Array.new
    self.order_items.each do |order_item|
      @variant = Admin::Variant.find(order_item.variant_id)
      @shipping = Admin::Product.shipping_cost(pincode, @variant.product, @variant)
      if @shipping.status == false
        @shipping_unavailable_item << order_item
      end
    end
    return @shipping_unavailable_item
  end  

  def check_cod_unavailable_item(pincode)
    @cod_unavailable_item = Array.new
    self.order_items.each do | order_item|
      @variant=Admin::Variant.where(:id => order_item.variant_id).first
      @product= @variant.product
      if @product.check_cod(pincode) == false
        @cod_unavailable_item << order_item
      end
    end
    return @cod_unavailable_item
  end

  def check_support_cancel_order_status
    if self.status == "up"
      return true
    else
      if self.consignments.first.present?
        self.consignments.each do |consignment|
          consignment.consignment_items.each do |consignment_item|
            if consignment_item.regulations.first.present?
              return false
            end
          end
        end
        return true
      else
        return true
      end
    end
  end

  def create_revocation_for_cancel
    @order_log = self.order_logs.first
    @revocation = Admin::OrderRevocation.create(:order_id => self.id  ,:revocation_cost => @order_log.order_cost)
    @order_log.order_item_logs.each do |order_log_item|
      Admin::OrderRevocationItem.create(:order_revocation_id => @revocation.id , :price => order_log_item.price , :qty => order_log_item.qty ,:variant_id => order_log_item.variant_id ,:per_qty_price => order_log_item.per_qty_price)
    end
    return @revocation
  end


  def find_cart_total_shipping_cost(country,pincode)
    @shipping_policy = Admin::ShippingPolicy.first
    @weight = 0
    @price = 0

    self.order_items.map do |order_item| 
      @variant = Admin::Variant.find(order_item.variant_id)
      @product = @variant.product
      if @product.based_on == "w" && @product.free_shipping != true
        @weight += (@variant.weight * order_item.qty) 
      elsif @product.based_on == "p" && @product.free_shipping != true
        @price += (@variant.inventories.first.selling_price * order_item.qty) 
      end
    end

    @weight_cost = 0
    @price_cost = 0
    @total_cost = 0
    
    if @shipping_policy.all_country
      if @weight > 0
        @weight_cost = self.find_shipping_range(@shipping_policy,false,"w",@weight)
      end
      if @price > 0
        @price_cost = self.find_shipping_range(@shipping_policy,false,"p",@price)
      end 
      return @total_cost = @weight_cost + @price_cost
    else
      @shipping_country = @shipping_policy.shipping_specific_countries.where(:country_id => country).first
      if  @shipping_country.present?
        if @shipping_country.all_place
          if @weight > 0
            @weight_cost = self.find_shipping_range(@shipping_country,false,"w",@weight)
          end
          if @price > 0
            @price_cost = self.find_shipping_range(@shipping_country,false,"p",@price)
          end 
          return @total_cost = @weight_cost + @price_cost
        else
          @pincode = Admin::ShippingSpecificPincode.where(:pincode => pincode).first
          if @pincode.present?
            if @weight > 0
              @weight_cost = self.find_shipping_range(@pincode,true,"w",@weight)
            end
            if @price > 0
              @price_cost = self.find_shipping_range(@pincode,true,"p",@price)
            end 
            return @total_cost = @weight_cost + @price_cost
          else
            return 0
          end
        end
      else
        return 0
      end
    end

    #return @weight_based_items.grep(Integer)
  end


  def availability_shipping_cost_with_pincode(pincode)
    @pincode = Admin::ShippingSpecificPincode.where(:pincode => pincode).first
    if @pincode.present?
      return self.find_shipping_range(@pincode,true)
    else
      return 0
    end
  end

  def find_shipping_range(associated_plan,pincode,based_on_attr,based_on_value)
    unless pincode
      if associated_plan.policy == "free"
        return 0
      elsif associated_plan.policy == "flat"
        return @shipping_policy.cost
      else 
        if based_on_attr == "w"
          associated_plan.shipping_weights.each do |weight|
            if based_on_value.between?(weight.range.split("-")[0].to_i,weight.range.split("-")[1].to_i) 
              @return_weight =  weight.cost
            end
          end
          unless @return_weight.present?
          @return_weight = associated_plan.shipping_weights.maximum("cost")
          end
          return @return_weight
        else
          associated_plan.shipping_prices.each do |price|
            if based_on_value.between?(price.range.split("-")[0].to_i,price.range.split("-")[1].to_i) 
              @return_price = price.cost
            end
          end
          unless @return_price.present?
          @return_price = associated_plan.shipping_prices.maximum("cost")
          end
          return @return_price
        end
      end
    else
      if based_on_attr == "w"
        associated_plan.shipping_weights.each do |weight|
          if self.based_on_value.between?(weight.range.split("-")[0].to_i,weight.range.split("-")[1].to_i) 
            @return_weight = weight.cost
          end
        end
        unless @return_weight.present?
          @return_weight = associated_plan.shipping_weights.maximum("cost")
        end
        return @return_weight
      else
        associated_plan.shipping_prices.each do |price|
          if based_on_value.between?(price.range.split("-")[0].to_i,price.range.split("-")[1].to_i) 
           @return_price =  price.cost
          end
        end
        unless @return_price.present?
          @return_price = associated_plan.shipping_prices.maximum("cost")
        end
        return @return_price
      end
    end
  end
  
  def availability_shipping_cost_with_country(country_id)
    @product = self.product
    @shipping_policy = Admin::ShippingPolicy.first
    if @product.availability_plan.all_Country
      if @shipping_policy.all_country
        return self.find_shipping_range(@shipping_policy,false)
      else
        @shipping_country = @shipping_policy.shipping_countries.where(:country_id => country_id).first
        if @shipping_country.all_place
          return self.find_shipping_range(@shipping_country,false)
        else
          return @shipping_country.shipping_specific_pincodes
        end
      end
    else
      @availability_country = @product.availability_plan.list_countries.where(:country_id => country_id).first
      if @availability_country.all_place
        return self.find_shipping_range(@shipping_country,false)
      else
        return @availability_country.country_pincodes
      end
    end
  end
  
end
