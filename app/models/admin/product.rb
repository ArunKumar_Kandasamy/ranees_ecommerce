# == Schema Information
#
# Table name: products
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  description          :text
#  status               :boolean          default(FALSE)
#  created_at           :datetime
#  updated_at           :datetime
#  brand_id             :integer
#  ship_invent          :boolean          default(FALSE)
#  unavail_purchase     :boolean          default(FALSE)
#  visibility           :boolean
#  promote              :string(255)
#  promotional_sort     :integer          default(0)
#  sort_order           :integer          default(0)
#  return_due           :integer
#  based_on             :string(255)
#  cod                  :boolean
#  free_shipping        :boolean
#  availability_plan_id :integer
#

class Admin::Product < ActiveRecord::Base  
  #GEMS USED
  #ACCESSORS 
  attr_accessor :new_brand , :brand_tokens, :type_tokens
  attr_reader :brand_tokens , :type_tokens
  #ASSOCIATIONS
  has_many :product_types
  has_many :types, through: :product_types  
  has_many :images, through: :variants
  has_many :variants, :dependent => :destroy
  has_many :inventories, through: :variants
  has_many :warehouses, through: :inventories
  has_many :vendors, through: :inventories
  has_many :product_properties, through: :variants
  has_many :products_tax_plans, class_name: "Admin::ProductsTaxPlans"
  has_many :tax_plans, through: :products_tax_plans
  belongs_to :brand
  belongs_to :availability_plan
  has_many :categories_products, :dependent => :destroy, class_name: "Admin::CategoriesProducts"
  has_many :categories, through: :categories_products
  accepts_nested_attributes_for :variants, allow_destroy: true
  accepts_nested_attributes_for :product_types, allow_destroy: true
  
  #VALIDATIONS
  validates :name, :presence => true
  validates :description, :presence => true
  validates :status, :default => false
  validates :unavail_purchase, :default => false
  
  #CALLBACKS
  before_save :create_brand 
  before_create :promotion_update

  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS 

   def related_products(category)
    @related_products = Array.new
    @types = Admin::Type.find(self.types.map{|x| x.id})
    @types.each do |type|
      @type_product = type.product_types.where('product_id != ?',nil)
      @type_products = @type_product.where('product_id != ?',self.id)
      if @type_products.first.present?
        @related_products.push(*@type_products.map{|x| x.product.visibility == true && x.product.status == true ? x.product_id : nil }).uniq
      end
    end
    unless @related_products.length > 3
      count = 4 - @related_products.length 
      @cat_products = category.categories_products.where('product_id != ?',self.id)
      @related_products.push(*@cat_products.map{|x| x.product.visibility == true && x.product.status == true ? x.product_id : nil }).uniq
    end 
    if @related_products.length == 0
      @related_products.push(*Admin::Product.where('id != ?',self.id).where(:visibility => true,:status => true).take(4).map{|x| x.id})
    end

    return Admin::Product.find(@related_products)
  end


  def qualify_variant_status 
    @flag = 0
    self.variants.each do | variant|
      if variant.activate_variant
        @flag = 1
      end
    end
    if @flag == 1
      return true 
    else
      return false
    end
  end

  def bal_qty_check
    self.variants.each do | variant|
      bal_qty=0
      variant.inventories.each do | inventory|
        bal_qty+=inventory.balance_qty
      end
      return bal_qty.to_s
    end
  end

  def check_cod(pincode)
    @status = false
    if self.cod == true
      @status = true
    else 
      @cod_policy = Admin::CodPolicy.first
      if @cod_policy.all_country
        if @cod_policy.cod_availability
          @status = true
        end
      end 
    end
    return @status
  end

  def general_fixed_rate
    if Admin::General.first.present?
      if Admin::General.first.fixed_rate == true
        return true
      else
        return false
      end
    else
      return false
    end
  end

  def plans_check 
    if (self.inventories.present? && self.categories.present? && (self.availability_plan.present? || (self.ship_invent == true) || general_fixed_rate)  && (self.tax_plans.present? || general_fixed_rate || self.tax_violate))
      return true
    else 
      return false
    end
  end

  def activate_product
    if self.visibility == true
      if self.plans_check
        if self.qualify_variant_status
          if self.return_due.present? && self.based_on.present?
            return true
          else
            return false
          end
        end
      end
    end
  end

  def selling_price_check
    self.inventories.each do |inventory|
      sel_price=inventory.selling_price
      if sel_price.present?
        return true
      else
        return false
      end 
    end
  end

  

  def self.next(product)
    where('id < ?', product.id).last
  end

  def self.previous(product)
    where('id > ?', product.id).first
  end

  #JOBS
  #PRIVATE 

  def promotion_update
    @sort_maximum= Admin::Product.maximum("sort_order") ? Admin::Product.maximum("sort_order") : 0
    @sort_maximum+=1
    self.sort_order=@sort_maximum
  end
  
  def brand_tokens=(tokens)
    self.brand_id = Admin::Brand.ids_from_tokens(tokens)
  end 

  def type_tokens=(tokens)
    self.type_ids = Admin::Type.ids_from_tokens(tokens)
  end  

  def create_brand
    @admin_brand=Admin::Brand.create(name: new_brand) if new_brand.present?
    self.brand_id = @admin_brand.id if @admin_brand.present?
  end

  private

  def self.ransackable_attributes(auth_object = nil)
    ['name']
  end
end
