# == Schema Information
#
# Table name: package_tasks
#
#  id           :integer          not null, primary key
#  task_number  :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  warehouse_id :integer
#

class Admin::PackageTask < ActiveRecord::Base
  #GEMS USED
  #ASSOCIATIONS
  has_many :package_tasks_consignments, :dependent => :destroy, class_name: "Admin::PackageTasksConsignments"
  has_many :consignments, through: :package_tasks_consignments, class_name: "Admin::Consignment"
  has_many :invoices, through: :consignments, class_name: "Admin::Invoice"
  belongs_to :warehouse , class_name: "Admin::Warehouse"
  accepts_nested_attributes_for :package_tasks_consignments, allow_destroy: true 
  accepts_nested_attributes_for :consignments, allow_destroy: true
  #VALIDATIONS
  #CALLBACKS
  before_create :task_number_auto_increment
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def task_number_auto_increment
    datee = Date.today.to_datetime
    day = datee.mday
    month = datee.mon  
    tasks = Admin::PackageTask.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if tasks.present?
      task_id = tasks.last.task_number[-3..-1]
      task_increment = task_id.to_i
      task_num = task_increment + 1     
      task_length = task_num.to_s.size
      if task_length == 1
        self.task_number = "TASK#{day}#{month}000#{task_num}"
      elsif task_length == 2
        self.task_number = "TASK#{day}#{month}00#{task_num}"
      elsif task_length == 3
        self.task_number = "TASK#{day}#{month}0#{task_num}"
      else
        self.task_number = "TASK#{day}#{month}#{task_num}"    
      end
    else
      self.task_number = "TASK#{day}#{month}0001"
    end
  end

  def check_consignment_exist
    @status = false
    if self.consignments.first.present?
      self.consignments.each do |consignment|
        if consignment.conform == false
          @status = true 
        end
      end
    end
    return @status
  end
  
  def check_regulation_exist    
    if self.consignments.first.present?
      self.consignments.each do |consignment|
        consignment.consignment_items.each do |consignment_item|
          if consignment_item.regulations.first.present?
            return false 
          else
            return true
          end
        end
      end
    end
  end

  #JOBS
  #PRIVATE 
  
end
