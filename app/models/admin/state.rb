# == Schema Information
#
# Table name: states
#
#  id         :integer          not null, primary key
#  country_id :string(255)
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  state_code :string(255)
#

class Admin::State < ActiveRecord::Base
  #GEMS USED
  #ASSOCIATIONS
  belongs_to :country
  has_many :district_cities, class_name: "Admin::DistrictCity", dependent: :destroy
  has_many :pincodes, through: :district_cities, class_name: "Admin::Pincode"
  #VALIDATIONS
  validates :name, :presence => true, :uniqueness => true, :length => { :maximum => 30 }
  #validates :state_code, :presence => true, :uniqueness => true, :length => { :maximum => 10 }
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      Admin::State.create! row.to_hash
    end
  end 
  #JOBS
  #PRIVATE
end
