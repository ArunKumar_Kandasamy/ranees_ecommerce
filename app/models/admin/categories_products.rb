# == Schema Information
#
# Table name: categories_products
#
#  id          :integer          not null, primary key
#  product_id  :integer
#  category_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Admin::CategoriesProducts < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :product
  belongs_to :category 
  #VALIDATIONS
  validates_uniqueness_of :product_id, :scope => [:category_id]
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
