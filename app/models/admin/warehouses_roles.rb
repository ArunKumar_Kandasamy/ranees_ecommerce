# == Schema Information
#
# Table name: warehouses_roles
#
#  id           :integer          not null, primary key
#  warehouse_id :integer
#  role_id      :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class Admin::WarehousesRoles < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :warehouse
  belongs_to :role  
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 

end
