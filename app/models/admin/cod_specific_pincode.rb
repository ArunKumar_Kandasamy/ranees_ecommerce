# == Schema Information
#
# Table name: cod_specific_pincodes
#
#  id                      :integer          not null, primary key
#  cod_specific_country_id :integer
#  pincode                 :string(255)
#  cod_availability        :boolean          default(TRUE)
#  created_at              :datetime
#  updated_at              :datetime
#

class Admin::CodSpecificPincode < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :cod_specific_country
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
end
