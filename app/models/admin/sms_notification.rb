# == Schema Information
#
# Table name: sms_notifications
#
#  id                            :integer          not null, primary key
#  user_id                       :integer
#  order_placed                  :boolean          default(FALSE)
#  tracking_updated              :boolean          default(FALSE)
#  order_success                 :boolean          default(FALSE)
#  order_failed                  :boolean          default(FALSE)
#  pre_deli_cancel               :boolean          default(FALSE)
#  post_deli_cancel              :boolean          default(FALSE)
#  refund_complete               :boolean          default(FALSE)
#  replacement_order             :boolean          default(FALSE)
#  partial_refund                :boolean          default(FALSE)
#  content_for_order_placed      :text
#  content_for_tracking_updated  :text
#  content_for_order_success     :text
#  content_for_order_failed      :text
#  content_for_pre_deli_cancel   :text
#  content_for_post_deli_cancel  :text
#  content_for_refund_complete   :text
#  content_for_replacement_order :text
#  content_for_partial_refund    :text
#  created_at                    :datetime
#  updated_at                    :datetime
#  order_create                  :boolean          default(FALSE)
#  content_for_order_create      :text
#  back_jobs                     :boolean          default(FALSE)
#  content_for_back_jobs         :text
#

class Admin::SmsNotification < ActiveRecord::Base
end
