# == Schema Information
#
# Table name: order_items_consignment_items
#
#  id                  :integer          not null, primary key
#  consignment_item_id :integer
#  order_item_id       :integer
#  created_at          :datetime
#  updated_at          :datetime
#

class Admin::OrderItemsConsignmentItems < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :consignment_item, class_name: "Admin::ConsignmentItem"
  belongs_to :order_item, class_name: "Admin::OrderItem"
  #VALIDATIONS
  validates_uniqueness_of :consignment_item_id, :scope => [:order_item_id]
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
end
