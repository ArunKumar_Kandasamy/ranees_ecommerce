# == Schema Information
#
# Table name: cod_specific_countries
#
#  id            :integer          not null, primary key
#  country_id    :integer
#  cod_policy_id :integer
#  all_place     :boolean          default(TRUE)
#  created_at    :datetime
#  updated_at    :datetime
#

class Admin::CodSpecificCountry < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :cod_policy
  belongs_to :country, class_name: "Admin::Country"
  has_many :cod_specific_pincodes, :dependent => :destroy, class_name: "Admin::CodSpecificPincode"
  accepts_nested_attributes_for :cod_specific_pincodes, allow_destroy: true
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
end
