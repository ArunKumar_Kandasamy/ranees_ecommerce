# == Schema Information
#
# Table name: pincodes
#
#  id               :integer          not null, primary key
#  district_city_id :integer
#  pincode          :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#  cost             :integer

class Admin::Pincode < ActiveRecord::Base
  #GEMS USED
  #ASSOCIATIONS
  belongs_to :district_city
  #VALIDATIONS
  validates :pincode, :presence => true, :uniqueness => true, :length => { :maximum => 30 }
  validates_uniqueness_of :district_city_id, :scope => [:pincode]
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE	


  def self.tokens(query)
    types = where("pincode ILIKE ?", "%#{query}%")
    types
  end

end
