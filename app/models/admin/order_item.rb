# == Schema Information
#
# Table name: order_items
#
#  id            :integer          not null, primary key
#  order_id      :integer
#  price         :float
#  qty           :integer
#  variant_id    :integer
#  per_qty_price :float
#  created_at    :datetime
#  updated_at    :datetime
#  return_due    :integer
#  shipping_cost :float
#  tax           :float
#  actual_cost   :float
#

class Admin::OrderItem < ActiveRecord::Base
  liquid_methods :qty, :price, :per_qty_price,:name
  #GEMS USED
  #ACCESSORS
  attr_accessor :warehouse_id,:reason
  #ASSOCIATIONS 
  belongs_to :order, class_name: "Admin::Order"
  has_many :order_items_consignment_items, :dependent => :destroy, class_name: "Admin::OrderItemsConsignmentItems"
  has_many :consignment_items, through: :order_items_consignment_items, class_name: "Admin::ConsignmentItem"
  #VALIDATIONS 
  #CALLBACKS
  before_save :update_price
  after_update :update_order_price
  after_destroy :update_order_price
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
  def update_price
    self.price= self.per_qty_price * self.qty
  end	

  def update_order_price
    total = 0
    @order = self.order
    @order.order_items.each do |order_item|
      total += order_item.price 
    end
    @order.order_cost = total
    @order.save
  end
  
  def bal_qty(variant) 
    bal_qty=0
    variant.inventories.each do |inventory|
      bal_qty += inventory.balance_qty
    end
    up_qty=0
    Admin::Order.where(:status => "up").where("id !=?",self.order_id).each do |order|
      up_item = order.order_items.where(:variant_id => variant.id).first
      if up_item.present?
        up_qty += up_item.qty
      end
    end
    return bal_qty = bal_qty-up_qty
  end
  
end
