# == Schema Information
#
# Table name: consignment_item_logs
#
#  id                 :integer          not null, primary key
#  consignment_log_id :integer
#  price              :float
#  qty                :integer
#  variant_id         :integer
#  per_qty_price      :float
#  return_due         :integer
#  policy             :boolean
#  created_at         :datetime
#  updated_at         :datetime
#  reason             :string(255)
#

class Admin::ConsignmentItemLog < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :consignment_log, class_name: "Admin::ConsignmentLog"
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
end
