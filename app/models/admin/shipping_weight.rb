# == Schema Information
#
# Table name: shipping_weights
#
#  id                           :integer          not null, primary key
#  range                        :string(255)
#  cost                         :float
#  shipping_policy_id           :integer
#  shipping_specific_country_id :integer
#  shipping_specific_pincode_id :integer
#  created_at                   :datetime
#  updated_at                   :datetime
#

class Admin::ShippingWeight < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  
  #ASSOCIATIONS
  belongs_to :shipping_policy
  belongs_to :shipping_specific_country
  belongs_to :shipping_specific_pincode
  #VALIDATIONS 
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
end
