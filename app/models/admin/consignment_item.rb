# == Schema Information
#
# Table name: consignment_items
#
#  id                             :integer          not null, primary key
#  consignment_id                 :integer
#  price                          :float
#  qty                            :integer
#  variant_id                     :integer
#  per_qty_price                  :float
#  created_at                     :datetime
#  updated_at                     :datetime
#  return_due                     :integer
#  policy                         :boolean          default(FALSE)
#  shipping_cost                  :float
#  tax                            :float
#  actual_cost                    :float
#  replacement_revocation_item_id :integer
#

 
class Admin::ConsignmentItem < ActiveRecord::Base
  liquid_methods :price, :qty, :per_qty_price, :order_items
  #GEMS USED
  #ACCESSORS
  attr_accessor :order_item_id,:warehouse_id 
  #ASSOCIATIONS
  has_many :order_items_consignment_items, :dependent => :destroy, class_name: "Admin::OrderItemsConsignmentItems"
  has_many :order_items, through: :order_items_consignment_items, class_name: "Admin::OrderItem"
  has_many :regulations, class_name: "Admin::Regulation"
  belongs_to :consignment, class_name: "Admin::Consignment"
  belongs_to :order, class_name: "Admin::Order"
  belongs_to :warehouse, class_name: "Admin::Warehouse"
  belongs_to :replacement_revocation_item, class_name: "Admin::ReplacementRevocationItem"
  #VALIDATIONS
  #CALLBACKS
  before_save :update_price
  after_save :update_consignment_price
  after_destroy :update_package_task_consignment
  after_destroy :update_consignment_price
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
  def update_price
    self.price= self.per_qty_price * self.qty
  end 

  def update_consignment_price
    total=0
    consignment = Admin::Consignment.find(self.consignment.id)
    consignment.consignment_items.each do |consignment_item|
      total += consignment_item.price 
    end
    consignment.update(:consignment_cost => total)
  end

  def update_package_task_consignment
    consignment = Admin::Consignment.find(self.consignment.id)
    if consignment.consignment_items.first.nil?
      consignment.destroy 
    end
  end

  def check_for_consignment_item_confirmation
    qty_sum = 0 
    if self.regulations.first.present?
      self.regulations.each do |regulation|
        qty_sum += regulation.qty 
      end
      if qty_sum == self.qty
        return true
      else
        return false
      end
    else
      return false
    end 
  end

  def check_availability_for_regulation_create(qty)
    qty_sum = qty 
    if self.regulations.first.present?
      self.regulations.each do |regulation|
        qty_sum += regulation.qty 
      end
      if qty_sum > self.qty
        return false
      else
        return true
      end
    else
      return true
    end
  end


  def check_for_return_initiate
    if ((self.consignment.delivery_date + self.return_due).to_datetime - DateTime.now).to_i > 0
      return true
    else
      return false 
    end  
  end

end
