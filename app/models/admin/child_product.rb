# == Schema Information
#
# Table name: child_products
#
#  id                :integer          not null, primary key
#  product_id        :integer
#  child_category_id :integer
#  created_at        :datetime
#  updated_at        :datetime
#

class Admin::ChildProduct < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :product
  belongs_to :child_category
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
