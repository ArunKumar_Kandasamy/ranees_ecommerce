# == Schema Information
#
# Table name: order_logs
#
#  id                  :integer          not null, primary key
#  updated_by          :integer
#  order_cost          :float
#  order_id            :integer
#  status              :string(255)
#  shipping_address_id :integer
#  pay_val             :boolean
#  cal_conf            :boolean
#  prev_rec            :boolean
#  cancel              :string(255)
#  remarks             :text
#  created_at          :datetime
#  updated_at          :datetime
#

class Admin::OrderLog < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :order_item_logs, class_name: "Admin::OrderItemLog"
  belongs_to :order, class_name: "Admin::Order"
  belongs_to :shipping_address
  #VALIDATIONS
  #CALLBACKS

  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

   def update_order_log_price
    total = 0
    self.order_item_logs.each do |order_item|
      total += order_item.price 
    end
    self.update(:order_cost => total)
  end
  
end
