# == Schema Information
#
# Table name: images
#
#  id                  :integer          not null, primary key
#  variant_id          :integer
#  created_at          :datetime
#  updated_at          :datetime
#  avatar_file_name    :string(255)
#  avatar_content_type :string(255)
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  alt_text            :string(255)
#  general_id          :integer
#  sort_order          :integer
#  type_of_image       :string(255)
#  link_url            :string(255)
#  banner_sort         :integer
#  category_id         :integer
#  parent_image_sort   :integer
#  child_image_sort    :integer
#

class Admin::Image < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_attached_file :avatar, :styles => { :large => "1100x1100!",:medium => "300x300!", :thumb => "100x100!" }, :default_url => "/images/:style/missing.png"
  belongs_to :variant 
  belongs_to :category
  belongs_to :product
  belongs_to :general
  #VALIDATIONS
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS   

  
  def limit_check 
    @admin_categories = Admin::Category.all
    @admin_category = @admin_categories.roots
    @admin_limit = @admin_category.roots.limit(2)
    @compare = @admin_category + @admin_limit
    @admin_par = @compare.detect{ |e| @compare.count(e) > 1 }
  end
  
  #JOBS
  #PRIVATE  
  
end
