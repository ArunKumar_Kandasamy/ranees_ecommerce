# == Schema Information
#
# Table name: pickup_consignment_items
#
#  id                             :integer          not null, primary key
#  pickup_consignment_id          :integer
#  price                          :float
#  qty                            :integer
#  variant_id                     :integer
#  per_qty_price                  :float
#  created_at                     :datetime
#  updated_at                     :datetime
#  reason                         :string(255)
#  refund_revocation_item_id      :integer
#  replacement_revocation_item_id :integer
#  received                       :boolean
#  current_user                   :integer
#

class Admin::PickupConsignmentItem < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  attr_accessor :reason,:consignment_item_id, :compensate,:revocation_mode,:refund_amount,:ship_dt,:tax_dt,:TDR_dt
  #ASSOCIATIONS
  has_many :pickup_consignment_items, class_name: "Admin::PickupConsignmentItem"
  belongs_to :pickup_consignment, class_name: "Admin::PickupConsignment"
  belongs_to :refund_revocation_item, class_name: "Admin::RefundRevocationItem"
  belongs_to :replacement_revocation_item, class_name: "Admin::ReplacementRevocationItem"
  #VALIDATIONS
  #CALLBACKS
  after_save :update_pickup_consignment_price
  before_save :update_price
  after_destroy :update_pickup_consignment_price
  after_destroy :check_pickup_consignment_item_exist
  after_update :pickup_consign_item_recevied
  
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def update_price
    self.price= self.per_qty_price * self.qty
  end  

  def update_pickup_consignment_price
    total=0
    pickup_consignment = Admin::PickupConsignment.find(self.pickup_consignment_id)
    pickup_consignment.pickup_consignment_items.each do |pickup_consignment_item|
      if pickup_consignment_item.received == nil || pickup_consignment_item.received == true
        total += pickup_consignment_item.price 
      end
    end
    pickup_consignment.update(:consignment_cost => total)
  end

  def check_for_consignment_item_confirmation
    qty_sum = 0 
    if self.regulations.first.present?
      self.regulations.each do |regulation|
        qty_sum += regulation.qty 
      end
      if qty_sum == self.qty
        return true
      else
        return false
      end
    else
      return false
    end 
  end

  def check_pickup_consignment_item_exist
    if self.pickup_consignment.pickup_consignment_items.first.nil?
      self.pickup_consignment.destroy
    end
  end

  def pickup_consign_item_recevied  
    if self.received_changed?
      @variant = Admin::Variant.find(self.variant_id)
      @consignment_item = self.pickup_consignment.consignment.consignment_items.where(:variant_id => @variant).first
      @inventory = @consignment_item.regulations.first.inventory
      @selling_price = Admin::Inventory.where(:variant_id => @variant.id).order('selling_price DESC').first.selling_price 
      if self.replacement_revocation_item_id.present?
        @revocation = self.replacement_revocation_item
      elsif self.refund_revocation_item_id.present?
        @revocation = self.refund_revocation_item
      end
      if self.received_was == nil  
        if self.received == true
          @revocation.update(:status => true)
          @new_inventory = Admin::Inventory.create(:expiry_date => @inventory.expiry_date, :purchase_price => @inventory.purchase_price, :vendor_id => @inventory.vendor_id, :product_id => nil, :parent_id => nil, :warehouse_id => self.pickup_consignment.warehouse_id, :qty => 1, :sold_qty => "0", :balance_qty => "0", :updated_through => "Revocation", :user_id => self.current_user, :variant_id => @variant.id, :selling_price => @selling_price, :regulation => "r")
          @new_inventory.save
        else
          @consignment_log = @consignment_item.consignment.create_consignment_log(current_user.id) 
          @consignment_item_log = @consignment_log.consignment_item_logs.where(:variant_id => @consignment_item.variant_id).first
          @consignment_item_log.update(:reason => "pickupconsignment")
          @consignment_item_qty = consignment_item.qty + 1
          @consignment_item.update(:qty => @consignment_item_qty)
          @revocation.update(:status => false)
        end  
      elsif self.received_was == true
        if self.received == false
          @revocation.update(:status => false) 
        end
      elsif self.received_was == false
        if self.received == true 
          @new_inventory = Admin::Inventory.create(:expiry_date => @inventory.expiry_date, :purchase_price => @inventory.purchase_price, :vendor_id => @inventory.vendor_id, :product_id => nil, :parent_id => nil, :warehouse_id => self.pickup_consignmnet.warehouse_id, :qty => 1, :sold_qty => "0", :balance_qty => "0", :updated_through => "Revocation", :user_id => self.current_user, :variant_id => @variant.id, :selling_price => @selling_price, :regulation => "r")
          @revocation.update(:status => true)
          @consignment_log = @consignment_item.consignment.create_consignment_log(current_user.id) 
          @consignment_item_log = @consignment_log.consignment_item_logs.where(:variant_id => @consignment_item.variant_id).first
          @consignment_item_log.update(:reason => "pickupconsignment")
          @consignment_item_qty = consignment_item.qty - 1
          @consignment_item.update(:qty => @consignment_item_qty)
        end
      end   
    end 
  end


end
