# == Schema Information
#
# Table name: ticket_replies
#
#  id             :integer          not null, primary key
#  ticket_id      :integer
#  body           :text
#  from_email     :string(255)
#  to_email       :string(255)
#  ticket_task_id :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class Admin::TicketReply < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :ticket, class_name: "Admin::Ticket"
  belongs_to :ticket_task, class_name: "Admin::TicketTask"
  #VALIDATIONS
  #CALLBACKS

  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
end
