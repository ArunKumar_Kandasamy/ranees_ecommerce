# == Schema Information
#
# Table name: refund_revocations
#
#  id                       :integer          not null, primary key
#  revocation_cost          :float
#  consignment_id           :integer
#  created_by               :integer
#  bank_account_id          :integer
#  revocation_mode          :string(255)
#  status                   :string(255)
#  cancelled_by             :integer
#  created_at               :datetime
#  updated_at               :datetime
#  refund_revocation_number :string(255)
#  transaction_id           :integer
#  trans_status             :boolean          default(FALSE)
#

class Admin::RefundRevocation < ActiveRecord::Base
		#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :refund_revocation_items, class_name: "Admin::RefundRevocationItem"
  has_many :pickup_consignment_items, class_name: "Admin::PickupConsignmentItem"
  belongs_to :consignment
  belongs_to :shipping_address
  belongs_to :user
  #VALIDATIONS
  #CALLBACKS
  #before_update :update_order_price
  before_save :revocation_number_auto_increment
  #after_create :order_item_create
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def revocation_number_auto_increment
    datee = Date.today.to_datetime
    day = datee.mday
    month = datee.mon  
    revocations = Admin::RefundRevocation.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if revocations.present?
      consign_id = revocations.last.refund_revocation_number[-3..-1].to_i
      consign_num = consign_id+=1      
      consign_length = consign_num.to_s.size
      if consign_length == 1
        self.refund_revocation_number = "RFREVOC#{day}#{month}000#{consign_num}"
      elsif consign_length == 2
        self.refund_revocation_number = "RFREVOC#{day}#{month}00#{consign_num}"
      elsif consign_length == 3
        self.refund_revocation_number = "RFREVOC#{day}#{month}0#{consign_num}"
      else
        self.refund_revocation_number = "RFREVOC#{day}#{month}#{consign_num}"    
      end
    else
      self.refund_revocation_number = "RFREVOC#{day}#{month}0001"
    end
  end 

  def update_revocation_status
    @status = true
    self.refund_revocation_items.each do |refund_revocation_item|
      if refund_revocation_item.pickup_consignment_items.first.present?
        if refund_revocation_item.pickup_consignment_items.first.received != true
          @status = false
        end
      end
    end
    if @status == true
      self.status = "pr"
    else
      self.status = "up"
    end
  end

end
