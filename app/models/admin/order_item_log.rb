# == Schema Information
#
# Table name: order_item_logs
#
#  id            :integer          not null, primary key
#  order_log_id  :integer
#  qty           :integer
#  price         :float
#  variant_id    :integer
#  per_qty_price :float
#  created_at    :datetime
#  updated_at    :datetime
#  reason        :string(255)
#

class Admin::OrderItemLog < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :order_log, class_name: "Admin::OrderLog"
  #VALIDATIONS
  #CALLBACKS
  #after_save :update_order_log_price
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  
  def update_order_log_price
    total = 0
    @order_log = self.order_log
    @order_log.order_item_logs.each do |order_item|
      total += order_item.price 
    end
    @order_log.order_cost = total
    @order_log.save
  end



end
