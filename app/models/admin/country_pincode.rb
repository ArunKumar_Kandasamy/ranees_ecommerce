# == Schema Information
#
# Table name: country_pincodes
#
#  id              :integer          not null, primary key
#  plan_country_id :integer
#  pincode         :string(255)
#  availability    :boolean          default(TRUE)
#  boolean         :boolean          default(TRUE)
#  created_at      :datetime
#  updated_at      :datetime
#

class Admin::CountryPincode < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :plan_country 
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
