# == Schema Information
#
# Table name: product_properties
#
#  id                  :integer          not null, primary key
#  product_question_id :integer
#  answer              :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  variant_id          :integer
#

class Admin::ProductProperty < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  attr_accessor :new_product_question, :product_question_tokens
  attr_reader :product_question_tokens
  #ASSOCIATION
  belongs_to :variant
  belongs_to :product_question
  #VALIDATIONS
  #validates :product_question_id, :presence => true
  validates :answer, :presence => true
  #CALLBACKS
  before_save :create_product_question
  #before_save :create_question
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
  def self.tokens(query)
    answers = where("answer ILIKE ?", "%#{query}%")
    if answers.empty?
      [{id: "<<<#{query}>>>", answer: "New: \"#{query}\""}]
    else
      answers
    end
  end

  def self.ids_from_tokens(tokens)  
    tokens.gsub!(/<<<(.+?)>>>/) do 
      @result = Admin::ProductProperty.where("lower(answer) = ?", $1.downcase)
      if @result.present?
        @result.first.id
      else
        create!(answer: $1).id 
      end
    end
    tokens
  end

  def product_question_tokens=(tokens)
    self.product_question_id = Admin::ProductQuestion.ids_from_tokens(tokens)
  end 
  
  def create_product_question 
    @admin_product_question=Admin::ProductQuestion.create(question: new_product_question) if new_product_question.present?
    self.product_question_id = @admin_product_question.id if @admin_product_question.present?
  end
end
