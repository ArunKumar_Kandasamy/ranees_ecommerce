# == Schema Information
#
# Table name: tax_countries
#
#  id          :integer          not null, primary key
#  tax_plan_id :integer
#  country_id  :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Admin::TaxCountry < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :tax_states, :dependent => :destroy
  has_many :states, through: :tax_states 
  belongs_to :tax_plan
  belongs_to :country
  accepts_nested_attributes_for :tax_states, allow_destroy: true
  #VALIDATIONS
  validates :country_id, :presence => true
  validates_uniqueness_of :tax_plan_id, :scope => [:country_id]
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  #JOBS
  #PRIVATE 
end
