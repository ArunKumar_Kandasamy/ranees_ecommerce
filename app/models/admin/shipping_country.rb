# == Schema Information
#
# Table name: shipping_countries
#
#  id               :integer          not null, primary key
#  shipping_plan_id :integer
#  country_id       :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class Admin::ShippingCountry < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :shipping_states, :dependent => :destroy
  has_many :states, through: :shipping_states
  belongs_to :shipping_plan
  belongs_to :country
  #VALIDATIONS
  validates :country_id, :presence => true 
  validates_uniqueness_of :shipping_plan_id, :scope => [:country_id]
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  #JOBS
  #PRIVATE 
end
