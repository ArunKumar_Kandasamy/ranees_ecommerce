# == Schema Information
#
# Table name: order_revocations
#
#  id              :integer          not null, primary key
#  revocation_cost :float
#  order_id        :integer
#  revocation_mode :string(255)
#  status          :boolean
#  bank_account_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#  transaction_id  :integer
#  trans_status    :boolean
#

class Admin::OrderRevocation < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :order_revocation_items,class_name: "Admin::OrderRevocationItem"
  belongs_to :order,class_name: "Admin::Order"
  belongs_to :bank_account,class_name: "BankAccount"
  has_one :bank_account,class_name: "BankAccount", :foreign_key => :bank_account_id
  accepts_nested_attributes_for :bank_account, allow_destroy: true
  has_many :vouchers, class_name: "Admin::Voucher"
  #VALIDATIONS
  #CALLBACKS

  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
 
