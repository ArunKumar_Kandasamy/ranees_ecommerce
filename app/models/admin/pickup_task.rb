# == Schema Information
#
# Table name: pickup_tasks
#
#  id           :integer          not null, primary key
#  task_number  :string(255)
#  warehouse_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class Admin::PickupTask < ActiveRecord::Base
	#GEMS USED
  #ASSOCIATIONS
  has_many :pickup_tasks_pickup_consignments, :dependent => :destroy, class_name: "Admin::PickupTasksPickupConsignments"
  has_many :pickup_consignments, through: :pickup_tasks_pickup_consignments, class_name: "Admin::PickupConsignment"
  has_many :invoices, through: :consignments, class_name: "Admin::Invoice"
  belongs_to :warehouse , class_name: "Admin::Warehouse"
  accepts_nested_attributes_for :pickup_tasks_pickup_consignments, allow_destroy: true 
  accepts_nested_attributes_for :pickup_consignments, allow_destroy: true
  #VALIDATIONS
  #CALLBACKS
  before_create :task_number_auto_increment
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def task_number_auto_increment
    datee = Date.today.to_datetime
    day = datee.mday
    month = datee.mon  
    tasks = Admin::PickupTask.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if tasks.present?
      task_id = tasks.last.task_number[-3..-1]
      task_increment = task_id.to_i
      task_num = task_increment + 1     
      task_length = task_num.to_s.size
      if task_length == 1
        self.task_number = "PTASK#{day}#{month}000#{task_num}"
      elsif task_length == 2
        self.task_number = "PTASK#{day}#{month}00#{task_num}"
      elsif task_length == 3
        self.task_number = "PTASK#{day}#{month}0#{task_num}"
      else
        self.task_number = "PTASK#{day}#{month}#{task_num}"    
      end
    else
      self.task_number = "PTASK#{day}#{month}0001"
    end
  end
end
