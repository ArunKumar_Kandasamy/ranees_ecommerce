# == Schema Information
#
# Table name: rejection_reasons
#
#  id         :integer          not null, primary key
#  reason     :string(255)
#  created_at :datetime
#  updated_at :datetime
#  attr_value :string(255)
#  order_id   :integer
#

class Admin::RejectionReason < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :order, class_name: "Admin::Order"
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
