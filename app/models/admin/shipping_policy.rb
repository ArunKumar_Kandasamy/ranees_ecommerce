# == Schema Information
#
# Table name: shipping_policies
#
#  id          :integer          not null, primary key
#  cost        :float
#  created_at  :datetime
#  updated_at  :datetime
#  all_country :boolean          default(TRUE)
#  policy      :string(255)      default("free")
#

class Admin::ShippingPolicy < ActiveRecord::Base 
	#GEMS USED
  #ACCESSORS
  
  #ASSOCIATIONS
  has_many :shipping_specific_countries, class_name: "Admin::ShippingSpecificCountry"
  has_many :shipping_weights, class_name: "Admin::ShippingWeight"
  has_many :shipping_prices, class_name: "Admin::ShippingPrice"
  accepts_nested_attributes_for :shipping_specific_countries, allow_destroy: true
  accepts_nested_attributes_for :shipping_weights, allow_destroy: true, update_only: true
  accepts_nested_attributes_for :shipping_prices, allow_destroy: true, update_only: true
  #VALIDATIONS 
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE

  
end
