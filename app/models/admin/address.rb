# == Schema Information
#
# Table name: addresses
#
#  id              :integer          not null, primary key
#  add_line1       :string(255)
#  add_line2       :string(255)
#  state           :string(255)
#  country         :string(255)
#  c_code          :string(255)
#  phone           :string(255)
#  connection_id   :integer
#  connection_type :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class Admin::Address < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  
  #ASSOCIATIONS
  belongs_to :connection, polymorphic: true
  #VALIDATIONS
  validates :add_line1, :presence => true
  validates :state, :presence => true
  validates :country, :presence => true
  validates :c_code, :presence => true
  validates :phone, :presence => true
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end