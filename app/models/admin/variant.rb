# == Schema Information
#
# Table name: variants
#
#  id            :integer          not null, primary key
#  product_id    :integer
#  sku           :string(255)
#  visible       :boolean          default(FALSE)
#  name          :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  bar_code      :string(255)
#  title         :string(255)
#  meta_desc     :string(255)
#  status        :boolean
#  sort_order    :integer
#  ready_to_wear :boolean          default(FALSE)
#  strike_rate   :float
#  weight        :float
#

 
class Admin::Variant < ActiveRecord::Base        
  liquid_methods :name    
  #GEMS USED 
  #ACCESSORS 
  #ASSOCIATIONS     
  has_many :cart_items, class_name: "CartItem"
  belongs_to :product
  has_many :inventories, :dependent => :destroy
  has_many :product_properties, :dependent => :destroy
  has_many :product_questions, through: :product_properties 
  has_many :images, :dependent => :destroy
  accepts_nested_attributes_for :images, allow_destroy: true
  accepts_nested_attributes_for :product_properties
  #VALIDATIONS
  validates :name, :presence => true
  validates :sku, :presence => true
  validates :status, :default => false
  #CALLBACKS
  before_create :update_sort_order
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def update_sort_order
    @sort_maximum= Admin::Variant.maximum("sort_order") ? Admin::Variant.maximum("sort_order") : 0
    @sort_maximum+=1
    self.sort_order=@sort_maximum
  end

  def order_shipping_price 
    @shipping_policy = Admin::ShippingPolicy.first
    if @shipping_policy.all_Country
      return false
    else
      return @shipping_policy.shipping_specific_countries
    end
  end


  def activate_variant 
    @flag = 1
    if self.product.based_on == "w"
      @flag = self.weight.present? ? 1 : 0  
    end
    @inventries = self.inventories.where('selling_price != ?',0).where('expiry_date > ?',DateTime.now)
    if self.visible && @flag == 1 && @inventries.length > 0
      self.status = true
      self.save
      return true
    else
      self.status = false
      self.save
      return false
    end
  end

  def find_variant_availability(country_id = '' ,pincode = '')
    @product = self.product
    if @product.availability_plan.present?
      if @product.availability_plan.all_country
        return "Available"
      else
        if !country_id.present?
          return @product.availability_plan.list_countries
        else
          @availability_country = @product.availability_plan.list_countries.where(:country_id => country_id).first
          if @availability_country.all_place
            return "Available at #{ @availability_country.country.name }"
          else
            if !pincode.present?
              return @availability_country.country_pincodes
            else
              @country_pincode = @availability_country.country_pincodes.where(:pincode => pincode).first
              if @country_pincode.availability 
                return "Available at #{ pincode }"
              else
                return "unavailable"
              end
            end
          end
        end
      end
    end
  end


  def find_product_shipping_cost(country_id = '' ,pincode = '')
    @product = self.product
    @shipping_policy = Admin::ShippingPolicy.first
    if @product.availability_plan.present?
      if @product.availability_plan.all_country
        self.check_shipping_policy_with_availability(country_id,pincode)
      else
        if !country_id.present?
          return @product.availability_plan.list_countries
        else
          @availability_country = @product.availability_plan.list_countries.where(:country_id => country_id).first
          if @availability_country.all_place
            self.check_shipping_policy_with_availability(country_id,pincode)
          else
            if !pincode.present?
              return @availability_country.country_pincodes
            else
              @country_pincode = @availability_country.country_pincodes.where(:pincode => pincode).first
              if @country_pincode.availability 
                self.check_shipping_policy_with_availability(country_id,pincode)
              else
                return "unavailable"
              end
            end
          end
        end
      end
    end
  end

  def check_shipping_policy_with_availability(country_id = '' ,pincode = '')
    @product = self.product
    @shipping_policy = Admin::ShippingPolicy.first
    if @product.free_shipping
      return 0
    else
      if @shipping_policy.all_country
        return self.find_shipping_range(@shipping_policy,false)
      else
        if !country_id.present?
          return @shipping_policy.shipping_specific_countries
        else
          @shipping_country = @shipping_policy.shipping_specific_countries.where(:country_id => country_id).first
          if @shipping_country.all_place
            return self.find_shipping_range(@shipping_country,false)
          else
            if !pincode.present?
              return @shipping_country.shipping_specific_pincodes
            else
              return self.availability_shipping_cost_with_pincode(pincode)
            end
          end
        end
      end
    end
  end

  def availability_shipping_cost_with_country(country_id)
    @product = self.product
    @shipping_policy = Admin::ShippingPolicy.first
    if @product.availability_plan.all_Country
      if @shipping_policy.all_country
        return self.find_shipping_range(@shipping_policy,false)
      else
        @shipping_country = @shipping_policy.shipping_countries.where(:country_id => country_id).first
        if @shipping_country.all_place
          return self.find_shipping_range(@shipping_country,false)
        else
          return @shipping_country.shipping_specific_pincodes
        end
      end
    else
      @availability_country = @product.availability_plan.list_countries.where(:country_id => country_id).first
      if @availability_country.all_place
        return self.find_shipping_range(@shipping_country,false)
      else
        return @availability_country.country_pincodes
      end
    end
  end

  def availability_shipping_cost_with_pincode(pincode)
    @pincode = Admin::ShippingSpecificPincode.where(:pincode => pincode).first
    if @pincode.present?
      return self.find_shipping_range(@pincode,true)
    else
      return "unavailable"
    end
  end

  def find_shipping_range(associated_plan,pincode)
    @product = self.product
    unless pincode
      if associated_plan.policy == "free"
        return 0
      elsif associated_plan.policy == "flat"
        return @shipping_policy.cost
      else 
        if @product.based_on == "w"
          associated_plan.shipping_weights.each do |weight|
            if self.weight.between?(weight.range.split("-")[0].to_i,weight.range.split("-")[1].to_i) 
              return weight.cost
            end
          end
        else
          associated_plan.shipping_prices.each do |price|
            if self.inventories.first.selling_price.between?(price.range.split("-")[0].to_i,price.range.split("-")[1].to_i) 
              return price.cost
            end
          end
        end
      end
    else
      if @product.based_on == "w"
        associated_plan.shipping_weights.each do |weight|
          if self.weight.between?(weight.range.split("-")[0].to_i,weight.range.split("-")[1].to_i) 
            return weight.cost
          end
        end
      else
        associated_plan.shipping_prices.each do |price|
          if self.inventories.first.selling_price.between?(price.range.split("-")[0].to_i,price.range.split("-")[1].to_i) 
            return price.cost
          end
        end
      end
    end
  end
  
  def check_for_status
    if (self.visible == true)
      @inventories= Admin::Inventory.where(:variant_id => self.id)
      @variant= Admin::Variant.where(:id => self.id)
      @product_id
      @inventories.each do | inventory |
        if inventory.expiry_date > DateTime.now
          if (inventory.selling_price !=0)
            self.status= true
            self.save
            return true
          else
            return false
          end
        end
      end
    end
  end

  def find_balance_qty
    bal_qty = 0 
    bal_qty= warehouse_reduction_qty
    order_qty = 0 
    replacement_qty = 0 
    orders = Admin::Order.where(:status => "up")
    orders.each do |order|
      order_item = order.order_items.where(:variant_id => self.id).first
      if order_item.present?
        order_qty= order_item.qty
      end
    end
    replacement_revocations = Admin::ReplacementRevocation.where(:status => "pr")
    replacement_revocations.each do |replacement_revocation|
      replacement_revocation_item = replacement_revocation.replacement_revocation_items.where(:variant_id => self.id).first
      if replacement_revocation_item.present?
        replacement_qty= replacement_revocation_item.qty
      end
    end
    bala_qty= bal_qty - order_qty - replacement_qty
    return bala_qty.to_s
  end

  def warehouse_reduction_qty
    wh_qty=0
    order_qty=0
    pwh_replacement_qty = 0
    to_count_qty=0
    consignment_item_qty = 0 
    @total_inventory_qty = 0
    replacement_consignment_item_qty = 0 
    wh_orders = Admin::Order.where(:status => "wh")
    wh_orders.each do |wh_order|
      wh_order.consignments.each do | consignment|
        if consignment.status !="conform"
          consignment.consignment_items.each do | consignment_item|
            if consignment_item.variant_id == self.id
              consignment_item_qty += consignment_item.qty
            end
          end
        end
      end
    end
    wh_replacements = Admin::ReplacementRevocation.where(:status => "wh")
    wh_replacements.each do |wh_replacement|
      wh_replacement.consignments.each do | consignment|
        if consignment.status !="conform"
          consignment.consignment_items.each do | consignment_item|
            if consignment_item.variant_id == self.id
              replacement_consignment_item_qty += consignment_item.qty
            end
          end
        end
      end
    end

    @inventory_qty = Admin::Inventory.select("variant_id,sum(balance_qty) as balance_qtys").where(:variant_id => self.id).where('balance_qty != ?','0').group('variant_id','inventories.id')
    @inventory_qty.each do |inventory_qty|
      @total_inventory_qty += inventory_qty.balance_qtys
    end
    wh_qty = @total_inventory_qty - consignment_item_qty - replacement_consignment_item_qty
    pwh_orders = Admin::Order.where(:status => "pwh")
    pwh_orders.each do | pwh_order|
    order_item = pwh_order.order_items.where(:variant_id => self.id).first 
      if order_item.present?
        order_item.consignment_items.each do | consignment_item |
          to_count_qty+=consignment_item.qty
        end
        if order_item.qty == to_count_qty
          order_qty = 0
        else
          order_qty += order_item.qty - to_count_qty
        end
      end
    end
    pwh_replacements = Admin::ReplacementRevocation.where(:status => "pwh")
    pwh_replacements.each do | pwh_replacement|
      pwh_replacement_item = pwh_replacement.replacement_revocation_items.where(:variant_id => self.id).first 
      if pwh_replacement_item.present?
        pwh_replacement_item.consignment_items.each do | consignment_item |
          to_count_qty1 +=consignment_item.qty
        end
        if pwh_replacement_item.qty == to_count_qty1
          pwh_replacement_qty = 0
        else
          pwh_replacement_qty += pwh_replacement_item.qty - to_count_qty1
        end
      end
    end
    return wh_qty - order_qty - pwh_replacement_qty
  end

  def need_pincode_for_tax
    if self.product.tax_violate
      return false
    else
        self.product.tax_plans.each do |tax_plan|
           if !tax_plan.all_country == true
              return false
           end
        end
    end
    return true
  end

  def find_variant_tax_cost(qty)
    if self.product.tax_violate
     return 0 
    else
        self.product.tax_plans.each do |tax_plan|
           if tax_plan.all_country == true
             @inventory= Admin::Inventory.where(:variant_id => self.id).where('balance_qty != ?', 0).order('selling_price DESC').first
             @selling_price= @inventory.selling_price
             @tax_name=tax_plan.formula
             @tax_value= tax_plan.all_country_tax * qty
             case @tax_name
               when "VAT"
                  @VAT_value= VAT_calc(@selling_price, @tax_value)
               when "X"
                  @X_value=x_calc(@selling_price, @tax_value)
             end
           end
        end
        return @total_tax_value = @VAT_value.to_f + @X_value.to_f
    end
  end

  def variant_tax_cost(pin,qty)
    if self.product.tax_violate
       return 0
    else
      @pincode= Admin::Pincode.where(:pincode => pin).first
      @shipping_cost = ""
      @district= Admin::DistrictCity.where(:id => @pincode.district_city_id).first
      self.product.tax_plans.each do |tax_plan|
        @inventory= Admin::Inventory.where(:variant_id => self.id).where('balance_qty != ?', 0).order('selling_price DESC').first
        @selling_price= @inventory.selling_price
        @tax_name=tax_plan.formula
          if tax_plan.all_country == true
             @tax_value= tax_plan.all_country_tax * qty
             case @tax_name
               when "VAT"
                  @VAT_value= VAT_calc(@selling_price, @tax_value)
               when "X"
                  @X_value=x_calc(@selling_price, @tax_value)
             end
           else
              @tax_country = tax_plan.tax_countries.where(:country_id => @district.state.country_id)
               if @tax_country.present?
                 @tax_states = @tax_country.first.tax_states.where(:state_id => @district.state_id)
                 if  @tax_states.present?
                   @tax_value= @tax_states.first.tax * qty
                 else
                   @tax_value= tax_plan.rest_of_the_country_tax * qty
                 end
               else
                  @tax_value= tax_plan.rest_of_the_country_tax * qty
               end
            case @tax_name
                when "VAT"
                    @VAT_value= VAT_calc(@selling_price, @tax_value)
                when "X"
                      @X_value=x_calc(@selling_price, @tax_value)
                end
          end
      end
      @total_tax_value = @VAT_value.to_f + @X_value.to_f
     end
  end
 
  
  
  def other_variants(order,order_item)
    pincode = order.shipping_address.pincode
    @product = self.product
    @shipping = self.find_variant_availability(order.shipping_address.country,order.shipping_address.pincode)
    @other_variants = Array.new
    if @shipping == "unavailable"
      if order.payment_mode == "COD"
        if @product.check_cod(pincode)
          @product.variants.each do |variant|
            if order_item.bal_qty(variant) >= order_item.qty
              @other_variants << variant
            end
          end
          return @other_variants
        else
          return self
        end
      else
        @product.variants.each do |variant|
          if Admin::Inventory.where(:variant_id => variant.id).order('selling_price DESC').first.selling_price == order_item.per_qty_price && order_item.bal_qty(variant) >= order_item.qty
            @other_variants << variant
          end
        end
        return @other_variants
      end
    else
      return self
    end
  end
  
  def VAT_calc(s_price, vat)
    @total_price= s_price * vat /100
  end

  def x_calc( s_price, vat)
    price= s_price + vat 
  end
  #JOBS
  #PRIVATE 
end
