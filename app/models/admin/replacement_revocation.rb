# == Schema Information
#
# Table name: replacement_revocations
#
#  id                            :integer          not null, primary key
#  revocation_cost               :float
#  consignment_id                :integer
#  created_by                    :integer
#  shipping_address_id           :integer
#  replacement_revocation_number :string(255)
#  status                        :string(255)
#  cancelled_by                  :integer
#  created_at                    :datetime
#  updated_at                    :datetime
#

class Admin::ReplacementRevocation < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :replacement_revocation_items, class_name: "Admin::ReplacementRevocationItem"
  has_many :pickup_consignment_items, class_name: "Admin::PickupConsignmentItem"
  belongs_to :consignment
  has_many :consignments
  belongs_to :shipping_address
  belongs_to :user
  #VALIDATIONS
  #CALLBACKS
  #before_update :update_order_price
  before_save :revocation_number_auto_increment
  #after_create :order_item_create
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def revocation_number_auto_increment
    datee = Date.today.to_datetime
    day = datee.mday
    daily = day.to_s.size
    if daily == 1
      day = "0#{day}"
    else
      day = day
    end
    month = datee.mon  
    revocations = Admin::ReplacementRevocation.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if revocations.present?
      consign_id = revocations.last.replacement_revocation_number[-3..-1].to_i
      consign_num = consign_id+=1      
      consign_length = consign_num.to_s.size
      if consign_length == 1
        self.replacement_revocation_number = "RPREVOC#{day}#{month}000#{consign_num}"
      elsif consign_length == 2
        self.replacement_revocation_number = "RPREVOC#{day}#{month}00#{consign_num}"
      elsif consign_length == 3
        self.replacement_revocation_number = "RPREVOC#{day}#{month}0#{consign_num}"
      else
        self.replacement_revocation_number = "RPREVOC#{day}#{month}#{consign_num}"    
      end
    else
      self.replacement_revocation_number = "RPREVOC#{day}#{month}0001"
    end
  end 
end
