# == Schema Information
#
# Table name: replacement_revocation_items
#
#  id                        :integer          not null, primary key
#  replacement_revocation_id :integer
#  price                     :float
#  qty                       :integer
#  variant_id                :integer
#  per_qty_price             :float
#  actual_cost               :float
#  shipping_cost             :float
#  tax                       :float
#  reason                    :string(255)
#  status                    :boolean
#  created_at                :datetime
#  updated_at                :datetime
#

class Admin::ReplacementRevocationItem < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :pickup_consignment_items, class_name: "Admin::PickupConsignmentItem"
  has_many :consignment_items, class_name: "Admin::ConsignmentItem"
  belongs_to :consignment, class_name: "Admin::Consignment"
  belongs_to :replacement_revocation, class_name: "Admin::ReplacementRevocation"
  #VALIDATIONS
  #CALLBACKS
  before_save :update_price
  after_save :update_replacement_revocation_price
  after_save :update_replacement_revocation_status
  after_destroy :check_replacement_revocation_item_exist
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def update_price
    self.price= self.per_qty_price * self.qty
  end  

  def update_replacement_revocation_price
    total=0
    replacement_revocation = Admin::ReplacementRevocation.find(self.replacement_revocation_id)
    replacement_revocation.replacement_revocation_items.each do |replacement_revocation_item|
      total += replacement_revocation_item.price 
    end
    replacement_revocation.update(:revocation_cost => total)
  end

  def update_replacement_revocation_status
    @status = true
    replacement_revocation = Admin::ReplacementRevocation.find(self.replacement_revocation_id)
    replacement_revocation.replacement_revocation_items.each do |replacement_revocation_item|
      if replacement_revocation_item.status != true 
        @status = false
      end
    end
    if @status == true
      replacement_revocation.update(:status => "pr")
    else
      replacement_revocation.update(:status => "up")
    end
  end

  def check_replacement_revocation_item_exist
    if self.replacement_revocation.replacement_revocation_items.first.nil?
      self.replacement_revocation.destroy
    end
  end

end
