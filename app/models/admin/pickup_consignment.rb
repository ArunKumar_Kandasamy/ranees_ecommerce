# == Schema Information
#
# Table name: pickup_consignments
#
#  id                        :integer          not null, primary key
#  pickup_consignment_number :string(255)
#  consignment_cost          :float
#  consignment_id            :integer
#  logistic_id               :integer
#  warehouse_id              :integer
#  status                    :string(255)
#  shipping_address_id       :integer
#  created_by                :integer
#  received                  :boolean
#  created_at                :datetime
#  updated_at                :datetime
#  compensate                :string(255)
#

class Admin::PickupConsignment < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  attr_accessor :compensate,:revocation_mode,:bank_account_id
  #ASSOCIATIONS
  has_many :pickup_consignment_items, class_name: "Admin::PickupConsignmentItem"
  has_many :pickup_tasks_pickup_consignments, :dependent => :destroy, class_name: "Admin::PickupTasksPickupConsignments"
  has_many :pickup_tasks, through: :pickup_tasks_pickup_consignments, class_name: "Admin::PickupTask"
  has_many :pickup_consignment_invoices, class_name: "Admin::PickupConsignmentInvoice"
  belongs_to :consignment
  belongs_to :shipping_address
  belongs_to :user
  belongs_to :logistic, class_name: "Admin::Logistic"
  belongs_to :warehouse, class_name: "Admin::Warehouse"
  accepts_nested_attributes_for :pickup_consignment_items, allow_destroy: true
  #VALIDATIONS
  #CALLBACKS
  #before_update :update_order_price
  before_save :consignment_number_auto_increment
  #after_create :order_item_create
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def consignment_number_auto_increment
    datee = Date.today.to_datetime
    day = datee.mday
    month = datee.mon  
    consignments = Admin::PickupConsignment.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if consignments.present?
      consign_id = consignments.last.pickup_consignment_number[-3..-1].to_i
      consign_num = consign_id+=1      
      consign_length = consign_num.to_s.size
      if consign_length == 1
        self.pickup_consignment_number = "CONNO#{day}#{month}000#{consign_num}"
      elsif consign_length == 2
        self.pickup_consignment_number = "CONNO#{day}#{month}00#{consign_num}"
      elsif consign_length == 3
        self.pickup_consignment_number = "CONNO#{day}#{month}0#{consign_num}"
      else
        self.pickup_consignment_number = "CONNO#{day}#{month}#{consign_num}"    
      end
    else
      self.pickup_consignment_number = "CONNO#{day}#{month}0001"
    end
  end 

  def check_for_delivery_status_visibility
    @status = false
    if self.logistic_id.present?
      @status = true
    end
    return @status
  end

  def check_for_consignment_confirmation
    @status = true
    self.pickup_consignment_items.each do |pickup_consignment_item|
      if pickup_consignment_item.check_for_consignment_item_confirmation == false
        @status = false
      end
    end
    return @status
  end

end
