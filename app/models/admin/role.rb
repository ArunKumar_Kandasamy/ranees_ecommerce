# == Schema Information
#
# Table name: roles
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class Admin::Role < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :block_lists, class_name: "Admin::BlockList"
  has_many :warehouses_roles, class_name: "Admin::WarehousesRoles"
  has_many :warehouses, through: :warehouses_roles
  belongs_to :user
  has_many :permissions, class_name: "Admin::Permission"
  accepts_nested_attributes_for :block_lists, allow_destroy: true,:reject_if => lambda { |a| a[:con_name].blank? }
  #VALIDATIONS
  validates :name, :presence => true, uniqueness: { case_sensitive: false }
  validates :description, :presence => true
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
end
 
