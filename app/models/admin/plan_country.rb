# == Schema Information
#
# Table name: plan_countries
#
#  id                   :integer          not null, primary key
#  availability_plan_id :integer
#  country_id           :integer
#  all_place            :boolean          default(TRUE)
#  boolean              :boolean          default(TRUE)
#  created_at           :datetime
#  updated_at           :datetime
#

class Admin::PlanCountry < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :country_pincodes, :dependent => :destroy, class_name: "Admin::CountryPincode"
  belongs_to :country, class_name: "Admin::Country"
  belongs_to :availability_plan
  accepts_nested_attributes_for :country_pincodes, allow_destroy: true
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 

  def country_name
    "#{country.name}"
  end

end
