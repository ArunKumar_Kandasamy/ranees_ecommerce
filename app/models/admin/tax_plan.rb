# == Schema Information
#
# Table name: tax_plans
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  formula    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Admin::TaxPlan < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :products_tax_plans, :dependent => :destroy,  class_name: "Admin::ProductsTaxPlans"
  has_many :products, through: :products_tax_plans
  has_many :tax_countries
  has_many :countries, through: :tax_countries
  #VALIDATIONS
  validates :name, :presence => true
  validates :formula, :presence => true
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  #JOBS
  #PRIVATE 
  private

  def self.ransackable_attributes(auth_object = nil)
    ['name','formula']
  end
end
