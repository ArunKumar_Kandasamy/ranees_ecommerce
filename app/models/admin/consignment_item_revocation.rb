# == Schema Information
#
# Table name: consignment_item_revocations
#
#  id                        :integer          not null, primary key
#  consignment_revocation_id :integer
#  price                     :float
#  qty                       :integer
#  variant_id                :integer
#  per_qty_price             :float
#  compensate                :string(255)
#  revocation_mode           :string(255)
#  bank_account_id           :integer
#  status                    :boolean
#  created_at                :datetime
#  updated_at                :datetime
#  reason                    :string(255)
#  refund_amount             :float
#

class Admin::ConsignmentItemRevocation < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :consignment_revocation,class_name: "Admin::ConsignmentRevocation"
  belongs_to :consignment ,class_name: "Admin::Consignment"
  #VALIDATIONS
  #CALLBACKS
  after_save :update_revocation_price
  before_save :update_price
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 

  def update_price
    self.price= self.per_qty_price * self.qty
  end  

  def update_revocation_price
    total=0
    consignment_revocation = Admin::ConsignmentRevocation.find(self.consignment_revocation_id)
    consignment_revocation.consignment_item_revocations.each do |consignment_item_revocation|
      total += consignment_item_revocation.price 
    end
    consignment_revocation.update(:revocation_cost => total)
  end

end
