# == Schema Information
#
# Table name: logistics
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  site       :string(255)
#  status     :boolean          default(FALSE)
#  created_at :datetime
#  updated_at :datetime
#

class Admin::Logistic < ActiveRecord::Base
  liquid_methods :name
  has_many :consignments, class_name: "Admin::Consignment"
  has_many :pickup_consignments, class_name: "Admin::PickupConsignment"
end
