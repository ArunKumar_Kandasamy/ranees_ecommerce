# == Schema Information
#
# Table name: order_revocation_items
#
#  id                  :integer          not null, primary key
#  price               :float
#  order_revocation_id :integer
#  qty                 :integer
#  variant_id          :integer
#  per_qty_price       :float
#  created_at          :datetime
#  updated_at          :datetime
#

class Admin::OrderRevocationItem < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :order_revocation,class_name: "Admin::OrderRevocation"
  belongs_to :order ,class_name: "Admin::Order"
  #VALIDATIONS
  #CALLBACKS

  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
