# == Schema Information
#
# Table name: pages
#
#  id           :integer          not null, primary key
#  page_name    :string(255)
#  page_type    :string(255)
#  page_link    :string(255)
#  page_content :text
#  created_at   :datetime
#  updated_at   :datetime
#  active       :boolean          default(FALSE)
#

class Admin::Page < ActiveRecord::Base
  extend FriendlyId
  friendly_id :page_name
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  #VALIDATIONS
  validates :page_name, :presence => true, :uniqueness => true
  validates :page_name, :presence => true
  #CALLBACKS
  before_save :downcase_fields
  before_save :link_validation
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
  def downcase_fields
    self.page_name.downcase!
  end

  def link_validation
    if self.page_link.match("https://").present?
    elsif self.page_link.match("http://").present?
    else
      self.page_link = "http://" + self.page_link
    end
  end
end
