# == Schema Information
#
# Table name: tax_states
#
#  id             :integer          not null, primary key
#  tax_country_id :integer
#  state_id       :integer
#  created_at     :datetime
#  updated_at     :datetime
#  tax            :float
#  status         :boolean
#

class Admin::TaxState < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :tax_values, :dependent => :destroy
  belongs_to :tax_country
  belongs_to :state
  #VALIDATIONS
  validates :state_id, :presence => true
  validates_uniqueness_of :tax_country_id, :scope => [:state_id]
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  #JOBS
  #PRIVATE 
end
