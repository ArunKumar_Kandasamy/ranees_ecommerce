# == Schema Information
#
# Table name: product_types
#
#  id         :integer          not null, primary key
#  product_id :integer
#  type_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Admin::ProductType < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  attr_accessor :new_product_type ,:type_tokens
  #ASSOCIATIONS
  belongs_to :product
  belongs_to :type
  #VALIDATIONS
  validates_uniqueness_of :product_id, :scope => [:type_id]
  #CALLBACKS
  before_save :create_type
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  #JOBS
  #PRIVATE
  def create_type
    @admin_type = Admin::Type.create(name: new_product_type) if new_product_type.present?
    self.type_id =@admin_type.id if @admin_type.present?
  end

  def self.ids_from_tokens(tokens)
    Admin::Type.ids_from_tokens(tokens)
  end

end
