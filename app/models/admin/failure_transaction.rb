# == Schema Information
#
# Table name: failure_transactions
#
#  id           :integer          not null, primary key
#  txnid        :string(255)
#  net_amount   :float
#  user_id      :integer
#  mode         :string(255)
#  status       :string(255)
#  addedon      :datetime
#  productinfo  :string(255)
#  name_on_card :string(255)
#  cardnum      :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#

class Admin::FailureTransaction < ActiveRecord::Base
  #GEMS USED
  #ASSOCIATIONS
  belongs_to :user
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
end
