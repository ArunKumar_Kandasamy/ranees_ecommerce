  # == Schema Information
#
# Table name: shipping_cities
#
#  id                :integer          not null, primary key
#  shipping_state_id :integer
#  district_city_id  :integer
#  created_at        :datetime
#  updated_at        :datetime
#  cost              :float
#  status            :boolean          default(FALSE)
#

class Admin::ShippingCity < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :shipping_costs, :dependent => :destroy
  has_many :pincodes, through: :shipping_costs
  belongs_to :shipping_state
  belongs_to :district_city
  accepts_nested_attributes_for :shipping_costs, allow_destroy: true
  #VALIDATIONS
  validates :district_city_id, :presence => true
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
 
 
