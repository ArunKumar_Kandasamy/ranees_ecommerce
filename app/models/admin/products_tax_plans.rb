# == Schema Information
#
# Table name: products_tax_plans
#
#  id          :integer          not null, primary key
#  product_id  :integer
#  tax_plan_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Admin::ProductsTaxPlans < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :product, class_name: "Admin::Product"
  belongs_to :tax_plan, class_name: "Admin::TaxPlan"
  #VALIDATIONS
  validates_uniqueness_of :product_id, :scope => [:tax_plan_id]
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  #JOBS
  #PRIVATE
	
end
