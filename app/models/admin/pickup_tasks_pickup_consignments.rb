# == Schema Information
#
# Table name: pickup_tasks_pickup_consignments
#
#  id                    :integer          not null, primary key
#  pickup_task_id        :integer
#  pickup_consignment_id :integer
#  created_at            :datetime
#  updated_at            :datetime
#

class Admin::PickupTasksPickupConsignments < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  attr_accessor :received
  #ASSOCIATIONS
  belongs_to :pickup_consignment, class_name: "Admin::PickupConsignment"
  belongs_to :pickup_task, class_name: "Admin::PickupTask"
  #VALIDATIONS
  validates_uniqueness_of :pickup_consignment_id, :scope => [:pickup_task_id]
  #CALLBACKS
  after_destroy :update_pickuptask
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  
  def update_pickuptask
    if self.pickup_task.pickup_consignments.first.nil?
      self.pickup_task.destroy
    end
  end
  #JOBS
  #PRIVATE
end
