# == Schema Information
#
# Table name: availability_plans
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  all_country :boolean          default(TRUE)
#  boolean     :boolean          default(TRUE)
#  created_at  :datetime
#  updated_at  :datetime
#  status      :boolean
#

 class Admin::AvailabilityPlan < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :plan_countries, :dependent => :destroy, class_name: "Admin::PlanCountry"
  has_many :products,class_name: "Admin::Products"
  accepts_nested_attributes_for :plan_countries, allow_destroy: true
  #VALIDATIONS
  validates :name, :presence => true
  #CALLBACKS
  before_update :update_plan_status
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE

  def update_plan_status
    if self.plan_countries.first.present?
      self.status = true
    else
      self.status = false
    end
  end 

  def list_countries
    @countries = self.plan_countries
    @shipping_policy = Admin::ShippingPolicy.first
    if @shipping_policy.all_country
      return @countries
    else
      @filter_availability_countries = self.plan_countries.map{|x| x.country_id}
      @existing_countries = @shipping_policy.shipping_specific_countries.map{|x| x.country_id}
      @filter_countries = @filter_availability_countries & @existing_countries
      @country_list = self.plan_countries.where(:country_id => @filter_countries)
      return @country_list
    end
  end 

end
