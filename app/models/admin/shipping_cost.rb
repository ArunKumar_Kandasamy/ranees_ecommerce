# == Schema Information
#
# Table name: shipping_costs
#
#  id               :integer          not null, primary key
#  shipping_city_id :integer
#  cost             :float
#  created_at       :datetime
#  updated_at       :datetime
#  pincode_id       :integer
#  status           :boolean          default(FALSE)
#  cod              :boolean          default(FALSE)
#

class Admin::ShippingCost < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :shipping_city
  belongs_to :pincode
  #VALIDATIONS
  validates_uniqueness_of :shipping_city_id, :scope => [:pincode_id]
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  #JOBS
  #PRIVATE 
end
