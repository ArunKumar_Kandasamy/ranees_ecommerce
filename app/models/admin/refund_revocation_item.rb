# == Schema Information
#
# Table name: refund_revocation_items
#
#  id                   :integer          not null, primary key
#  refund_revocation_id :integer
#  price                :float
#  qty                  :integer
#  variant_id           :integer
#  per_qty_price        :float
#  actual_cost          :float
#  reason               :string(255)
#  refund_amount        :float
#  ship_dt              :boolean
#  tax_dt               :boolean
#  TDR_dt               :boolean
#  status               :boolean
#  created_at           :datetime
#  updated_at           :datetime
#  shipping_cost        :float
#  tax                  :float
#

class Admin::RefundRevocationItem < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :pickup_consignment_items, class_name: "Admin::PickupConsignmentItem"
  belongs_to :consignment, class_name: "Admin::Consignment"
  belongs_to :refund_revocation, class_name: "Admin::RefundRevocation"
  #VALIDATIONS
  #CALLBACKS
  after_save :update_refund_revocation_price
  after_save :update_refund_revocation_status
  before_save :update_price
  after_destroy :update_refund_revocation_price
  after_destroy :check_refund_revocation_item_exist
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def update_price
    self.price= self.per_qty_price * self.qty
  end  

  def update_refund_revocation_price
    total=0
    refund_revocation = Admin::RefundRevocation.find(self.refund_revocation_id)
    refund_revocation.refund_revocation_items.each do |refund_revocation_item|
      total += refund_revocation_item.price 
    end
    refund_revocation.update(:revocation_cost => total)
  end

  def check_refund_revocation_item_exist
    if self.refund_revocation.refund_revocation_items.first.nil?
      self.refund_revocation.destroy
    end
  end

  def update_refund_revocation_status
    @status = true
    refund_revocation = Admin::RefundRevocation.find(self.refund_revocation_id)
    refund_revocation.refund_revocation_items.each do |refund_revocation_item|
      if refund_revocation_item.status != true 
        @status = false
      end
    end
    if @status == true
      refund_revocation.update(:status => "pr")
    else
      refund_revocation.update(:status => "up")
    end
  end

end
