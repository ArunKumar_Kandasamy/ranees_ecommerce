# == Schema Information
#
# Table name: cod_policies
#
#  id               :integer          not null, primary key
#  cod_availability :boolean          default(TRUE)
#  all_country      :boolean          default(TRUE)
#  created_at       :datetime
#  updated_at       :datetime
#

class Admin::CodPolicy < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :cod_specific_countries, :dependent => :destroy, class_name: "Admin::CodSpecificCountry"
  accepts_nested_attributes_for :cod_specific_countries, allow_destroy: true
  #VALIDATIONS
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
end
