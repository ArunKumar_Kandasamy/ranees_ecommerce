# == Schema Information
#
# Table name: package_tasks_consignments
#
#  id              :integer          not null, primary key
#  consignment_id  :integer
#  package_task_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class Admin::PackageTasksConsignments < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  attr_accessor :conform
  #ASSOCIATIONS
  belongs_to :consignment, class_name: "Admin::Consignment"
  belongs_to :package_task, class_name: "Admin::PackageTask"
  #VALIDATIONS
  validates_uniqueness_of :consignment_id, :scope => [:package_task_id]
  #CALLBACKS
  after_destroy :update_packagetask
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  
  def update_packagetask
    if self.package_task.consignments.first.nil?
      self.package_task.destroy
    end
  end
  #JOBS
  #PRIVATE
  
end
 
