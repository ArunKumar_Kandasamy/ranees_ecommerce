# == Schema Information
#
# Table name: types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Admin::Type < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  attr_accessor :new_product_type
  #ASSOCIATIONS
  has_many :product_types, :dependent => :destroy
  has_many :products, through: :product_types
  #VALIDATIONS
  validates :name, :presence => true
  #CALLBACKS
  before_save :create_type
 
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  #JOBS
  #PRIVATE 
   
  def self.tokens(query)
    types = where("name ILIKE ?", "%#{query}%")
    if types.empty?
      [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
    else
      types
    end
  end

  def self.ids_from_tokens(tokens)
    tokens.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id }
    tokens.split(",")
  end

  def create_type
    @admin_type = Admin::Type.create(name: new_product_type) if new_product_type.present?
  end

  private

  def self.ransackable_attributes(auth_object = nil)
    ['name','id']
  end

end
