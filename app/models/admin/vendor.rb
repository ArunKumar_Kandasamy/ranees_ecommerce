# == Schema Information
#
# Table name: vendors
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Admin::Vendor < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  has_many :warehouses, through: :products  
  #ASSOCIATIONS
  has_many :addresses, as: :connection, class_name: "Admin::Address"
  has_many :inventories
  accepts_nested_attributes_for :addresses, allow_destroy: true,:reject_if => lambda { |a| a[:add_line1].blank? }
  #VALIDATIONS
  validates :name, :presence => true
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
  private

  
end
