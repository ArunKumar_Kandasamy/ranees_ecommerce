# == Schema Information
#
# Table name: product_questions
#
#  id         :integer          not null, primary key
#  question   :string(255)
#  created_at :datetime
#  updated_at :datetime
#
 
class Admin::ProductQuestion < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  attr_accessor :new_product_question
  #ASSOCIATIONS
  has_many :product_properties, :dependent => :destroy
  #VALIDATIONS
  #validates :question, :presence => true
  #CALLBACKS
  before_save :create_question
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
  
  def self.tokens(query)
    questions = where("question ILIKE ?", "%#{query}%")
    if questions.empty?
      [{id: "<<<#{query}>>>", question: "New: \"#{query}\""}]
    else
      questions
    end
  end

  def self.ids_from_tokens(tokens)
    tokens.gsub!(/<<<(.+?)>>>/) do 
      @result = Admin::ProductQuestion.where("lower(question) = ?", $1.downcase)
      if @result.present?
        @result.first.id
      else
        create!(question: $1).id 
      end
    end
    tokens
  end

  def create_question
      @product_question= Admin::ProductQuestion.create(question: new_product_question) if new_product_question.present?
  end
end
