# == Schema Information
#
# Table name: consignment_logs
#
#  id               :integer          not null, primary key
#  consignment_id   :integer
#  consignment_cost :float
#  confirm          :boolean          default(FALSE)
#  logistic_id      :integer
#  reason           :string(255)
#  dispatch         :boolean          default(FALSE)
#  tracking         :string(255)
#  delivery_status  :boolean
#  delivery_date    :date
#  remarks          :text
#  cancel           :string(255)
#  cancelled_by     :integer
#  created_at       :datetime
#  updated_at       :datetime
#  updated_by       :integer
#

class Admin::ConsignmentLog < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :consignment_item_logs, class_name: "Admin::ConsignmentItemLog"
  belongs_to :consignment, class_name: "Admin::Consignment"
  #VALIDATIONS
  #CALLBACKS

  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
end
