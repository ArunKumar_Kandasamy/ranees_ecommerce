# == Schema Information
#
# Table name: permissions
#
#  id         :integer          not null, primary key
#  email      :string(255)
#  active     :boolean          default(FALSE)
#  user_id    :integer
#  role_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Admin::Permission < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :user
  belongs_to :role, class_name: "Admin::Role"
  #VALIDATIONS
  validates :active, :default => false
  validates :email, :presence => true,
                    :uniqueness => true,##  This should be done at the DB this is too expensive in rails
                    :format   => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i },
                    :length => { :maximum => 255 }
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
