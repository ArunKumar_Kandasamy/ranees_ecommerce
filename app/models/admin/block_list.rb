# == Schema Information
#
# Table name: block_lists
#
#  id         :integer          not null, primary key
#  con_name   :string(255)
#  met_name   :string(255)
#  created_at :datetime
#  updated_at :datetime
#  role_id    :integer
#

class Admin::BlockList < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :role
  #VALIDATIONS
  validates :con_name, :presence => true
  validates :met_name, :presence => true
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
