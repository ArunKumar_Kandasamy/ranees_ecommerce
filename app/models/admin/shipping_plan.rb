# == Schema Information
#
# Table name: shipping_plans
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  criteria   :string(255)
#  created_at :datetime
#  updated_at :datetime
#  range_from :float
#  range_upto :float
#

class Admin::ShippingPlan < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :product_shipping_plans, :dependent => :destroy, class_name: "Admin::ProductShippingPlan"
  has_many :products, through: :product_shipping_plans
  has_many :products, through: :product_shipping_plans
  has_many :shipping_countries, :dependent => :destroy
  has_many :countries, through: :shipping_countries
  #VALIDATIONS
  validates :name, :presence => true
  validates :criteria, :presence => true
  validates :range_from, :presence => true
  validates :range_upto, :presence => true
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  #JOBS
  #PRIVATE  
  private

  def self.ransackable_attributes(auth_object = nil)
    ['name','criteria','range_from','range_upto']
  end
end
