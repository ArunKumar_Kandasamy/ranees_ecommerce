# == Schema Information
#
# Table name: image_types
#
#  id          :integer          not null, primary key
#  height      :integer
#  width       :integer
#  created_at  :datetime
#  updated_at  :datetime
#  name        :string(255)
#  is_category :boolean          default(FALSE)
#  is_video    :boolean          default(FALSE)
#  image_type  :string(255)
#

class Admin::ImageType < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :general_images, class_name:"Admin::GeneralImage"
  #VALIDATIONS
  validates :name, :presence => true, :uniqueness => true
  validates :width, :presence => true
  validates :height, :presence => true
  #CALLBACKS
  before_save :downcase_fields
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
  def downcase_fields
    self.name.downcase!
    self.image_type.downcase!
  end
end
