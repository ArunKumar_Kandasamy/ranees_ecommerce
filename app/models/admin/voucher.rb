# == Schema Information
#
# Table name: vouchers
#
#  id                   :integer          not null, primary key
#  user_id              :integer
#  voucher_amount       :float
#  credit               :float
#  debit                :float
#  created_at           :datetime
#  updated_at           :datetime
#  order_id             :integer
#  order_revocation_id  :integer
#  refund_revocation_id :integer
#  voucher_number       :string(255)
#

class Admin::Voucher < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS

  #ASSOCIATIONS
  belongs_to :order_revocation 
  belongs_to :order
  belongs_to :user
  #VALIDATIONS
  #CALLBACKS
  before_save :update_price
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
  def update_price
    if self.user.vouchers.last.present?
      @voucher_amount = self.user.vouchers.last.voucher_amount
      if self.debit.present?
        self.voucher_amount =  @voucher_amount - self.debit
      else
        self.voucher_amount =  @voucher_amount + self.credit
      end
    else
      self.voucher_amount =  self.credit
    end
  end 
end
