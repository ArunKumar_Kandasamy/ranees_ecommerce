# == Schema Information
#
# Table name: consignments
#
#  id                        :integer          not null, primary key
#  order_id                  :integer
#  warehouse_id              :integer
#  created_at                :datetime
#  updated_at                :datetime
#  consignment_cost          :float
#  conform                   :boolean          default(FALSE)
#  consignment_number        :string(255)
#  logistic_id               :integer
#  reason                    :string(255)
#  dispatch                  :boolean          default(FALSE)
#  tracking                  :string(255)
#  delivery_status           :boolean
#  delivery_date             :date
#  remarks                   :text
#  cancel                    :string(255)
#  cancelled_by              :integer
#  invoice_cost              :float
#  status                    :string(255)
#  replacement_revocation_id :integer
#  parent_consignment_id     :integer
#

class Admin::Consignment < ActiveRecord::Base
  liquid_methods :consignment_number, :order, :consignment_cost, :tracking,:logistic
  require 'rubygems'
  require 'twilio-ruby'
  #GEMS USED
  attr_accessor :cart_value,:order_item_id,:qty ,:product_code
  #ASSOCIATIONS
  has_many :consignment_items, class_name: "Admin::ConsignmentItem"
  has_many :consignment_revocations, class_name: "Admin::ConsignmentRevocation"
  has_many :pickup_consignments, class_name: "Admin::PickupConsignment"
  has_many :refund_revocations, class_name: "Admin::RefundRevocation"
  has_many :replacement_revocations, class_name: "Admin::ReplacementRevocation"
  belongs_to :replacement_revocation, class_name: "Admin::ReplacementRevocation"
  has_many :invoices, class_name: "Admin::Invoice"
  has_many :package_tasks_consignments, :dependent => :destroy, class_name: "Admin::PackageTasksConsignments"
  has_many :package_tasks, through: :package_tasks_consignments, class_name: "Admin::PackageTask"
  has_many :resend_tasks_consignments, :dependent => :destroy, class_name: "Admin::ResendTasksConsignments"
  has_many :resend_tasks, through: :resend_tasks_consignments, class_name: "Admin::ResendTask"
  belongs_to :order, class_name: "Admin::Order"
  has_many :order_items, through: :order, class_name: "Admin::OrderItem"
  has_many :consignment_logs, :dependent => :destroy, class_name: "Admin::ConsignmentLog"
  belongs_to :warehouse, class_name: "Admin::Warehouse"
  belongs_to :logistic, class_name: "Admin::Logistic"
  has_many :resends, :class_name => "Admin::Consignment",
    :foreign_key => "parent_consignment_id"
  belongs_to :parent_consignment, :class_name => "Admin::Consignment",
    :foreign_key => "parent_consignment_id"
  accepts_nested_attributes_for :consignment_items, allow_destroy: true
  accepts_nested_attributes_for :consignment_revocations, allow_destroy: true
  #VALIDATIONS
  #CALLBACKS
  before_create :consignment_number_auto_increment
  before_update :update_consignment_item_qty_remove_consignment
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  def qty  
    quantity = 0
    self.consignment_items.each do |consignment_item|
      quantity = quantity + consignment_item.qty
    end
    return quantity
  end

  def create_revocation_for_cancel
    @revocation = Admin::RefundRevocation.create(:consignment_id => self.id  ,:revocation_cost => self.consignment_cost, :status => "pr")
    self.consignment_items.each do |consignment_item|
      Admin::RefundRevocationItem.create(:refund_revocation_id => @revocation.id , :price => consignment_item.price , :qty => consignment_item.qty ,:variant_id => consignment_item.variant_id ,:per_qty_price => consignment_item.per_qty_price,:actual_cost => consignment_item.actual_cost,:shipping_cost =>consignment_item.shipping_cost,:tax => consignment_item.tax,:status => true,:reason => 'not_delivered')
    end
    return @revocation
  end
  
  def create_replacement_for_lost
    @revocation = Admin::ReplacementRevocation.create(:consignment_id => self.id  ,:revocation_cost => self.consignment_cost, :status => "pr", :shipping_address_id => self.order.shipping_address_id)
    self.consignment_items.each do |consignment_item|
      for i in 1..consignment_item.qty
        @replacement_item = Admin::ReplacementRevocationItem.create(:replacement_revocation_id => @revocation.id , :price => consignment_item.per_qty_price , :qty => 1 ,:variant_id => consignment_item.variant_id ,:per_qty_price => consignment_item.per_qty_price,:actual_cost => consignment_item.actual_cost,:shipping_cost =>consignment_item.shipping_cost,:tax => consignment_item.tax,:status => true,:reason => 'not_delivered')
      end
    end
    return @revocation
  end

  def apply_voucher
    if self.order.balance_amount.present?
      if self.order.balance_amount > 0
        if self.consignment_cost <  self.order.balance_amount
          remaining = self.order.balance_amount - self.consignment_cost
          self.update(:invoice_cost => 0)
          self.order.update(:balance_amount => remaining)
        else 
          remaining = self.consignment_cost - self.order.balance_amount
          self.update(:invoice_cost => remaining)
          self.order.update(:balance_amount => 0)
        end
      end
    else
      self.update(:invoice_cost => self.consignment_cost)
    end
  end

  
  def update_consignment_item_qty_remove_consignment
    if self.delivery_status.nil?
      qty=0   
      if self.cancel.present?
        self.consignment_items.each do |consignment_item|
          consignment_item_qty = consignment_item.qty
          order_item_qty = consignment_item.order_items.first.qty
          qty = order_item_qty - consignment_item_qty
          consignment_item.order_items.first.update(:qty => qty)
          if consignment_item.order_items.first.qty == 0
            consignment_item.order_items.first.destroy
          end
        end
      end
    end
  end

  def check_for_return
    if self.delivery_status == true 
      self.consignment_items.each do |consignment_item|
        if consignment_item.check_for_return_initiate == true
          return true
        end
      end 
    end
  end

  def create_resend_consignment
    @consignment = Admin::Consignment.create(:order_id => self.order_id ,:consignment_cost => self.consignment_cost,:conform => self.conform,:parent_consignment_id => self.id,:warehouse_id => self.warehouse_id)
    self.consignment_items.each do |consignment_item|
      Admin::ConsignmentItem.create(:consignment_id =>  @consignment.id , :variant_id => consignment_item.variant_id,:qty => consignment_item.qty ,:per_qty_price => consignment_item.per_qty_price ,:return_due => consignment_item.return_due,:shipping_cost => consignment_item.shipping_cost, :tax => consignment_item.tax, :actual_cost => consignment_item.actual_cost)
    end
  end

  def create_consignment_log(user_id)
    @consignment_log = Admin::ConsignmentLog.create(:updated_by => user_id ,:consignment_id => self.id , :consignment_cost => self.consignment_cost ,:confirm => self.conform, :logistic_id => self.logistic_id , :reason => self.reason ,:dispatch => self.dispatch ,:tracking => self.tracking ,:delivery_status => self.delivery_status ,:delivery_date => self.delivery_date ,:remarks => self.remarks, :cancel => self.cancel ,:cancelled_by => self.cancelled_by)
    self.consignment_items.each do |consignment_item|
      Admin::ConsignmentItemLog.create(:consignment_log_id => @consignment_log.id, :qty => consignment_item.qty, :variant_id => consignment_item.variant_id , :per_qty_price => consignment_item.per_qty_price , :price => consignment_item.price, :return_due => consignment_item.return_due, :policy => consignment_item.policy)
    end
    return @consignment_log
  end

  def check_regulation_present
    self.consignment_items.each do |consignment_item|
      if consignment_item.regulations.first.present?
        return true
      end
    end
    return false
  end

  #JOBS
  #PRIVATE

  def check_consignment_item_task_status        
    if self.conform == true
      return true
    else 
      return false
    end     
  end
  
  def check_consignment_deligate_status
    if self.package_tasks.first.present?
      return true
    else
      return false
    end
  end


  def consignment_number_auto_increment
    datee = Date.today.to_datetime
    day = datee.mday
    month = datee.mon  
    consignments = Admin::Consignment.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if consignments.present?
      consign_id = consignments.last.consignment_number[-3..-1].to_i
      consign_num = consign_id+=1      
      consign_length = consign_num.to_s.size
      if consign_length == 1
        self.consignment_number = "CONNO#{day}#{month}000#{consign_num}"
      elsif consign_length == 2
        self.consignment_number = "CONNO#{day}#{month}00#{consign_num}"
      elsif consign_length == 3
        self.consignment_number = "CONNO#{day}#{month}0#{consign_num}"
      else
        self.consignment_number = "CONNO#{day}#{month}#{consign_num}"    
      end
    else
      self.consignment_number = "CONNO#{day}#{month}0001"
    end
  end  

  def check_for_consignment_confirmation
    @status = true
    self.consignment_items.each do |consignment_item|
      if consignment_item.check_for_consignment_item_confirmation == false
        @status = false
      end
    end
    return @status
  end

  def check_for_consignment_cancel
    if self.cancel.nil? && self.status != "Resend" && self.replacement_revocations.first.nil? && self.refund_revocations.first.nil?
      return true
    else
      return false
    end
  end

  def compensate_mode_for_not_delivered
    if self.delivery_status == false
      if self.status == "Resend"
        return "RESEND"
      elsif self.replacement_revocations.first.present?
        return "REPLACED"
      elsif self.refund_revocations.first.present?
        return "REFUND"
      end
    end
  end


  def check_for_delivery_status_visibility
    @status = false
    if self.logistic_id.present? && self.tracking.present? && self.conform == true
      @status = true
    end
    return @status
  end

  def  check_for_policy_status
    self.consignment_items.each do |consignment_item|
      @consign_item_due = consignment_item.return_due
      @consign_deli_date = self.delivery_date
    end
  end

  def send_sms 
    if self.order.present?
      @shipping_id= self.order.shipping_address_id
      @country= ShippingAddress.find(@shipping_id).country
      c= Country.find_country_by_name(@country)
      @country_code= c.country_code.to_s
      @shipping_phone= "+" + @country_code + ShippingAddress.find(@shipping_id).phone
      @admin_notification_customer=Admin::NotificationCustomer.first
      account_sid = "ACa7b7742ad9c96cc541db77b78ee7067c"
      auth_token = "50743bd26cf4d7faca5df98b729a1e67"
      client = Twilio::REST::Client.new account_sid, auth_token 
      @body = Liquid::Template.parse(Admin::SmsNotification.first.content_for_order_success).render 'consignment' => self,'order' => self.order
       
      from = "+18057255347" # Your Twilio number
      client.account.messages.create(
        :from => from,
        :to => @shipping_phone,
        :body => @body.html_safe
        )
        puts "Sent message to #{@shipping_phone}"
    end
  end

  def send_pre_deli_cancel_sms 
    if self.order.present?
      @shipping_id= self.order.shipping_address_id
      @country= ShippingAddress.find(@shipping_id).country
      c= Country.find_country_by_name(@country)
      @country_code= c.country_code.to_s
      @shipping_phone= "+" + @country_code + ShippingAddress.find(@shipping_id).phone
      @admin_notification_customer=Admin::NotificationCustomer.first
      account_sid = "ACa7b7742ad9c96cc541db77b78ee7067c"
      auth_token = "50743bd26cf4d7faca5df98b729a1e67"
      client = Twilio::REST::Client.new account_sid, auth_token 
      @body = Liquid::Template.parse(Admin::SmsNotification.first.content_for_pre_deli_cancel).render 'consignment' => self,'order' => self.order
       
      from = "+18057255347" # Your Twilio number
      client.account.messages.create(
        :from => from,
        :to => @shipping_phone,
        :body => @body.html_safe
        )
        puts "Sent message to #{@shipping_phone}"
    end
  end

  def send_post_deli_cancel_sms 
    if self.order.present?
      @shipping_id= self.order.shipping_address_id
      @country= ShippingAddress.find(@shipping_id).country
      c= Country.find_country_by_name(@country)
      @country_code= c.country_code.to_s
      @shipping_phone= "+" + @country_code + ShippingAddress.find(@shipping_id).phone
      @admin_notification_customer=Admin::NotificationCustomer.first
      account_sid = "ACa7b7742ad9c96cc541db77b78ee7067c"
      auth_token = "50743bd26cf4d7faca5df98b729a1e67"
      client = Twilio::REST::Client.new account_sid, auth_token 
      @body = Liquid::Template.parse(Admin::SmsNotification.first.content_for_post_deli_cancel).render 'consignment' => self,'order' => self.order
       
      from = "+18057255347" # Your Twilio number
      client.account.messages.create(
        :from => from,
        :to => @shipping_phone,
        :body => @body.html_safe
        )
        puts "Sent message to #{@shipping_phone}"
    end
  end

  def send_tracking_update_sms 
    if self.order.present?
      @shipping_id= self.order.shipping_address_id
      @country= ShippingAddress.find(@shipping_id).country
      c= Country.find_country_by_name(@country)
      @country_code= c.country_code.to_s
      @shipping_phone= "+" + @country_code + ShippingAddress.find(@shipping_id).phone
      @admin_notification_customer=Admin::NotificationCustomer.first
      account_sid = "ACa7b7742ad9c96cc541db77b78ee7067c"
      auth_token = "50743bd26cf4d7faca5df98b729a1e67"
      client = Twilio::REST::Client.new account_sid, auth_token 
      @body = Liquid::Template.parse(Admin::SmsNotification.first.content_for_tracking_updated).render 'consignment' => self,'order' => self.order
       
      from = "+18057255347" # Your Twilio number
      client.account.messages.create(
        :from => from,
        :to => @shipping_phone,
        :body => @body.html_safe
        ) 
        puts "Sent message to #{@shipping_phone}"
    end
  end

    def  import_bludart_waybill(zip)
     msg_xml1 = '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://schemas.datacontract.org/2004/07/SAPI.Entities.WayBillGeneration" xmlns:ns2="http://tempuri.org/" xmlns:ns3="http://schemas.datacontract.org/2004/07/SAPI.Entities.Admin">
  <SOAP-ENV:Body>
    <ns2:GenerateWayBill>
      <ns2:Request>
        <ns1:Consignee>
          <ns1:ConsigneeAddress1>12/26 4th Main Road</ns1:ConsigneeAddress1>
          <ns1:ConsigneeAddress2>Gangai Nagar Velachery</ns1:ConsigneeAddress2>
          <ns1:ConsigneeAddress3>Chennai</ns1:ConsigneeAddress3>
          <ns1:ConsigneeAttention>A</ns1:ConsigneeAttention>
          <ns1:ConsigneeMobile>9962321420</ns1:ConsigneeMobile>
          <ns1:ConsigneeName>Ramesh</ns1:ConsigneeName>
          <ns1:ConsigneePincode>600042</ns1:ConsigneePincode>
          <ns1:ConsigneeTelephone>9962321420</ns1:ConsigneeTelephone>
        </ns1:Consignee>
        <ns1:Services>
          <ns1:ActualWeight>.500</ns1:ActualWeight>
          <ns1:CollectableAmount>1000</ns1:CollectableAmount>
          <ns1:Commodity>
            <ns1:CommodityDetail1>Books</ns1:CommodityDetail1>
            <ns1:CommodityDetail2>Books</ns1:CommodityDetail2>
            <ns1:CommodityDetail3>Books</ns1:CommodityDetail3>
          </ns1:Commodity>
          <ns1:CreditReferenceNo>5645282</ns1:CreditReferenceNo>
          <ns1:DeclaredValue>1000</ns1:DeclaredValue>
          <ns1:Dimensions>
            <ns1:Dimension>
              <ns1:Breadth>8</ns1:Breadth>
              <ns1:Count>1</ns1:Count>
              <ns1:Height>10</ns1:Height>
              <ns1:Length>10</ns1:Length>
            </ns1:Dimension>
          </ns1:Dimensions>
          <ns1:InvoiceNo>1230</ns1:InvoiceNo>
          <ns1:PackType>?</ns1:PackType>
          <ns1:PickupDate>2014-07-10</ns1:PickupDate>
          <ns1:PickupTime>1800</ns1:PickupTime>
          <ns1:PieceCount>1</ns1:PieceCount>
          <ns1:ProductCode>A</ns1:ProductCode>
          <ns1:ProductType>Dutiables</ns1:ProductType>
          <ns1:SpecialInstruction>cbse books</ns1:SpecialInstruction>
          <ns1:SubProductCode>C</ns1:SubProductCode>
        </ns1:Services>
        <ns1:Shipper>
          <ns1:CustomerAddress1>12/26 4th Main Road</ns1:CustomerAddress1>
          <ns1:CustomerAddress2>Gangai Nagar</ns1:CustomerAddress2>
          <ns1:CustomerAddress3>Velachery</ns1:CustomerAddress3>
          <ns1:CustomerCode>531xxx</ns1:CustomerCode>
          <ns1:CustomerEmailID>firstwebserver@gmail.com</ns1:CustomerEmailID>
          <ns1:CustomerMobile>9962321420</ns1:CustomerMobile>
          <ns1:CustomerName>Ramesh A</ns1:CustomerName>
          <ns1:CustomerPincode>600042</ns1:CustomerPincode>
          <ns1:CustomerTelephone>9962321420</ns1:CustomerTelephone>
          <ns1:IsToPayCustomer>true</ns1:IsToPayCustomer>
          <ns1:OriginArea>MAA</ns1:OriginArea>
          <ns1:Sender>RAVIE AGENCIES - PREPAID</ns1:Sender>
          <ns1:VendorCode>?</ns1:VendorCode>
        </ns1:Shipper>
      </ns2:Request>
      <ns2:Profile>
        <ns3:Api_type>S</ns3:Api_type>
        <ns3:Area>#{@areacode}</ns3:Area>
        <ns3:Customercode>#{@customercode}</ns3:Customercode>
        <ns3:IsAdmin>?</ns3:IsAdmin>
        <ns3:LicenceKey>#{@licencekey}</ns3:LicenceKey>
        <ns3:LoginID>#{@loginid}</ns3:LoginID>
        <ns3:Password>#{@pass}</ns3:Password>
        <ns3:Version>#{@version}</ns3:Version>
      </ns2:Profile>
    </ns2:GenerateWayBill>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>'
  end

  def  bluedart_locationfinder(pincode) 
    @areacode = "MAA"
    @licencekey = "38c86d074cbf8471c00fd32cfb3b72a4"
    @loginid = "MAA02979"
    @pass = "ranees2015"
    @version = "1.3"
    @customercode = "501970"
    @locationfinder_xml = "<?xml version='1.0' encoding='UTF-8'?><SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://tempuri.org/' xmlns:ns2='http://schemas.datacontract.org/2004/07/SAPI.Entities.Admin'><SOAP-ENV:Body><ns1:GetServicesforPincode><ns1:pinCode>#{pincode}</ns1:pinCode><ns1:profile><ns2:Api_type>S</ns2:Api_type><ns2:Area>#{@areacode}</ns2:Area><ns2:Customercode>#{@customercode}</ns2:Customercode><ns2:IsAdmin>?</ns2:IsAdmin><ns2:LicenceKey>#{@licencekey}</ns2:LicenceKey><ns2:LoginID>#{@loginid}</ns2:LoginID><ns2:Password>#{@pass}</ns2:Password><ns2:Version>#{@version}</ns2:Version></ns1:profile></ns1:GetServicesforPincode></SOAP-ENV:Body></SOAP-ENV:Envelope>"
    @locationfinder_url = "http://netconnect.bluedart.com/ShippingAPI/Finder/ServiceFinderQuery.svc?wsdl"
    client = Savon.client(wsdl: @locationfinder_url,convert_request_keys_to: :camelcase)
    response =  client.call( :get_servicesfor_pincode,xml: @locationfinder_xml)
     if response.success?
      data = response.to_hash
      if data[:get_servicesfor_pincode_response][:get_servicesfor_pincode_result][:is_error] == false
          @available_product_type = []
          if data[:get_servicesfor_pincode_response][:get_servicesfor_pincode_result][:domestic_priority_outbound] == "Yes"
              @available_product_type.unshift('Domestic')
          end
          if data[:get_servicesfor_pincode_response][:get_servicesfor_pincode_result][:ground_outbound] == "Yes"
              @available_product_type.unshift('Ground')
          end
          if data[:get_servicesfor_pincode_response][:get_servicesfor_pincode_result][:apex_outbound] == "Yes"
            @available_product_type.unshift('Air')
          end
          if data[:get_servicesfor_pincode_response][:get_servicesfor_pincode_result][:e_tail_pre_paid_air_outound] == "Yes"
            @available_product_type.unshift('Air Prepaid')
          end
           if data[:get_servicesfor_pincode_response][:get_servicesfor_pincode_result][:e_tail_pre_paid_ground_outbound] == "Yes"
            @available_product_type.unshift('Ground Prepaid')
          end
          return @available_product_type, data[:get_servicesfor_pincode_response][:get_servicesfor_pincode_result][:area_code]
      else
         return "error"
      end
    end
  end


  def bluedart_generate_pickupregistration(product_code,actual_weight,awb_no) 
      if product_code == "Domestic"
          product_type = "D"
          subproduct_type = ""
      elsif  product_code == "Ground"
          product_type = "E"
           subproduct_type = ""
      elsif  product_code == "Air"
          product_type = "A"
           subproduct_type = ""
      elsif  product_code == "Air Prepaid"
          product_type = "A"
          subproduct_type = "P"
      elsif  product_code == "Ground Prepaid"
          product_type = "E"
          subproduct_type = "P"
      end
      
    @areacode = "MAA"
    @licencekey = "38c86d074cbf8471c00fd32cfb3b72a4"
    @loginid = "MAA02979"
    @pass = "ranees2015"
    @version = "1.3"
    @customercode = "501970"
    @generate_pickupregistration_xml = "<?xml version='1.0' encoding='UTF-8'?><SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://schemas.microsoft.com/2003/10/Serialization/Arrays' xmlns:ns2='http://schemas.datacontract.org/2004/07/SAPI.Entities.Pickup' xmlns:ns3='http://tempuri.org/' xmlns:ns4='http://schemas.datacontract.org/2004/07/SAPI.Entities.Admin'><SOAP-ENV:Body><ns3:RegisterPickup><ns3:request><ns2:AWBNo><ns1:string>#{awb_no}</ns1:string></ns2:AWBNo><ns2:AreaCode>#{@areacode}</ns2:AreaCode><ns2:ContactPersonName>#{self.package_tasks.first.contact_person}</ns2:ContactPersonName><ns2:CustomerAddress1>#{self.warehouse.addresses.first.add_line1.slice(0, 30)}</ns2:CustomerAddress1><ns2:CustomerAddress2>#{self.warehouse.addresses.first.add_line1.slice!(30)}</ns2:CustomerAddress2><ns2:CustomerAddress3>#{self.warehouse.addresses.first.add_line2}</ns2:CustomerAddress3><ns2:CustomerCode>#{@customercode}</ns2:CustomerCode><ns2:CustomerName>#{self.package_tasks.first.contact_person}</ns2:CustomerName><ns2:CustomerPincode>#{self.warehouse.addresses.first.c_code}</ns2:CustomerPincode><ns2:CustomerTelephoneNumber></ns2:CustomerTelephoneNumber><ns2:DoxNDox></ns2:DoxNDox><ns2:EmailID></ns2:EmailID><ns2:MobileTelNo></ns2:MobileTelNo><ns2:NumberofPieces>#{self.consignment_items.count}</ns2:NumberofPieces><ns2:OfficeCloseTime>#{self.package_tasks.first.closing_time.strftime('%H%M')}</ns2:OfficeCloseTime><ns2:ProductCode>#{product_type}</ns2:ProductCode><ns2:ReferenceNo>?</ns2:ReferenceNo><ns2:Remarks>?</ns2:Remarks><ns2:RouteCode></ns2:RouteCode><ns2:ShipmentPickupDate>#{self.pickup_date.strftime("%Y-%m-%d")}</ns2:ShipmentPickupDate><ns2:ShipmentPickupTime>#{self.pickup_date.strftime('%H%M') }</ns2:ShipmentPickupTime><ns2:VolumeWeight>#{actual_weight}</ns2:VolumeWeight><ns2:WeightofShipment>#{actual_weight}</ns2:WeightofShipment><ns2:isToPayShipper>false</ns2:isToPayShipper></ns3:request><ns3:profile><ns4:Api_type>S</ns4:Api_type><ns4:Area>#{@areacode}</ns4:Area><ns4:Customercode>#{@customercode}</ns4:Customercode><ns4:IsAdmin></ns4:IsAdmin><ns4:LicenceKey>#{@licencekey}</ns4:LicenceKey><ns4:LoginID>#{@loginid}</ns4:LoginID><ns4:Password>#{@pass}</ns4:Password><ns4:Version>#{@version}</ns4:Version></ns3:profile></ns3:RegisterPickup></SOAP-ENV:Body></SOAP-ENV:Envelope>"
    @generate_pickupregistration_url = "http://netconnect.bluedart.com/ShippingAPI/Pickup/PickupRegistrationService.svc?wsdl"
    client = Savon.client(wsdl: @generate_pickupregistration_url,convert_request_keys_to: :camelcase)
    response =  client.call( :register_pickup,xml: @generate_pickupregistration_xml)
    if response.success?
      data = response.to_hash
      if data[:register_pickup_response][:register_pickup_result][:is_error] == false
         @token_number = data[:register_pickup_response][:register_pickup_result][:token_number]
         return @token_number
      else
        @status = data[:register_pickup_response][:register_pickup_result][:status][:response_status][:status_information]
        return "error",@status
      end
    end
  end

  def bluedart_cancel_pickupregistration() 
    @areacode = "MAA"
    @licencekey = "38c86d074cbf8471c00fd32cfb3b72a4"
    @loginid = "MAA02979"
    @pass = "ranees2015"
    @version = "1.3"
    @customercode = "501970" 
    @cancel_pickupregistration_xml = "<?xml version='1.0' encoding='UTF-8'?><SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://schemas.datacontract.org/2004/07/SAPI.Entities.Pickup' xmlns:ns2='http://tempuri.org/' xmlns:ns3='http://schemas.datacontract.org/2004/07/SAPI.Entities.Admin'><SOAP-ENV:Body><ns2:CancelPickup><ns2:request><ns1:PickupRegistrationDate>#{self.pickup_date.strftime('%Y-%m-%d')}</ns1:PickupRegistrationDate><ns1:Remarks></ns1:Remarks><ns1:TokenNumber>#{self.token_no}</ns1:TokenNumber></ns2:request><ns2:profile><ns3:Api_type>S</ns3:Api_type><ns3:Area>#{@areacode}</ns3:Area><ns3:Customercode>#{@customercode}</ns3:Customercode><ns3:IsAdmin>?</ns3:IsAdmin><ns3:LicenceKey>#{@licencekey}</ns3:LicenceKey><ns3:LoginID>#{@loginid}</ns3:LoginID><ns3:Password>#{@pass}</ns3:Password><ns3:Version>#{@version}</ns3:Version></ns2:profile></ns2:CancelPickup></SOAP-ENV:Body></SOAP-ENV:Envelope>"
    @cancel_pickupregistration_url =  "http://netconnect.bluedart.com/ShippingAPI/Pickup/PickupRegistrationService.svc?wsdl"
    client = Savon.client(wsdl: @cancel_pickupregistration_url,convert_request_keys_to: :camelcase)
    response =  client.call( :cancel_pickup,xml: @cancel_pickupregistration_xml)
    if response.success?
      data = response.to_hash
       if  data[:cancel_pickup_response][:cancel_pickup_result][:is_error] == false
         @status_code = data[:cancel_pickup_response][:cancel_pickup_result][:status][:cancel_pickup_response_status][:status_code]
         return @status_code
       else
        @status_code = data[:cancel_pickup_response][:cancel_pickup_result][:status][:cancel_pickup_response_status][:status_information]
        return "error",@status_code 
      end
    end
    end

    def bluedart_generate_waybill(product_code,actual_weight) 
      if product_code == "Domestic"
          product_type = "D"
          subproduct_type = ""
      elsif  product_code == "Ground"
          product_type = "E"
           subproduct_type = ""
      elsif  product_code == "Air"
          product_type = "A"
           subproduct_type = ""
      elsif  product_code == "Air Prepaid"
          product_type = "A"
          subproduct_type = "P"
      elsif  product_code == "Ground Prepaid"
          product_type = "E"
          subproduct_type = "P"
      end
      variant = Admin::Variant.find(self.consignment_items.first.variant_id)
      @areacode = "MAA"
      @licencekey = "38c86d074cbf8471c00fd32cfb3b72a4"
      @loginid = "MAA02979"
      @pass = "ranees2015"
      @version = "1.3"
      @customercode = "501970"
      @generate_waybill_xml = "<?xml version='1.0' encoding='UTF-8'?><SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://schemas.datacontract.org/2004/07/SAPI.Entities.WayBillGeneration' xmlns:ns2='http://tempuri.org/' xmlns:ns3='http://schemas.datacontract.org/2004/07/SAPI.Entities.Admin'><SOAP-ENV:Body><ns2:GenerateWayBill><ns2:Request><ns1:Consignee><ns1:ConsigneeAddress1>#{self.order.shipping_address.add_line1.slice(0, 30)}</ns1:ConsigneeAddress1><ns1:ConsigneeAddress2></ns1:ConsigneeAddress2><ns1:ConsigneeAddress3>#{self.order.shipping_address.add_line2}</ns1:ConsigneeAddress3><ns1:ConsigneeAttention>A</ns1:ConsigneeAttention><ns1:ConsigneeMobile>#{self.order.shipping_address.phone}</ns1:ConsigneeMobile><ns1:ConsigneeName>#{self.order.shipping_address.name}</ns1:ConsigneeName><ns1:ConsigneePincode>#{self.order.shipping_address.pincode}</ns1:ConsigneePincode></ns1:Consignee><ns1:Services><ns1:ActualWeight>.500</ns1:ActualWeight><ns1:Commodity><ns1:CommodityDetail1></ns1:CommodityDetail1><ns1:CommodityDetail2></ns1:CommodityDetail2><ns1:CommodityDetail3></ns1:CommodityDetail3></ns1:Commodity><ns1:CreditReferenceNo>#{self.consignment_number}</ns1:CreditReferenceNo><ns1:DeclaredValue>#{self.consignment_cost}</ns1:DeclaredValue><ns1:Dimensions><ns1:Breadth>#{variant.breadth}</ns1:Breadth><ns1:Count>1</ns1:Count><ns1:Height>#{variant.width}</ns1:Height><ns1:Length>#{variant.length}</ns1:Length></ns1:Dimensions><ns1:InvoiceNo></ns1:InvoiceNo><ns1:PDFOutputNotRequired>true</ns1:PDFOutputNotRequired><ns1:PackType></ns1:PackType><ns1:PickupDate>#{self.pickup_date.strftime("%Y-%m-%d")}</ns1:PickupDate><ns1:PickupTime>#{self.pickup_date.strftime('%H%M') }</ns1:PickupTime><ns1:PieceCount>#{self.consignment_items.count}</ns1:PieceCount><ns1:ProductCode>#{product_type}</ns1:ProductCode><ns1:ProductType>Dutiables</ns1:ProductType><ns1:SpecialInstruction></ns1:SpecialInstruction><ns1:SubProductCode>#{subproduct_type}</ns1:SubProductCode></ns1:Services><ns1:Shipper><ns1:CustomerAddress1>#{self.warehouse.addresses.first.add_line1.slice(0, 30)}</ns1:CustomerAddress1><ns1:CustomerAddress2>#{self.warehouse.addresses.first.add_line2.slice!(0, 30)}</ns1:CustomerAddress2><ns1:CustomerAddress3></ns1:CustomerAddress3><ns1:CustomerCode>#{@customercode}</ns1:CustomerCode><ns1:CustomerEmailID></ns1:CustomerEmailID><ns1:CustomerMobile>#{self.warehouse.addresses.first.phone}</ns1:CustomerMobile><ns1:CustomerName>#{self.package_tasks.first.contact_person}</ns1:CustomerName><ns1:CustomerPincode>#{self.warehouse.addresses.first.c_code}</ns1:CustomerPincode><ns1:CustomerTelephone></ns1:CustomerTelephone><ns1:IsToPayCustomer>false</ns1:IsToPayCustomer><ns1:OriginArea>MAA</ns1:OriginArea><ns1:Sender>GLENWORTH ESTATE LTD</ns1:Sender><ns1:VendorCode></ns1:VendorCode></ns1:Shipper></ns2:Request><ns2:Profile><ns3:Api_type>S</ns3:Api_type><ns3:Area>#{@areacode}</ns3:Area><ns3:Customercode>#{@customercode}</ns3:Customercode><ns3:IsAdmin>?</ns3:IsAdmin><ns3:LicenceKey>#{@licencekey}</ns3:LicenceKey><ns3:LoginID>#{@loginid}</ns3:LoginID><ns3:Password>#{@pass}</ns3:Password><ns3:Version>#{@version}</ns3:Version></ns2:Profile></ns2:GenerateWayBill></SOAP-ENV:Body></SOAP-ENV:Envelope>"
      @generate_waybill_url = "http://netconnect.bluedart.com/ShippingAPI/WayBill/WayBillGeneration.svc?wsdl"
      client = Savon.client(wsdl:  @generate_waybill_url ,convert_request_keys_to: :camelcase)
      response =  client.call( :generate_way_bill,xml: @generate_waybill_xml)
      if response.success?
        data = response.to_hash
         if data[:generate_way_bill_response][:generate_way_bill_result][:is_error] == false
           @waybill_number = data[:generate_way_bill_response][:generate_way_bill_result][:awb_no]
           return @waybill_number
        else
          @status = data[:generate_way_bill_response][:generate_way_bill_result][:status][:way_bill_generation_status][:status_information]
           return "error",@status
        end
      end
  end

  def bluedart_cancel_waybill() 
      @areacode = "MAA"
      @licencekey = "38c86d074cbf8471c00fd32cfb3b72a4"
      @loginid = "MAA02979"
      @pass = "ranees2015"
      @version = "1.3"
      @customercode = "501970"
      @cancel_waybill_xml = "<?xml version='1.0' encoding='UTF-8'?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://schemas.datacontract.org/2004/07/SAPI.Entities.WayBillGeneration' xmlns:ns2='http://tempuri.org/' xmlns:ns3='http://schemas.datacontract.org/2004/07/SAPI.Entities.Admin'>
        <SOAP-ENV:Body>
          <ns2:CancelWaybill>
            <ns2:Request>
              <ns1:AWBNo>#{self.awb_no}</ns1:AWBNo>
            </ns2:Request>
            <ns2:Profile>
                <ns3:Api_type>S</ns3:Api_type>
                <ns3:Area>#{@areacode}</ns3:Area>
                <ns3:Customercode>#{@customercode}</ns3:Customercode>
                <ns3:IsAdmin>?</ns3:IsAdmin>
                <ns3:LicenceKey>#{@licencekey}</ns3:LicenceKey>
                <ns3:LoginID>#{@loginid}</ns3:LoginID>
                <ns3:Password>#{@pass}</ns3:Password>
                <ns3:Version>#{@version}</ns3:Version>
            </ns2:Profile>
          </ns2:CancelWaybill>
        </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>"
      @cancel_waybill_url = "http://netconnect.bluedart.com/ShippingAPI/WayBill/WayBillGeneration.svc?wsdl"
      client = Savon.client(wsdl: @cancel_waybill_url,convert_request_keys_to: :camelcase)
      response =  client.call( :cancel_waybill,xml: @cancel_waybill_xml)
      if response.success?
        data = response.to_hash
         if data[:cancel_waybill_response][:cancel_waybill_result][:is_error] == false
            @status_code = data[:cancel_waybill_response][:cancel_waybill_result][:status][:way_bill_generation_status][:status_code]
            return @status_code
        else
          @status_code = data[:cancel_waybill_response][:cancel_waybill_result][:status][:way_bill_generation_status][:status_information]
          return "error",@status_code
        end
      end
    end



end
