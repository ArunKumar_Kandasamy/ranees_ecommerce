# == Schema Information
#
# Table name: brands
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  description :text
#

class Admin::Brand < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  
  #ASSOCIATIONS
  has_many :products, class_name: "Admin::Product"
  #VALIDATIONS 
  validates_uniqueness_of :name, :presence => true
  validates :description, :presence => true
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE
  
  def self.tokens(query)
    types = where("name ILIKE ?", "%#{query}%")
    if types.empty?
      [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
    else
      types
    end
  end
  
  def self.ids_from_tokens(tokens)
    tokens.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id }
    tokens
  end

private

  def self.ransackable_attributes(auth_object = nil)
    ['name','id']
  end

end
