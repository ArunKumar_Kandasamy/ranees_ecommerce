# == Schema Information
#
# Table name: product_shipping_plans
#
#  id               :integer          not null, primary key
#  product_id       :integer
#  shipping_plan_id :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class Admin::ProductShippingPlan < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :product
  belongs_to :shipping_plan
  #VALIDATIONS
  validates_uniqueness_of :product_id, :scope => [:shipping_plan_id]
  validates :product_id, presence: :true
  validates :shipping_plan_id, presence: :true
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
