# == Schema Information
#
# Table name: generals
#
#  id                  :integer          not null, primary key
#  store_name          :string(255)
#  page_title          :string(255)
#  meta_des            :string(255)
#  account_email       :string(255)
#  com_email           :string(255)
#  legal_name          :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  avatar_file_name    :string(255)
#  avatar_content_type :string(255)
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  acc_currency        :string(255)
#  show_currency       :string(255)
#  a_text              :text
#  dt_format           :string(255)
#  term_use            :text
#  privacy             :text
#  fb_link             :string(255)
#  tw_link             :string(255)
#  gp_link             :string(255)
#  pt_link             :string(255)
#  about_us            :text
#  return              :text
#  refund              :text
#  cancellation        :text
#  ship_delivery       :text
#  contact_us          :text
#  category_level      :integer          default(3)
#  fixed_rate          :boolean          default(FALSE)
#  cod_preference      :boolean          default(FALSE)
#  min_order           :boolean          default(FALSE)
#  min_cost            :float
#

#

class Admin::General < ActiveRecord::Base 
  #GEMS USED
  #ASSOCIATIONS
  has_many :addresses, as: :connection, class_name: "Admin::Address"
  accepts_nested_attributes_for :addresses, allow_destroy: true
  #VALIDATIONS
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES 
  #OTHER METHODS
  #JOBS
  #PRIVATE
end  
