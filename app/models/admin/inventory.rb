# == Schema Information
#
# Table name: inventories
#
#  id              :integer          not null, primary key
#  product_id      :integer
#  warehouse_id    :integer
#  vendor_id       :integer
#  qty             :integer
#  purchase_price  :float
#  sold_qty        :integer
#  balance_qty     :integer
#  updated_through :string(255)      default("manual")
#  created_at      :datetime
#  updated_at      :datetime
#  user_id         :integer
#  variant_id      :integer
#  expiry_date     :date
#  regulation      :string(255)
#  profit_price    :float
#  selling_price   :float
#  parent_id       :integer
#

class Admin::Inventory < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  
  #ASSOCIATIONS 
  belongs_to :vendor, class_name:"Admin::Vendor"
  belongs_to :warehouse, class_name:"Admin::Warehouse"
  belongs_to :variant, class_name:"Admin::Variant"
  belongs_to :product, class_name:"Admin::Product"
  has_many :regulations, class_name:"Admin::Regulation"
  belongs_to :user
  #VALIDATIONS
  #validates :vendor_id, :presence => true
  validates :warehouse_id, :presence => true
  #validates :purchase_price, :presence => true
  validates :qty, :presence => true
  #validates :expiry_date, :presence => true
  
  
  #CALLBACKS
  before_create :form_fill
  before_save :check_for_reason

  #after_save :to_clear_inventory
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  def self.tokens(query)
    @admin_regulations = where("reason like (?)","%#{query}%" )
    if @admin_regulations.empty?      
    else
       return @admin_regulations
    end
  end
  #JOBS
  #PRIVATE 
  def check_for_reason
    unless self.regulation== "c"
      self.regulation= "r"
    end
  end

  def form_fill
    self.balance_qty= self.qty
  end

  #def to_clear_inventory
   # HardWorker.perform_in(1.minute.from_now, self.id)
  #end
  
  private

  def self.ransackable_attributes(auth_object = nil)
    ['name','selling_price','qty','purchase_price','id']
  end
  

  def full_name
    @warehouse= Admin::Warehouse.where(:id => "#{warehouse_id}").first
    "#{@warehouse.name} (#{balance_qty})"
  end 

  def combined_qty_price
    @inventory= Admin::Inventory.where(:id => "#{id}").first
    "#{@inventory.balance_qty} QTY/ #{@inventory.selling_price}"
  end
end