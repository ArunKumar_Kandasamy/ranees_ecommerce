# == Schema Information
#
# Table name: shipping_specific_countries
#
#  id                 :integer          not null, primary key
#  cost               :float
#  country_id         :integer
#  shipping_policy_id :integer
#  created_at         :datetime
#  updated_at         :datetime
#  all_place          :boolean          default(TRUE)
#  policy             :string(255)      default("free")
#

class Admin::ShippingSpecificCountry < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  
  #ASSOCIATIONS
  has_many :shipping_specific_pincodes, class_name: "Admin::ShippingSpecificPincode"
  has_many :shipping_weights, class_name: "Admin::ShippingWeight"
  has_many :shipping_prices, class_name: "Admin::ShippingPrice"
  belongs_to :shipping_policy 
  belongs_to :country 
  accepts_nested_attributes_for :shipping_specific_pincodes, allow_destroy: true 
  accepts_nested_attributes_for :shipping_weights, allow_destroy: true
  accepts_nested_attributes_for :shipping_prices, allow_destroy: true
  #VALIDATIONS 
  #CALLBACKS
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 


  def country_name
    "#{country.name}"
  end
  
end
