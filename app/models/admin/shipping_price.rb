# == Schema Information
#
# Table name: shipping_prices
#
#  id                           :integer          not null, primary key
#  range                        :string(255)
#  cost                         :float
#  shipping_policy_id           :integer
#  shipping_specific_country_id :integer
#  shipping_specific_pincode_id :integer
#  created_at                   :datetime
#  updated_at                   :datetime
#

class Admin::ShippingPrice < ActiveRecord::Base
end
