# == Schema Information
#
# Table name: carts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#  pincode    :string(255)
#  ip         :string(255)
#  location   :string(255)
#

class Cart < ActiveRecord::Base
  
  apply_simple_captcha
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :cart_items                                                    
  has_many :carts_shipping_addresses, class_name: "CartsShippingAddresses"
  has_many :shipping_addresses, through: :carts_shipping_addresses
  belongs_to :user
  #VALIDATIONS
  #CALLBACKS

  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 

  def cart_availability_list(country_id = '' ,pincode = '')
    @country = Array.new 
    @unavailable_item_array = Array.new
    @pincode_list = Array.new
    self.cart_items.each do |cart_item|
      @variant = Admin::Variant.find(cart_item.variant_id)
      @product = @variant.product
      if !@product.availability_plan.all_country
        if !country_id.present?
          @country.push(*@product.availability_plan.plan_countries.map{|x| x.country_id})
        else
          @availability_countries = @product.availability_plan.list_countries.where(:country_id => country_id)
          if @availability_countries.length > 0
            @availability_country = @availability_countries.first
            unless @availability_country.all_place
              if !pincode.present?
                @pincode_list.push(*@availability_country.country_pincodes.map{|x| Admin::Pincode.find_by_pincode(x.pincode).id })
              else
                @country_pincode = @availability_country.country_pincodes.where(:pincode => pincode).first
                unless @country_pincode.availability 
                  @unavailable_item_array.push(*cart_item.id)
                end
              end
            end
          else
            @unavailable_item_array.push(*cart_item.id)
          end
        end
      end
    end
    if @country.length > 0
      return Admin::Country.find(@country)
    elsif @pincode_list.length > 0
      return Admin::Pincode.find(@pincode_list)
    elsif @unavailable_item_array.length > 0
      return CartItem.find(@unavailable_item_array)
    else
      return "Available"
    end

  end

  def general_fixed_rate
    if Admin::General.first.present?
      if Admin::General.first.fixed_rate == true
        return true
      else
        return false
      end
    else
      return false
    end
  end

  def show_estimation
    @shipping_policy = Admin::ShippingPolicy.first
    if general_fixed_rate
       return false
    else
      @tax_violate = true
      @shipping_violate = true
      self.cart_items.each do |cart_item|
        @variant = Admin::Variant.find(cart_item.variant_id)
        @product = @variant.product
        if @product.tax_violate != true
          if !@variant.need_pincode_for_tax
           @tax_violate = false
          end
        end
        if @product.free_shipping != true
           unless @shipping_policy.all_country 
             @shipping_violate = false
           end
        end
      end
      if @tax_violate && @shipping_violate
        return false
      else
        return true
      end
    end
  end

  def cart_availability_listss
    @country = Array.new 
    self.cart_items.each do |cart_item|
      @variant = Admin::Variant.find(cart_item.variant_id)
      @product = @variant.product
      unless @product.availability_plan.all_country
        @country.push(*@product.availability_plan.plan_countries.map{|x| x.country_id})
      end
    end
    if @new_country.length == 0
      return "Available"
    else
      return Admin::Country.find(@new_country)
    end
  end

  def cart_shipping_cost
    @shipping_policy = Admin::ShippingPolicy.first
    @country = Array.new 
    self.cart_items.each do |cart_item|
      @variant = Admin::Variant.find(cart_item.variant_id)
      @product = @variant.product
      unless @product.availability_plan.all_country
        @country.push(*@product.availability_plan.plan_countries.map{|x| x.country_id})
      end
    end
    @new_country =  @country.uniq
    unless @shipping_policy.all_country 
      if @new_country.length == 0
        @new_country = @shipping_policy.shipping_specific_countries.map{|x| x.country_id}
      else
        @new_country = @new_country & @shipping_policy.shipping_specific_countries.map{|x| x.country_id}
      end
    end
    if @new_country.length == 0
      self.find_cart_total_shipping_cost
    else
      #return Admin::Country.find(@new_country)
      return self.find_cart_total_shipping_cost
    end
  end

  def find_cart_total_shipping_cost(country,pincode)
    @shipping_policy = Admin::ShippingPolicy.first
    @weight = 0
    @price = 0

    self.cart_items.map do |cart_item| 
      @variant = Admin::Variant.find(cart_item.variant_id)
      @product = @variant.product
      if @product.based_on == "w" && @product.free_shipping != true
        @weight += (@variant.weight * cart_item.qty) 
      elsif @product.based_on == "p" && @product.free_shipping != true
        @price += (@variant.inventories.first.selling_price * cart_item.qty) 
      end
    end

    @weight_cost = 0
    @price_cost = 0
    @total_cost = 0
    
    if @shipping_policy.all_country
      if @weight > 0
        @weight_cost = self.find_shipping_range(@shipping_policy,false,"w",@weight)
      end
      if @price > 0
        @price_cost = self.find_shipping_range(@shipping_policy,false,"p",@price)
      end 
      return @total_cost = @weight_cost + @price_cost
    else
      @shipping_country = @shipping_policy.shipping_specific_countries.where(:country_id => country).first
      if  @shipping_country.present?
        if @shipping_country.all_place
          if @weight > 0
            @weight_cost = self.find_shipping_range(@shipping_country,false,"w",@weight)
          end
          if @price > 0
            @price_cost = self.find_shipping_range(@shipping_country,false,"p",@price)
          end 
          return @total_cost = @weight_cost + @price_cost
        else
          @pincode = Admin::ShippingSpecificPincode.where(:pincode => pincode).first
          if @pincode.present?
            if @weight > 0
              @weight_cost = self.find_shipping_range(@pincode,true,"w",@weight)
            end
            if @price > 0
              @price_cost = self.find_shipping_range(@pincode,true,"p",@price)
            end 
            return @total_cost = @weight_cost + @price_cost
          else
            return 0
          end
        end
      else
        return 0
      end
    end

    #return @weight_based_items.grep(Integer)
  end


  def availability_shipping_cost_with_pincode(pincode)
    @pincode = Admin::ShippingSpecificPincode.where(:pincode => pincode).first
    if @pincode.present?
      return self.find_shipping_range(@pincode,true)
    else
      return 0
    end
  end

  def find_shipping_range(associated_plan,pincode,based_on_attr,based_on_value)
    unless pincode
      if associated_plan.policy == "free"
        return 0
      elsif associated_plan.policy == "flat"
        return @shipping_policy.cost
      else 
        if based_on_attr == "w"
          associated_plan.shipping_weights.each do |weight|
            if based_on_value.between?(weight.range.split("-")[0].to_i,weight.range.split("-")[1].to_i) 
              @return_weight =  weight.cost
            end
          end
          unless @return_weight.present?
          @return_weight = associated_plan.shipping_weights.maximum("cost")
          end
          return @return_weight
        else
          associated_plan.shipping_prices.each do |price|
            if based_on_value.between?(price.range.split("-")[0].to_i,price.range.split("-")[1].to_i) 
              @return_price = price.cost
            end
          end
          unless @return_price.present?
          @return_price = associated_plan.shipping_prices.maximum("cost")
          end
          return @return_price
        end
      end
    else
      if based_on_attr == "w"
        associated_plan.shipping_weights.each do |weight|
          if self.based_on_value.between?(weight.range.split("-")[0].to_i,weight.range.split("-")[1].to_i) 
            @return_weight = weight.cost
          end
        end
        unless @return_weight.present?
          @return_weight = associated_plan.shipping_weights.maximum("cost")
        end
        return @return_weight
      else
        associated_plan.shipping_prices.each do |price|
          if based_on_value.between?(price.range.split("-")[0].to_i,price.range.split("-")[1].to_i) 
           @return_price =  price.cost
          end
        end
        unless @return_price.present?
          @return_price = associated_plan.shipping_prices.maximum("cost")
        end
        return @return_price
      end
    end
  end
  
  def availability_shipping_cost_with_country(country_id)
    @product = self.product
    @shipping_policy = Admin::ShippingPolicy.first
    if @product.availability_plan.all_Country
      if @shipping_policy.all_country
        return self.find_shipping_range(@shipping_policy,false)
      else
        @shipping_country = @shipping_policy.shipping_countries.where(:country_id => country_id).first
        if @shipping_country.all_place
          return self.find_shipping_range(@shipping_country,false)
        else
          return @shipping_country.shipping_specific_pincodes
        end
      end
    else
      @availability_country = @product.availability_plan.list_countries.where(:country_id => country_id).first
      if @availability_country.all_place
        return self.find_shipping_range(@shipping_country,false)
      else
        return @availability_country.country_pincodes
      end
    end
  end

  def check_min_order
    general= Admin::General.first
    @total_price= 0.0
    self.cart_items.each do | cart_item| 
      @total_price += cart_item.price
    end
    if general
      if general.min_order
        if general.min_cost.present?
          if general.min_cost < @total_price
            return true
          else
            return false
          end
        end
      end
    end
  end
  
end
