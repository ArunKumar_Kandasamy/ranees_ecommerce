# == Schema Information
#
# Table name: tickets
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  email          :string(255)
#  user_id        :integer
#  order_id       :integer
#  consignment_id :integer
#  reason         :string(255)
#  status         :string(255)
#  subject        :string(255)
#  body           :text
#  created_at     :datetime
#  updated_at     :datetime
#  ticket_number  :string(255)
#

class Support::Ticket < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  attr_accessor :order_number,:consignment_number
  #ASSOCIATIONS
  has_many :ticket_tasks, class_name: "Support::TicketTask"
  has_many :ticket_replies, class_name: "Support::TicketReply"
  belongs_to :user, class_name: "User"
  belongs_to :order, class_name: "Admin::Order"
  belongs_to :consignment, class_name: "Admin::Consignment"
  belongs_to :voucher, class_name: "Admin::Voucher"
  #VALIDATIONS
  #CALLBACKS
  before_create :ticket_number_auto_increment
  before_create :update_order_id
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def update_order_id  
    if self.order_number.present?
      @order = Admin::Order.where(:order_number => self.order_number).first
      self.order_id = @order.id
    end
    if self.consignment_number.present?
      @consignment = Admin::Consignment.where(:consignment_number => self.consignment_number).first
      self.consignment_id = @consignment.id
    end
  end

  def ticket_number_auto_increment
    datee = Date.today.to_datetime
    day = datee.mday
    daily = day.to_s.size
    if daily == 1
      day = "0#{day}"
    else
      day = day
    end
    month = datee.mon  
    tickets = Support::Ticket.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if tickets.present?
      ticket_id = tickets.last.ticket_number[-3..-1]
      ticket_increment = ticket_id.to_i
      ticket_num = ticket_increment + 1     
      ticket_length = ticket_num.to_s.size
      if ticket_length == 1
        self.ticket_number = "TICK#{day}#{month}000#{ticket_num}"
      elsif order_length == 2
        self.ticket_number = "TICK#{day}#{month}00#{ticket_num}"
      elsif order_length == 3
        self.ticket_number = "TICK#{day}#{month}0#{ticket_num}"
      else
        self.ticket_number = "TICK#{day}#{month}#{ticket_num}"    
      end
    else
      self.ticket_number = "TICK#{day}#{month}0001"
    end
  end

end
