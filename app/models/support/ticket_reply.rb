# == Schema Information
#
# Table name: ticket_replies
#
#  id             :integer          not null, primary key
#  ticket_id      :integer
#  body           :text
#  from_email     :string(255)
#  to_email       :string(255)
#  ticket_task_id :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class Support::TicketReply < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :ticket, class_name: "Support::Ticket"
  belongs_to :ticket_task, class_name: "Support::TicketTask"
  #VALIDATIONS
  #CALLBACKS
  after_create :update_ticket_status
  after_create :ticket_reply_email
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def ticket_reply_email
    ticket= Support::Ticket.find(self.ticket_id)
    if ticket.user_id.present?
      user_email= User.find(ticket.user_id).email
      if ticket.order_id.present?
        your_api_key = 'b79b9a26-b7e7-41d7-90dd-36c53984dc7d'
        client = Postmark::ApiClient.new(your_api_key)
        client.deliver(from: "pk@proproots.com",
             to: user_email,
             subject: "Support for Ticket #{ticket.ticket_number}",
             html_body: "
             Subject: #{ticket.subject}<br />
             Reason: #{ticket.reason}<br />
             Order Id: #{ticket.order_id}<br />
             Body: #{self.body}<br />")
      elsif ticket.subject == "Voucher"
        user_voucher= User.find(ticket.user_id).vouchers.last
        your_api_key = 'b79b9a26-b7e7-41d7-90dd-36c53984dc7d'
        client = Postmark::ApiClient.new(your_api_key)
        client.deliver(from: "pk@proproots.com",
             to: user_email,
             subject: "Support for Ticket #{ticket.ticket_number}",
             html_body: "
             Subject: #{ticket.subject}<br />
             Body: #{self.body}<br />")
      else
        your_api_key = 'b79b9a26-b7e7-41d7-90dd-36c53984dc7d'
        client = Postmark::ApiClient.new(your_api_key)
        client.deliver(from: "pk@proproots.com",
             to: user_email,
             subject: "Support for Ticket #{ticket.ticket_number}",
             html_body: "
             Subject: #{ticket.subject}<br />
             Body: #{self.body}<br />")
      end
    else
      your_api_key = 'b79b9a26-b7e7-41d7-90dd-36c53984dc7d'
      client = Postmark::ApiClient.new(your_api_key)
      client.deliver(from: "pk@proproots.com",
           to: ticket.email,
           subject: "Support for Ticket #{ticket.ticket_number}",
           html_body: "
           Subject: #{ticket.subject}<br />
           Body: #{self.body}<br />")
    end
  end

  def update_ticket_status
    self.ticket.update(:status => "complete")
    if self.ticket_task_id.present?
      self.ticket_task.update(:status => "complete")
    end
  end

  def ticket_number_auto_increment
    datee = Date.today.to_datetime
    day = datee.mday
    daily = day.to_s.size
    if daily == 1
      day = "0#{day}"
    else
      day = day
    end
    month = datee.mon  
    tickets = Support::Ticket.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if tickets.present?
      ticket_id = tickets.last.ticket_number[-3..-1]
      ticket_increment = ticket_id.to_i
      ticket_num = ticket_increment + 1     
      ticket_length = ticket_num.to_s.size
      if ticket_length == 1
        self.ticket_number = "TASK#{day}#{month}000#{ticket_num}"
      elsif order_length == 2
        self.ticket_number = "TASK#{day}#{month}00#{ticket_num}"
      elsif order_length == 3
        self.ticket_number = "TASK#{day}#{month}0#{ticket_num}"
      else
        self.ticket_number = "TASK#{day}#{month}#{ticket_num}"    
      end
    else
      self.ticket_number = "TASK#{day}#{month}0001"
    end
  end

end
