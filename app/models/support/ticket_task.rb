# == Schema Information
#
# Table name: ticket_tasks
#
#  id             :integer          not null, primary key
#  assigned_by    :integer
#  assigned_to    :integer
#  priority       :string(255)
#  status         :string(255)
#  parent_task_id :integer
#  ticket_id      :integer
#  created_at     :datetime
#  updated_at     :datetime
#  task_number    :string(255)
#

class Support::TicketTask < ActiveRecord::Base
	#GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :ticket_replies, class_name: "Support::TicketReply"
  belongs_to :ticket, class_name: "Support::Ticket"
  has_many :tasks, :class_name => "Support::TicketTask",
    :foreign_key => "parent_task_id"
  belongs_to :parent_task, :class_name => "Support::TicketTask",
    :foreign_key => "parent_task_id"
  #VALIDATIONS
  #CALLBACKS
  before_create :task_number_auto_increment
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def task_number_auto_increment
    datee = Date.today.to_datetime
    day = datee.mday
    daily = day.to_s.size
    if daily == 1
      day = "0#{day}"
    else
      day = day
    end
    month = datee.mon  
    ticket_tasks = Support::TicketTask.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if ticket_tasks.present?
      ticket_id = ticket_tasks.last.task_number[-3..-1]
      ticket_increment = ticket_id.to_i
      ticket_num = ticket_increment + 1     
      ticket_length = ticket_num.to_s.size
      if ticket_length == 1
        self.task_number = "TASK#{day}#{month}000#{ticket_num}"
      elsif order_length == 2
        self.task_number = "TASK#{day}#{month}00#{ticket_num}"
      elsif order_length == 3
        self.task_number = "TASK#{day}#{month}0#{ticket_num}"
      else
        self.task_number = "TASK#{day}#{month}#{ticket_num}"    
      end
    else
      self.task_number = "TASK#{day}#{month}0001"
    end
  end

end
