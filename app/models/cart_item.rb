# == Schema Information
#
# Table name: cart_items
#
#  id            :integer          not null, primary key
#  cart_id       :integer
#  price         :integer
#  qty           :integer
#  variant_id    :integer
#  created_at    :datetime
#  updated_at    :datetime
#  per_qty_price :integer
#  shipping_cost :float
#  tax           :float
#  actual_cost   :float
#

class CartItem < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :variant, class_name: "Admin::Variant"
  belongs_to :cart
  belongs_to :order
  belongs_to :consignment, class_name: "Admin::Consignment"
  #VALIDATIONS
  #CALLBACKS
  before_save :update_price
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
  def update_price
    if self.per_qty_price.present?
      self.price= self.per_qty_price * self.qty
    end
  end
end
