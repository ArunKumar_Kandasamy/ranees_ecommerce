# == Schema Information
#
# Table name: bank_accounts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  user_id    :integer
#  num        :float
#  bank_code  :string(255)
#  ifsc_code  :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class BankAccount < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  belongs_to :user,class_name: "User"
  has_many :order_revocations,class_name: "Admin::OrderRevocation"
  #VALIDATIONS
  #CALLBACKS

  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
end
