class CreateShippingAddresses < ActiveRecord::Migration
  def up
    create_table :shipping_addresses do |t|
      t.integer :user_id
      t.string :name
      t.string :add_line1
      t.string :add_line2
      t.string :state
      t.string :country
      t.string :c_code
      t.string :phone
      t.string :pincode

      t.timestamps
    end
  end

  def down
    drop_table :shipping_addresses
  end
end
