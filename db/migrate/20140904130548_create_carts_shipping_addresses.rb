class CreateCartsShippingAddresses < ActiveRecord::Migration
  def change
    create_table :carts_shipping_addresses do |t|
      t.integer :shipping_address_id
      t.integer :cart_id

      t.timestamps
    end
  end
end
