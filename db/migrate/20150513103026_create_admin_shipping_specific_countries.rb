class CreateAdminShippingSpecificCountries < ActiveRecord::Migration
  def change
    create_table :shipping_specific_countries do |t|
      t.float :cost
      t.integer :country_id
      t.integer :shipping_policy_id
      t.boolean :all_place, :default =>  true
      t.string :policy, :default =>  "free"
      t.timestamps
    end
  end
end
