class CreateAdminResendTasksConsignments < ActiveRecord::Migration
  def change
    create_table :resend_tasks_consignments do |t|
      t.integer :consignment_id
      t.integer :resend_task_id

      t.timestamps
    end
  end
end
