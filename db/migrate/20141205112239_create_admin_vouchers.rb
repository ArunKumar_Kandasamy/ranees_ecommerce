class CreateAdminVouchers < ActiveRecord::Migration
  def change
    create_table :vouchers do |t|
      t.integer :user_id 
      t.float :voucher_amount
      t.float :credit
      t.float :debit
      t.integer :order_id
      t.integer :order_revocation_id
      t.integer :refund_revocation_id
      t.string :voucher_number
      t.timestamps
    end
    add_index :vouchers, :user_id
  end
end
