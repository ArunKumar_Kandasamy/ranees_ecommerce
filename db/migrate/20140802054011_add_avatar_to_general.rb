class AddAvatarToGeneral < ActiveRecord::Migration
  def self.up
    change_table :generals do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :generals, :avatar
  end
end
