class AddAllCountryToTaxCountries < ActiveRecord::Migration
  def change
    add_column :tax_plans,:all_country,:boolean
    add_column :tax_plans,:all_country_tax,:float
    add_column :tax_plans,:rest_of_the_country_tax,:float
  end
end
