class CreateAdminTaxCountries < ActiveRecord::Migration
  def change
    create_table :tax_countries do |t|
      t.integer :tax_plan_id
      t.integer :country_id
      t.timestamps
    end
    add_index :tax_countries, :tax_plan_id
  end
end
