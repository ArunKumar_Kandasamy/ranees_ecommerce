class CreateAdminRefundRevocations < ActiveRecord::Migration
  def change
    create_table :refund_revocations do |t|
      t.float :revocation_cost
      t.integer :consignment_id
      t.integer :created_by
      t.integer :bank_account_id
      t.string :revocation_mode
      t.string :status
      t.integer :cancelled_by
      t.string :refund_revocation_number
      t.integer :transaction_id
      t.boolean :trans_status, :default => false
      
      t.timestamps
    end
  end
end
