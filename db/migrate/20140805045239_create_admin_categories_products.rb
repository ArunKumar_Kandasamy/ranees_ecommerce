class CreateAdminCategoriesProducts < ActiveRecord::Migration
  def up
    create_table :categories_products do |t|
      t.integer :product_id
      t.integer :category_id

      t.timestamps
    end
    add_index :categories_products, :product_id
    add_index :categories_products, :category_id
  end
  def down 
  	drop_table :admin_categories_products
  end
end
