class CreateDistrictCities < ActiveRecord::Migration
  def change
    create_table :district_cities do |t|
      t.integer :state_id
      t.string :name
      t.string :district_code

      t.timestamps
    end
  end
end
