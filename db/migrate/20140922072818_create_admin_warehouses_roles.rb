class CreateAdminWarehousesRoles < ActiveRecord::Migration
  def change
    create_table :warehouses_roles do |t|
      t.integer :warehouse_id
      t.integer :role_id

      t.timestamps
    end
    add_index :warehouses_roles, :warehouse_id
    add_index :warehouses_roles, :role_id
  end
end
