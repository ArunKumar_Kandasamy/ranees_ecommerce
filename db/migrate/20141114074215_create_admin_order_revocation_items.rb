class CreateAdminOrderRevocationItems < ActiveRecord::Migration
  def change
    create_table :order_revocation_items do |t|
      t.float :price
      t.integer :order_revocation_id
      t.integer :qty
      t.integer :variant_id
      t.float :per_qty_price

      t.timestamps
    end
    add_index :order_revocation_items, :order_revocation_id
  end
end
