class CreateAdminOrderLogs < ActiveRecord::Migration
  def change
    create_table :order_logs do |t|
      t.integer :updated_by
      t.float :order_cost
      t.integer :order_id
      t.string :status
      t.integer :shipping_address_id
      t.boolean :pay_val
      t.boolean :cal_conf
      t.boolean :prev_rec
      t.string :cancel
      t.text :remarks

      t.timestamps
    end
  end
end
