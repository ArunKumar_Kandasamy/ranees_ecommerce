class CreateAdminOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.float :price
      t.integer :qty
      t.integer :variant_id
      t.float :per_qty_price
      t.integer :return_due
      t.float  :shipping_cost
      t.float  :tax
      t.float  :actual_cost
      t.float  :shipping_cost
      t.timestamps
    end
    add_index :order_items, :order_id
    add_index :order_items, :variant_id
  end
end
