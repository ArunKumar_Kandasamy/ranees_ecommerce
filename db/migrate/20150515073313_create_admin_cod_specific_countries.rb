class CreateAdminCodSpecificCountries < ActiveRecord::Migration
  def change
    create_table :cod_specific_countries do |t|
      t.integer :country_id
      t.integer :cod_policy_id
      t.boolean :all_place, :default => true
      t.timestamps
    end
    add_index :cod_specific_countries, :country_id
    add_index :cod_specific_countries, :cod_policy_id
  end
end
