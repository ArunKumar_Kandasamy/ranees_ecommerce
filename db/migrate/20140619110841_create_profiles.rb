class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :f_name
      t.string :l_name
      t.string :m_number
      t.string :gender
      t.integer :user_id

      t.timestamps
    end
    add_index :profiles, :user_id
  end
end
