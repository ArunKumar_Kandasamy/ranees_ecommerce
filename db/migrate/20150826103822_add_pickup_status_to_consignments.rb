class AddPickupStatusToConsignments < ActiveRecord::Migration
  def change
    add_column :consignments,:pickup_status,:boolean
  end
end
