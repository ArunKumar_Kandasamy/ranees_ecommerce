class CreateAdminGeneralImages < ActiveRecord::Migration
  def change
    create_table :general_images do |t|
      t.attachment :avatar
      t.integer :image_type_id
      t.integer :category_id
      t.string :alt_text
      t.integer :sort_order
      t.string :link
      t.text :description
      t.string  :title

      t.timestamps
    end
  end
end
