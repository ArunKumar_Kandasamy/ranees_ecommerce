class CreateAdminShippingSpecificPincodes < ActiveRecord::Migration
  def change
    create_table :shipping_specific_pincodes do |t|
      t.integer :shipping_specific_country_id
      t.string :pincode

      t.timestamps
    end
  end
end
