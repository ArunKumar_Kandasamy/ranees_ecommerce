class CreateAdminShippingPolicies < ActiveRecord::Migration
  def change
    create_table :shipping_policies do |t|
      t.float :cost
      t.boolean :all_country, :default =>  true
      t.string :policy, :default =>  "free"
      t.timestamps
    end
  end
end
