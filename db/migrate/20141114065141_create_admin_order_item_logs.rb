class CreateAdminOrderItemLogs < ActiveRecord::Migration
  def change
    create_table :order_item_logs do |t|
      t.integer :order_log_id
      t.integer :qty
      t.float :price
      t.integer :variant_id
      t.float :per_qty_price
      t.string :reason

      t.timestamps
    end
  end
end
