class CreateAdminWeightRanges < ActiveRecord::Migration
  def change
    create_table :weight_ranges do |t|
      t.string :range

      t.timestamps
    end
  end
end
