class CreateAdminShippingPrices < ActiveRecord::Migration
  def change
    create_table :shipping_prices do |t|
      t.string :range
      t.float :cost
      t.integer :shipping_policy_id
      t.integer :shipping_specific_country_id
      t.integer :shipping_specific_pincode_id

      t.timestamps
    end
  end
end
