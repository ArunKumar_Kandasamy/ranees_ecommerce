class CreateBlockLists < ActiveRecord::Migration
  def change
    create_table :block_lists do |t|
      t.string :con_name
      t.string :met_name
      t.integer :role_id

      t.timestamps
    end
    add_index :block_lists, :role_id
  end
end
