class CreateAdminOrderRevocations < ActiveRecord::Migration
  def change
    create_table :order_revocations do |t|
      t.float :revocation_cost
      t.integer :order_id
      t.string :revocation_mode
      t.boolean :status
      t.integer :bank_account_id
      t.integer :transaction_id
      t.boolean :trans_status, :default => false

      t.timestamps
    end
    add_index :order_revocations, :bank_account_id
    add_index :order_revocations, :order_id
  end
end
