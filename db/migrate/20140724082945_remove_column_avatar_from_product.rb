class RemoveColumnAvatarFromProduct < ActiveRecord::Migration
  def change
  	remove_attachment :products, :avatar
  end
end
