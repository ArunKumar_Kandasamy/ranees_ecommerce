class CreateAdminRejectionReasons < ActiveRecord::Migration
  def change
    create_table :rejection_reasons do |t|
      t.string :reason
      t.string :attr_value
      t.integer :order_id 

      t.timestamps
    end
    add_index :rejection_reasons, :order_id
  end
end
