class AddDimensionsToVariants < ActiveRecord::Migration
  def change
     add_column :variants, :length, :float
     add_column :variants, :breadth, :float
     add_column :variants, :width, :float
  end
end
