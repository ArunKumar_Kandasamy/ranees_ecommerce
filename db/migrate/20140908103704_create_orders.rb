class CreateOrders < ActiveRecord::Migration
  def up
    create_table :orders do |t|
      t.integer :user_id
      t.float :order_cost
      t.string :payment_mode
      t.boolean :payment_status
      t.string :order_number
      t.string :status
      t.integer :shipping_address_id
      t.string :ip
      t.string :location
      t.boolean :pay_val
      t.boolean :cal_conf
      t.boolean :prev_rec
      t.string :cancel
      t.text :remarks
      t.integer :cancelled_by
      t.float :voucher_amount
      t.float :balance_amount
      t.float :tax
      t.float :shipping_cost
      t.timestamps
    end
    add_index :orders, :user_id
  end

  def down
    drop_table :orders
  end
end