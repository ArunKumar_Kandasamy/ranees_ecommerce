class CreateAdminCategories < ActiveRecord::Migration
  def up
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.string :page_title
      t.text :meta_description
      t.boolean :active
      t.datetime :pub_time
      t.integer :parent_id
      t.boolean :status
      t.integer :sort_order, :default => 0
      t.integer :promotional_sort
      t.timestamps
    end
  end

  def down
    drop_table :categories
  end
end
