class CreateAdminPackageTasks < ActiveRecord::Migration
  def change
    create_table :package_tasks do |t|
      t.string :task_number
      t.integer :warehouse_id

      t.timestamps
    end
  end
end
