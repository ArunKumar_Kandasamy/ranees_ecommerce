class CreateAdminCartItems < ActiveRecord::Migration
  def change
    create_table :cart_items do |t|
      t.integer :cart_id
      t.integer :price
      t.integer :qty
      t.integer :variant_id
      t.float :per_qty_price
      t.float :shipping_cost
      t.float  :tax
      t.float  :actual_cost
      t.timestamps
    end
    add_index :cart_items, :cart_id
    add_index :cart_items, :variant_id
  end
end
