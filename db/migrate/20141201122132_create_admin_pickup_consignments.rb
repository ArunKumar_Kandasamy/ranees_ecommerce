class CreateAdminPickupConsignments < ActiveRecord::Migration
  def change
    create_table :pickup_consignments do |t|
      t.string :pickup_consignment_number
      t.float :consignment_cost
      t.integer :consignment_id
      t.integer :logistic_id
      t.integer :warehouse_id
      t.boolean :status
      t.integer :shipping_address_id
      t.integer :created_by
      t.string :status
      t.string :compensate
      t.string :reason
      t.boolean :received
        
      t.timestamps
    end
  end
end
