class CreateAdminPickupConsignmentItems < ActiveRecord::Migration
  def change
    create_table :pickup_consignment_items do |t|
      t.integer :pickup_consignment_id
      t.float :price
      t.integer :qty
      t.integer :variant_id
      t.float :per_qty_price
      t.integer :refund_revocation_item_id
      t.integer :replacement_revocation_item_id
      t.boolean :received
      t.integer :current_user

      t.timestamps
    end
  end
end
