class CreateAdminRefundRevocationItems < ActiveRecord::Migration
  def change
    create_table :refund_revocation_items do |t|
      t.integer :refund_revocation_id
      t.float :price
      t.integer :qty
      t.integer :variant_id
      t.float :per_qty_price
      t.float :actual_cost
      t.string :reason
      t.float :refund_amount
      t.boolean :ship_dt
      t.boolean :tax_dt
      t.boolean :TDR_dt
      t.boolean :status

      t.timestamps
    end
  end
end
