class CreateAdminTaxStates < ActiveRecord::Migration
  def change
    create_table :tax_states do |t|
      t.integer :tax_country_id
      t.integer :state_id
      t.float :tax
      t.boolean :status
      t.timestamps
    end
    add_index :tax_states, :tax_country_id
  end
end
