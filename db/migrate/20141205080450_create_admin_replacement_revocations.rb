class CreateAdminReplacementRevocations < ActiveRecord::Migration
  def change
    create_table :replacement_revocations do |t|
      t.float :revocation_cost
      t.integer :consignment_id
      t.integer :created_by
      t.integer :shipping_address_id
      t.string :replacement_revocation_number
      t.string :status
      t.integer :cancelled_by

      t.timestamps
    end
  end
end
