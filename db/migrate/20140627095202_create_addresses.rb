class CreateAddresses < ActiveRecord::Migration
  def up
    create_table :addresses do |t|
      t.string :add_line1
      t.string :add_line2
      t.string :state
      t.string :country
      t.string :c_code
      t.string :phone
      t.references :connection, polymorphic: true
      t.timestamps
    end
  end
end
