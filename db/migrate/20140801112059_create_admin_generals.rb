class CreateAdminGenerals < ActiveRecord::Migration
  def up
    create_table :generals do |t|
    	t.string :store_name
    	t.string :page_title
    	t.string :meta_des
    	t.string :account_email
    	t.string :com_email
    	t.string :legal_name
      t.string :acc_currency
      t.string :show_currency
      t.text :a_text
      t.string :dt_format
      t.text :term_use
      t.text :privacy
      t.string :fb_link
      t.string :tw_link
      t.string :gp_link
      t.string :pt_link
      t.text :about_us
      t.text :return
      t.text :refund
      t.text :cancellation
      t.text :ship_delivery
      t.text :contact_us
      t.integer :category_level, :default => 3
      t.boolean :fixed_rate
      t.boolean :cod_preference, :default => false
      t.boolean :min_order, :default =>  false
      t.float :min_cost
      t.timestamps
    end
    add_index :generals, :show_currency
    add_index :generals, :acc_currency
  end

  def down
  	drop_table :generals
  end
end