class CreateAdminInventories < ActiveRecord::Migration
  def up
    create_table :inventories do |t|
      t.integer :product_id
      t.integer :warehouse_id
      t.integer :vendor_id
      t.integer :qty
      t.float :purchase_price
      t.integer :user_id
      t.integer :sold_qty
      t.integer :balance_qty
      t.string :updated_through, :default => "manual"
      t.integer :variant_id
      t.datetime :expiry_date
      t.string :regulation
      t.float :profit_price
      t.float :selling_price
      t.date :expiry_date
      t.integer :parent_id
      t.timestamps
    end
    add_index :inventories, :product_id
    add_index :inventories, :warehouse_id
    add_index :inventories, :vendor_id
    add_index :inventories, :variant_id
  end

  def down
  	drop_table :inventories
  end
end