class CreateAdminConsignmentLogs < ActiveRecord::Migration
  def change
    create_table :consignment_logs do |t|
      t.integer :consignment_id 
      t.float :consignment_cost 
      t.boolean  :confirm,  :default => false
      t.integer :logistic_id 
      t.string :reason 
      t.boolean :dispatch, :default => false
      t.string :tracking 
      t.boolean  :delivery_status 
      t.date :delivery_date 
      t.text :remarks 
      t.string :cancel 
      t.integer :cancelled_by
      t.string :reason
      t.integer :updated_by 
      t.timestamps
    end
  end
end
