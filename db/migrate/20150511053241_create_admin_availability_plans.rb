class CreateAdminAvailabilityPlans < ActiveRecord::Migration
  def change
    create_table :availability_plans do |t|
      t.string :name
      t.boolean :all_country, :boolean, :default =>  true 
      t.boolean :status 	

      t.timestamps
    end
  end
end
