class CreateAdminRegulations < ActiveRecord::Migration
  def up
    create_table :regulations do |t|
      t.integer :inventory_id
      t.integer :qty
      t.string :reason
      t.string :note
      t.integer :consignment_item_id
      t.integer :request_id
      t.timestamps
    end
    add_index :regulations, :inventory_id
  end
  def down
  	drop_table :regulations
  end
end
