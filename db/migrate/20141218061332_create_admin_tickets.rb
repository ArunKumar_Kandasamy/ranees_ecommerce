class CreateAdminTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :name
      t.string :email
      t.integer :user_id
      t.integer :order_id
      t.integer :consignment_id
      t.string :reason
      t.string :ticket_number
      t.string :status
      t.string :subject
      t.text :body

      t.timestamps
    end
  end
end
