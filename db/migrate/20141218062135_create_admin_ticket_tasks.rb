class CreateAdminTicketTasks < ActiveRecord::Migration
  def change
    create_table :ticket_tasks do |t|
      t.integer :assigned_by
      t.integer :assigned_to
      t.string :priority
      t.string :status
      t.integer :parent_task_id
      t.integer :ticket_id
      t.string :task_number

      t.timestamps
    end
  end
end
