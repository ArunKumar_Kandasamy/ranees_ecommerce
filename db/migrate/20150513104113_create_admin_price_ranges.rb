class CreateAdminPriceRanges < ActiveRecord::Migration
  def change
    create_table :price_ranges do |t|
      t.string :range

      t.timestamps
    end
  end
end
