class CreateAdminPickupTasksPickupConsignments < ActiveRecord::Migration
  def change
    create_table :pickup_tasks_pickup_consignments do |t|
      t.integer :pickup_task_id
      t.integer :pickup_consignment_id

      t.timestamps
    end
  end
end
