class CreateIdentities < ActiveRecord::Migration
  def up
    create_table :identities do |t|
      t.string :provider
      t.string :uid
      t.integer :user_id
      t.timestamps
    end
    add_index :identities, :user_id
  end
  def down
   drop_table :identities 
  end
end
