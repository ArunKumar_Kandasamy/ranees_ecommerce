class CreateValues < ActiveRecord::Migration
  def up
    create_table :values do |t|
      t.string :name
      t.integer :specification_id
      t.timestamps
    end
    add_index :values, :specification_id
  end
  def down
  	drop_table :values
  end
end
