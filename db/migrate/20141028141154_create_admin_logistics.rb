class CreateAdminLogistics < ActiveRecord::Migration
  def change
    create_table :logistics do |t|
      t.string :name
      t.string :site
      t.boolean :status, :default => false
      t.timestamps
    end
  end
end
