class RenameDeliItem < ActiveRecord::Migration
  def change
  	rename_table :deli_items, :consignment_items
  	rename_column :consignment_items, :deli_id, :consignment_id
  	rename_column :consignments, :deli_cost, :consignment_cost
  	add_column :consignment_items, :return_due, :integer
  	add_column :consignment_items, :policy, :boolean, :default => false
  	add_index :consignment_items, :policy
  	add_column :consignment_items, :shipping_cost, :float
  	add_column :consignment_items, :tax, :float
  	add_column :consignment_items, :actual_cost, :float
  	add_column :consignment_items, :replacement_revocation_item_id, :integer
  end
end
