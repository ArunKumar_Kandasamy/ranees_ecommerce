class CreateAdminTicketReplies < ActiveRecord::Migration
  def change
    create_table :ticket_replies do |t|
      t.integer :ticket_id
      t.text :body
      t.string :from_email
      t.string :to_email
      t.integer :ticket_task_id

      t.timestamps
    end
  end
end
