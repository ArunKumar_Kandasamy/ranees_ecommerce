class RenameDeli < ActiveRecord::Migration
  def change
  	rename_table :delis, :consignments
  	add_column :consignments, :conform, :boolean, :default => false
  	add_column :consignments, :consignment_number, :string
  	add_column :consignments, :logistic_id, :integer
  	add_column :consignments, :reason, :string
  	add_column :consignments, :dispatch, :boolean, :default => false
  	add_column :consignments, :tracking, :string 
  	add_column :consignments, :delivery_status, :boolean
  	add_column :consignments, :delivery_date, :date
  	add_index :consignments, :delivery_date
  	add_column :consignments, :remarks, :text
  	add_column :consignments, :cancel, :string
  	add_column :consignments, :cancelled_by, :integer
  	add_column :consignments, :invoice_cost, :float
  	add_column :consignments, :status, :string
  	add_column :consignments, :parent_consignment_id, :integer
  end
end
