class CreateBankAccounts < ActiveRecord::Migration
  def change
    create_table :bank_accounts do |t|
      t.string :name
      t.integer :user_id
      t.float :num
      t.string :bank_code
      t.string :ifsc_code

      t.timestamps
    end
  end
end
