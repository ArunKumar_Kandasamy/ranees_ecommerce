class CreateAdminCodPolicies < ActiveRecord::Migration
  def change
    create_table :cod_policies do |t|
      t.boolean :cod_availability, :default => true
      t.boolean :all_country, :default => true
      t.timestamps
    end
  end
end
