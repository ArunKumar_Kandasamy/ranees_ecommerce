class CreateAdminPackageTasksConsignments < ActiveRecord::Migration
  def change
    create_table :package_tasks_consignments do |t|
      t.integer :consignment_id
      t.integer :package_task_id

      t.timestamps
    end
  end
end
