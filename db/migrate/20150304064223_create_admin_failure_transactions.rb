class CreateAdminFailureTransactions < ActiveRecord::Migration
  def change
    create_table :failure_transactions do |t|
      t.string :txnid
      t.float :net_amount
      t.integer :user_id
      t.string :mode
      t.string :status
      t.timestamp :addedon
      t.string :productinfo
      t.string :name_on_card
      t.string :cardnum
      t.timestamps
    end
    add_index :failure_transactions, :user_id
  end
end
