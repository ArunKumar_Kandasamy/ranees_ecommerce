class CreateAdminPlanCountries < ActiveRecord::Migration
  def change
    create_table :plan_countries do |t|
      t.integer :availability_plan_id
      t.integer :country_id
      t.boolean :all_place, :boolean, :default =>  true  	

      t.timestamps
    end
  end
end
