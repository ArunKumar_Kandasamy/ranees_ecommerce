class CreateAdminSmsNotifications < ActiveRecord::Migration
  def change
    create_table :sms_notifications do |t| 
      t.integer :user_id
      t.boolean :order_placed, :default => false
      t.boolean :tracking_updated, :default => false
      t.boolean :order_success, :default => false
      t.boolean :order_failed, :default =>false
      t.boolean :pre_deli_cancel, :default =>false
      t.boolean :post_deli_cancel, :default =>false
      t.boolean :refund_complete, :default =>false
      t.boolean :replacement_order, :default =>false
      t.boolean :partial_refund, :default =>false
      t.boolean :replacement_order, :default =>false
      t.boolean :order_create, :default => false
      t.text :content_for_order_placed
      t.text :content_for_tracking_updated
      t.text :content_for_order_success
      t.text :content_for_order_failed
      t.text :content_for_pre_deli_cancel
      t.text :content_for_post_deli_cancel
      t.text :content_for_refund_complete
      t.text :content_for_replacement_order
      t.text :content_for_partial_refund
      t.text :content_for_replacement_order
      t.text :content_for_order_create
      t.boolean :back_jobs, :default => false
      t.text :content_for_back_jobs
      t.timestamps
    end
    add_index :sms_notifications, :user_id 
  end
end
