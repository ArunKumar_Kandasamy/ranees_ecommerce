class CreateAdminTaxPlans < ActiveRecord::Migration
  def change
    create_table :tax_plans do |t|
      t.string :name
      t.string :formula
      t.timestamps
    end
  end
end
