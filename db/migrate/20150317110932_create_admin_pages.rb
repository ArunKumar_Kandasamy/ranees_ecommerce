class CreateAdminPages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :page_name
      t.string :page_type
      t.string :page_link
      t.text :page_content
      t.boolean :active

      t.timestamps
    end
  end
end
