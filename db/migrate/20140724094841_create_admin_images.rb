class CreateAdminImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :variant_id
      t.string :alt_text
      t.integer :general_id
      t.integer :sort_order
      t.string :type_of_image
  	  t.string :link_url
  	  t.integer :banner_sort
  	  t.integer :category_id
  	  t.integer :parent_image_sort
  	  t.integer :child_image_sort

      t.timestamps
    end
    add_index :images, :variant_id
    add_index :images, :general_id
  end
end
