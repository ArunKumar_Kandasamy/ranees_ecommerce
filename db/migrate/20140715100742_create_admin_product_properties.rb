class CreateAdminProductProperties < ActiveRecord::Migration
  def change
    create_table :product_properties do |t|
      t.integer :product_question_id
      t.integer :variant_id
      t.string :answer

      t.timestamps
    end
    add_index :product_properties, :variant_id
  end
end
