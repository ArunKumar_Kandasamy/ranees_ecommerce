class AddDefaultTaxCost < ActiveRecord::Migration
  def change
    remove_column :tax_plans,:all_country_tax
    remove_column :tax_plans,:rest_of_the_country_tax
    add_column :tax_plans,:all_country_tax,:float , :default => 0
    add_column :tax_plans,:rest_of_the_country_tax,:float, :default => 0
  end
end
