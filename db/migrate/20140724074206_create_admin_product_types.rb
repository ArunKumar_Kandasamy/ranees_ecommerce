class CreateAdminProductTypes < ActiveRecord::Migration
  def change
    create_table :product_types do |t|
	  t.integer :product_id
	  t.integer :type_id

      t.timestamps
    end
    add_index :product_types, :product_id
    add_index :product_types, :type_id
  end
end
