class CreateAdminPickupConsignmentInvoices < ActiveRecord::Migration
  def change
    create_table :pickup_consignment_invoices do |t|
      t.integer :pickup_consignment_id
      t.string :barcode
      t.string :invoice_number

      t.timestamps
    end
  end
end
