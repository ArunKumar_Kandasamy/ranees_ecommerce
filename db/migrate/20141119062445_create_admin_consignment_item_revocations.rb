class CreateAdminConsignmentItemRevocations < ActiveRecord::Migration
  def change
    create_table :consignment_item_revocations do |t|
      t.integer :consignment_revocation_id
      t.float :price
      t.integer :qty
      t.integer :variant_id
      t.float :per_qty_price
      t.string :compensate
      t.string :revocation_mode
      t.integer :bank_account_id
      t.boolean :status
      t.string :reason
      t.float  :refund_amount

      t.timestamps
    end
  end
end
