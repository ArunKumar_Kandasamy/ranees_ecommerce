class CreateAdminProductQuestions < ActiveRecord::Migration
  def change
    create_table :product_questions do |t|
      t.string :question
      t.timestamps
    end
  end
  def down
  	drop_table :product_questions
  end
end
