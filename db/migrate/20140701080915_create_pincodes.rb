class CreatePincodes < ActiveRecord::Migration
  def up
    create_table :pincodes do |t|
      t.integer :district_city_id
      t.string :pincode
      t.float :cost

      t.timestamps
    end
    add_index :pincodes, :cost
  end

  def down
  end
end
