class CreateAdminReplacementRevocationItems < ActiveRecord::Migration
  def change
    create_table :replacement_revocation_items do |t|
      t.integer :replacement_revocation_id
      t.float :price
      t.integer :qty
      t.integer :variant_id
      t.float :per_qty_price
      t.float :actual_cost
      t.float :shipping_cost
      t.float :tax
      t.string :reason
      t.boolean :status
      t.float  :shipping_cost
      t.float  :tax

      t.timestamps
    end
  end
end
