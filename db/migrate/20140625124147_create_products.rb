class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.boolean :status, :default => false
      t.integer :brand_id
      t.boolean :ship_invent, :default => false
      t.boolean :unavail_purchase, :default => false
      t.boolean :visibility
      t.integer :sort_order, :default => 0
      t.string :promote
      t.integer :promotional_sort, :default => 0
      t.boolean :cod, :default => nil
      t.integer :return_due
      t.string  :based_on
      t.boolean :free_shipping, :default =>  nil
      t.integer :availability_plan_id

      t.timestamps
    end
    add_index :products, :brand_id
  end
end
