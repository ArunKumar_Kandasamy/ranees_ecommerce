class CreateAdminDeliItems < ActiveRecord::Migration
  def change
    create_table :deli_items do |t|
      t.integer :deli_id
      t.float :price
      t.integer :qty
      t.integer :variant_id
      t.float :per_qty_price

      t.timestamps
    end
    add_index :deli_items, :deli_id
    add_index :deli_items, :variant_id
  end
end
