class CreateAdminDelis < ActiveRecord::Migration
  def change
    create_table :delis do |t|
      t.integer :order_id
      t.integer :warehouse_id
      t.float :deli_cost
      t.timestamps
    end
    add_index :delis, :order_id
    add_index :delis, :warehouse_id
  end
end
