class CreateAdminProductsTaxPlans < ActiveRecord::Migration
  def up
    create_table :products_tax_plans do |t|
      t.integer :product_id
      t.integer :tax_plan_id
      t.timestamps
    end
    add_index :products_tax_plans, :product_id
    add_index :products_tax_plans, :tax_plan_id
  end
  def down
  	drop_table :products_tax_plans
  end
end
