class CreateAdminInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :consignment_id
      t.string :barcode
      t.string :invoice_number

      t.timestamps
    end
  end
end
