class CreateAdminConsignmentItemLogs < ActiveRecord::Migration
  def change
    create_table :consignment_item_logs do |t|
      t.integer :consignment_log_id 
      t.float  :price 
      t.integer :qty 
      t.integer :variant_id 
      t.float  :per_qty_price 
      t.integer :return_due 
      t.boolean  :policy 
      t.timestamps
    end
  end
end
