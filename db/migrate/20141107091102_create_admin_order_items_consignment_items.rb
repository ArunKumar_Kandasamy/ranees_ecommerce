class CreateAdminOrderItemsConsignmentItems < ActiveRecord::Migration
  def change
    create_table :order_items_consignment_items do |t|
      t.integer :consignment_item_id
      t.integer :order_item_id

      t.timestamps
    end
  end
end
