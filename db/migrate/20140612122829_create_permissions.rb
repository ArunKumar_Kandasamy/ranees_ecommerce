class CreatePermissions < ActiveRecord::Migration
  def up
    create_table :permissions do |t|
      t.string :email
      t.boolean :active, :default => false
      t.integer :user_id
      t.integer :role_id
      t.timestamps
    end
    add_index :permissions, :role_id
    add_index :permissions, :user_id
  end
  def down
  	drop_table :permissions
  end
end
