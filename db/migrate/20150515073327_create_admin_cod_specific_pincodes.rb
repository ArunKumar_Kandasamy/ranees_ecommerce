class CreateAdminCodSpecificPincodes < ActiveRecord::Migration
  def change
    create_table :cod_specific_pincodes do |t|
      t.integer :cod_specific_country_id
      t.string :pincode
      t.boolean :cod_availability, :default => true
      t.timestamps
    end
    add_index :cod_specific_pincodes, :cod_specific_country_id
  end
end
