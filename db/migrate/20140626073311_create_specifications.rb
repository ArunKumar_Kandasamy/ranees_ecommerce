class CreateSpecifications < ActiveRecord::Migration
  def up
    create_table :specifications do |t|
      t.string :key
      t.integer :product_id
      t.timestamps
    end
    add_index :specifications, :product_id
  end
  def down
  	remove_table :specifications
  end
end
