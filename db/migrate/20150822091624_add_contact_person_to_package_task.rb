class AddContactPersonToPackageTask < ActiveRecord::Migration
  def change
     add_column :package_tasks, :contact_person, :string
     add_column :package_tasks, :closing_time, :time
     add_column :consignments,:pickup_date,:datetime
  end
end
