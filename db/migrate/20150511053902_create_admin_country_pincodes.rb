class CreateAdminCountryPincodes < ActiveRecord::Migration
  def change
    create_table :country_pincodes do |t|
      t.integer :plan_country_id
      t.string :pincode
      t.boolean :availability, :boolean, :default =>  true  	

      t.timestamps
    end
  end
end
