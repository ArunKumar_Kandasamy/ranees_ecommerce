class CreateAdminVariants < ActiveRecord::Migration
  def change
    create_table :variants do |t|
      t.integer :product_id
      t.string :sku
      t.boolean :visible, :default => false
      t.string :name
      t.string :bar_code
      t.string :title
      t.string :meta_desc
      t.boolean :status
      t.integer :sort_order
      t.boolean :ready_to_wear, :default => false
      t.float :strike_rate
      t.float :weight 
      t.timestamps
    end
    add_index :variants, :product_id
    add_index :variants, :sku
  end
  def down
  	drop_table :variants
  end
end
